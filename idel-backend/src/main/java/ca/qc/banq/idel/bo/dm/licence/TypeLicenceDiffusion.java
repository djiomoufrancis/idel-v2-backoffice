package ca.qc.banq.idel.bo.dm.licence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(of = {"idTypeLicenceDiffusion"})
@Table(name = "TYPE_LICENCEDIFFUSION", schema = "")
public class TypeLicenceDiffusion implements Serializable {
	
	@Id
	@Column(name = "ID_TYPELICENCEDIFFUSION", unique = true, nullable = false, precision = 22, scale = 0)
	@NotNull
	private Long idTypeLicenceDiffusion;
	
	@Column(name = "CODE", length = 2)
	private String code;
	
	@Column(name = "DESCRIPTION", length=200)
	private String description;

}
