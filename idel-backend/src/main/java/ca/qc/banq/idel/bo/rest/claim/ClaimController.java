/**
 * 
 */
package ca.qc.banq.idel.bo.rest.claim;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.qc.banq.idel.bo.business.claim.IClaimManagementService;
import ca.qc.banq.idel.bo.dm.claim.Reclamation;
import ca.qc.banq.idel.bo.shared.Messaging;
import ca.qc.banq.idel.bo.shared.MsgKeys;
import ca.qc.banq.idel.bo.shared.Utils;
import ca.qc.banq.idel.bo.shared.exceptions.IdelException;
import ca.qc.banq.idel.bo.shared.models.ApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * Webservices REST de gestion des reclamations
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/claim", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Api(description = "Service de gestion des reclamations")
public class ClaimController {

	@Autowired
	private IClaimManagementService claimService;

	@Autowired
	Messaging messaging;

	/**
	 * Affiche la liste des reclamations d'un editeur
	 * @param editorUid Code UserID de l'editeur
	 * @return liste des reclamations
	 */
	@GetMapping("/display/{id}")
	@Secured("ROLE_displayEditorClaims")
	@ApiOperation("Affichage de la liste des reclamations d'un editeur")
	public ResponseEntity<ApiResponse> displayEditorClaims(@PathParam("uid") @ApiParam(value = "Identifiant de l'editeur") String editorUid) {

		// Initialise response to return
		ApiResponse resp = null;
		
		try {
			
			// Try something
			List<Reclamation> list = claimService.listEditorClaims(editorUid);
			
			// Build response
			resp = new ApiResponse(true, HttpStatus.OK.value(), messaging.getMessage(MsgKeys.MSG_SUCCESS), list);
			
		} catch(IdelException ie) {
			
			// Threat an Idel Exception
			resp = new ApiResponse(false, HttpStatus.BAD_REQUEST.value(), messaging.getMessage(ie.getMessage()), ie);
			
		} catch(Exception ex) {
			
			// Threat other exceptions type
			resp = new ApiResponse(false, HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage() , Utils.stackTraceToString(ex) );
			
			// Trace Log
			log.error(ex.getMessage(), ex);
		}
		
		// Return generic API response
		return ResponseEntity.ok(resp);
	}
	
}
