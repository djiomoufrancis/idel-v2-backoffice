package ca.qc.banq.idel.bo.dm.isbn;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "RESPONSABLE", schema = "")
public class Responsable implements Serializable {
	
	@Id
	@SequenceGenerator(name = "SEQ_RESPONSABLE", sequenceName = "SEQ_RESPONSABLE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_RESPONSABLE")
	@Column(name = "ID_RESPONSABLE", unique = true, nullable = false)
	@NotNull
	private Long idResponsable;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_DEMANDEISBN", nullable = false)
	@NotNull
	private DemandeISBN demandeISBN;
	
	@Column(name = "TITRE", length = 10)
	@Length(max = 10)
	private String titre;
	
	@Column(name = "NOM", length = 100)
	@Length(max = 100)
	private String nom;
	
	@Column(name = "PRENOM", length = 100)
	@Length(max = 100)
	private String prenom;
	
	@Column(name = "COURRIEL", length = 200)
	@Length(max = 200)
	private String courriel;
	
	@Column(name = "TELEPHONE", length = 10)
	@Length(max = 10)
	private String telephone;
	
	@Column(name = "POSTE", length = 10)
	@Length(max = 10)
	private String poste;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "responsable", cascade = {CascadeType.ALL})
	private List<ResponsableActivite> activiteList = new ArrayList<ResponsableActivite>(0);
	
}
