package ca.qc.banq.idel.bo.dm.publication;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue(Contributeur.RESPONSABLEGRAPHISME)
public class ContributeurResponsableGraphisme extends Contributeur implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String contributeurType() {

		return "Repsonsable graphisme et mise en page";
	}

	public String contributeurCode() {

		return "9";	
	}

}
