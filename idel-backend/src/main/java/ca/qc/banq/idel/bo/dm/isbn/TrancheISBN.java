package ca.qc.banq.idel.bo.dm.isbn;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "TRANCHE_ISBN", schema = "")
public class TrancheISBN implements Serializable {
	
	@Id
	@SequenceGenerator(name = "SEQ_TRANCHE_ISBN", sequenceName = "SEQ_TRANCHE_ISBN", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TRANCHE_ISBN")
	@Column(name = "ID_TRANCHEISBN", unique = true, nullable = false)
	@NotNull
	private Long idTrancheISBN;
	
	@Column(name = "CODIFICATION_EAN", length = 25)
	@Length(max = 25)
	private String codificationEAN;
	
	@Column(name = "TRANCHE", length = 25)
	@Length(max = 25)
	private String tranche;
	
	@Column(name = "INTERVALLE_MIN", length = 25)
	@Length(max = 25)
	private String intervalleMIN;
	
	@Column(name = "INTERVALLE_MAX", length = 25)
	@Length(max = 25)
	private String intervalleMAX;

	@Column(name = "DERNIER_NUMERO_ATTRIBUE", length = 25)
	@Length(max = 25)
	private String dernierNumeroAttribue;
	
	@Column(name = "TRANCHE_RESERVEE")
	private boolean reserveesPersonnel;	
	
	@Column(name = "DESCRIPTION", length = 200)
	@Length(max = 200)
	private String description;	

}
