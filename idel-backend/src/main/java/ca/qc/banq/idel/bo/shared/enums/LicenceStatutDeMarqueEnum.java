package ca.qc.banq.idel.bo.shared.enums;

public enum LicenceStatutDeMarqueEnum {
	
	
	PRESENT(1,"licence.statutDeMarque.Present"),
	RETIRE(2,"licence.statutDeMarque.Retire");
	
	private Integer code;
	private String desc;
	
	LicenceStatutDeMarqueEnum(int code, String desc){
		this.code = code;
		this.desc = desc;
	}

	public int getCode() {
		return code;
	}
	
	public String getDesc() {
		return desc;
	}	

}
