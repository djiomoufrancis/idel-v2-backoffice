package ca.qc.banq.idel.bo.repo.editor;



import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ca.qc.banq.idel.bo.dm.editor.Editeur;

/**
 * Editeur @Repository
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Repository
public interface IEditeurRepository extends JpaRepository<Editeur, Long> {
	
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true, noRollbackFor=Exception.class)
	@Query(value = "select e from Editeur e where e.idCompteAcces = :idCompteAcces ")  
	public List<Editeur> findByIdCompteAcces(@Param("idCompteAcces") String idCompteAcces);
	
}
