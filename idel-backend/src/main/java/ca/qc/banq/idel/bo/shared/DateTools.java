package ca.qc.banq.idel.bo.shared;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class DateTools {

	// on set 23:59:59 pour signaler la fin de la journée
	public static Date setEndDay(Date date){
		Date result = date;
		Calendar cld = Calendar.getInstance();
		if(date != null){
			cld.setTime(date);			
			if(cld.get(Calendar.HOUR) == 0){
				cld.set(Calendar.HOUR_OF_DAY, 23);
				cld.set(Calendar.MINUTE, 59);
				cld.set(Calendar.SECOND, 59);
				result = cld.getTime();
			}
		}
		return result;
	}
	
	// on set 23:59:59 pour signaler la fin de la journée
	public static Date setBeginningOfDay(Date date){
		Date result = date;
		Calendar cld = Calendar.getInstance();
		if(date != null){
			cld.setTime(date);			
			if(cld.get(Calendar.HOUR) == 0){
				cld.set(Calendar.HOUR_OF_DAY, 00);
				cld.set(Calendar.MINUTE, 00);
				cld.set(Calendar.SECOND, 00);
				result = cld.getTime();
			}
		}
		return result;
	}
	
	public static String getDateFormat(Date date, String format){
		DateFormat formatter = new SimpleDateFormat(format);
		return formatter.format(date);
	}
	
	public static Date getDatefromDateFormat(String date, String format){
		Date result = null;
		DateFormat formatter = new SimpleDateFormat(format);
		try {
			result = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static String getStringfromDateAAAAMMJJ(Date date){	
		if(date != null){
			return getDateFormat(date, "yyyyMMdd");
		}
		return "";
	}
	
	public static Date getDatefromStringAAAAMMJJ(String date){
		if(StringUtils.isNotEmpty(date)){
			return getDatefromDateFormat(date, "yyyyMMdd");
		}
		return null;
	}
	
	public static Date getDateByYear(String year){
		if(StringUtils.isNotEmpty(year)){
			DateFormat formatter = new SimpleDateFormat("yyyy");
			try {
				return formatter.parse(year);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
}
