package ca.qc.banq.idel.bo.dm.publication;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


/**
 * ExemplaireInfo generated by hbm2java
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "NOTICE_PRINCIPALE_INFO", schema = "")
public class NoticePrincipaleInfo implements java.io.Serializable {

	@Id
	@SequenceGenerator(name = "SEQ_NOTICE_PRINCIPALE_INFO", sequenceName = "SEQ_NOTICE_PRINCIPALE_INFO", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_NOTICE_PRINCIPALE_INFO")
	@Column(name = "ID_NOTICEPRINCIPALEINFO", unique = true, nullable = false, precision = 22, scale = 0)
	@NotNull
	private Long idNoticePrincipaleInfo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PUBLICATION", nullable = false)
	@NotNull
	private Publication publication;
	
	@Column(name = "TITRE_505")
	private String titre505;
	
	@Column(name = "ISBN_020")
	private String isbn020;
	
	@Column(name = "NOTE_SIGB_589")
	private String noteSigbNoticeInfo;

}
