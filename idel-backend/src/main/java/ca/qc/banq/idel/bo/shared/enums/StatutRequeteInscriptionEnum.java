package ca.qc.banq.idel.bo.shared.enums;



/**
 * This enum has a matching data from STATUT_REQUETE table. 
 * Any change of data in STATUT_REQUETE table requires a code change to this enum. 
 * 
 * @author Youssef.Soltani
 *
 */
public enum StatutRequeteInscriptionEnum {
	
	A_VALIDER  (0l,"0","A valider"),
	EN_ATTENTE (1l,"1","En attente"),
	EXISTANT   (2l,"2","Existant"),
	REJETE     (3l,"3","Rejetée"),
	VALIDE     (4l,"4","Validée");
	

	private Long id;
	private String code;
	private String description;
	
	StatutRequeteInscriptionEnum(long id, String code, String description) {
		this.id = id;
        this.code = code;
        this.description = description;
    }
    
	public String getCode() {
		return code;
	}
	
	public String getDescription() {
		return description;
	}

	public Long getId() {
		return id;
	}

}
