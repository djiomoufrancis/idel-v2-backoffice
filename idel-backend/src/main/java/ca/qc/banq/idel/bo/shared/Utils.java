/**
 * 
 */
package ca.qc.banq.idel.bo.shared;

import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Gestionnaire d'utilitaires
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
public class Utils {

	public static int LEFT = 1;
    public static int RIGHT = 2;
    public static String DefaultBankCode = "10005";

	public static Locale defaultLocale = Locale.ENGLISH;
    /**
     * Format de date court
     */
    public static SimpleDateFormat shortDateFormat = new SimpleDateFormat("yyMMdd");
    /**
     * Format de date par defaut
     */
    public static SimpleDateFormat defaultDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    /**
     * Format de date complet
     */
    public static SimpleDateFormat completedDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat ecwDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    
    /**
     * Determine le nombre de jours entre 2 dates
     * @param date1 Date de debut
     * @param date2 Date de Fin
     * @return Nbre de Jours
     */
    public static long getNbreJoursBetween(Date date1, Date date2){
        if(date1 == null || date2 == null) return 0;
        return (date2.getTime() - date1.getTime()) / (long)(1000 * 60 * 60 * 24);
    }
    
    /**
     * Complete une chaine en rajoutant la chaine vide pour atteindre la longueur
     * @param text
     * @param direction
     * @param lenght
     * @param addString
     * @return
     */
    public static String padText(String text, int direction, int lenght, String addString ){
        String s = text;
        if(s == null){
            for(int i=1; i<=lenght; i++) s += addString;
        }else{
            if(s.length() < lenght){
                while(s.length() < lenght){
                    s = direction == LEFT ? addString + s : s + addString;
                }
            }
        }
        s = s.substring(0, lenght);
        return s;
    }
    
    /**
     * Nbre de mois entre 2 dates
     * @param date1
     * @param date2
     * @return
     */
    public static int nbOfMonthsBetweenTwoDates(Date date1, Date date2)  {
        GregorianCalendar gc1 = new GregorianCalendar();
        gc1.setTime(date1);
        GregorianCalendar gc2 = new GregorianCalendar();
        gc2.setTime(date2);
        int gap = 0;
        gc1.add(GregorianCalendar.MONTH, 1);
        while(gc1.compareTo(gc2)<=0) {
            gap++;
            gc1.add(GregorianCalendar.MONTH, 1);
        }
        return gap;
    }

    /**
     * Formatte un numero de compte fournit au format de la zone monetaire
     * @param acc
     * @return
     */
	public static String cleanAndCompleteAccount(String acc) {
		if(acc == null || acc.isEmpty()) return DefaultBankCode + "-00000-00000000000-00";
		acc = acc.trim();
		String [] tab = acc.split("-");
		switch(tab.length) {
			case 1 : acc = DefaultBankCode + "-00000-" + acc + "-00"; break;
			case 2 : acc = DefaultBankCode + "-" + acc + "-00"; break;
			case 3 : acc = DefaultBankCode + "-" + acc; break;
		}
		if(acc.startsWith("00019-")) acc = acc.replaceFirst("00019", DefaultBankCode);
		
		if(acc.split("-")[3].equals("00")){
	    	// Calcul de la cle du compte
	    	Long cle = 97 - Math.floorMod((89*Integer.valueOf(acc.split("-")[0])+15*Integer.valueOf(acc.split("-")[1])+3* Long.valueOf(acc.split("-")[2]) ), 97);
	    	acc = acc.substring(0, acc.length()-2) + padText(cle.toString(), LEFT, 2, "0");
		}
		return acc;
	}
	
	/**
	 * Duree en millisecondes entre 2 dates
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static long differenceEnMilisecondeEntre2Dates(Date date1, Date date2){
 		Calendar debut=Calendar.getInstance();
 		Calendar fin=Calendar.getInstance();
 		debut.setTime(date1);
 		fin.setTime(date2);
        long diff = fin.getTimeInMillis() - debut.getTimeInMillis();
        return Math.round(diff);
 	}
	
	/**
	 * Met a jour la date en se positionnant a la 1ere heure de la journee
	 * @param date
	 * @return
	 */
	public static Date getUpdateToStart(Date date) {
		try {
			SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			return sdf2.parse( sdf1.format(date) + " 00:01:01" );
		}catch(Exception ex) { return date;}
	}
	
	/**
	 * MAJ de la date en se positionnant a la derniere heure de la journee
	 * @param date
	 * @return
	 */
	public static Date getUpdateToEnd(Date date) {
		try {
			SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			return sdf2.parse( sdf1.format(date) + " 23:59:59" );
		}catch(Exception ex) { return date;}
	}
	
	/**
	 * Ajoute un nbre de jours a une date
	 * @param date
	 * @param nbJrs
	 * @return
	 */
	public static Date addDate(Date date, Integer nbJrs) {		
		if(nbJrs == null || nbJrs == 0) return date;
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.DATE, nbJrs);
		return cal.getTime();
	}


    /**
     * Formatte la Trace d'une Exception en Chaine de caractere
     * @param ex Exception
     * @return Texte
     */
    public static String stackTraceToString(Exception ex) {   	
    	try {    		
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.printStackTrace();
			ex.printStackTrace(pw);
			return sw.toString();
    	}catch(Exception e) {e.printStackTrace(); return ex.getMessage();}
    }
    
    /*public static ApiResponse threatException(Exception ex) {
    	String trace = stackTraceToString(ex);
    	String msg = trace.indexOf("Caused by:") > 0 ? trace.substring(trace.indexOf("Caused by:")+11, trace.length()) : trace;
    	msg = msg.indexOf("Exception") > 0 ? msg.substring(msg.indexOf("Exception")+10, 200) : msg.substring(0,200);
    	return new ApiResponse(false, ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : msg, trace);
    }*/
    
	public static <T> T callRestAPI(String url, Map<String, Object> queryParams, Object body, HttpMethod method, Class<T> objClass, boolean auth) throws Exception {
    	
		// Initialisation de la ressource bindee sur l'URL d'envoi
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl( url );
		
		// Si la la map des parametres est non vide
		if(queryParams != null && !queryParams.isEmpty()) {
			for(Entry<String, Object> entry : queryParams.entrySet()) {
				builder.queryParam(entry.getKey(), entry.getValue());
			}
		}
		
		// Initialisation de l'objet RestTemplate
		RestTemplate restTemplate = new RestTemplate();
		
	  	// les valeurs du Header
	  	HttpHeaders requestHeaders = new HttpHeaders();
	  		
	  	// le type de contenu
	  	requestHeaders.setContentType(MediaType.TEXT_PLAIN);
	  	//if(auth) requestHeaders.set("Authorization", getTokenPmtApi(gc) );

	  	// objet contenant le corps et les param�tres entete
	  	HttpEntity<?> request = body == null ? new HttpEntity(requestHeaders) : new HttpEntity(body, requestHeaders);
	  	ResponseEntity<?> responseEntity = null;
	  	
		// Envoi de la requete
	  	responseEntity = restTemplate.exchange(builder.build().toUri(), method, request, objClass);
		
	  	// Recuperation du resultat
	  	return new ObjectMapper().convertValue(responseEntity.getBody(), objClass);
		
    }
	
	public static String formatNumberWithThousandSeparator(Double amount) {
		
		DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(Locale.FRANCE);
		DecimalFormatSymbols symbols = df.getDecimalFormatSymbols();
		char thousandSep = symbols.getGroupingSeparator();
		String input = NumberFormat.getNumberInstance(Locale.FRANCE).format(amount);
		input = input.replace(thousandSep, ' '); 
		return input; // new DecimalFormat("#,##0").format(amount);
	}
    

	@SuppressWarnings("unchecked")
	public static <T> T parseXmlStringToObject(String xmlString, Class<T> objClass) throws Exception {
		if(xmlString == null || objClass == null) return null;
		JAXBContext jaxbContext = JAXBContext.newInstance(objClass);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		StringReader reader = new StringReader(xmlString);
		return (T) unmarshaller.unmarshal(reader);
	}
	
  
}
