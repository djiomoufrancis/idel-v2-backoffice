package ca.qc.banq.idel.bo.repo.editor;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ca.qc.banq.idel.bo.dm.editor.Repondant;



/**
 * Repondant @Repository
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Repository
public interface IRepondantRepository extends JpaRepository<Repondant, Long> {
	
	/**
	 * Liste les repondants d'un editeur
	 * @param uid Compte d'acces de l'editeur
	 * @return Liste des repondants
	 */
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true, noRollbackFor=Exception.class)
	@Query("select r from Repondant r where r.editeur.idCompteAcces = :uid")
	public List<Repondant> listEditorRepondants(@Param("uid") String uid);
	
}
