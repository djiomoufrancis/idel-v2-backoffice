package ca.qc.banq.idel.bo.shared.enums;

public enum ModaliteDiffusionEnum {

	LOCAUX(1,"licence.modaliteDiffusion.locaux"),
	WEB(2,"licence.modaliteDiffusion.web");
	
	private Integer code;
	private String desc;
	
	ModaliteDiffusionEnum(int code, String desc){
		this.code = code;
		this.desc = desc;
	}

	public int getCode() {
		return code;
	}
	
	public String getDesc() {
		return desc;
	}

}
