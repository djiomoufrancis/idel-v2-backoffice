/**
 * 
 */
package ca.qc.banq.idel.bo.business.editor;

import java.util.List;

import org.springframework.stereotype.Service;

import ca.qc.banq.idel.bo.dm.editor.Editeur;
import ca.qc.banq.idel.bo.dm.editor.Repondant;

/**
 * Service client de gestion des editeurs
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Service
public interface IEditorManagementService {

	/**
	 * Service de modification des infos d'un editeur
	 * @param obj Editor to update
	 * @return Editor updated
	 * @throws Exception IdelException or Other generic exception
	 */
	public Editeur updateEditor(Editeur obj) throws Exception;
	
	/**
	 * Recherche un editeur a partir de son identifiant de connexion
	 * @param uid Editor UserID
	 * @return Editor
	 * @throws Exception IdelException or Other generic exception
	 */
	public Editeur findEditorByUid(String uid) throws Exception;
	
	/**
	 * Affiche la liste des repondants d'un editeur
	 * @param uid Identifiant utilisateur de l'editeur
	 * @return Liste des repondants
	 * @throws Exception errors
	 */
	public List<Repondant> listEditorRepondants(String uid) throws Exception;
	
	/**
	 * Determine s'il existe une requete de demande de modification de l'editeur en cours
	 * @param uid identifiant utilisateur
	 * @return true/false
	 * @throws Exception
	 */
	public boolean isUpdateRequestPending(String uid) throws Exception;
	
}
