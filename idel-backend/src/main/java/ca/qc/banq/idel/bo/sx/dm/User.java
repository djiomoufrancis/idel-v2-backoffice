/**
 * 
 */
package ca.qc.banq.idel.bo.sx.dm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ca.qc.banq.idel.bo.dm.Address;
import ca.qc.banq.idel.bo.shared.enums.Gender;
import ca.qc.banq.idel.bo.sx.enums.UserStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Utilisateurs des Webservices
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "sx_usr")
@SuppressWarnings("serial")
public class User implements Serializable {

	@Id
	@Column(name = "id")
	@ApiModelProperty(readOnly=true)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull(message = "User.login.NotNull")
	@Column(name = "login", unique = true)
	private String login;
	
	@JsonIgnore
	@NotNull(message = "User.passord.NotNull")
	@Column(name = "pwd", unique = true)
	private String password;
	
	@Column(name = "name")
	@NotNull(message = "User.name.NotNull")
	private String name;
	
	@Embedded
	private Address address;
	
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	@NotNull(message = "User.status.NotNull")
	private UserStatus status = UserStatus.DISABLED;

	@Column(name = "gender")
	@Enumerated(EnumType.STRING)
	@NotNull(message = "User.gender.NotNull")
	private Gender gender;
	
	@Column(name = "reg_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date date = new Date();
	
	@ManyToOne
	@JoinColumn(name = "profile_id")
	private UserProfile profile;
	
}
