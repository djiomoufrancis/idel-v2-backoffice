package ca.qc.banq.idel.bo.dm.editor;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * DomainePublication generated by hbm2java
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(of = {"idDomainepublication", "code"})
@Table(name = "DOMAINE_PUBLICATION", schema = "")
public class DomainePublication implements Serializable {

	@Id
	@SequenceGenerator(name = "SEQ_DOMAINE_PUBLICATION", sequenceName = "SEQ_DOMAINE_PUBLICATION", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DOMAINE_PUBLICATION")
	@Column(name = "ID_DOMAINEPUBLICATION", unique = true, nullable = false)
	private Long idDomainepublication;
	
	@NotNull(message = "DomainePublication.code.NotNull")
	@Column(name = "CODE", length = 2)
	@Length(max = 2)
	private String code;
	
	@Column(name = "DESCRIPTION", length = 200)
	@Length(max = 200)
	private String description;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "EDITEUR_DOMAINEPUBLICATION", schema = "", joinColumns = { @JoinColumn(name = "ID_DOMAINEPUBLICATION", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "ID_EDITEUR", nullable = false, updatable = false) })
	private Set<Editeur> editeurs = new HashSet<Editeur>(0);

	/*
	@Transient
	public String getDescriptionDisplay() {
		return StringUtils.isBlank(this.getCode()) ? "" : this.getDescription() + "-" + this.getCode();
	}
	
	public DomainePublication clone(){
		DomainePublication object = new DomainePublication();
		object.setIdDomainepublication(this.idDomainepublication);
		object.setCode(this.code);
		object.setDescription(this.description);
		return object;
	}
	*/
	
}
