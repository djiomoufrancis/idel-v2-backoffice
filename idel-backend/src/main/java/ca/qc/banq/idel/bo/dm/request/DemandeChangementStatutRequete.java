package ca.qc.banq.idel.bo.dm.request;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@SuppressWarnings("serial")
@Entity
@DiscriminatorValue(StatutRequete.DEMANDECHANGEMENT)
public class DemandeChangementStatutRequete extends StatutRequete{}