package ca.qc.banq.idel.bo.shared;

public class IdelModelTools {

	public static boolean getAsBoolean(Character value) {
 		if (value == null || value != '1') {
 			return false;
 		} else {
 			return true;
 		}
 	}
 
	public static Character setAsCharacter(Boolean value) {
 		if (null == value || !value) {
 			return '0';
 		} else {
 			return '1';
 		}
	}
}
