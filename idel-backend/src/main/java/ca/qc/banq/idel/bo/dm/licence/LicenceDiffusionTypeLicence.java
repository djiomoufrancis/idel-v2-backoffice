package ca.qc.banq.idel.bo.dm.licence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;

import ca.qc.banq.idel.bo.dm.AbstractEntity;
import ca.qc.banq.idel.bo.shared.IdelModelTools;
import ca.qc.banq.idel.bo.shared.NumericTools;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "LICENCEDIFFUSION_TYPELICENCE", schema = "")
public class LicenceDiffusionTypeLicence extends AbstractEntity implements Serializable {

	@Id
	@SequenceGenerator(name = "SEQ_LICDIFFUSION_TYPELICENCE", sequenceName = "SEQ_LICDIFFUSION_TYPELICENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_LICDIFFUSION_TYPELICENCE")
	@Column(name = "ID_LICENCETYPELICENCE", unique = true, nullable = false)
	@NotNull
	private Long idLicenceDiffusionTypeLicence;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_LICENCEDIFFUSION", nullable = false)
	@NotNull
	private LicenceDiffusion licenceDiffusion;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_TYPELICENCEDIFFUSION", nullable = false)
	@NotNull
	private TypeLicenceDiffusion typeLicenceDiffusion;
	
	@Column(name = "DELAI")
	private Long delai;
	
	@Column(name = "BANQ_LOCAL", length = 1)
	private Character banqLocal;
	
	@Column(name = "BANQ_SITE_WEB", length = 1)
	private Character banqSiteWeb;
	
	@Column(name = "SIGNE", length = 1)
	private Character signe;

	@Column(name = "ACTIVE", length = 1)
	private Character active;
	
	@Column(name = "REJETE", length = 1)
	private Character rejete;
	
	public LicenceDiffusionTypeLicence(long idLicenceDiffusionTypeLicence){
		this.idLicenceDiffusionTypeLicence = idLicenceDiffusionTypeLicence;
	}
	
	public String toString(){
		return "Object LicenceDiffusionTypeDisffusion avec ID " + this.getIdLicenceDiffusionTypeLicence();
	}
	
	public Boolean getBanqLocal() {
		return IdelModelTools.getAsBoolean(banqLocal);
	}

	public void setBanqLocal(Boolean banqLocal) {
		this.banqLocal = IdelModelTools.setAsCharacter(banqLocal);
	}

	public Boolean getBanqSiteWeb() {
		return IdelModelTools.getAsBoolean(banqSiteWeb);
	}

	public void setBanqSiteWeb(Boolean banqSiteWeb) {
		this.banqSiteWeb = IdelModelTools.setAsCharacter(banqSiteWeb);
	}

	public Boolean getSigne() {
		return IdelModelTools.getAsBoolean(signe);
	}

	public void setSigne(Boolean signe) {
		this.signe = IdelModelTools.setAsCharacter(signe);
	}

	public Boolean getActive() {
		return IdelModelTools.getAsBoolean(active);
	}

	public void setActive(Boolean actif) {
		this.active = IdelModelTools.setAsCharacter(actif);
	}

	public Boolean getRejete() {
		return IdelModelTools.getAsBoolean(rejete);
	}

	public void setRejete(Boolean rejete) {
		this.rejete = IdelModelTools.setAsCharacter(rejete);
	}

	@Transient
	public static Long setDelaiLicenceDiffusionP(String choix, String value){
		if(NumericTools.isIntegerPositif(choix) != null){
				return NumericTools.isIntegerPositif(choix).longValue();
		} else {
			if(StringUtils.isNotEmpty(choix)
					&& "AutreDelai".equals(choix)){
				if(NumericTools.isIntegerPositif(value) != null){
					return NumericTools.isIntegerPositif(value).longValue();
				}
			}
		}
		return null;
	} 
	
}
