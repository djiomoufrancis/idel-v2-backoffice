/**
 * 
 */
package ca.qc.banq.idel.bo.business.request;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.qc.banq.idel.bo.dm.editor.Editeur;
import ca.qc.banq.idel.bo.dm.request.Requete;
import ca.qc.banq.idel.bo.repo.request.IContexteRequeteRepository;
import ca.qc.banq.idel.bo.repo.request.IRequeteRepository;
import ca.qc.banq.idel.bo.repo.request.IStatutRequeteRepository;
import ca.qc.banq.idel.bo.repo.request.ITypeRequeteRepository;

/**
 * Implementation du Service client de gestion des requetes
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Service
public class RequestManagementImpl implements IRequestManagementService {

	@Autowired
	ITypeRequeteRepository reqTypeRepo;
	
	@Autowired
	IStatutRequeteRepository reqStatusRepo;
	
	@Autowired
	IContexteRequeteRepository reqContextrepo;

	@Autowired
	IRequeteRepository requestRepo;
	
	
	/**
	 * Construction d'une requete de demande d'inscription d'un editeur
	 */
	public Requete buildEditorRegistrationRequest(Editeur ed) {
		return saveDmdRequest(ed, 0L, 0L, 14L);
	}

	/**
	 * Construction d'une requete de demande de modification d'un editeur
	 */
	public Requete buildUpdateEditorRequest(Editeur ed) {
		return saveDmdRequest(ed, 4L, 0L, 14L);
	}

	/**
	 * Construction d'une requete de demande de Depot Legal d'un editeur
	 */
	public Requete buildDmdDepotRequest(Editeur ed) {
		return saveDmdRequest(ed, 1L, 0L, 14L);
	}

	/**
	 * Construction d'une requete de demande d'ISBN d'un editeur
	 */
	public Requete buildDmdISBNRequest(Editeur ed) {
		return saveDmdRequest(ed, 2L, 0L, 0L);
	}

	/**
	 * Construction d'une requete de demande de creation d'une licence de diffusion d'un editeur
	 */
	public Requete buildDmdLicenceDiffusionRequest(Editeur ed) {
		return saveDmdRequest(ed, 3L, 0L, 12L);
	}

	/**
	 * Construction d'une requete de demande de reclamation d'un editeur
	 */
	public Requete buildDmdClaimRequest(Editeur ed) {
		return saveDmdRequest(ed, 5L, 0L, 14L);
	}
	
	private Requete saveDmdRequest(Editeur ed, Long idTypReq, Long idStatutReq, Long idContextReq) {
		Requete registerRequest = new Requete();
		registerRequest.setTypeRequete(reqTypeRepo.findById(idTypReq).orElse(null) );
		registerRequest.setStatutRequete( reqStatusRepo.findById(idStatutReq).orElse(null) );
		registerRequest.setContexteRequete(reqContextrepo.findById(idContextReq).orElse(null) );
		registerRequest.setDateCreation(new Date());	
		registerRequest.setDateModification(new Date());
		registerRequest.setEditeur(ed);
		return requestRepo.save(registerRequest);
	} 

}
