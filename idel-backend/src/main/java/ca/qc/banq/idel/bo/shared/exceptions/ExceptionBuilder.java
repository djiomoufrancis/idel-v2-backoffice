package ca.qc.banq.idel.bo.shared.exceptions;

import java.io.Serializable;
import java.util.List;

import org.springframework.http.HttpStatus;

import ca.qc.banq.idel.bo.shared.models.ApiResponse;
import lombok.Data;

@Data
@SuppressWarnings("serial")
public class ExceptionBuilder implements Serializable {

	private Integer statutCode;
	private String statutMessage; 
	private String httpMethode;
	private String descriptionErreur;
	private String detail;
	private String pathUri;
    private List<String> erreurs;	
	
	public static Builder builder() {
		return new Builder();
	}
		
	public static class Builder {
		
		private ExceptionBuilder details;
		
		Builder() {
			this.details =  new ExceptionBuilder();
		}
		
		public Builder addStatus(HttpStatus statut) {
			this.details.statutCode = statut.value();
			this.details.statutMessage = statut.getReasonPhrase();
			return this;
		}
		public Builder addHttpMethod(String methode)	{			
			this.details.httpMethode = methode;			
			return this;
		}
		
		public Builder addDescription(String descriptionErreur){			
			this.details.descriptionErreur = descriptionErreur;			
			return this;
		}

		public Builder addDetail(String detail){			
			this.details.detail = detail;			
			return this;
		}
					
		public Builder addPathUri(String pathUri){			
			this.details.pathUri = pathUri;			
			return this;
		}		
		
		public Builder addErrors(List<String> erreurs){			
			this.details.erreurs = erreurs;			
			return this;
		}			
		
		public ApiResponse build() {			
			return new ApiResponse(false, HttpStatus.BAD_REQUEST.value(), "Bad Request", this.details) ;
		}		
	}
}