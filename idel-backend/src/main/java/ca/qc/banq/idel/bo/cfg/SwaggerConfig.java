package ca.qc.banq.idel.bo.cfg;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo())
                .enable(true)
                .securityContexts(Lists.newArrayList(securityContext()))
    			.securitySchemes(Lists.newArrayList(apiKey()));
    }
	
	
	private ApiInfo getApiInfo() {	
		 ApiInfo apiInfo = new ApiInfoBuilder()
				.title("IDEL Back-Office")
				.description("REST API documentation for IDEL BackEnd v2.0")
				.termsOfServiceUrl("Terms of service Url")
				.contact(new Contact("FD", "https://www.banq.qc.ca", "depot@banq.qc.ca"))
				.license("LGPL BAnQ Open Source")
				.licenseUrl("http://intranet.banq.qc.ca/")	
				.version("2.0")
				.build();
		return apiInfo;
	}
	
	
	private ApiKey apiKey() {
		return new ApiKey("Authorization", "Authorization", "header");
	}
	
	@Bean
	public SecurityConfiguration security() {
		return SecurityConfigurationBuilder.builder().clientId(null).clientSecret(null).realm(null).appName(null).scopeSeparator(",").additionalQueryStringParams(null).useBasicAuthenticationWithAccessCodeGrant(false).build();
	}
	
	
	private SecurityContext securityContext() {
		return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.any()).build();
	}
	 
	
	private ArrayList<SecurityReference> defaultAuth() {
		AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
		AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
		authorizationScopes[0] = authorizationScope;
		return Lists.newArrayList(new SecurityReference("Authorization", authorizationScopes));
	}
	
	
}
