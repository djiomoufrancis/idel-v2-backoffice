package ca.qc.banq.idel.bo.dm.publication;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


/***
 * 
 * @author Anis.Hachemi
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)

@Table(name = "ZONE_NOTICE", schema = "")
public class ZoneNotice implements Serializable {

	@Id
	@SequenceGenerator(name = "SEQ_ZONE_NOTICE", sequenceName = "SEQ_ZONE_NOTICE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ZONE_NOTICE")
	@Column(name = "ID_ZONENOTICE", unique = true, nullable = false, precision = 22, scale = 0)
	@NotNull
	private Long idZoneNotice;
	
	@Column(name = "CODE", length = 5)
	private String code;
	
	@Column(name = "DESCRIPTION", length = 200)
	private String description;
	
	
}
