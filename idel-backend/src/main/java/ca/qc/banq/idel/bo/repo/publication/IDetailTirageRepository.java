package ca.qc.banq.idel.bo.repo.publication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ca.qc.banq.idel.bo.dm.publication.DetailTirage;

/**
 * DetailTirage @Repository
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Repository
public interface IDetailTirageRepository extends JpaRepository<DetailTirage, Long> {}
