package ca.qc.banq.idel.bo.shared.enums;

public enum DepotLicenceFaitParEnum {

	BANQ(1,"licence.depotFaitPar.banq"),
	EDITEUR(2,"licence.depotFaitPar.editeur");
	
	private Integer code;
	private String desc;
	
	DepotLicenceFaitParEnum(int code, String desc){
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

}
