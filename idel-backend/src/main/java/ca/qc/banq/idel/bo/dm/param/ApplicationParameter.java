package ca.qc.banq.idel.bo.dm.param;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@Table(name = "APPLICATION_PARAMETERS")
public class ApplicationParameter implements Serializable {
	
	@EmbeddedId
	private ApplicationParameterPK applicationParametersId = new ApplicationParameterPK();

	@Column(name = "PARAMETER_VALUE")
	private String parameterValue = null;

	public String getApplicationName() {
		return applicationParametersId.getApplicationName();
	}

	public String getParameterName() {
		return applicationParametersId.getParameterName();
	}
	
	@Transient
	public String[] getParameterValueToArray() {
		return StringUtils.split( getParameterValue(), ',');
	}

	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof ApplicationParameter)) {
			return false;
		}

		ApplicationParameter that = (ApplicationParameter) o;
		return new EqualsBuilder().append(this.applicationParametersId, that.applicationParametersId).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(applicationParametersId).toHashCode();
	}
}
