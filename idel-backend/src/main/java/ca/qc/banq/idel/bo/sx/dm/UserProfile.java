/**
 * 
 */
package ca.qc.banq.idel.bo.sx.dm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * profils des utilisateurs de webservices
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sx_profile")
@SuppressWarnings("serial")
public class UserProfile implements Serializable {

	@Id
	@Column(name = "id")
	@ApiModelProperty(readOnly=true)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull(message = "code.NotNull")
	@Column(name = "code", unique = true)
	private String code;
	
	@NotNull(message = "name.NotNull")
	@Column(name = "name")
	private String name;

	@ApiModelProperty(readOnly=true)
	@Fetch(FetchMode.SUBSELECT)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "ss_roleprofile", joinColumns = { @JoinColumn(name = "pro_id") }, inverseJoinColumns = {
			@JoinColumn(name = "role_id", nullable = false) })
	private List<Role> roles = new ArrayList<Role>();
	
	@JsonIgnore
	public List<GrantedAuthority> getListGrantedAuthorities(){
		List<GrantedAuthority> rls = new ArrayList<GrantedAuthority>();
		if(roles == null || roles.isEmpty()) return rls;
		for(Role r : roles) rls.add( new SimpleGrantedAuthority( r.getCode() ) );
		rls.add( new SimpleGrantedAuthority( "ROLE_USER" ) );
		return rls;
	}

	
}
