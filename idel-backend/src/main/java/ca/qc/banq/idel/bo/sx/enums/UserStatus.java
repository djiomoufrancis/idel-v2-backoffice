package ca.qc.banq.idel.bo.sx.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * Enumeration des statuts d'utilisateurs de WS
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
public enum UserStatus {
	
	ACTIVATED("UserStatus.Activated"),
	SUSPENDED("UserStatus.Suspended"),
	DISABLED("UserStatus.Disabled");
	
	/**
	 * Valeur
	 */
	private String value;
	
	/**
	 * Constructeur
	 * @param value
	 */
	private UserStatus(String value){
		this.setValue(value);
	}
	
	/**
	 * Retourne la liste des valeus
	 * @return liste des valeurs
	 */
	public static List<UserStatus> getValues() {
		
		// Initialisation de la collection a retourner
		List<UserStatus> ops = new ArrayList<UserStatus>();
		
		// Ajout des valeurs
		ops.add(ACTIVATED);
		ops.add(SUSPENDED);
		ops.add(DISABLED);
		
		// Retourne la collection
		return ops;
		
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
}
