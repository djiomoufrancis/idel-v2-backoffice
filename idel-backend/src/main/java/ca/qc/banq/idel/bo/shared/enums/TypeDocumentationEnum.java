package ca.qc.banq.idel.bo.shared.enums;



/**
 * This enum has a matching data from TYPE_DOCUMENTATION table. 
 * Any change of data in TYPE_DOCUMENTATION table requires a code change to this enum. 
 * 
 * @author Youssef.Soltani
 *
 */
public enum TypeDocumentationEnum {
	
	AUTRES      (0L,"0","Autres"),
	CV 	        (1L,"1","Curriculum Vitae");
	

	private Long id;
	private String code;
	private String description;
	
	TypeDocumentationEnum(Long id, String code, String description) {
		this.id = id;
        this.code = code;
        this.description = description;
    }
    
	public String getCode() {
		return code;
	}
	
	public String getDescription() {
		return description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
