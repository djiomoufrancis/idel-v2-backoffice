package ca.qc.banq.idel.bo.dm.request;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


/**
 * 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "STATUT_REQUETE", schema = "")
@DiscriminatorColumn(name = "TYPEREQUETE_CODE", discriminatorType = DiscriminatorType.STRING)
public abstract class StatutRequete implements Serializable {

	/**
	 * Statut Type Inscription
	 */
	final static public String INSCRIPTION = "0";
	
	/** 
	 * Statut Type Depot
	 */
	final static public String DEPOT = "1";
	
	/**
	 * Statut Type Demande ISBN
	 */
	final static public String DEMANDEISBN = "2";
	
	/**
	 * Statut Type Depot
	 */
	final static public String DEMANDECHANGEMENT = "4";
	
	@Id
	@SequenceGenerator(name = "SEQ_STATUT_REQUETE", sequenceName = "SEQ_STATUT_REQUETE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STATUT_REQUETE")
	@Column(name = "ID_STATUTREQUETE", unique = true, nullable = false)
	@NotNull
	protected Long idStatutrequete;
	
	@Column(name = "CODE", length = 2)
	@Length(max = 2)
	protected String code;
	
	@Column(name = "DESCRIPTION", length = 100)
	@Length(max = 100)
	protected String description;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "statutRequete")
	protected Set<Requete> requetes = new HashSet<Requete>(0);
		
}
