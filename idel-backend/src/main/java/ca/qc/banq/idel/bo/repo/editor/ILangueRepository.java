package ca.qc.banq.idel.bo.repo.editor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ca.qc.banq.idel.bo.dm.editor.Langue;

/**
 * Langue @Repository
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Repository
public interface ILangueRepository extends JpaRepository<Langue, Long> {
	
	public List<Langue> findByCode(@Param("code")String code);
	
}
