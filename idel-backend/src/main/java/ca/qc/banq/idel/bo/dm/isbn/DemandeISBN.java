package ca.qc.banq.idel.bo.dm.isbn;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import ca.qc.banq.idel.bo.dm.request.Requete;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Demande d'ISBN
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "DEMANDE_ISBN", schema = "")
public class DemandeISBN implements Serializable {
	
	@Id
	@SequenceGenerator(name = "SEQ_DEMANDE_ISBN", sequenceName = "SEQ_DEMANDE_ISBN", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEMANDE_ISBN")
	@Column(name = "ID_DEMANDEISBN", unique = true, nullable = false)
	@NotNull
	private Long idDemandeISBN;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_REQUETE")
	private Requete requete;
	
	@Column(name = "FORMULAIRE_UTILISE", length = 200)
	@Length(max = 200)
	private String formulaireUtilise;

	@Column(name = "SEQ_ISBN_DEJA_DEMANDE")
	private boolean sequenceISBNDejaDemande;
	
	@Column(name = "DERNIER_NUMERO_ATTRIBUE", length = 25)
	@Length(max = 25)
	private String dernierNumeroAttribue;
	
	@Column(name = "CATEGORIE_EDITEUR", length = 100)
	@Length(max = 100)
	private String categorieEditeur;
	
	@Column(name = "NUMERO_ENTREPRISE_QUEBEC", length = 25)
	@Length(max = 25)
	private String numeroEntrepriseQuebec;
	
	@Column(name = "LANGUE_PUBLICATION", length = 50)
	@Length(max = 50)
	private String languePublication;
	
	@Column(name = "NOMBRE_TITRE_PREVUS", length = 50)
	@Length(max = 50)
	private String nombreTitrePrevus;
	
	@Column(name = "DOMAINE_PUBLICATION", length = 250)
	@Length(max = 250)
	private String domainePublication;
	
	@Column(name = "COMMENTAIRES_EDITEUR", length = 500)
	@Length(max = 500)
	private String commentaireEditeur;
	
	@Column(name = "NOTE_INTERNE", length = 1000)
	@Length(max = 1000)
	private String noteInterne;
	
	@Column(name = "CONFIRMATION_EDITEUR")
	private boolean confirmationEditeur;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_TRANCHEISBN")
	private TrancheISBN trancheISBN;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "demandeISBN", cascade = {CascadeType.ALL})	
	private List<PublicationInfo> publicationInfoList = new ArrayList<PublicationInfo>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "demandeISBN", cascade = {CascadeType.ALL})	
	private List<Responsable> responsableList = new ArrayList<Responsable>(0);
		
	@Column(name = "DEPLIANT", nullable=false, columnDefinition="boolean default false")
	private boolean depliant;
	
	@Column(name = "REIMPRESSION", nullable=false, columnDefinition="boolean default false")
	private boolean reimpression;	
	
	@Column(name = "ID_REPONDANT")
	private Long idRepondant;
	
}
