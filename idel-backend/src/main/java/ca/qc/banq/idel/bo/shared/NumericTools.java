package ca.qc.banq.idel.bo.shared;

import org.apache.commons.lang3.StringUtils;

public class NumericTools {

	public static Integer isInteger(String str)  
	{  
		try {  
			if(StringUtils.isNotEmpty(str)){
				return Integer.parseInt(str);	
			} else{
				return null;
			}			  
		} catch(NumberFormatException e){  
			return null;  
		}    
	}

	public static Integer isIntegerPositif(String str)  
	{  
		Integer item = isInteger(str);
		if(item != null && item > 0){
			return item;
		}
		return null;
	}
	
	public static Integer isIntegerPositifOrEqualZero(String str)  
	{  
		Integer result;
		try {  
			if(StringUtils.isNotEmpty(str)){
				result = Integer.parseInt(str);	
			} else{
				result = 0;
			}			  
		} catch(NumberFormatException e){  
			return null;  
		}    
		
		if(result != null && result >= 0){
			return result;
		}
		return null;
	}
	
	public static boolean isIntegerFormatAAAA(String str)  
	{  
		Integer temp = isInteger(str);
		if(temp != null
				&& temp > 999
				&& temp < 10000){			
			return true;	
		} else{
			return false;
		}			  
	}
	
	public static boolean isIntegerFormatMM(String str)  
	{  
		Integer temp = isInteger(str);
		if(temp != null
				&& temp > 0
				&& temp < 13){			
			return true;	
		} else{
			return false;
		}			  
	}
	
	public static boolean isIntegerFormatJJ(String str)  
	{  
		Integer temp = isInteger(str);
		if(temp != null
				&& temp > 0
				&& temp < 32){			
			return true;	
		} else{
			return false;
		}			  
	}
	
	public static Long isLong(String str)  
	{  
		try {  
			if(StringUtils.isNotEmpty(str)){	
				return Long.parseLong(str);
			} else{
				return null;
			}			  
		} catch(NumberFormatException e){  
			return null;  
		}    
	}

	public static Long isLongPositif(String str)  
	{  
		Long item = isLong(str) ;
		if(item != null && item > 0){
			return item;
		}
		return null; 
	}
	
	public static Long isLongHex(String str)  
	{  
		try {  
			if(StringUtils.isNotEmpty(str)){	
				return Long.parseLong(str, 16);
			} else{
				return null;
			}			  
		} catch(NumberFormatException e){  
			return null;  
		}    
	}
	
}
