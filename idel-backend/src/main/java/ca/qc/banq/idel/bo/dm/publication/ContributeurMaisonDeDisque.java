package ca.qc.banq.idel.bo.dm.publication;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue(Contributeur.MAISONDEDISQUE)
public class ContributeurMaisonDeDisque extends Contributeur implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String contributeurType() {

		return "Maison de disque";
	}

	public String contributeurCode() {

		return "12";	
	}

}
