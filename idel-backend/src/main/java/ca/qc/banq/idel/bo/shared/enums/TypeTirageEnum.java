package ca.qc.banq.idel.bo.shared.enums;



/**
 * This enum has a matching data from TYPE_TIRAGE table. 
 * Any change of data in TYPE_TIRAGE table requires a code change to this enum. 
 * 
 * @author Youssef.Soltani
 *
 */
public enum TypeTirageEnum {
	
	EPREUVES_IDENTIQUE   (0L,"0","Épreuves identiques"),
	TIRAGE_LIMITE 		 (1L,"1","Tirage limité"),
	TIRAGE_OUVERT        (2L,"2","Tirage ouvert"),
	RETIRAGE   			 (3L,"3","Retirage"),
	TIRAGE_VARIE         (5L,"5","Tirage varié");
	

	private Long id;
	private String code;
	private String description;
	
	TypeTirageEnum(Long id, String code, String description) {
		this.id = id;
        this.code = code;
        this.description = description;
    }
    
	public String getCode() {
		return code;
	}
	
	public String getDescription() {
		return description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
