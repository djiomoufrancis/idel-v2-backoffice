/**
 * 
 */
package ca.qc.banq.idel.bo.business.editor;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ca.qc.banq.idel.bo.business.request.RequestManagementImpl;
import ca.qc.banq.idel.bo.dm.editor.Editeur;
import ca.qc.banq.idel.bo.dm.editor.Repondant;
import ca.qc.banq.idel.bo.repo.editor.IEditeurRepository;
import ca.qc.banq.idel.bo.repo.editor.ILangueRepository;
import ca.qc.banq.idel.bo.repo.editor.IRepondantRepository;
import ca.qc.banq.idel.bo.shared.Messaging;
import ca.qc.banq.idel.bo.shared.exceptions.IdelException;

/**
 * Implementation metier du service ILoginExtranetService
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Service
@Transactional
public class LoginExtranetImpl implements ILoginExtranetService {
	
	@Autowired
	IEditeurRepository editorRepo;
	
	@Autowired
	IRepondantRepository repondantRepo;
	
	@Autowired
	RequestManagementImpl requestService;
	
	@Autowired
	ILangueRepository langRepo;
	
	@Autowired
	Messaging messaging;
	
	
	
	@Override
	public Editeur signin(String uid, String password) throws Exception {
		return editorRepo.findByIdCompteAcces(uid).stream().findFirst().orElse(null);
	}

	
	
	@Override
	public Editeur signup(Editeur obj) throws Exception {
		
		// Check business Constraints
		if(obj.getRepondants() == null || obj.getRepondants().isEmpty()) throw new IdelException("Repondants.List.Cannot.Be.Empty");
		List<Repondant> reps = new ArrayList<Repondant>(obj.getRepondants());
		
		// Create a new Editor
		obj.setEditeurActif('O');
		obj.setEditeurQuebecois('O');
		obj.setEstEditeur('O');
		obj.setParaitBottin('N');
		obj.setPrincipal('N');
		obj.setRepondants(null);
		obj.setLangue(langRepo.findById(4L).orElse(null) );	
		obj = editorRepo.save(obj);
		
		// Save repondants
		for(Repondant r : reps) r.setEditeur(obj);
		repondantRepo.saveAll(reps);
		
		// Postage d'une requete de demande d'inscription d'un editeur dans l'intranet
		requestService.buildEditorRegistrationRequest(obj);
		
		// Notification de l'editeur
		if(obj.getCourriel() != null && !obj.getCourriel().isEmpty()) messaging.sendNotificationByMail("1", obj.getIdEditeur().toString(), messaging.getMessage("Notification.email.Body.Inscription"), messaging.getMessage("Notification.email.Object.Inscription"), new String[] {obj.getCourriel()} );
		
		// retourne l'editeur
		return obj;
	}

}
