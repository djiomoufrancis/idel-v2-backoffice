package ca.qc.banq.idel.bo.dm.publication;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * PeriodiqueInfo generated by hbm2java
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "PERIODIQUE_INFO", schema = "")
public class PeriodiqueInfo implements Serializable {
	
	@Id
	@SequenceGenerator(name = "SEQ_PERIODIQUE_INFO", sequenceName = "SEQ_PERIODIQUE_INFO", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PERIODIQUE_INFO")
	@Column(name = "ID_PERIODIQUEINFO", unique = true, nullable = false, precision = 22, scale = 0)
	@NotNull
	private Long idPeriodiqueinfo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PUBLICATION", nullable = false)
	@NotNull
	private Publication publication;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "DATE_PREMIERE_PARUTION", length = 7)
	private Date datePremiereParution;
	
	@Column(name = "VOLUME_PREMIERE_PARUTION", length = 25)
	@Length(max = 25)
	private String volumePremiereParution;
	
	@Column(name = "NUMERO_PREMIERE_PARUTION", length = 25)
	@Length(max = 25)
	private String numeroPremiereParution;
	
	@Column(name = "PRIX_ABONNEMENT", length = 30)
	@Length(max = 30)
	private String prixAbonnement;
	
	@Column(name = "PRIX_NUMERO", length = 30)
	@Length(max = 30)
	private String prixNumero;
	
	@Column(name = "TITRE_PRECEDENT", length = 2000)
	@Length(max = 2000)
	private String titrePrecedent;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "DATE_DEBUT_TITRE_PRECEDENT", length = 7)
	private Date dateDebutTitrePrecedent;
	
	@Column(name = "VOLUME_DEBUT_TITRE_PRECEDENT", length = 25)
	@Length(max = 25)
	private String volumeDebutTitrePrecedent;
	
	@Column(name = "NUMERO_DEBUT_TITRE_PRECEDENT", length = 25)
	@Length(max = 25)
	private String numeroDebutTitrePrecedent;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "DATE_FIN_TITRE_PRECEDENT", length = 7)
	private Date dateFinTitrePrecedent;
	
	@Column(name = "VOLUME_FIN_TITRE_PRECEDENT", length = 25)
	@Length(max = 25)
	private String volumeFinTitrePrecedent;
	
	@Column(name = "NUMERO_FIN_TITRE_PRECEDENT", length = 25)
	@Length(max = 25)
	private String numeroFinTitrePrecedent;
	
	@Column(name = "NOUVEAU_TITRE", length = 1)
	private Character nouveauTitre;


}
