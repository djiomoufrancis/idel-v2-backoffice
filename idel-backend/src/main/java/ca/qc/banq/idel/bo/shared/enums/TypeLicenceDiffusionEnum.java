package ca.qc.banq.idel.bo.shared.enums;

public enum TypeLicenceDiffusionEnum {

	LICENCE_L("L"),
	LICENCE_P("P"),
	LICENCE_W("W");
	
	private String code;
	
	TypeLicenceDiffusionEnum(String code){
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public static String isTypeLDValide(String code){
		for(TypeLicenceDiffusionEnum item : TypeLicenceDiffusionEnum.values()){
			if(item.getCode().equalsIgnoreCase(code)){
				return item.getCode();				
			}
		}
		return null;
	}
}
