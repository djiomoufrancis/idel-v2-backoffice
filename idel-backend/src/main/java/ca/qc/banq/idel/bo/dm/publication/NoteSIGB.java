package ca.qc.banq.idel.bo.dm.publication;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


/**
 * NoteSIGB.
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "NOTE_SIGB", schema = "")
public class NoteSIGB implements Serializable {
	
	@Id
	@SequenceGenerator(name = "SEQ_NOTE_SIGB", sequenceName = "SEQ_NOTE_SIGB", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_NOTE_SIGB")
	@Column(name = "ID_NOTESIGB", unique = true, nullable = false, precision = 22, scale = 0)
	@NotNull
	private Long idNoteSIGB;
	
	@Column(name = "CODE", length = 2)
	private String code;
	
	@Column(name = "DESCRIPTION", length=4000)
	private String description;


}
