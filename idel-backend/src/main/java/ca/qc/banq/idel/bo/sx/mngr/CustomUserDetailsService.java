package ca.qc.banq.idel.bo.sx.mngr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ca.qc.banq.idel.bo.sx.dm.User;
import ca.qc.banq.idel.bo.sx.enums.UserStatus;
import ca.qc.banq.idel.bo.sx.repository.IUserRepository;



/**
 * Implementation d'un service personnalise de gestion des utilisateurs
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Service("CustomUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

	/**
	 * Repository
	 */
    @Autowired
    private IUserRepository userRepository;

    /*
     * (non-Javadoc)
     * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
     */
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // Let people login with either username or email
    	User user = userRepository.findByLogin(username).stream().findFirst().orElse(null); 
    	if(user == null) throw new UsernameNotFoundException("User '"+username+"' not found!" );
    	else if(!user.getStatus().equals(UserStatus.ACTIVATED)) throw new UsernameNotFoundException("User '"+username+"' Suspended or Disabled" );
        return UserPrincipal.create(user);
    }

    /**
     * 
     * @param id Identifiant utilisateur
     * @return UserDetails 
     */
    @Transactional
    public UserDetails loadUserById(String id) {
    	User user = userRepository.findById(id).orElseThrow( () -> new RuntimeException("User with id: " + id) );
    	UserDetails principal = UserPrincipal.create(user);
    	user = null;
    	return principal;
    }
}