package ca.qc.banq.idel.bo.shared.enums;



/**
 * This enum has a matching data from TYPE_REQUETE table. 
 * Any change of data in TYPE_REQUETE table requires a code change to this enum. 
 * 
 * @author Youssef.Soltani
 *
 */
public enum ContexteRequeteEnum {
	
	ISBN         	(0L,"0","ISBN"),
	ISBN_GOUV 		(1L,"1","ISBN-Gouv"),
	DL_MONO       	(2L,"2","DL-Mono"),
	DL_PES   		(3L,"3","DL-PES"),
	DL_SON  		(4L,"4","DL-Son"),
	DL_DOC_ELEC     (5L,"5","DL-Doc-Elec"),
	DL_ARTISTE      (6L,"6","DL-Artiste"),
	DL_ESTAMPES 	(7L,"7","DL-Estampes"),
	DL_AFFICHES     (8L,"8","Dl-Affiches"),
	DL_CART_POST   	(9L,"9","DL-Cart-Post"),
	DL_CART_PLAN  	(10L,"10","DL-Cart-Plan"),
	DL_PGQ         	(11L,"11","DL-PGQ");
	

	private Long id;
	private String code;
	private String description;
	
	ContexteRequeteEnum(Long id, String code, String description) {
		this.id = id;
        this.code = code;
        this.description = description;
    }
    
	public String getCode() {
		return code;
	}
	
	public String getDescription() {
		return description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
