package ca.qc.banq.idel.bo.dm.publication;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Categorie document
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "LOCALISATION_CATEGORIEDOCUMENT", schema = "")
public class LocalisationCategorieDocument implements Serializable {
	
	@Id
	@Column(name = "ID_LOCALISATIONCATEGORIEDOC", unique = true, nullable = false, precision = 22, scale = 0)
	@NotNull
	private Long idLocalisationCategorieDocument;	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_LOCALISATION", nullable = false)
	@NotNull
	private Localisation localisation;	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_CATEGORIEDOCUMENT", nullable = false)
	@NotNull
	private CategorieDocument categorieDocument;	
	}
