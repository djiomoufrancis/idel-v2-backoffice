package ca.qc.banq.idel.bo.dm.requests.enums;

public enum DepotStatutRequeteTraitement {
	
	SIGNALE ("10"),
	EN_COURS_DENREGISTREMENT ("11"),
	ENREGISTRE ("12"),
	ANNULE ("21"),
	REJETE ("13"),
	TERMINE ("14"),
	INCOMPLET ("15");
	
	
	private String code;
	
	public String getCode() {
		return code;
	}

	DepotStatutRequeteTraitement(String code){
		this.code = code;
	}

}
