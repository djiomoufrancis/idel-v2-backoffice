package ca.qc.banq.idel.bo.dm.publication;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


/**
 * 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "ETAT_PHYSIQUE", schema = "")
public class EtatPhysique implements Serializable {
	
	@Id
	@Column(name = "ID_ETATPHYSIQUE", unique = true, nullable = false, precision = 22, scale = 0)
	@NotNull
	private Long idEtatPhysique;
	
	@Column(name = "CODE", length = 10)
	private String code;
	
	@Column(name = "DESCRIPTION", length=100)
	private String description;

}
