package ca.qc.banq.idel.bo.shared.enums;



/**
 * This enum has a matching data from TYPE_REQUETE table. 
 * Any change of data in TYPE_REQUETE table requires a code change to this enum. 
 * 
 * @author Youssef.Soltani
 *
 */
public enum TypeRequeteEnum {
	
	INSCRIPTION         (0L,"0","Inscription"),
	DEPOT 		        (1L,"1","Depot"),
	DEMANDE_ISBN        (2L,"2","Demande ISBN"),
	LICENSE_DIFFUSION   (3L,"3","License de diffusion"),
	DEMANDE_CHANGEMENT  (4L,"4","Demande de changement"),
	RECLAMATION         (5L,"5","Reclamation");
	

	private Long id;
	private String code;
	private String description;
	
	TypeRequeteEnum(Long id, String code, String description) {
		this.id = id;
        this.code = code;
        this.description = description;
    }
    
	public String getCode() {
		return code;
	}
	
	public String getDescription() {
		return description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
