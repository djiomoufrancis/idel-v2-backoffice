package ca.qc.banq.idel.bo.dm.requests.enums;

public enum InscriptionStatutRequeteTraitement {
	
	A_VALIDER,
	EN_ATTENTE,
	EXISTANT,
	REJETEE,
	VALIDEE;
	
	public String stringValue() {
		return Integer.toString(this.ordinal());
	}

}
