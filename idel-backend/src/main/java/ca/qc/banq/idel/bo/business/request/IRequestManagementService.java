/**
 * 
 */
package ca.qc.banq.idel.bo.business.request;

import org.springframework.stereotype.Service;

/**
 * Service client de gestion des requetes
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Service
public interface IRequestManagementService {
	
}
