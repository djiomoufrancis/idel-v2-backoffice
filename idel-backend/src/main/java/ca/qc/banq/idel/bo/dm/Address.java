/**
 * 
 */
package ca.qc.banq.idel.bo.dm;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Classe representant une Adresse
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@SuppressWarnings("serial")
public class Address implements Serializable {

	private String street;
	private String city;
	private String state;
	private String country;
	private String countryIso3;
	private String postalCode;
	private String email;
	private String website;
	private String phone1;
	private String phone2;
	
}
