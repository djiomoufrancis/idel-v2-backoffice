package ca.qc.banq.idel.bo.dm.publication;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue(Contributeur.CONCEPTEUR)
public class ContributeurConcepteur extends Contributeur implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String contributeurType() {

		return "Concepteur";
	}

	public String contributeurCode() {

		return "4";	
	}

}
