package ca.qc.banq.idel.bo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import ca.qc.banq.idel.bo.cfg.AppConfig;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableAsync
@EnableSwagger2
@EnableAutoConfiguration
@EnableScheduling
@ComponentScan(basePackages = {"ca.qc.banq.idel.bo", "ca.qc.banq.notification"})

/**
 * Classe de Bootage
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(AppConfig.class, args);
	}

}
