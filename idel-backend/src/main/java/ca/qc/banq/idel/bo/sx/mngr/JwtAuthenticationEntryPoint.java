package ca.qc.banq.idel.bo.sx.mngr;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

/**
 * Point d'entree du gestionnaire de l'authentification JWT
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

	/**
	 * Initialisation d'un Logger
	 */
    private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationEntryPoint.class);
    
    /**
     * Point d'entree des requetes Web
     */
    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        
    	// Log
    	logger.error("Responding with unauthorized error. Message - {}", e.getMessage());
        
        // Exception Ressource non authorisee
        httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Sorry, You're not authorized to access this resource.");
        
    }
}
