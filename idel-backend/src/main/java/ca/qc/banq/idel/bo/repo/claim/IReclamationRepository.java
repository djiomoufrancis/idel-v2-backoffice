package ca.qc.banq.idel.bo.repo.claim;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ca.qc.banq.idel.bo.dm.claim.Reclamation;


/**
 * Reclamation Repository
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Repository
public interface IReclamationRepository extends JpaRepository<Reclamation, Long> {
	
	@Query("select distinct object (ligneReclamation) " +
			"from LigneReclamation ligneReclamation," +
			"IN (ligneReclamation.reclamation ) reclamation , " +
			"IN (ligneReclamation.niveauReclamation) niveauReclamation, "+
			"IN (reclamation.depot) depot, "+
			"IN (depot.publications) publication, "+
			"IN (depot.requete) requete, "+
			"IN (requete.editeur) editeur "  +
			"where ( ligneReclamation.active IS true ) and ( reclamation.active IS true ) and " +
			"(publication.version = 1) " + 
			" and (editeur.idCompteAcces =:uid ) ") // "select c from Reclamation c where c.editor.idCompteAcces = :uid")
	public List<Reclamation> findByEditor(@Param("uid")String uid);
}
