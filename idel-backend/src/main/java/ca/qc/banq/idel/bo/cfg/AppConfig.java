package ca.qc.banq.idel.bo.cfg;

import java.io.File;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Configuration Generale de l'application
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Configuration
@ComponentScan(basePackages = { "ca.qc.banq.idel" })
@Import({ DaoConfig.class, SwaggerConfig.class, SecurityConfig.class, ListenerConfig.class, WebMvcConfig.class })
public class AppConfig {

	/**
	 * Chemin local des fichiers temporaire
	 */
	public static String workPathDir = System.getProperty("user.dir").concat(File.separator).concat("idel").concat(File.separator).concat("work").concat(File.separator);
	
}
