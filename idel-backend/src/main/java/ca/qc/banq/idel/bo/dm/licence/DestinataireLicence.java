package ca.qc.banq.idel.bo.dm.licence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import ca.qc.banq.idel.bo.shared.IdelModelTools;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "DESTINATAIRE_LICENCE", schema = "")
public class DestinataireLicence implements Serializable {

	@Id
	@SequenceGenerator(name = "SEQ_DESTINATAIRE_LICENCE", sequenceName = "SEQ_DESTINATAIRE_LICENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DESTINATAIRE_LICENCE")
	@Column(name = "ID_DESTINATAIRELICENCE", unique = true, nullable = false, precision = 22, scale = 0)
	@NotNull
	private Long idDestinataireLicence;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_LICENCEDIFFUSIONINFO", nullable = false)
	@NotNull
	private LicenceDiffusionInfo licenceDiffusionInfo;
	
	@Column(name = "DESTINATAIRE_PRINCIPAL", length = 1)
	private Character destPrincipal;
	
	@Column(name = "NOM", length = 200)
	private String nom;
	
	@Column(name = "FONCTION", length = 100)
	private String fonction;
		
	@Column(name = "COURRIEL", length = 200)
	private String courriel;

	public Boolean getDestPrincipal() {
		return IdelModelTools.getAsBoolean(this.destPrincipal);
	}

	public void setDestPrincipal(Boolean destPrincipal) {
		this.destPrincipal = IdelModelTools.setAsCharacter(destPrincipal);
	}

}
