package ca.qc.banq.idel.bo.shared;

import java.util.ArrayList;
import java.util.List;

public class IsbnTools {

	public static String nettoieIsbnCode(String isbnCode){
		
		int index = isbnCode.indexOf("(");
		if(index > -1){
			isbnCode = isbnCode.substring(0, index);			
		}
		isbnCode.trim();
		List<Character> charArray = new ArrayList<Character>();
		Character xExist = null;
		for (int i = 0; i < isbnCode.length(); i++) {
			if(isbnCode.charAt(i) == 'X' || isbnCode.charAt(i) == 'x'){
				xExist = isbnCode.charAt(i);
			} else if(Character.isDigit(isbnCode.charAt(i))){
				charArray.add(isbnCode.charAt(i));
				xExist = null;
			}
		}	
		if(xExist != null){
			charArray.add(xExist);
		}
		StringBuilder builder = new StringBuilder(charArray.size());
	    for(Character ch: charArray){
	        builder.append(ch);
	    }
	    return builder.toString();	   		
	}
}
