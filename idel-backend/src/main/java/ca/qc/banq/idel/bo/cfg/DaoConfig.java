package ca.qc.banq.idel.bo.cfg;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Configuration
@ComponentScan(basePackages = { "ca.qc.banq.idel.bo", "ca.qc.banq.notification" })
@EntityScan(basePackages = { "ca.qc.banq.idel.bo", "ca.qc.banq.notification" } )
@EnableJpaRepositories(basePackages = { "ca.qc.banq.idel.bo", "ca.qc.banq.notification" } )
public class DaoConfig {

	/**
	 * packages des entités JPA
	 */
	public final static String[] ENTITIES_PACKAGES = { "ca.qc.banq.idel.bo.dm", "ca.qc.banq.idel.bo.sx.dm", "ca.qc.banq.notification" };


    @Value("${spring.datasource.url}")
    private String dbUrl;

    @Value("${spring.datasource.username}")
    private String dbUsername;

    @Value("${spring.datasource.password}")
    private String dbPwd;

    @Value("${spring.datasource.driver-class-name}")
    private String dbDriverClassName;

    @Value("${spring.datasource.database.type}")
    private String dbType;

    @Value("${spring.jpa.show-sql}")
    private boolean showSql;

    @Value("${spring.jpa.generate-ddl}")
    private boolean genDDL;

	/**
	 * Initialisation de la source de donnees JDBC
	 * @return
	 */
	@Bean
	public DataSource dataSource() {
		DataSource dataSource = new DataSource();
		dataSource.setDriverClassName(dbDriverClassName);
		dataSource.setUrl(dbUrl);
		dataSource.setUsername(dbUsername);
		dataSource.setPassword(dbPwd);
		dataSource.setInitialSize(1);
		return dataSource;
	}
	
	/**
	 * Initialisation du JBDCTemplate
	 * @return
	 */
	@Bean
	public JdbcTemplate jdbcTemplate() {
		return new JdbcTemplate( dataSource() );
	}

	/**
	 * le provider JPA est Hibernate
	 * @return
	 */
	@Bean
	public JpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
		hibernateJpaVendorAdapter.setShowSql(showSql);
		hibernateJpaVendorAdapter.setGenerateDdl(genDDL);
		hibernateJpaVendorAdapter.setDatabase(Database.valueOf(dbType));
		return hibernateJpaVendorAdapter;
	}


	/**
	 * Initialisation de la Fabrique du gestionnaire d'entites
	 * @param jpaVendorAdapter
	 * @param dataSource
	 * @return
	 */
	@Bean
	public EntityManagerFactory entityManagerFactory(JpaVendorAdapter jpaVendorAdapter, DataSource dataSource) {
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(jpaVendorAdapter);		
		factory.setPackagesToScan(ENTITIES_PACKAGES);
		factory.setDataSource(dataSource);
		Properties jpaProperties = new Properties(); 
		factory.setJpaProperties(jpaProperties);
		factory.afterPropertiesSet();
		return factory.getObject();
	}

	/**
	 * Initialisation du gestionnaire des transactions
	 * @param entityManagerFactory
	 * @return
	 */
	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(entityManagerFactory);
		return txManager;
	}

}
