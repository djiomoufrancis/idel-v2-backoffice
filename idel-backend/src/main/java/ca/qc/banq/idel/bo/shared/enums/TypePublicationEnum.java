package ca.qc.banq.idel.bo.shared.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Youssef.Soltani
 *
 */
public enum TypePublicationEnum {
	
	MONOGRAPHIE  	(0L,"0","Monographie"),
	PES 		 	(1L,"1","Publication en série"),
	AFFICHE      	(2L,"2","Affiche ou reproduction d'oeuvre d'art"),
	CARTE_POSTALE  	(3L,"3","Carte postale"),
	DOC_ELEC   	 	(4L,"4","Document électronique/logiciel"),
	ENR_SONORE   	(5L,"5","Enregistrement sonore"),
	ESTAMPE  		(6L,"6","Estampe"),
	LIVRE_ARTISTE 	(7L,"7","Livre d'Artiste/Ouvrage bibliophilie"),
	CARTE_PLAN      (8L,"8","Carte géographique et plan"),	
	PUBLICATION_EN_LIGNE  (9L,"9","Publication en ligne ou publication en ligne avec accès restreint");	

	private Long id;
	private String code;
	private String description;
	public static List<TypePublicationEnum> listTypePubs = (List<TypePublicationEnum>)Arrays.asList(TypePublicationEnum.values());
	
	TypePublicationEnum(Long id, String code, String description) {
		this.id = id;
        this.code = code;
        this.description = description;
    }
    
	public String getCode() {
		return code;
	}
	
	public String getDescription() {
		return description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public static List<TypePublicationEnum> getListTypePubs() {
		return listTypePubs;
	}


	public static List<String> getListDescription(){
		List<String> result = new ArrayList<String>();
		for(TypePublicationEnum item : listTypePubs){
			result.add(item.getDescription());
		}
		return result;
	}
	
	public static TypePublicationEnum getTypePublicationByDesc(String desc){
		TypePublicationEnum result = null;
		for(TypePublicationEnum item : listTypePubs){
			if(item.getDescription().equals(desc)){
				return item;
			}
		}
		return result;
	}
	
	public static String getCodeTypePublicationByDesc(String desc){
		String result = null;
		for(TypePublicationEnum item : listTypePubs){
			if(item.getDescription().equals(desc)){
				return item.getCode();
			}
		}
		return result;
	}
}
