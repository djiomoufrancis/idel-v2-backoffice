package ca.qc.banq.idel.bo.sx.mngr;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Modele d'une requete d'autentification
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
public class LoginRequest implements Serializable {
	
	/**
	 * Username
	 */
    @NotBlank
    private String username;

    /**
     * Password
     */
    private String password;
}
