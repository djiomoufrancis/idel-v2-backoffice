package ca.qc.banq.idel.bo.dm.publication;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue(Contributeur.AUTEURDUTEXTE)
public class ContributeurAuteurDuTexte extends Contributeur implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String contributeurType() {

		return "AuteurDuTexte";
	}

	public String contributeurCode() {

		return "14";	
	}
	

	
}
