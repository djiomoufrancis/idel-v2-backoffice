/**
 * 
 */
package ca.qc.banq.idel.bo.rest.publication;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.qc.banq.idel.bo.dm.publication.Depot;
import ca.qc.banq.idel.bo.repo.publication.IDepotRepository;
import ca.qc.banq.idel.bo.shared.MsgKeys;
import ca.qc.banq.idel.bo.shared.Utils;
import ca.qc.banq.idel.bo.shared.models.ApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * Webservices REST de gestion des publications
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/pub", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Api(description = "Service de gestion des publications")
public class PublicationController {

	@Autowired
	private IDepotRepository depotService;

	@Autowired
	MessageSource messageSource;
	
	@PostMapping("/depot/new")
	@Secured("ROLE_newDepot")
	@ApiOperation("Signaler un nouveau depot d'imprime")
	public ResponseEntity<ApiResponse> newDepot(@RequestBody @ApiParam(value = "Demande de depot a creer") Depot depot) {

		// Initialise response to return
		ApiResponse resp = null;
		
		try {
			
			// Try something
			Depot d = depotService.save(depot);
			
			// Build response
			resp = new ApiResponse(true, HttpStatus.OK.value(), messageSource.getMessage(MsgKeys.MSG_SUCCESS, null, null), d);
			
		} /*catch(IdelException ie) {
			
			// Threat an Idel Exception
			resp = new ApiResponse(false, HttpStatus.BAD_REQUEST.value(), messageSource.getMessage(ie.getMessage(), null, null), ie);
			
		}*/ catch(Exception ex) {
			
			// Threat other exceptions type
			resp = new ApiResponse(false, HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage(), Utils.stackTraceToString(ex) );
			
			// Trace Log
			log.error(ex.getMessage(), ex);
		}
		
		// Return generic API response
		return ResponseEntity.ok(resp);
	}
	
	
	@GetMapping("/depot/list/{id}")
	@Secured("ROLE_listDepot")
	@ApiOperation("Afficher la liste des depots d'un editeur")
	public ResponseEntity<ApiResponse> listDepot(@PathParam("id") @ApiParam(value = "Identifiant de l'editeur") String editorId) {

		// Initialise response to return
		ApiResponse resp = null;
		
		try {
			
			// Try something
			List<Depot> list = depotService.findAll();
			
			// Build response
			resp = new ApiResponse(true, HttpStatus.OK.value(), messageSource.getMessage(MsgKeys.MSG_SUCCESS, null, null), list);
			
		} /*catch(IdelException ie) {
			
			// Threat an Idel Exception
			resp = new ApiResponse(false, HttpStatus.BAD_REQUEST.value(), messageSource.getMessage(ie.getMessage(), null, null), ie);
			
		}*/ catch(Exception ex) {
			
			// Threat other exceptions type
			resp = new ApiResponse(false, HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage(), Utils.stackTraceToString(ex) );
			
			// Trace Log
			log.error(ex.getMessage(), ex);
		}
		
		// Return generic API response
		return ResponseEntity.ok(resp);
	}
	
}
