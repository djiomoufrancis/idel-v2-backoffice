package ca.qc.banq.idel.bo.dm.publication;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(Contributeur.DISTRIBUTEUR)
public class ContributeurDistributeur extends Contributeur implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String contributeurType() {

		return "Distributeur";
	}

	public String contributeurCode() {

		return "11";	
	}

}
