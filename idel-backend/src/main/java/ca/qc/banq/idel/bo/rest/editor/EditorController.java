/**
 * 
 */
package ca.qc.banq.idel.bo.rest.editor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.qc.banq.idel.bo.business.editor.IEditorManagementService;
import ca.qc.banq.idel.bo.dm.editor.Editeur;
import ca.qc.banq.idel.bo.shared.Messaging;
import ca.qc.banq.idel.bo.shared.MsgKeys;
import ca.qc.banq.idel.bo.shared.Utils;
import ca.qc.banq.idel.bo.shared.exceptions.IdelException;
import ca.qc.banq.idel.bo.shared.models.ApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * Webservices REST de gestion des Editeurs
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Slf4j
@RestController
@RequestMapping("/api/editor")
@Api(description = "Service de gestion des editeurs")
public class EditorController {

	@Autowired
	private IEditorManagementService editorService;

	@Autowired
	Messaging messaging;
	
	/**
	 * Enregistre une demande de Modification des coordonnees d'un Editeur
	 * @param editor Editeur a modifier
	 * @return Editeur mis a jour
	 */
	@PutMapping("/update")
	@Secured("ROLE_updateEditor")
	@ApiOperation("Service de demande de mise a jour des informations d'un editeur")
	public ResponseEntity<ApiResponse> updateEditor(@RequestBody @ApiParam(value = "Objet Editeur a modifier") Editeur editor) {

		// Initialise response to return
		ApiResponse resp = null;
		
		try {
			
			// Try something
			Editeur e = editorService.updateEditor(editor);
			
			// Build response
			resp = new ApiResponse(true, HttpStatus.OK.value(), messaging.getMessage(MsgKeys.MSG_SUCCESS), e);
			
		} catch(IdelException ie) {
			
			// Threat an Idel Exception
			resp = new ApiResponse(false, HttpStatus.BAD_REQUEST.value(), messaging.getMessage(ie.getMessage()), ie);
			
		} catch(Exception ex) {
			
			// Threat other exceptions type
			resp = new ApiResponse(false, HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage(), Utils.stackTraceToString(ex) );
			
			// Trace Log
			log.error(ex.getMessage(), ex);
		}
		
		// Return generic API response
		return ResponseEntity.ok(resp);
	}
	
	

	/**
	 * Determine s'il existe une requete de demande de modification en cours 
	 * @param editor Editeur a modifier
	 * @return Liste des repondants
	 */
	@GetMapping("/isUpdateRequestPending/{uid}")
	@ApiOperation("Determine s'il existe une requete de demande de modification en cours")
	public ResponseEntity<ApiResponse> isUpdateRequestPending(@PathVariable("uid") @ApiParam(value = "Identifiant utilisateur de l'editeur") String uid) {

		// Initialise response to return
		ApiResponse resp = null;
		
		try {
			
			// Build response
			resp = new ApiResponse(true, HttpStatus.OK.value(), messaging.getMessage(MsgKeys.MSG_SUCCESS), editorService.isUpdateRequestPending(uid));

			// Trace Log
			log.error("resp = " + resp);
			
		} catch(IdelException ie) {
			
			// Threat an Idel Exception
			resp = new ApiResponse(false, HttpStatus.BAD_REQUEST.value(), messaging.getMessage(ie.getMessage()), ie);
			
		} catch(Exception ex) {
			
			// Threat other exceptions type
			resp = new ApiResponse(false, HttpStatus.INTERNAL_SERVER_ERROR.value(), messaging.getMessage(MsgKeys.MSG_SYSTEM_ERROR) + " " + ex.getMessage(), Utils.stackTraceToString(ex) );
			
			// Trace Log
			log.error(ex.getMessage(), ex);
		}

		// Trace Log
		log.error("resp = " + resp);
		
		// Return generic API response
		return ResponseEntity.ok(resp);
	}
	
}
