/**
 * 
 */
package ca.qc.banq.idel.bo.business.claim;

import java.util.List;

import org.springframework.stereotype.Service;

import ca.qc.banq.idel.bo.dm.claim.Reclamation;

/**
 * Service client de gestion des reclamations
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Service
public interface IClaimManagementService {

	/**
	 * Retourne la liste des reclamations de l'editeur
	 * @param editorUid
	 * @return
	 */
	public List<Reclamation> listEditorClaims(String editorUid) throws Exception;
	
}
