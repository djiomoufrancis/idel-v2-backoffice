package ca.qc.banq.idel.bo.dm.publication;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import ca.qc.banq.idel.bo.dm.claim.Reclamation;
import ca.qc.banq.idel.bo.dm.request.Requete;
import ca.qc.banq.idel.bo.dm.request.StatutRequete;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


/**
 * 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
//@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
//@Table(name = "V_LISTE_DEPOT", schema = "") 
public class ListDepot implements Serializable {
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_REQUETE", nullable = false)
	@NotNull
	private Requete requete;
	
	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PUBLICATION", nullable = false)
	@NotNull
	private Publication publication;
	
	@Column(name = "ID_DEPOSANT")
	private Long idEditeurDeposant;
	
	@Column(name = "ID_EDITEUR_PUBLICATION")
	private Long idEditeurPublication;
	
	@Column(name = "CODE_EDITEUR_PUBLICATION")
	private String codeEditeurPublication;
	
	@Column(name = "ID_DEPOT")
	private Long idDepot;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_STATUTREQUETE", nullable = false)
	@NotNull
	private StatutRequete statutRequete;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_RECLAMATION")
	private Reclamation reclamation;
	
	@Column(name = "DATE_CREATION")
	private Date dateCreation;
	
	@Column(name = "DATE_MODIFICATION")
	private Date dateModification;
	
	@Column(name = "USAGER_MODIFICATION")
	private String usagerModification;
	
	@Column(name = "DATE_REJET")
	private Date dateRejet;
	
	@Column(name = "DATE_ENREGISTREMENT")
	private Date dateEnregistrement;
	
	@Column(name = "USAGER_REJET")
	private String usagerRejet;
	
	@Column(name = "TITRE_PUBLICATION")
	private String titrePublication;
	
	@Column(name = "TYPE_PUBLICATION_CODE")
	private String typePublicationCode;
	
	@Column(name = "NUMERO_NOTICE_CATALOGUE")
	private String numeroNoticeCatalogue;
	
	@Column(name = "DATE_RECEPTION_DOCUMENT")
	private Date dateReceptionDocument;
	
	@Column(name = "DTDCP")
	private boolean dtdcp;
	
	@Column(name = "STATUT_REQUETE_CODE")
	private String statutRequeteCode;
	
	@Column(name = "STATUT_REQUETE_DESC")
	private String statutRequeteDesc;
	
	@Column(name = "CODE_DEPOSANT")
	private String codeDeposant;
	
	@Column(name = "ACTIVE")
	private Boolean active;
	
	@Column(name = "PRIORITE")
	private String priorite;
	
	@Column(name = "SECTEURPNQ_CODE_EDITEUR")
	private String secteurPNQCodeEditeur;
	
	@Column(name = "SECTEURPNQ_CODE_DEPOSANT")
	private String secteurPNQCodeDeposant;
	
	@Column(name = "ID_TYPEDOCUMENT")
	private Long idtypeDocument;
	
	@Column(name = "DESCRIPTION_TYPEDOCUMENT")
	private String descriptionTypeDocument;
	
}
