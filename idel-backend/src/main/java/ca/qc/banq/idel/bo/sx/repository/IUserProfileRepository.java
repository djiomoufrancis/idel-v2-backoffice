package ca.qc.banq.idel.bo.sx.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ca.qc.banq.idel.bo.sx.dm.UserProfile;

/**
 * Profiles Management Repository
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Repository
public interface IUserProfileRepository extends JpaRepository<UserProfile, Long> {

	public List<UserProfile> findByCode(String code);
	
}
