package ca.qc.banq.idel.bo.dm.isbn;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "ACTIVITE", schema = "")
public class Activite implements Serializable {

	@Id
	@Column(name = "ID_ACTIVITE", unique = true, nullable = false)
	@NotNull
	private Long idActivite;
	
	@Column(name = "CODE", length = 2)
	@Length(max = 2)
	private String code;
	
	@Column(name = "DESCRIPTION", length = 100)
	@Length(max = 100)
	private String description;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "activite", cascade = CascadeType.ALL)
	private Set<ResponsableActivite> responsableActiviteList = new HashSet<ResponsableActivite>(0);
	
}
