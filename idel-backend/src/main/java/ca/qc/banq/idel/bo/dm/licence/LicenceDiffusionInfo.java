package ca.qc.banq.idel.bo.dm.licence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import ca.qc.banq.idel.bo.shared.IdelModelTools;
import ca.qc.banq.idel.bo.shared.enums.TypeLicenceDiffusionEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "LICENCE_DIFFUSION_INFO", schema = "")
public class LicenceDiffusionInfo implements Serializable{

	@Id
	@SequenceGenerator(name = "SEQ_LICENCE_DIFFUSION_INFO", sequenceName = "SEQ_LICENCE_DIFFUSION_INFO", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_LICENCE_DIFFUSION_INFO")
	@Column(name = "ID_LICENCEDIFFUSIONINFO", unique = true, nullable = false)
	@NotNull
	private Long idLicenceDiffusionInfo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_LICENCEDIFFUSION", nullable = false)
	@NotNull
	private LicenceDiffusion licenceDiffusion;
	
	@Column(name = "TYPE_LICENCE", length = 3)
	private String typeLicence;
	
	@Column(name = "NOM_FICHIER", length = 200)
	private String nomFichier;
	
	@Column(name = "COMMENTAIRE_EDITEUR", length = 1024)
	private String commentaireEditeur;

	@Column(name = "CAPTURE_RETRO", length = 1)
	private Character captureRetro;
	
	@Column(name = "DATE_CAPTURE")
	private Date dateCapture;
		
	@Column(name = "DATE_CREATION")
	private Date dateCreation;

	@Column(name = "USAGER_CREATION", length = 100)
	private String usagerCreation;
	
	@Column(name = "DATE_MODIFICATION")
	private Date dateModification;
	
	@Column(name = "USAGER_MODIFICATION", length = 100)
	private String usagerModification;
	
	@Column(name = "COMMENTAIRE_BANQ", length = 1024)
	private String commentaireBANQ;
	
	@Column(name = "DATE_SIGNATURE")
	private Date dateSignature;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "licenceDiffusionInfo", cascade = {CascadeType.ALL})
	private List<DestinataireLicence> destLicences = new ArrayList<DestinataireLicence>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "licenceDiffusionInfo", cascade = {CascadeType.ALL})
	private List<ContactLicence> contactLicences = new ArrayList<ContactLicence>(0);
	
	public String toString(){
		return "Objet LicenceDiffusionInfo avec ID " + this.getIdLicenceDiffusionInfo();
	}
	
	public Boolean getCaptureRetro() {
		return IdelModelTools.getAsBoolean(captureRetro);
	}
	
	public void setCaptureRetro(Boolean captureRetro) {
		this.captureRetro = IdelModelTools.setAsCharacter(captureRetro);
	}
	
	@Transient
	public boolean isDateSignatureNotNull(){
		return (dateSignature != null);
	}
	
	@Transient
	public boolean isDateSignatureNull(){
		return (dateSignature == null);
	}
	
	@PrePersist
	public void beforeCreate(){
		this.dateModification  = new Date(); 
	}
	
	@PreUpdate
	public void beforeUpdate(){
		this.dateModification  = new Date(); 
	}
	
	@Transient
	public ContactLicence getContactLicenceNum(){
		if(getContactLicences() != null){
			for(ContactLicence item : getContactLicences()){
				if(TypeLicenceDiffusionEnum.LICENCE_L.getCode().equalsIgnoreCase(item.getTypeLicenceDiffusion().getCode())
						|| TypeLicenceDiffusionEnum.LICENCE_P.getCode().equalsIgnoreCase(item.getTypeLicenceDiffusion().getCode())){
					return item;
				}
			}
		}
		return null;
	}
	
	@Transient
	public ContactLicence getContactLicenceWeb(){
		if(getContactLicences() != null){
			for(ContactLicence item : getContactLicences()){
				if(TypeLicenceDiffusionEnum.LICENCE_W.getCode().equalsIgnoreCase(item.getTypeLicenceDiffusion().getCode())){
					return item;
				}
			}
		}
		return null;
	}
	
	@Transient
	public DestinataireLicence getDestinatairePrinc(){
		if(getDestLicences() != null){
			for(DestinataireLicence item : getDestLicences()){
				if(item.getDestPrincipal()){
					return item;
				}
			}
		}
		return null;
	}
	
	@Transient
	public List<DestinataireLicence> getOtherDestinataires(){
		List<DestinataireLicence> result = new ArrayList<DestinataireLicence>();
		if(getDestLicences() != null){
			for(DestinataireLicence item : getDestLicences()){
				if(!item.getDestPrincipal()){
					result.add(item);
				}
			}
		}
		return result;
	}
	
}
