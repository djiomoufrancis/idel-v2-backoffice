package ca.qc.banq.idel.bo.shared.exceptions;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import ca.qc.banq.idel.bo.shared.models.ApiResponse;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler({NullPointerException.class, IllegalArgumentException.class})
	public ResponseEntity<Object> serveurException(RuntimeException runtimeException, WebRequest request){
		
		return handleExceptionInternal(				
				runtimeException, ExceptionBuilder.builder()
				.addDetail("An Exception occured...")
				.addDescription(runtimeException.getMessage())
				.addStatus(HttpStatus.INTERNAL_SERVER_ERROR)
				.addPathUri(getPath(request))
				.addHttpMethod(getHttpMethod(request))
				.build(),
				new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}
	
    @ExceptionHandler(ConstraintViolationException.class)
    public void constraintViolationException(HttpServletResponse response) throws IOException {
    	response.sendError(HttpStatus.BAD_REQUEST.value());
    }
    
			
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException excetion, HttpHeaders headers, HttpStatus statut, WebRequest request) {

        Map<String, Object> map = new LinkedHashMap<>();
        /*map.put("datetime", new Date());
        map.put("status", statut.value());*/
        List<String> erreurs = excetion.getBindingResult().getFieldErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.toList());
        map.put("errors", erreurs);
        return new ResponseEntity<Object>( new ApiResponse(false, HttpStatus.BAD_REQUEST.value(), "Bad Request", map), headers, statut);

    }		
	
	private String getPath(WebRequest request) {
		ServletWebRequest servletWebRequest = (ServletWebRequest) request;
		return servletWebRequest.getRequest().getRequestURI();
	}
	
	private String getHttpMethod(WebRequest request) {
		ServletWebRequest servletWebRequest = (ServletWebRequest) request;
		return servletWebRequest.getRequest().getMethod();
	}

}
