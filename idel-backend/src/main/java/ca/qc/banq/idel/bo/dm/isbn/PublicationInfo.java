package ca.qc.banq.idel.bo.dm.isbn;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import ca.qc.banq.idel.bo.dm.publication.IsbnIssn;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "PUBLICATION_INFO", schema = "")
public class PublicationInfo implements Serializable {
	
	@Id
	@SequenceGenerator(name = "SEQ_PUBLICATION_INFO", sequenceName = "SEQ_PUBLICATION_INFO", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PUBLICATION_INFO")
	@Column(name = "ID_PUBLICATIONINFO", unique = true, nullable = false)
	@NotNull
	private Long idPublicationInfo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_DEMANDEISBN")
	private DemandeISBN demandeISBN;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ISBNISSN")
	private IsbnIssn isbnIssn;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_FORMATPUBLICATION")
	private FormatPublication formatPublication;

	@Column(name = "TITRE", length = 2000)
	@Length(max = 2000)
	private String titre;
	
	@Column(name = "VILLE_PUBLICATION", length = 100)
	@Length(max = 100)
	private String villePublication;
	
	@Column(name = "DATE_MISE_SOUS_PRESSE")
	private Date dateMiseSousPresse;
	
	@Column(name = "DATE_PROBABLE_PUBLICATION")
	private Date dateProbablePublication;
	
	@Column(name = "ANNUELLE")
	private boolean annuelle;
	
	@Column(name = "PUBLICATION_GRATUITE")
	private boolean publicationGratuite;
	
	@Column(name = "NOTIFICATION_ENVOYEE")
	private boolean notificationEnvoyee;		
	
	@Column(name = "PETIT_DOCUMENT", nullable=false, columnDefinition="boolean default false")
	private boolean petitDocument;		

	@Column(name = "PUBLICATION_ACTIVE", nullable=false, columnDefinition="boolean default true")
	private boolean publicationActive = true;		

}
