package ca.qc.banq.idel.bo.repo.isbn;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ca.qc.banq.idel.bo.dm.isbn.DemandeISBN;

/**
 * Demande d'ISBN @Repository
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Repository
public interface IDemandeISBNRepository extends JpaRepository<DemandeISBN, Long> {}
