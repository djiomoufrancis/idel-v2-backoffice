package ca.qc.banq.idel.bo.dm.requests.enums;

public enum DemandeChangementStatutRequeteTraitement {
	
	DEMANDE_DE_CHANGEMENT_EN_COURS,
	DEMANDE_DE_CHANGEMENT_TRAITEE,
	DEMANDE_DE_CHANGEMENT_EN_COURS_COPIE;
	
	public String stringValue() {
		return Integer.toString(this.ordinal());
	}

}
