package ca.qc.banq.idel.bo.shared.enums;



/**
 * This enum has a matching data from TYPE_REQUETE table. 
 * Any change of data in TYPE_REQUETE table requires a code change to this enum. 
 * 
 * @author Youssef.Soltani
 *
 */
public enum CategorieMusicaleEnum {
	
	CLASSIQUE         	(1L,"1","Classique"),
	FOLKLORIQUE_TRAD	(2L,"2","Folklorique/Trad"),
	NON_MUSICAL       	(3L,"3","Non musical"),
	COUNTRY_FOLK   		(4L,"4","Country/Folk"),
	JAZZ_BLUES  		(5L,"5","Jazz/Blues"),
	POP_ROCK		    (6L,"6","Pop/Rock"),
	DANCE_TECHNO	    (7L,"7","Dance/Techno"),
	METAL_PUNK	    	(8L,"8","Metal/Punk"),
	RAP_HIPHOP          (9L,"9","Rap/Hip-Hop"),
	ENFANTS   			(10L,"10","Enfants"),
	MUSIQUE_DU_MONDE  	(11L,"11","Musique du monde"),
	REGGAE_SKA         	(12L,"12","Reggae/SKA"),
	AUTRE         		(0L,"0","Autre");
	

	private Long id;
	private String code;
	private String description;
	
	CategorieMusicaleEnum(Long id, String code, String description) {
		this.id = id;
        this.code = code;
        this.description = description;
    }
    
	public String getCode() {
		return code;
	}
	
	public String getDescription() {
		return description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
