/**
 * 
 */
package ca.qc.banq.idel.bo.business.editor;

import org.springframework.stereotype.Service;

import ca.qc.banq.idel.bo.dm.editor.Editeur;

/**
 * Service client d'authentification a l'extranet
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Service
public interface ILoginExtranetService {

	/**
	 * Service d'authentification a l'extranet
	 * @param uid UserID
	 * @param password Password
	 * @return Editor
	 * @throws Exception
	 */
	public Editeur signin(String uid, String password) throws Exception;
	
	/**
	 * Service d'inscription d'un editeur  sur l'extranet
	 * @param obj Editor to create
	 * @return Saved Editor
	 * @throws Exception
	 */
	public Editeur signup(Editeur obj) throws Exception;
	
}
