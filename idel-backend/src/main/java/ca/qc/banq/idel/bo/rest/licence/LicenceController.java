/**
 * 
 */
package ca.qc.banq.idel.bo.rest.licence;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.qc.banq.idel.bo.dm.licence.LicenceDiffusionInfo;
import ca.qc.banq.idel.bo.repo.licence.ILicenceDiffusionInfoRepository;
import ca.qc.banq.idel.bo.shared.MsgKeys;
import ca.qc.banq.idel.bo.shared.Utils;
import ca.qc.banq.idel.bo.shared.exceptions.IdelException;
import ca.qc.banq.idel.bo.shared.models.ApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * Webservices REST de gestion des Licences
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/licence", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Api(description = "Service de gestion des licences")
public class LicenceController {


	@Autowired
	private ILicenceDiffusionInfoRepository licenceService;

	@Autowired
	MessageSource messageSource;
	
	@GetMapping("/display")
	@Secured("ROLE_displayLicences")
	@ApiOperation("Affichage de la liste des licenses d'autorisation accordees a la BAnQ")
	public ResponseEntity<ApiResponse> displayLicences() {

		// Initialise response to return
		ApiResponse resp = null;
		
		try {
			
			// Try something
			List<LicenceDiffusionInfo> list = licenceService.findAll();
			
			// Build response
			resp = new ApiResponse(true, HttpStatus.OK.value(), messageSource.getMessage(MsgKeys.MSG_SUCCESS, null, null), list);
			
		} /*catch(IdelException ie) {
			
			// Threat an Idel Exception
			resp = new ApiResponse(false, HttpStatus.BAD_REQUEST.value(), messageSource.getMessage(ie.getMessage(), null, null), ie);
			
		}*/ catch(Exception ex) {
			
			// Threat other exceptions type
			resp = new ApiResponse(false, HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage(), Utils.stackTraceToString(ex) );
			
			// Trace Log
			log.error(ex.getMessage(), ex);
		}
		
		// Return generic API response
		return ResponseEntity.ok(resp);
	}
	
}
