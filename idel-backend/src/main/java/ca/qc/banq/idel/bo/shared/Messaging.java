package ca.qc.banq.idel.bo.shared;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import ca.qc.banq.notification.domaine.TypeDestinataire;
import ca.qc.banq.notification.domaine.TypeNotification;
import ca.qc.banq.notification.entity.Notification;
import ca.qc.banq.notification.entity.NotificationDest;
import ca.qc.banq.notification.servicemetier.NotificationClientServiceMetier;
import lombok.extern.slf4j.Slf4j;

/**
 * Messaging Component
 * @author francis.djiomou
 * @version 1.0
 */
@Slf4j
@Component
public class Messaging {

	/**
	 * Inject notification Service
	 */
	@Autowired
	private NotificationClientServiceMetier notificationService;
	
	@Autowired
	MessageSource messageSource;
	
	/**
	 * Send Emails to a list of email addresses
	 * @param ref Subject reference code
	 * @param text Body of the mail
	 * @param object Object of the mail
	 * @param emails List of email addresses
	 */
	public void sendNotificationByMail(String ref, String codeDoc, String text, String object, String[] emails ) {
		
		// Create notification
		Notification msg = new Notification();
		msg.setCodeModeleDocument(codeDoc);
		msg.setCodeReference(ref);
		msg.setContenuBrut(text);
		msg.setObjet(object);
		msg.setContenuHtml("");
		msg.setDateCreation(new Date());
		msg.setDateEnvoi(new Date());
		msg.setDateModification(null);
		msg.setTypeNotification(TypeNotification.COU);
		msg.setUserCreation(null);
		
		// Create the list of receivers
		List<NotificationDest> dests = new ArrayList<NotificationDest>();
		for(String e : emails) {
			NotificationDest dest = new NotificationDest();
			dest.setCourriel(e);
			dest.setTypeDestinataire(TypeDestinataire.DES);
			dest.setIndLu(false);
			dest.setNotification(msg);
			dests.add(dest);
		}
		msg.setNotificationsDest(dests);
		
		try {
			
			// Send Emails
			notificationService.envoyerCourriel(msg);
			
		} catch(Exception ex) {
			// Log sending mail error
			log.error(ex.getMessage(), ex);
		}
	}
	
	public String getMessage(String code) {
		String msg = code;
		try {
			log.trace("Display Locale : ", LocaleContextHolder.getLocale());
			msg = messageSource.getMessage(code, null, LocaleContextHolder.getLocale());
		} catch(Exception ex) {
			// Log
			log.error(ex.getMessage(), ex);
		}
		return msg;
	}
}
