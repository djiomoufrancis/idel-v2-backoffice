package ca.qc.banq.idel.bo.shared.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * Enumeration des Genres
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
public enum Gender {
	
	MALE("Gender.Male"),
	FEMALE("Gender.Female"),
	UNDEFINED("Gender.Undefined");
	
	/**
	 * Valeur
	 */
	private String value;
	
	/**
	 * Constructeur
	 * @param value
	 */
	private Gender(String value){
		this.setValue(value);
	}
	
	/**
	 * Retourne la liste des valeus
	 * @return
	 */
	public static List<Gender> getValues() {
		
		// Initialisation de la collection a retourner
		List<Gender> ops = new ArrayList<Gender>();
		
		// Ajout des valeurs
		ops.add(MALE);
		ops.add(FEMALE);
		ops.add(UNDEFINED);
		
		// Retourne la collection
		return ops;
		
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
}
