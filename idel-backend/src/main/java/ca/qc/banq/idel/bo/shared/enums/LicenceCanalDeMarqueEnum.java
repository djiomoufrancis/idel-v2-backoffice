package ca.qc.banq.idel.bo.shared.enums;

public enum LicenceCanalDeMarqueEnum {

	OUVERT(1,"licence.canalDeMarque.Ouvert"),
	FERME(2,"licence.canalDeMarque.Ferme");
	
	private Integer code;
	private String desc;
	
	LicenceCanalDeMarqueEnum(int code, String desc){
		this.code = code;
		this.desc = desc;
	}

	public int getCode() {
		return code;
	}
	
	public String getDesc() {
		return desc;
	}	
}
