package ca.qc.banq.idel.bo.shared.models;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Structure de reponse gneerique des API
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
public class ApiResponse implements Serializable {
	/**
	 * Etat du resultat de l'execution du webservice
	 */
	private boolean success;
	/**
	 * Code de la reponse
	 */
	private Integer code;
	/**
	 * Message a afficher au client
	 */
	private String message;
	/**
	 * Donnees retournees par la requete 
	 */
	private Object data;
	
}
