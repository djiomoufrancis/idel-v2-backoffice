package ca.qc.banq.idel.bo.dm.isbn;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "RESPONSABLE_ACTIVITE", schema = "")
public class ResponsableActivite implements Serializable {

	@Id
	@SequenceGenerator(name = "SEQ_RESPONSABLE_ACTIVITE", sequenceName = "SEQ_RESPONSABLE_ACTIVITE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_RESPONSABLE_ACTIVITE")
	@Column(name = "ID_RESPONSABLEACTIVITE", unique = true, nullable = false)
	@NotNull
	private Long idResponsableActivite;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_RESPONSABLE", nullable = false)
	@NotNull
	private Responsable responsable;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ACTIVITE", nullable = false)
	@NotNull
	private Activite activite;
}
