package ca.qc.banq.idel.bo.dm.licence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "CONTACT_LICENCE", schema = "")
public class ContactLicence implements java.io.Serializable {

	@Id
	@SequenceGenerator(name = "SEQ_CONTACT_LICENCE", sequenceName = "SEQ_CONTACT_LICENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CONTACT_LICENCE")
	@Column(name = "ID_CONTACTLICENCE", unique = true, nullable = false)
	@NotNull
	private Long idContactLicence;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_LICENCEDIFFUSIONINFO", nullable = false)
	@NotNull
	private LicenceDiffusionInfo licenceDiffusionInfo;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_TYPELICENCEDIFFUSION", nullable = false)
	@NotNull
	private TypeLicenceDiffusion typeLicenceDiffusion;
		
	@Column(name = "NOM", length = 100)
	private String nom;
	
	@Column(name = "PRENOM", length = 100)
	private String prenom;
	
	@Column(name = "TELEPHONE")
	private String phone;
		
	@Column(name = "POSTE")
	private String poste;
	
	@Column(name = "COURRIEL", length = 200)
	private String courriel;


}
