package ca.qc.banq.idel.bo.shared.enums;

/**
 * This enum has a matching data from TYPE_PROCEDE_TECHNIQUE table. Any change of data in
 * TYPE_PROCEDE_TECHNIQUE table requires a code change to this enum.
 * 
 * @author Youssef.Soltani
 * 
 */
public enum TypeProcedeTechniqueEnum {

	GRAUVRE_SUR_BOIS	(1L, "1", "Gravure sur bois"), 
	POINTE_SECHE		(2L, "2","Pointe-seche"), 
	EAU_FORTE			(3L, "3", "Eau-forte"), 
	MANIERE_NOIRE		(4L, "4", "Maniere noire"), 
	LITHOGRAPHIE		(5L, "5", "Lithographie"), 
	SERIGRAPHIE			(6L, "6", "Sérigraphie"), 
	LINOGRAVURE			(7L, "7", "Linogravure"), 
	COLLAGRAPHIE		(8L, "8", "Collagraphie"), 
	AUTRE				(0L, "0", "Autre");

	private Long id;
	private String code;
	private String description;

	TypeProcedeTechniqueEnum(Long id, String code, String description) {
		this.id = id;
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
