package ca.qc.banq.idel.bo.shared.enums;



/**
 * This enum has a matching data from STATUT_REQUETE table. 
 * Any change of data in STATUT_REQUETE table requires a code change to this enum. 
 * 
 * @author Youssef.Soltani
 *
 */
public enum StatutRequeteModificationEnum {
	
	EN_COURS   				(16L,"16","En cours"),
	ACCEPTE                 (17L,"17","Accepté"),
	MODIF_EN_COURS_COPIE    (18L,"18","Modification en cours - copie"),
	REFUSE				    (19L,"19","Refusé"),
	MODIF_EN_COURS_TRAITE   (20L,"20","Modification en cours - traité"),
	ISBN_EN_ATTENTE   		(22L,"22","En Attente"),
	ISBN_ACCEPTE  			(23L,"23","Accepté"),
	ISBN_REJETEE  			(24L,"24","Rejetée");	
	

	private Long id;
	private String code;
	private String description;
	
	StatutRequeteModificationEnum(long id, String code, String description) {
		this.id = id;
        this.code = code;
        this.description = description;
    }
    
	public String getCode() {
		return code;
	}
	
	public String getDescription() {
		return description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
