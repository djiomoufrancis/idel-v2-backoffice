package ca.qc.banq.idel.bo.shared.enums;



/**
 * This enum has a matching data from STATUT_REQUETE table. 
 * Any change of data in STATUT_REQUETE table requires a code change to this enum. 
 * 
 * @author Youssef.Soltani
 *
 */
public enum StatutRequeteDepotEnum {
	
	SIGNALE      (10L,"10","Signalé"),
	EN_COURS_ENR (11L,"11","En cours d'enregistrement"),
	ENREGISTRE   (12L,"12","Enregistré"),
	REJETE       (13L,"13","Rejetée"),
	TERMINE      (14L,"14","Terminé"),
	INCOMPLET    (15L,"15","Incomplet"),
	ANNULE       (21L,"21","Annulé");
	

	private Long id;
	private String code;
	private String description;
	
	StatutRequeteDepotEnum(Long id, String code, String description) {
		this.id = id;
        this.code = code;
        this.description = description;
    }
    
	public String getCode() {
		return code;
	}
	
	public String getDescription() {
		return description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
