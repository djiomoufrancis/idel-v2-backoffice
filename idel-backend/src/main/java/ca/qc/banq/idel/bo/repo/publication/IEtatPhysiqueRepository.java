package ca.qc.banq.idel.bo.repo.publication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ca.qc.banq.idel.bo.dm.publication.EtatPhysique;


/**
 * @Repository EtatPhysique
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Repository
public interface IEtatPhysiqueRepository extends JpaRepository<EtatPhysique, Long> {}
