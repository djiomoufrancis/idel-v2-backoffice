/**
 * 
 */
package ca.qc.banq.idel.bo.business.editor;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ca.qc.banq.idel.bo.business.request.RequestManagementImpl;
import ca.qc.banq.idel.bo.dm.editor.Editeur;
import ca.qc.banq.idel.bo.dm.editor.Repondant;
import ca.qc.banq.idel.bo.repo.editor.IEditeurRepository;
import ca.qc.banq.idel.bo.repo.editor.IRepondantRepository;
import ca.qc.banq.idel.bo.shared.Messaging;
import ca.qc.banq.idel.bo.shared.exceptions.IdelException;

/**
 * Implementation du service metier IEditorManagementService
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Service
@Transactional
public class EditorManagementImpl implements IEditorManagementService {
	
	@Autowired
	IEditeurRepository editorRepo;
	
	@Autowired
	IRepondantRepository repondRepo;
	
	@Autowired
	RequestManagementImpl requestService;

	@Autowired
	Messaging messaging;

	@Override
	public Editeur updateEditor(Editeur obj) throws Exception {

		// Check business Constraints
		if(obj.getRepondants() == null || obj.getRepondants().isEmpty()) throw new IdelException("Repondants.List.Cannot.Be.Empty");
		List<Repondant> reps = new ArrayList<Repondant>(obj.getRepondants());
		
		// Create a new Editor
		obj.setIdEditeurAssocie(obj.getIdEditeur());
		obj.setIdEditeur(null);
		obj.setRepondants(null);
		obj = editorRepo.save(obj);
		
		// Save repondants
		for(Repondant r : reps) {
			r.setIdRepondantOrig(r.getIdRepondant());
			r.setIdRepondant(null);
			r.setDupliquer(Boolean.TRUE);
			r.setEditeur(obj);
		}
		repondRepo.saveAll(reps);
		
		// Postage d'une requete de demande de modification d'un editeur dans l'intranet
		requestService.buildUpdateEditorRequest(obj);
		
		// Notification de l'editeur
		if(obj.getCourriel() != null && !obj.getCourriel().isEmpty()) messaging.sendNotificationByMail("1", obj.getIdEditeur().toString(), messaging.getMessage("Notification.email.body.UpdateEditorRequest"), messaging.getMessage("Notification.email.Object.UpdateEditorRequest"), new String[] {obj.getCourriel()} );
		
		// retourne l'editeur
		return obj;
	}

	@Override
	public Editeur findEditorByUid(String uid) throws Exception {
		Editeur e =  editorRepo.findByIdCompteAcces(uid).stream().findFirst().orElse(null);
		if(e != null) e.setRepondants( repondRepo.listEditorRepondants(uid) );
		return e;
	}

	@Override
	public List<Repondant> listEditorRepondants(String uid) throws Exception {
		return repondRepo.listEditorRepondants(uid);
	}

	@Override
	public boolean isUpdateRequestPending(String uid) throws Exception {
		return editorRepo.findByIdCompteAcces(uid).stream().count() > 1l;
	}

}
