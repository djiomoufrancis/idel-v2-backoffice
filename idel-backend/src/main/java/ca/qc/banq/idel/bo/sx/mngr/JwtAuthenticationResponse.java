package ca.qc.banq.idel.bo.sx.mngr;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Modele de reponse de l'authentification JWT
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("unused")
public class JwtAuthenticationResponse {
	
	/**
	 * Token
	 */
	private String accessToken;
    
    /**
     * Type de token
     */
    private String tokenType = "Bearer";

	/**
	 * @param accessToken Token d'acces aux API
	 */
	public JwtAuthenticationResponse(String accessToken) {
		super();
		this.accessToken = accessToken;
	}

}
