package ca.qc.banq.idel.bo.dm.licence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import ca.qc.banq.idel.bo.dm.editor.Editeur;
import ca.qc.banq.idel.bo.shared.IdelModelTools;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "LICENCE_DIFFUSION", schema = "")
public class LicenceDiffusion implements java.io.Serializable{

	@Id
	@SequenceGenerator(name = "SEQ_LICENCE_DIFFUSION", sequenceName = "SEQ_LICENCE_DIFFUSION", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_LICENCE_DIFFUSION")
	@Column(name = "ID_LICENCEDIFFUSION", unique = true, nullable = false)
	@NotNull
	private Long idLicenceDiffusion;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_EDITEUR", nullable = false)
	@NotNull
	private Editeur editeur;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SECTEURPNQ", nullable = true)
	private SecteurPNQ secteurPNQ;
	
	@Column(name = "DEPOT_PAR_BANQ", length = 1)
	private Character depotParBanq;
	
	@Column(name = "DEPOT_PAR_EDITEUR", length = 1)
	private Character depotParEditeur;
	
	@Column(name = "CONSERVATION", length = 1)
	private Character conservation;
		
	@Column(name = "DEPOT_BAC", length = 1)
	private Character depotBAC;
	
	@Column(name = "NOTES", length = 1024)
	private String notes;
	
	@Column(name = "STATUT_DEMARQUE_PRESENT", length = 1)
	private Character statutDeMarquePresent;	
	
	@Column(name = "STATUT_DEMARQUE_RETIRE", length = 1)
	private Character statutDeMarqueRetire;		
	
	@Column(name = "CANAL_DEMARQUE_OUVERT", length = 1)
	private Character canalDeMarqueOuvert;	
	
	@Column(name = "CANAL_DEMARQUE_FERME", length = 1)
	private Character canalDeMarqueFerme;		
	
	@Column(name = "INVITATION_AUTO_TRANSMISE", length = 1)
	private Character invitationAutorisationTransmise;
	
	@Column(name = "REFUS_AUTORISATION", length = 1)
	private Character refusAutorisation;
	
	@Column(name = "SOLLICITATION_VIA_DEMARQUE", length = 1)
	private Character sollicitationViaDeMarque;

	@Column(name = "CODE_UNIQUE_DEMARQUE", length = 1024)
	private String codeUniqueDeMarque;	
	
	@Column(name = "REFUS_BAC", length = 1)
	private Character refusBAC;


	@OneToMany(fetch = FetchType.LAZY, mappedBy = "licenceDiffusion")
	private List<LicenceDiffusionTypeLicence> licenceDiffusionTypeLs = new ArrayList<LicenceDiffusionTypeLicence>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "licenceDiffusion")
	private List<LicenceDiffusionInfo> licenceDiffusionInfos = new ArrayList<LicenceDiffusionInfo>(0);
	
	public Boolean getDepotParEditeur() {
		return IdelModelTools.getAsBoolean(depotParEditeur);
	}

	public void setDepotParEditeur(Boolean depotParEditeur) {
		this.depotParEditeur = IdelModelTools.setAsCharacter(depotParEditeur);
	}
	
	public Boolean getStatutDeMarquePresent() {
		return IdelModelTools.getAsBoolean(statutDeMarquePresent);
	}

	public void setStatutDeMarquePresent(Boolean statutDeMarquePresent) {
		this.statutDeMarquePresent = IdelModelTools.setAsCharacter(statutDeMarquePresent);
	}

	public Boolean getStatutDeMarqueRetire() {
		return IdelModelTools.getAsBoolean(statutDeMarqueRetire);
	}

	public void setStatutDeMarqueRetire(Boolean statutDeMarqueRetire) {
		this.statutDeMarqueRetire = IdelModelTools.setAsCharacter(statutDeMarqueRetire);
	}		
	
	public Boolean getCanalDeMarqueOuvert() {
		return IdelModelTools.getAsBoolean(canalDeMarqueOuvert);
	}

	public void setCanalDeMarqueOuvert(Boolean canalDeMarqueOuvert) {
		this.canalDeMarqueOuvert = IdelModelTools.setAsCharacter(canalDeMarqueOuvert);
	}

	public Boolean getCanalDeMarqueFerme() {
		return IdelModelTools.getAsBoolean(canalDeMarqueFerme);
	}

	public void setCanalDeMarqueFerme(Boolean canalDeMarqueFerme) {
		this.canalDeMarqueFerme = IdelModelTools.setAsCharacter(canalDeMarqueFerme);
	}		
	
	public Boolean getConservation() {
		return IdelModelTools.getAsBoolean(conservation);
	}

	public void setConservation(Boolean conservation) {
		this.conservation = IdelModelTools.setAsCharacter(conservation);
	}
	
	public Boolean getInvitationAutorisationTransmise() {
		return IdelModelTools.getAsBoolean(invitationAutorisationTransmise);
	}

	public void setInvitationAutorisationTransmise(
			Boolean invitationAutorisationTransmise) {
		this.invitationAutorisationTransmise = IdelModelTools.setAsCharacter(invitationAutorisationTransmise);
	}	
	
	public Boolean getDepotBAC() {
		return IdelModelTools.getAsBoolean(depotBAC);
	}

	public void setDepotBAC(Boolean depotBAC) {
		this.depotBAC = IdelModelTools.setAsCharacter(depotBAC);
	}		
	
	
	public boolean getRefusBAC() {
		return IdelModelTools.getAsBoolean(refusBAC);
	}

	public void setRefusBAC(Boolean refusBAC) {
		this.refusBAC = IdelModelTools.setAsCharacter(refusBAC);
	}
	
	
	public Boolean getRefusAutorisation() {
		return IdelModelTools.getAsBoolean(refusAutorisation);
	}

	public void setRefusAutorisation(Boolean refusAutorisation) {
		this.refusAutorisation = IdelModelTools.setAsCharacter(refusAutorisation);
	}	

	public Boolean getSollicitationViaDeMarque() {
		return IdelModelTools.getAsBoolean(sollicitationViaDeMarque);
	}

	public void setSollicitationViaDeMarque(Boolean sollicitationViaDeMarque) {
		this.sollicitationViaDeMarque = IdelModelTools.setAsCharacter(sollicitationViaDeMarque);
	}

	@Transient
	public boolean isLicenceEnAttente(){
		List<LicenceDiffusionTypeLicence> listLDTypeLic = getLicenceDiffusionTypeLs();
		for(LicenceDiffusionTypeLicence item : listLDTypeLic){
			if(item.getActive() 
					&& !item.getSigne() 
					&& !item.getRejete()){
				return true;
			}
		}
		return false;
	}
}
