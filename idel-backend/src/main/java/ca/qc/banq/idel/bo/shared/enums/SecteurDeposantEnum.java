package ca.qc.banq.idel.bo.shared.enums;

/**
 * This enum has a matching data from TYPE_REQUETE table. 
 * Any change of data in TYPE_REQUETE table requires a code change to this enum. 
 * 
 * @author Youssef.Soltani
 *
 */
public enum SecteurDeposantEnum {
	
	GOUV         (0L,"0","Gouvernemental"),
	PRIVE 		 (1L,"1","Prive - Parapublique");
	

	private Long id;
	private String code;
	private String description;
	
	SecteurDeposantEnum(Long id, String code, String description) {
		this.id = id;
        this.code = code;
        this.description = description;
    }
    
	public String getCode() {
		return code;
	}
	
	public String getDescription() {
		return description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
