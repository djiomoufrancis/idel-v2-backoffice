package ca.qc.banq.idel.bo.dm.publication;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import ca.qc.banq.idel.bo.dm.claim.Reclamation;
import ca.qc.banq.idel.bo.dm.editor.DestinataireAvis;
import ca.qc.banq.idel.bo.dm.request.Requete;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Depot generated by hbm2java
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper=false)
@Table(name = "DEPOT", schema = "")
public class Depot implements Serializable {
	
	@Id
	@SequenceGenerator(name = "SEQ_DEPOT", sequenceName = "SEQ_DEPOT", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEPOT")
	@Column(name = "ID_DEPOT", unique = true, nullable = false)
	@NotNull
	private Long idDepot;
	
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_REQUETE")
	private Requete requete;
	
	@Column(name = "FORMULAIRE_UTILISE", length = 200)
	@Length(max = 200)
	private String formulaireUtilise;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "depot", cascade = {CascadeType.ALL})
	private List<Avis> avis = new ArrayList<Avis>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "depot", cascade = {CascadeType.ALL})
	private List<Publication> publications = new ArrayList<Publication>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "depot", cascade = {CascadeType.ALL})
	private List<Reclamation> reclamations = new ArrayList<Reclamation>(0);

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "DESTINATAIREAVIS_DEPOT", schema = "", joinColumns = { @JoinColumn(name = "ID_DEPOT", nullable = true, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "ID_DESTINATAIREAVIS", nullable = true, updatable = false) })
	private List<DestinataireAvis> destinataireAvis = new ArrayList<DestinataireAvis>();
	
	@Column(name = "DATE_CREATION")
	private Date dateCreation;
	
	@Column(name = "DATE_MODIFICATION")
	private Date dateModification;
	
	@Column(name = "USAGER_MODIFICATION")
	private String usagerModification;
	
	@Column(name = "USAGER_CREATION")
	private String usagerCreation;
	
	@Column(name = "SIGNATAIRE_DEPOT")
	private String signataireDepot="";
	
	@Column(name = "NOTE_EXTERNE")
	private String noteExterne;
	
	@Column(name = "DATE_SOUMISSION", length = 7)
	private Date dateSoumission;
	
	@Column(name = "USAGER_SOUMISSION")
	private String usagerSoumission;
	
	@Column(name = "DATE_PREMIERE_SOUMISSION")
	private Date datePremiereSoumission;
	
	@Column(name = "PRIORITE")
	private String priorite;
	
	
	@Column(name = "REFERENCE_DSPACE")
	private String referenceDspace;
	
	@Column(name = "NUMERO_TRANSACTION_PNQ")
	private String numeroTransactionPNQ;
	
	
	
	/*
	@SuppressWarnings("unused")
	@Transient
	private Reclamation reclamation = new Reclamation();
	
	@SuppressWarnings("unused")
	@Transient
	private Publication publicationTravail = new Publication();
	*/
}
