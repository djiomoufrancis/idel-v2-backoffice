/**
 * 
 */
package ca.qc.banq.idel.bo.dm;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Base des entites persistantes
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public class AbstractEntity {

	@Version
	protected Integer version;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATE_CREATION")
	protected Date dateCreation;
	
	@Column(name = "USAGER_CREATION", length = 100)
	@Length(max = 100)
	protected String usagerCreation;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATE_MODIFICATION")
	protected Date dateModification;
	
	@Column(name = "USAGER_MODIFICATION", length = 100)
	@Length(max = 100)
	protected String usagerModification;

}
