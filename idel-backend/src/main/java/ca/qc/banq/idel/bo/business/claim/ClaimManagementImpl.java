/**
 * 
 */
package ca.qc.banq.idel.bo.business.claim;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.qc.banq.idel.bo.dm.claim.Reclamation;
import ca.qc.banq.idel.bo.repo.claim.IReclamationRepository;

/**
 * Implementation du Service client de gestion des reclamations
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Service
public class ClaimManagementImpl implements IClaimManagementService {

	@Autowired
	IReclamationRepository claimRepo;
	
	@Override
	public List<Reclamation> listEditorClaims(String editorUid) throws Exception {
		return claimRepo.findByEditor(editorUid);
	}

}
