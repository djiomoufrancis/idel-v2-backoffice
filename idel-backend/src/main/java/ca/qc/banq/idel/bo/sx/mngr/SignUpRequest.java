package ca.qc.banq.idel.bo.sx.mngr;

import javax.validation.constraints.NotBlank;

import ca.qc.banq.idel.bo.dm.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Modele d'une requete de souscription au service de Paiement
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@AllArgsConstructor 
@NoArgsConstructor
@ToString
public class SignUpRequest {
	
	@NotBlank
	private String login;
	
	@NotBlank
	private String password;
	
	@NotBlank
	private String name;
	
	private Address address;
	
	@NotBlank
	private String gender;
	
	
}
