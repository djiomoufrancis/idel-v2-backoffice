package ca.qc.banq.idel.bo.shared;

public class MsgKeys {
	
	public static String MSG_SUCCESS = "Msg.success";
	public static String MSG_SYSTEM_ERROR = "system.error";
	public static String MSG_RESOURCE_NOTFOUND = "resource.notfound";

}
