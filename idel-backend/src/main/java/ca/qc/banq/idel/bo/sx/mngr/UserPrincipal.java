package ca.qc.banq.idel.bo.sx.mngr;

import java.util.Collection;
import java.util.Objects;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ca.qc.banq.idel.bo.sx.dm.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Modele de l'utilisateur d'authentification
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@AllArgsConstructor 
@NoArgsConstructor
@ToString
@SuppressWarnings("serial")
public class UserPrincipal implements UserDetails {
	
    /**
     * Nom de conexion (Login)
     */
    private String username;

    /**
     * Mot de passe
     */
    @JsonIgnore
    private String password;

    /**
     * Collection de roles
     */
    private Collection<? extends GrantedAuthority> authorities;
    
    /**
     * Creation d'un Principal sur la base de l'objet utilisateur
     * @param user
     * @return
     */
    public static UserPrincipal create(User user) {
    	
    	return new UserPrincipal(user.getLogin(), user.getPassword(), user.getProfile().getListGrantedAuthorities() );
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.security.core.userdetails.UserDetails#isAccountNonExpired()
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.security.core.userdetails.UserDetails#isAccountNonLocked()
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.security.core.userdetails.UserDetails#isCredentialsNonExpired()
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.security.core.userdetails.UserDetails#isEnabled()
     */
    @Override
    public boolean isEnabled() {
        return true;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPrincipal that = (UserPrincipal) o;
        return Objects.equals(username, that.username);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {

        return Objects.hash(username);
    }

    
}
