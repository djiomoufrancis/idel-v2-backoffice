package ca.qc.banq.idel.bo.sx.mngr;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ca.qc.banq.idel.bo.shared.enums.Gender;
import ca.qc.banq.idel.bo.shared.exceptions.IdelException;
import ca.qc.banq.idel.bo.shared.models.ApiResponse;
import ca.qc.banq.idel.bo.sx.dm.Role;
import ca.qc.banq.idel.bo.sx.dm.User;
import ca.qc.banq.idel.bo.sx.dm.UserProfile;
import ca.qc.banq.idel.bo.sx.enums.UserStatus;
import ca.qc.banq.idel.bo.sx.repository.IRoleRepository;
import ca.qc.banq.idel.bo.sx.repository.IUserProfileRepository;
import ca.qc.banq.idel.bo.sx.repository.IUserRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * Service web de gestion des utilisateurs consommateurs des webservices
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Slf4j
@RestController
@RequestMapping("/api/sx")
@Api(description = "Service de gestion de la securite d'acces aux API de IDEL Back-Office")
public class SecurityController {
	
    /**
     * Inject User Repository
     */
    @Autowired
    IUserRepository userRepository;
    
    /**
     * Inject Profile Repository
     */
    @Autowired
    IUserProfileRepository profileRepository;

    /**
     * Inject Role Repository
     */
    @Autowired
    IRoleRepository roleRepository;

    /**
     * Inject Password Encoder
     */
    @Autowired
    PasswordEncoder passwordEncoder;
	
	/**
	 * Inject Authentication Manager
	 */
    @Autowired
    AuthenticationManager authenticationManager;

    /**
     * Inject Token Provider
     */
    @Autowired
    JwtTokenProvider tokenProvider;
    
    /**
     * LogIN
     * @param loginRequest requete de demande d'authentification
     * @return Token JWT
     */
    @PostMapping("/auth")
    @ApiOperation("Authentification d'une application cliente cherchant a acceder aux API de IDEL BackOffice")
    public ResponseEntity<?> authenticatePartner(@Valid @RequestBody LoginRequest loginRequest) {
    	log.trace("Authenticating " + loginRequest);
        Authentication authentication = authenticationManager.authenticate( new UsernamePasswordAuthenticationToken( loginRequest.getUsername(), loginRequest.getPassword() ) );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }
    
    /**
     * Signup Service
     * @param SignUpRequest Requete d'inscription
     * @return utilisateur
     */
    @PostMapping("/signup")
    @ApiOperation("Inscription d'une application cliente")
    public ResponseEntity<ApiResponse> signup(@Valid @RequestBody SignUpRequest request) throws Exception {
    	User usr = userRepository.findById(request.getLogin()).orElse(null);
    	if(usr != null) throw new IdelException("UserAlreadyExistsException");
    	User u = new User(null, request.getLogin(), passwordEncoder.encode(request.getPassword()), request.getName(), request.getAddress(), UserStatus.DISABLED, Gender.valueOf(request.getGender()), new Date(), null );
    	u = userRepository.save(u);
    	log.trace("User "+ u +" signed up successfully!");
    	return ResponseEntity.ok( new ApiResponse(true, HttpStatus.CREATED.value(), "success", u) );
    }
    
    /**
     * Create a new profile service
     * @param request profil utilisateur
     * @return Profil sauvegarde
     * @throws Exception Errors
     */
    @Secured("ROLE_saveProfile")
    @PostMapping("/saveProfile")
    @ApiOperation("Creation d'un profil")
    public ResponseEntity<ApiResponse> saveProfile(@Valid @RequestBody UserProfile request) throws Exception {
    	UserProfile p = profileRepository.save(request);
    	return ResponseEntity.ok(new ApiResponse(true, HttpStatus.CREATED.value(), "success", p) );
    }

    /**
     * Display the list of user profiles
     * @return Liste des profils utilisateurs
     * @throws Exception errors
     */
    @Secured("ROLE_listUserProfiles")
    @GetMapping("/listUserProfiles")
    @ApiOperation("Lister les profils des clients")
    public ResponseEntity<ApiResponse> listUserProfiles() throws Exception {
    	return ResponseEntity.ok( new ApiResponse(true, HttpStatus.OK.value(), "success", profileRepository.findAll()) );
    }

    /**
     * Display the list of roles
     * @return Liste des roles utilisateurs
     * @throws Exception errors
     */
    @Secured("ROLE_listRoles")
    @GetMapping("/listRoles")
    @ApiOperation("Lister tous les roles (services proteges)")
    public ResponseEntity<ApiResponse> listRoles() throws Exception {
    	return ResponseEntity.ok( new ApiResponse(true, HttpStatus.OK.value(), "success", roleRepository.findAll()) );
    }

    /**
     * Display the list of users
     * @return liste des utilisateurs
     * @throws Exception errors
     */
    @Secured("ROLE_listUsers")
    @GetMapping("/listUsers")
    @ApiOperation("Afficher la liste des applications clientes enregistrees")
    public ResponseEntity<ApiResponse> listUsers() throws Exception {
    	return ResponseEntity.ok( new ApiResponse(true, HttpStatus.OK.value(), "success", userRepository.findAll()) );
    }
    
    /**
     * Add roles to a profile
     * @param profileId Identifiant du profil
     * @param roleIDs Liste des roles
     * @return profil mis a jour
     * @throws Exception
     */
    @Secured("ROLE_addRoles")
    @PutMapping("/addRoles")
    @ApiOperation("Ajouter des roles a un profil")
    public ResponseEntity<ApiResponse> addRoles(@RequestParam("profileId")Long profileId, @RequestBody List<Long> roleIDs) throws Exception {
    	UserProfile p = profileRepository.findById(profileId).orElse(null);
    	if(p == null) throw new Exception("Invalid Profile Id");
    	if(roleIDs == null || roleIDs.isEmpty()) throw new Exception("Invalid List roles Ids");
    	for(Long id : roleIDs){
    		Role role = roleRepository.findById(id).orElse(null);
    		if(role != null && !p.getRoles().contains(role)) p.getRoles().add(role);
    	}
    	p = profileRepository.save(p);
    	return ResponseEntity.ok(new ApiResponse(true, HttpStatus.CREATED.value(), "success", p) );
    }


    /**
     * Find a user by it's username
     * @param id User id
     * @return User
     * @throws Exception errors
     */
    @GetMapping("/findUser/{id}")
    @ApiOperation("Rechercher une application cliente a partir de son identifiant")
    public ResponseEntity<ApiResponse> findUser(@PathVariable("id") String id) throws Exception {
    	User u = userRepository.findByLogin(id).stream().findFirst().orElse(null);
    	return ResponseEntity.ok(new ApiResponse(true, u != null ? HttpStatus.OK.value() : HttpStatus.NOT_FOUND.value(), u != null ? "User found" : "User not found", u) );
    }

    /**
     * Delete a User
     * @param id user Id
     * @return void
     * @throws Exception errors
     */
    @Secured("ROLE_removeUser")
    @DeleteMapping("/removeUser/{id}")
    @ApiOperation("Supprimer une application cliente")
    public ResponseEntity<ApiResponse> removeUser(@PathVariable("id") String id) throws Exception {
    	userRepository.deleteById(id);
    	return ResponseEntity.ok(new ApiResponse(true, HttpStatus.OK.value(), "success", "User removed") );
    }

    /**
     * Delete a profile
     * @param id profil id
     * @return void
     * @throws Exception errors
     */
    @Secured("ROLE_removeUserProfile")
    @DeleteMapping("/removeUserProfile/{id}")
    @ApiOperation("Supprimer un profil")
    public ResponseEntity<ApiResponse> removeUserProfile(@PathVariable("id") Long id) throws Exception {
    	profileRepository.deleteById(id);
    	return ResponseEntity.ok(new ApiResponse(true, HttpStatus.OK.value(), "success", "Profile Deleted") );
    }

}
