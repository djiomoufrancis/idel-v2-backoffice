/**
 * 
 */
package ca.qc.banq.idel.bo.cfg;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.ContextLoaderListener;

import com.google.common.collect.Iterables;

import ca.qc.banq.idel.bo.dm.Address;
import ca.qc.banq.idel.bo.shared.enums.Gender;
import ca.qc.banq.idel.bo.sx.dm.Role;
import ca.qc.banq.idel.bo.sx.dm.User;
import ca.qc.banq.idel.bo.sx.dm.UserProfile;
import ca.qc.banq.idel.bo.sx.enums.UserStatus;
import ca.qc.banq.idel.bo.sx.repository.IRoleRepository;
import ca.qc.banq.idel.bo.sx.repository.IUserProfileRepository;
import ca.qc.banq.idel.bo.sx.repository.IUserRepository;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * Service d'initialisation du module
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Slf4j
@Component
@WebListener
@Configurable
public class ListenerConfig extends ContextLoaderListener {

	@Autowired
	private IRoleRepository roleRepo;

	@Autowired
	private IUserProfileRepository profilRepo;

	@Autowired
	private IUserRepository userRepo;

    /**
     * Inject Password Encoder
     */
    @Autowired
    PasswordEncoder passwordEncoder;
    
    /**
     * Inject servletContext
     */
	@Autowired
	ServletContext servletContext;

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// Init Project
		initProject();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {}

	
	

	
	/**
	 * Scan all the project to retrieve roles
	 * @return
	 */
	private List<Role> scanProject() throws Exception {		
		List<Role> roles = new ArrayList<Role>();
		
		ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
	    provider.addIncludeFilter(new AnnotationTypeFilter(RestController.class));
	    Set<BeanDefinition> beans = provider.findCandidateComponents("ca.qc.banq.idel.bo");
	    for (BeanDefinition bean : beans) {
			Class<?> beanClass = Class.forName(bean.getBeanClassName());
			Method[] methods = beanClass.getMethods();
			for(Method m : methods) {
				Secured secured = m.getAnnotation(Secured.class);
				if(secured != null) {
					ApiOperation doc = m.getAnnotation(ApiOperation.class);
					roles.add( new Role(null, secured.value()[0], doc.value() ) ); // beanClass.getName().concat(".").concat(m.getName())
				}
			}
			
		}
		return roles;
	}
	
	/**
	 * Initialisation des donnees
	 */
	public void initProject() {
		iniRoles();
		iniRoot();
	}
	
	/**
	 * Initialisation des Roles
	 */
	private void iniRoles() {
		try{
			// Initialisation des roles
			List<Role> roles = scanProject();
			List<Role> all = roleRepo.findAll();
			roles.removeAll(all);
			for(Role r : roles) {
				try{roleRepo.save(r);}catch(Exception e1) {}
			}
			roles.clear();
		} catch(Exception e) {
			//e.printStackTrace();
			log.trace(e.getMessage(), e);
		}

	}
	
	/**
	 * Initialisation du profil et de l'utilisateur root
	 */
	private void iniRoot() {

		try{
			List<Role> all = roleRepo.findAll();
			
			// Init Root Profile
			UserProfile profil = Iterables.getOnlyElement(profilRepo.findByCode("SUPER"), null);
			if(profil == null) {
				profil = new UserProfile(null, "SUPER", "SUPER ADMIN", null );
				profil = profilRepo.save(profil);
				profil.setRoles(all);
				profil = profilRepo.save(profil);
			} else {
				if(profil.getRoles().size() < all.size()) {
					profil.setRoles( all );
					profil = profilRepo.save(profil);
				}
			}
			
			User root = userRepo.findByLogin("root").stream().findFirst().orElse(null);
			if(root == null) userRepo.save( new User(null, "root", passwordEncoder.encode("1234"), "Administrator", new Address("475 boulevard De Maisonneuve Est", "Montreal", "Quebec", "Canada", "CAN", "H2L 5C4", "francis.djiomou@banq.qc.ca", "http://intranet.banq.qc.ca/", "5148731101", "" ), UserStatus.ACTIVATED, Gender.MALE, new Date(), profil ) );

		} catch(Exception e) {
			log.trace(e.getMessage(), e);
		}

	}
	

}
