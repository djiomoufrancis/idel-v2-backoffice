/**
 * 
 */
package ca.qc.banq.idel.bo.rest.editor;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.qc.banq.idel.bo.business.editor.IEditorManagementService;
import ca.qc.banq.idel.bo.business.editor.ILoginExtranetService;
import ca.qc.banq.idel.bo.dm.editor.Editeur;
import ca.qc.banq.idel.bo.shared.Messaging;
import ca.qc.banq.idel.bo.shared.MsgKeys;
import ca.qc.banq.idel.bo.shared.Utils;
import ca.qc.banq.idel.bo.shared.exceptions.IdelException;
import ca.qc.banq.idel.bo.shared.models.ApiResponse;
import ca.qc.banq.idel.bo.sx.mngr.LoginRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * Webservice REST de gestion de l'authentification a l'extranet
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Slf4j
@RestController
@RequestMapping("/api/ext/login")
@Api(description = "Service de gestion de l'authentification a l'extranet")
public class LoginExtranetController {
	
	@Autowired
	private ILoginExtranetService loginService;

	@Autowired
	private IEditorManagementService editorService;

	@Autowired
	Messaging messaging;
	
	/**
	 * Recupere un editeur sur la base de l'uid retourne par l'authentification Shibboleth 
	 * @param uid UserID
	 * @param pwd password (should be empty)
	 * @return Editeur
	 */
	@Transactional
	@PostMapping("/signin")
	@ApiOperation("Recupere un editeur sur la base de l'uid retourne par l'authentification Shibboleth")
	public ResponseEntity<ApiResponse> signin(@Valid @RequestBody @ApiParam(value = "Identifiants de connexion de l'editeur (NB: password is not mandatory)") LoginRequest loginRequest) {	
		
		// Initialise response to return
		ApiResponse resp = null;
		
		try {
			
			// Find Editor
			Editeur e = loginService.signin(loginRequest.getUsername(), loginRequest.getPassword());
			e.setRepondants(editorService.listEditorRepondants(loginRequest.getUsername()));
			
			// Build Response
			resp = new ApiResponse(true, HttpStatus.OK.value(), messaging.getMessage(MsgKeys.MSG_SUCCESS), e);
	
		} catch(IdelException ie) {
			
			// Threat an Idel Exception
			resp = new ApiResponse(false, HttpStatus.BAD_REQUEST.value(), messaging.getMessage(ie.getMessage()), ie);
			
		} catch(Exception ex) {
			
			// Threat other exceptions type
			resp = new ApiResponse(false, HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage(), Utils.stackTraceToString(ex) );
			
			// Trace Log
			log.error(ex.getMessage(), ex);
		}
		
		// Return generic API response
		return ResponseEntity.ok(resp);
	}
	
	/**
	 * Inscription d'un editeur
	 * @param editor Editeur a inscrire
	 * @return Editeur
	 */
	@PostMapping("/signup")
	@ApiOperation("Inscription d'un nouvel editeur")
	public ResponseEntity<ApiResponse> signup( @Valid @RequestBody @ApiParam(value = "Editeur a sauvegarder") Editeur editor) {

		// Initialise response to return
		ApiResponse resp = null;
		
		try {
			
			// Call Editor Signup Service
			Editeur e = loginService.signup(editor);
			
			// Build response
			resp = new ApiResponse(true, HttpStatus.OK.value(), messaging.getMessage(MsgKeys.MSG_SUCCESS), e);
			
		} catch(IdelException ie) {
			
			// Threat an Idel Exception
			resp = new ApiResponse(false, HttpStatus.BAD_REQUEST.value(), messaging.getMessage(ie.getMessage()), ie);
			
		} catch(Exception ex) {
			
			// Threat other exceptions type
			resp = new ApiResponse(false, HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage(), Utils.stackTraceToString(ex) );
			
			// Trace Log
			log.error(ex.getMessage(), ex);
		}
		
		// Return generic API response
		return ResponseEntity.ok(resp);
	}


	
	@SuppressWarnings({ "unused" })
	private ResponseEntity<ApiResponse> sample() {

		// Initialise response to return
		ApiResponse resp = null;
		
		try {
			
			// Try something
			Editeur e = loginService.signin("", "");
			resp = new ApiResponse(true, HttpStatus.OK.value(), messaging.getMessage(MsgKeys.MSG_SUCCESS), e);
			
		} catch(IdelException ie) {
			
			// Threat an Idel Exception
			resp = new ApiResponse(false, HttpStatus.BAD_REQUEST.value(), messaging.getMessage(ie.getMessage()), ie);
			
		} catch(Exception ex) {
			
			// Threat other exceptions type
			resp = new ApiResponse(false, HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage(), Utils.stackTraceToString(ex) );
			
			// Trace Log
			log.error(ex.getMessage(), ex);
		}
		
		// Return generic API response
		return ResponseEntity.ok(resp);
	}
	
}
