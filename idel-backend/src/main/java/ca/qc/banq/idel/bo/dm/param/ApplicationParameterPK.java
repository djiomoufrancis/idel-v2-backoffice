package ca.qc.banq.idel.bo.dm.param;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Embeddable
@SuppressWarnings("serial")
@EqualsAndHashCode(of = {"applicationName", "parameterName"})
public class ApplicationParameterPK implements Serializable {

	@Column(name = "APPLICATION_NAME")
	private String applicationName;

	@Column(name = "PARAMETER_NAME")
	private String parameterName;
}
