/**
 * 
 */
package ca.qc.banq.idel.bo.sx.dm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Role representant les services proteges
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"code"})
@Table(name = "sx_role")
@SuppressWarnings("serial")
public class Role implements Serializable {

	@Id
	@Column(name = "id")
	@ApiModelProperty(readOnly=true)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ApiModelProperty(readOnly=true)
	@NotNull(message = "code.NotNull")
	@Column(name = "code", unique = true)
	private String code;

	@NotNull(message = "title.NotNull")
	@Column(name = "title")
	private String title;

}
