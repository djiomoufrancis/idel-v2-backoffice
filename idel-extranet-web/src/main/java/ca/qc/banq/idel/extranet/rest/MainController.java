/**
 * 
 */
package ca.qc.banq.idel.extranet.rest;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

/**
 * Webservices REST Principal
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Slf4j
@RestController
@RequestMapping(value = "/api", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class MainController {
	
	/**
	 * UID de l'utilisateur connecte
	 */
	private static String UID = null;
	
	/**
	 * Retourne le User ID de l'utilisateur authentifie via Shibboleth
	 * @return User ID
	 */
	@GetMapping(value = "/getUid", produces = "text/plain" )
	public ResponseEntity<String> getUid() {
		log.info("uid = ", UID);
		return ResponseEntity.ok(UID);
	}
	
	/**
	 * Deconnexion
	 */
	@PostMapping("/logout")
	public void logout() {
		UID = null;
	}
	
	public static void setUid(String uid) {
		UID = uid;
	}

	public static String getUID() {
		if(UID != null && UID.isEmpty()) UID = null;
		return UID;
	}

}
