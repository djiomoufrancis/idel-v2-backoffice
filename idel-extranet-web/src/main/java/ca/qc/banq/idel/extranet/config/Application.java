package ca.qc.banq.idel.extranet.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Application Launcher
 * @author <a href="mailto:francis.djiomou@banq.qc.ca.com">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-17
 */
@SpringBootApplication
@EnableAsync
@EnableAutoConfiguration
@EnableScheduling
@ComponentScan(basePackages = {"ca.qc.banq.idel.extranet"})
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(AppConfig.class, args);
	}

}
