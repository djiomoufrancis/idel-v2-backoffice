package ca.qc.banq.idel.extranet.config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import ca.qc.banq.idel.extranet.rest.MainController;
import lombok.extern.slf4j.Slf4j;

/**
 * Filtre permettant de recuperer l'identifiant de l'utilisateur connecte (via Shibboleth) et de le mettre dans la session 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca.com">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-17
 */
@Slf4j
@Order(1)
@Component
public class ShibbolethFilter implements Filter {
	
	/**
	 * Nom de l'attribut qui contient le user id
	 */
	protected static final String PARAM_KEY_UID = "uid";

	/**
	 * Do Filter
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		if(MainController.getUID() != null) {
			chain.doFilter(request, response);
			return;
		}
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String uid = getCodeUsager(httpRequest);
		log.info("httpRequest.getAttribute('uid') = " + uid);
		request.setAttribute(PARAM_KEY_UID, uid);
		MainController.setUid(uid);
		chain.doFilter(request, response);
	}

	/**
	 * Obtient le code d'usager.
	 * @param httpRequest
	 * @return String Le code d'usager.
	 */
	protected String getCodeUsager(HttpServletRequest httpRequest) {
		return (String) httpRequest.getAttribute(PARAM_KEY_UID);
	}

	@Override
	public void destroy() {}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {}
	
}