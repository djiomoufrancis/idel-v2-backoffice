package ca.qc.banq.idel.extranet.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Configuration Generale de l'application
 * @author <a href="mailto:francis.djiomou@banq.qc.ca.com">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-17
 */
@Configuration
@ComponentScan(basePackages = { "ca.qc.banq.idel.extranet" })
@Import({ SecurityConfig.class, WebMvcConfig.class })
public class AppConfig {

}
