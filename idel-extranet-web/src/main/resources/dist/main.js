(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/alert-dialog/alert-dialog.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/alert-dialog/alert-dialog.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div id=\"dlgAlert\" #dlgAlert class=\"modal fade\" [ngStyle]=\"{'display': myAlert.display ? 'block' : 'none'}\">\n    <div class=\"modal-dialog modal-sm\" role=\"document\">\n      <div class=\"modal-content\">\n        <div [ngClass]=\"'modal-header bg-' + getStyleClassSuffix()\">\n            <h4 class=\"modal-title\">{{myAlert.title}}</h4>\n            <button type=\"button\" class=\"close\" (click)=\"myAlert.hide()\" aria-label=\"Close\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n        </div>\n        <div class=\"modal-body\">\n            <p>{{myAlert.msg}}&hellip;</p>\n        </div>\n        <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myAlert.hide()\">Close</button>\n            <button type=\"button\" [ngClass]=\"'btn btn-' + getStyleClassSuffix()\" (click)=\"myAlert.hide()\">Ok</button>\n        </div>\n    </div>\n</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/claim/claim.component.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/claim/claim.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"card m-0 p-0\">\n  <!-- HEADER -->\n  <div class=\"p-2 bg-banq justify-content-center text-center\">\n      <strong style=\"font-size: 15px\">Consulter la liste des réclamations</strong>\n  </div>\n\n  <!-- BODY -->\n  <div class=\"card-body col-sm-12 m-0 animated fadeIn\">\n      <table id=\"tblClaims\" datatable class=\"table table-bordered table-sm table-responsive-sm table-striped row-border hover m-0 p-0\" scrollX=\"false\" scrollY=\"true\" style=\"width:100%\" maxHeight=\"800\">\n          <thead>\n              <tr>\n                  <th class=\"th-sm offspaces bg-banq text-center align-middle\">Numéro d'avis</th>\n                  <th class=\"th-sm offspaces bg-banq text-center align-middle\">Titre</th>\n                  <th class=\"th-sm offspaces bg-banq text-center align-middle\">Niveau</th>\n                  <th class=\"th-sm offspaces bg-banq text-center align-middle\">Date 1iere réclamatiom</th>\n                  <th class=\"th-sm offspaces bg-banq text-center align-middle\">Date derniere réclamatiom</th>\n                  <th class=\"th-sm offspaces bg-banq text-center align-middle\">Liste des avis</th>\n              </tr>\n          </thead>\n        </table>\n  </div>\n\n  <!-- FOOTER -->\n  <div class=\"card-footer\">\n    <div class=\" form-inline d-flex justify-content-start\">\n        <button type=\"reset\" class=\"btn btn-sm btn-dark\" (click)=\"back()\"><i class=\"fa fa-times\"></i>&nbsp;Retour</button>\n    </div>\n  </div>\n\n</div>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/depol/depol.component.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/depol/depol.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<app-alert-dialog [myAlert]=\"myAlert\"></app-alert-dialog>\n\n\n<div *ngIf=\"errs.length > 0\">\n  <ul>\n    <li *ngFor=\"let err of errs\">\n      <span style=\"font-weight:bold; color:red; font-size: 12px\"><i class=\"rounded bg-danger fa fa-warning\" style=\"padding: 2px\"></i>&nbsp;{{err}}</span>\n    </li>\n  </ul>\n  <br>\n</div>\n\n<div class=\"card m-0 p-0\">\n<div class=\"p-2 bg-banq justify-content-center text-center\">\n<strong style=\"font-size: 15px\">Formulaire de dépôt légal - Monographie</strong>\n\n</div>\n<div class=\"card-body col-sm-12 m-0 px-2 py-2 animated fadeIn\">\n\n\n    <div class=\"panel-body col-sm-12 p-2\">\n        <p><b>IMPORTANT:</b> assurez-vous de mettre à jour régulièrement la liste des répondants qui sont associés à votre Éditeur via votre dossier.</p>\n        <form #frmDepol=\"ngForm\" class=\"w100\">\n\n        <div class=\"w100 p-0 m-2\">\n          <table class=\"p-0 m-0 w100\">\n            <tr>\n              <td class=\"align-left\" style=\"width:50%; vertical-align: top\">\n                  <div class=\"input-group w100\">\n                      <div class=\"input-group-prepend\"><span class=\"name\">Dépôt fait par</span></div>&nbsp;\n                      <select class=\"form-control\">\n                          <option>item</option>\n                      </select>\n                  </div>\n              </td>\n              <td class=\"align-right\" style=\"width:50%\">\n                  <table class=\"table table-bordered table-responsive-md table-striped text-center w100 m-0 p-0\" scrollX=\"false\" scrollY=\"true\" maxHeight=\"200\">\n                      <thead>\n                          <tr>\n                              <th class=\"offspaces text-center\">\n                                  <button class=\"btn btn-sm btn-success\"><i class=\"fa fa-plus\"></i></button>\n                              </th>\n                              <th class=\"offspaces bg-banq text-center align-middle\">Courriels pour les communiqués</th>\n                              <th class=\"offspaces bg-banq text-center align-middle\">Suppr.</th>\n                          </tr>\n                      </thead>\n                      <tbody>\n                          <tr>\n                              <td class=\"offspaces align-middle text-center\">1</td>\n                              <td class=\"offspaces\">\n                                  <select class=\"form-control\" >\n                                      <option></option>\n                                  </select>\n                              </td>\n                              <td class=\"offspaces align-middle text-center\">\n                                  <button type=\"button\" rounded=\"true\" size=\"sm\" class=\"btn btn-sm btn-danger my-0\"><i class=\"fa fa-remove\"></i></button>\n                              </td>\n                          </tr>\n                      </tbody>\n                  </table>\n\n              </td>\n            </tr>\n          </table>\n        </div>\n\n        <div class=\"col-sm-12 p-0 m-2\">\n          <div class=\"input-group\">\n              <div class=\"input-group-prepend\"><span class=\"name\">Titre</span></div>&nbsp;\n              <textarea class=\"form-control\" rows=\"2\"></textarea>\n          </div>\n          <div style=\"padding-left: 140px;\"><small style=\"font-weight: bold; font-size: 9px; color:grey\">Le contenu ne peut pas excéder 2000 caractères.</small></div>\n      </div>\n      <div class=\"col-sm-12 p-0 m-2\">\n          <div class=\"input-group\">\n              <div class=\"input-group-prepend\"><span class=\"name\">Titre original</span></div>&nbsp;\n              <textarea class=\"form-control\" rows=\"2\"></textarea>\n          </div>\n          <div style=\"padding-left: 140px;\"><small style=\"font-weight: bold; font-size: 9px; color:grey\">Le contenu ne peut pas excéder 2000 caractères.</small></div>\n      </div>\n\n      <div class=\"col-sm-12 p-0 m-2\">\n          <div class=\"panel-body border p-0 m-0\">\n              <h6 class=\"text-on-pannel\"><strong>Auteur(s)</strong></h6>\n              <div class=\"w100\">\n                <table class=\"p-0 m-0 w100\">\n                  <tr>\n                    <td style=\"width: 19%\">\n                      &nbsp;\n                    </td>\n                    <td style=\"width: 81%\">\n\n                        <div class=\"w100 p-0 m-0 justify-content-end align-right\">\n                            <table class=\"table table-bordered table-responsive-md table-striped text-center w100 m-0 p-0\" scrollX=\"false\" scrollY=\"true\" maxHeight=\"200\">\n                                <thead>\n                                    <tr>\n                                        <th class=\"offspaces text-center\">\n                                            <button class=\"btn btn-sm btn-success\"><i class=\"fa fa-plus\"></i></button>\n                                        </th>\n                                        <th class=\"offspaces bg-banq text-center align-middle\">Nom</th>\n                                        <th class=\"offspaces bg-banq text-center align-middle\">Prénom</th>\n                                        <th class=\"offspaces bg-banq text-center align-middle\">Date de naissance</th>\n                                        <th class=\"offspaces bg-banq text-center align-middle\">Pseudonyme (s'il y a lieu)</th>\n                                        <th class=\"offspaces bg-banq text-center align-middle\">Suppr.</th>\n                                    </tr>\n                                </thead>\n                                <tbody>\n                                    <tr>\n                                        <td class=\"offspaces align-middle text-center\">1</td>\n                                        <td class=\"offspaces\">\n                                            <input type=\"text\" class=\"form-control\">\n                                        </td>\n                                        <td class=\"offspaces\">\n                                            <input type=\"text\" class=\"form-control\">\n                                        </td>\n                                        <td class=\"offspaces\">\n                                            <table class=\"p-0 m-0\" style=\"border:none\"><tr>\n                                                <td class=\"p-0 m-0\"><input matInput type=\"text\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer bg-white\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\" placeholder=\"jj\"/></td>\n                                                <td class=\"p-0 m-0\"><input matInput type=\"text\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer bg-white\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\" placeholder=\"mm\"/></td>\n                                                <td class=\"p-0 m-0\"><input matInput type=\"text\" pattern=\"^\\d{4}$\" class=\"form-control telephone4 divCentrer bg-white\" maxlength=\"4\" size=\"4\" app-numeric numericType=\"number\" placeholder=\"aaaa\"/></td>\n                                              </tr></table>\n                                        </td>\n                                        <td class=\"offspaces\">\n                                            <input type=\"text\" class=\"form-control\">\n                                        </td>\n                                        <td class=\"offspaces align-middle text-center\">\n                                            <button type=\"button\" rounded=\"true\" size=\"sm\" class=\"btn btn-sm btn-danger my-0\"><i class=\"fa fa-remove\"></i></button>\n                                        </td>\n                                    </tr>\n                                </tbody>\n                            </table>\n\n\n                        </div>\n\n                      </td>\n                    </tr>\n                </table>\n\n              </div>\n          </div>\n\n      </div>\n\n      <div class=\"col-sm-12 p-0 m-2\">\n          <div class=\"input-group\">\n              <div class=\"input-group-prepend\"><span class=\"name\">Éditeur<br><small class=\"hintText\">(si différent du déposant)</small></span></div>&nbsp;\n              <textarea class=\"form-control\" rows=\"2\"></textarea>\n          </div>\n          <div style=\"padding-left: 140px;\"><small style=\"font-weight: bold; font-size: 9px; color:grey\">Le contenu ne peut pas excéder 500 caractères.</small></div>\n      </div>\n\n      <div class=\"col-sm-12 p-0 m-2\">\n          <div class=\"input-group\">\n              <div class=\"input-group-prepend\"><span class=\"name\">Lieu de publication<br>(ville)<br><small class=\"hintText\">(si différent du déposant)</small></span></div>&nbsp;\n              <textarea class=\"form-control\" rows=\"2\"></textarea>\n          </div>\n          <div style=\"padding-left: 140px;\"><small style=\"font-weight: bold; font-size: 9px; color:grey\">Le contenu ne peut pas excéder 500 caractères.</small></div>\n      </div>\n\n\n      <div class=\"col-sm-12 p-0 m-2\">\n          <div class=\"panel-body border p-0 m-0\">\n              <h6 class=\"text-on-pannel\"><strong>Section Monographie</strong></h6>\n              <div class=\"w100\">\n                <table class=\"w100 p-0 m-0\">\n                  <tr>\n                    <td style=\"width: 19%\"></td>\n                    <td style=\"width: 81%\">\n\n                        <div class=\"w100 p-0 m-0\">\n                            <table class=\"table table-bordered table-responsive-md table-striped text-center w100 m-0 p-0\" scrollX=\"false\" scrollY=\"true\" maxHeight=\"200\">\n                                <thead>\n                                    <tr>\n                                        <th class=\"offspaces bg-banq text-center align-middle\">ISBN</th>\n                                        <th class=\"offspaces bg-banq text-center align-middle\">Prix de détail</th>\n                                        <th class=\"offspaces bg-banq text-center align-middle\">Tirage</th>\n                                    </tr>\n                                </thead>\n                                <tbody>\n                                    <tr>\n                                        <td class=\"offspaces\">\n                                            <input type=\"text\" class=\"form-control\">\n                                        </td>\n                                        <td class=\"offspaces\">\n                                            <input type=\"text\" class=\"form-control\">\n                                        </td>\n                                        <td class=\"offspaces\">\n                                            <input type=\"text\" class=\"form-control\">\n                                        </td>\n                                    </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </td>\n                  </tr>\n                </table>\n\n              </div>\n          </div>\n\n      </div>\n\n\n      <div class=\"col-sm-12 p-0 m-2\">\n          <div class=\"input-group\">\n              <div class=\"input-group-prepend\"><span class=\"name\">Année de<br>publication</span></div>&nbsp;\n              <input type=\"text\" class=\"form-control\" style=\"width: 80px\">\n          </div>\n      </div>\n\n      <div class=\"col-sm-12 p-0 m-2\">\n          <div class=\"input-group\">\n              <div class=\"input-group-prepend\"><span class=\"name\">Remarques</span></div>&nbsp;\n              <textarea class=\"form-control\" rows=\"2\"></textarea>\n          </div>\n          <div style=\"padding-left: 140px;\"><small style=\"font-weight: bold; font-size: 9px; color:grey\">Le contenu ne peut pas excéder 2000 caractères.</small></div>\n      </div>\n\n\n\n\n\n      </form>\n    </div>\n\n\n</div>\n<div class=\"card-footer\">\n<div class=\" form-inline d-flex justify-content-end px-5\">\n    <button type=\"reset\" class=\"btn btn-sm btn-danger\" ><i class=\"fa fa-ban\"></i>&nbsp;Effacer tout</button>&nbsp;\n    <button type=\"submit\" class=\"btn btn-sm btn-primary\" data-toggle=\"modal\" ><i class=\"fa fa-dot-circle-o\"></i>&nbsp;Soumettre</button>&nbsp;<!--  [disabled]=\"frmDepol.form.invalid && registerForm.invalid\" -->\n    <button type=\"reset\" class=\"btn btn-sm btn-dark\" (click)=\"back()\"><i class=\"fa fa-plus\"></i>&nbsp;Retour</button>\n</div>\n</div>\n\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/depol/list-depot/list-depot.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/depol/list-depot/list-depot.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"card m-0 p-0\">\n    <!-- HEADER -->\n    <div class=\"p-2 bg-banq justify-content-center text-center\">\n        <strong style=\"font-size: 15px\">Consulter la liste des dépôts</strong>\n    </div>\n\n    <!-- BODY -->\n    <div class=\"card-body col-sm-12 m-0 animated fadeIn\">\n        <table id=\"tblDepots\" datatable class=\"table table-bordered table-sm table-responsive-sm table-striped row-border hover m-0 p-0\" scrollX=\"false\" scrollY=\"true\" style=\"width:100%\" maxHeight=\"800\">\n            <thead>\n                <tr>\n                    <th class=\"th-sm offspaces bg-banq text-center align-middle\">Numéro de dépôt</th>\n                    <th class=\"th-sm offspaces bg-banq text-center align-middle\">Date du Signalement</th>\n                    <th class=\"th-sm offspaces bg-banq text-center align-middle\">Titre de la publication</th>\n                    <th class=\"th-sm offspaces bg-banq text-center align-middle\">Type de document</th>\n                    <th class=\"th-sm offspaces bg-banq text-center align-middle\">Statut du dépôt</th>\n                    <th class=\"th-sm offspaces bg-banq text-center align-middle\">1er envoi recu le</th>\n                    <th class=\"th-sm offspaces bg-banq text-center align-middle\">Date d'enreg ou de rejet</th>\n                    <th class=\"th-sm offspaces bg-banq text-center align-middle\">Liste des avis</th>\n                </tr>\n            </thead>\n          </table>\n    </div>\n\n    <!-- FOOTER -->\n    <div class=\"card-footer\">\n      <div class=\" form-inline d-flex justify-content-start\">\n          <button type=\"reset\" class=\"btn btn-sm btn-dark\" (click)=\"back()\"><i class=\"fa fa-times\"></i>&nbsp;Retour</button>\n      </div>\n    </div>\n\n  </div>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/dmd-isbn/dmd-isbn.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/dmd-isbn/dmd-isbn.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"card m-0 p-0\">\n  <!-- HEADER -->\n  <div class=\"p-2 bg-banq justify-content-center text-center\">\n      <strong style=\"font-size: 15px\">Demande de numéro(s) ISBN</strong>\n  </div>\n\n  <!-- BODY -->\n  <div class=\"card-body col-sm-12 m-0 animated fadeIn\">\n    <div>\n      <span>Assurez-vous que le document est éligible à une demande ISBN en consultant le <a href=\"#\">Guide sur l'utilisation de l'ISBN</a></span>\n    </div>\n\n    <div>\n      <h6><strong>1. Avez-vous déjà demandé une séquence ISBN?</strong> <span class=\"required\">*</span></h6>\n      <div class=\"input-group\">\n        <div class=\"input-group-prepend\"><input type=\"radio\">&nbsp;<span>Oui</span></div>\n        <div class=\"input-group-prepend\" style=\"margin-left: 15px;\"><input type=\"radio\">&nbsp;<span>Non</span></div>\n      </div>\n      <div>\n        <span>Si oui, quel était le dernier numéro attribué?</span>\n        <input type=\"text\">\n      </div>\n    </div>\n\n    <div class=\"my-5\">\n      <h6><strong>2. Catégorie d'éditeur</strong> <span class=\"required\">*</span></h6>\n      <div class=\"input-group\">\n        <div class=\"input-group-prepend\"><input type=\"radio\">&nbsp;<span>À compte d'auteur, sans nom commercial (auto-édition)</span></div>\n      </div>\n      <div class=\"input-group\">\n        <div class=\"input-group-prepend\"><input type=\"radio\" >&nbsp;<span>Raison sociale immatriculée. Veuillez inscrire votre numéro d'entreprise de Québec (NEQ) :</span></div>\n        <div class=\"input-group-prepend\" style=\"margin-left: 15px;\"><input type=\"text\"></div>\n      </div>\n      <div class=\"input-group\">\n        <div class=\"input-group-prepend\"><input type=\"radio\" >&nbsp;<span>Compagnie incorporée. Veuillez inscrire votre numéro d'entreprise du Québec (NEQ) :</span></div>\n        <div class=\"input-group-prepend\" style=\"margin-left: 15px;\"><input type=\"text\"></div>\n      </div>\n      <div class=\"input-group\">\n        <div class=\"input-group-prepend\"><input type=\"radio\" >&nbsp;<span>Autre (ex. : Municipalité, institution d'enseignement, groupe de recherche, CSSS, etc.) Précisez :</span></div>\n      </div>\n      <div class=\"w100\">\n        <input type=\"text\" class=\"form-control\">\n      </div>\n    </div>\n\n    <div class=\"my-5\">\n      <h6><strong>3. Dans quelle langue prévoyez-vous publier principalement?</strong></h6>\n      <div class=\"input-group\">\n        <div class=\"input-group-prepend\"><input type=\"radio\" >&nbsp;<span>Français</span></div>\n        <div class=\"input-group-prepend\" style=\"margin-left: 25px;\"><input type=\"radio\" >&nbsp;<span>Anglais</span></div>\n        <div class=\"input-group-prepend\" style=\"margin-left: 25px;\"><input type=\"radio\" >&nbsp;<span>Autre. Précisez :</span></div>\n        <div class=\"input-group-prepend\" style=\"margin-left:5px;\"><input type=\"text\"></div>\n      </div>\n    </div>\n\n    <div class=\"my-5\">\n      <h6><strong>4. Publication(s) à paraître.</strong></h6>\n      <h6>Veuillez décrire le ou les titres à paraître, en tenant compte des différents formats et langues</h6>\n      <div class=\"w100 p-0 m-0\">\n        <table class=\"table table-bordered table-responsive-md table-striped text-center m-0 p-0\" scrollX=\"false\" scrollY=\"true\" style=\"width:100%\" maxHeight=\"200\">\n          <thead>\n              <tr>\n                  <th class=\"text-center align-middle justify-content-center p-0 m-0\">\n                    <table class=\"w100 p-0 m-0\" style=\"border: 0 none;\">\n                      <tr>\n                        <td class=\"p-0 m-0\" style=\"border: 0 none;\"><button class=\"btn btn-sm bg-banq border\" style=\"width: 60px;\">T<i class=\"fa fa-plus\"></i></button></td></tr><tr>\n                        <td class=\"py-1 m-0\" style=\"border: 0 none;\"><button class=\"btn btn-sm bg-banq border\" style=\"width: 60px;\">F<i class=\"fa fa-plus\"></i></button></td>\n                      </tr>\n                    </table>\n                  </th>\n                  <th class=\"offspaces bg-banq text-center align-middle\">Titre<span class=\"required\">*</span></th>\n                  <th class=\"offspaces bg-banq text-center align-middle\">Est-ce un document de moins de cinq pages?<span class=\"required\">*</span></th>\n                  <th class=\"offspaces bg-banq text-center align-middle\">Format de la publication</th>\n                  <th class=\"offspaces bg-banq text-center align-middle\">Date de mise sous presse<br>(jj-mm-aaaa)</th>\n                  <th class=\"offspaces bg-banq text-center align-middle\">Date probable de publication<span class=\"required\">*</span><br>(jj-mm-aaaa)</th>\n                  <th class=\"offspaces bg-banq text-center align-middle\">Annuelle</th>\n                  <th class=\"offspaces bg-banq text-center align-middle\">Suppr.</th>\n              </tr>\n          </thead>\n          <tbody class=\"p-0 m-0\">\n              <td class=\"p-0 m-0\">1</td>\n              <td class=\"p-0 m-0\"><input type=\"text\" class=\"form-control\"></td>\n              <td class=\"p-0 m-0\">\n                  <div class=\"input-group\">\n                      <div class=\"input-group-prepend\"><input type=\"radio\" >&nbsp;<span>Oui</span></div>\n                      <div class=\"input-group-prepend\" style=\"margin-left: 10px;\"><input type=\"radio\">&nbsp;<span>Non</span></div>\n                  </div>\n              </td>\n              <td class=\"p-0 m-0\"><select class=\"form-control\"></select></td>\n              <td class=\"p-0 m-0\">\n                  <table class=\"p-0 m-0\" style=\"border:none\"><tr>\n                      <td class=\"p-0 m-0\"><input matInput type=\"text\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer bg-white\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\"/></td>\n                      <td class=\"p-0 m-0\"><input matInput type=\"text\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer bg-white\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\" /></td>\n                      <td class=\"p-0 m-0\"><input matInput type=\"text\" pattern=\"^\\d{4}$\" class=\"form-control telephone4 divCentrer bg-white\" maxlength=\"4\" size=\"4\" app-numeric numericType=\"number\" /></td>\n                    </tr></table>\n              </td>\n              <td class=\"p-0 m-0\">\n                  <table class=\"p-0 m-0\" style=\"border:none\"><tr>\n                      <td class=\"p-0 m-0\"><input matInput type=\"text\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer bg-white\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\"/></td>\n                      <td class=\"p-0 m-0\"><input matInput type=\"text\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer bg-white\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\" /></td>\n                      <td class=\"p-0 m-0\"><input matInput type=\"text\" pattern=\"^\\d{4}$\" class=\"form-control telephone4 divCentrer bg-white\" maxlength=\"4\" size=\"4\" app-numeric numericType=\"number\" /></td>\n                    </tr></table>\n              </td>\n              <td class=\"p-0 m-0\"><input type=\"checkbox\"></td>\n              <td class=\"p-0 m-0\">\n                  <button type=\"button\" class=\"btn btn-sm btn-danger m-0\"><i class=\"fa fa-remove\"></i></button>\n              </td>\n            </tbody>\n      </table>\n      </div>\n    </div>\n\n    <div class=\"my-5\">\n      <h6><strong>5. Combien de titres prévoyez-vous publier sous ce nom d'éditeur au cours des cinq prochaines années?</strong></h6>\n      <div class=\"input-group\">\n        <div class=\"input-group-prepend\"><input type=\"radio\">&nbsp;<span>10 et moins</span></div>\n        <div class=\"input-group-prepend\" style=\"margin-left: 25px;\"><input type=\"radio\">&nbsp;<span>11 à 100</span></div>\n        <div class=\"input-group-prepend\" style=\"margin-left: 25px;\"><input type=\"radio\">&nbsp;<span>Plus de 100</span></div>\n      </div>\n    </div>\n\n    <div class=\"my-5\">\n      <h6><strong>6. À quel(s) domaine(s) vos publications sont-elles rattachées (ex. : environnement, généalogie, psychologie, éducation, sociologie, littérature, etc.)?</strong></h6>\n      <div class=\"input-group w100\">\n        <input type=\"text\" class=\"form-control\">\n      </div>\n    </div>\n\n    <div class=\"my-5\">\n      <h6><strong>7a. Responsable désigné pour le dépôt de vos publications :</strong></h6>\n      <table class=\"w100 p-0 m-0 \">\n        <tr>\n          <td style=\"width: 15%;\">\n            <div class=\"input-group\">\n              <div class=\"input-group-prepend\"><input type=\"radio\">&nbsp;<span>M.</span></div>\n              <div class=\"input-group-prepend\" style=\"margin-left: 15px;\"><input type=\"radio\">&nbsp;<span>Mme</span></div>\n            </div>\n          </td>\n          <td style=\"width: 85%;\">\n            <table class=\"table table-bordered table-responsive-md table-striped text-center m-0 p-0 w100\" scrollX=\"false\" scrollY=\"true\" maxHeight=\"200\">\n              <thead>\n                  <tr>\n                      <th class=\"offspaces bg-banq text-center align-middle\">Nom</th>\n                      <th class=\"offspaces bg-banq text-center align-middle\">Prénom</th>\n                      <th class=\"offspaces bg-banq text-center align-middle\">Courriel</th>\n                      <th class=\"offspaces bg-banq text-center align-middle\">Téléphone</th>\n                      <th class=\"offspaces bg-banq text-center align-middle\">Poste</th>\n                  </tr>\n              </thead>\n              <tbody>\n                  <td class=\"p-0 m-0\"><input type=\"text\" class=\"form-control\"></td>\n                  <td class=\"p-0 m-0\"><input type=\"text\" class=\"form-control\"></td>\n                  <td class=\"p-0 m-0\"><input type=\"text\" class=\"form-control\"></td>\n                  <td class=\"p-0 m-0\">\n                      <table class=\"p-0 m-0\" style=\"border:none\"><tr>\n                          <td class=\"p-0 m-0\"><input matInput type=\"text\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer bg-white\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\"/></td>\n                          <td class=\"p-0 m-0\"><input matInput type=\"text\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer bg-white\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\" /></td>\n                          <td class=\"p-0 m-0\"><input matInput type=\"text\" pattern=\"^\\d{4}$\" class=\"form-control telephone4 divCentrer bg-white\" maxlength=\"4\" size=\"4\" app-numeric numericType=\"number\" /></td>\n                        </tr></table>\n                  </td>\n                  <td class=\"p-0 m-0\"><input type=\"text\" class=\"form-control\" style=\"width: 60px;\"></td>\n                </tbody>\n              </table>\n\n          </td>\n        </tr>\n      </table>\n    </div>\n\n    <div class=\"my-5\">\n      <h6><strong>7b. Responsable désigné pour vos ISBN :<input type=\"checkbox\" style=\"margin-left: 25px;\">&nbsp;<span>Idem à 7a</span></strong></h6>\n      <table class=\"w100 p-0 m-0 \">\n        <tr>\n          <td style=\"width: 15%;\">\n            <div class=\"input-group\">\n              <div class=\"input-group-prepend\"><input type=\"radio\">&nbsp;<span>M.</span></div>\n              <div class=\"input-group-prepend\" style=\"margin-left: 15px;\"><input type=\"radio\">&nbsp;<span>Mme</span></div>\n            </div>\n          </td>\n          <td style=\"width: 85%;\">\n            <table class=\"table table-bordered table-responsive-md table-striped text-center m-0 p-0 w100\" scrollX=\"false\" scrollY=\"true\" maxHeight=\"200\">\n              <thead>\n                  <tr>\n                      <th class=\"offspaces bg-banq text-center align-middle\">Nom</th>\n                      <th class=\"offspaces bg-banq text-center align-middle\">Prénom</th>\n                      <th class=\"offspaces bg-banq text-center align-middle\">Courriel</th>\n                      <th class=\"offspaces bg-banq text-center align-middle\">Téléphone</th>\n                      <th class=\"offspaces bg-banq text-center align-middle\">Poste</th>\n                  </tr>\n              </thead>\n              <tbody>\n                <td class=\"p-0 m-0\"><input type=\"text\" class=\"form-control\"></td>\n                <td class=\"p-0 m-0\"><input type=\"text\" class=\"form-control\"></td>\n                <td class=\"p-0 m-0\"><input type=\"text\" class=\"form-control\"></td>\n                <td class=\"p-0 m-0\">\n                    <table class=\"p-0 m-0\" style=\"border:none\"><tr>\n                        <td class=\"p-0 m-0\"><input matInput type=\"text\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer bg-white\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\"/></td>\n                        <td class=\"p-0 m-0\"><input matInput type=\"text\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer bg-white\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\" /></td>\n                        <td class=\"p-0 m-0\"><input matInput type=\"text\" pattern=\"^\\d{4}$\" class=\"form-control telephone4 divCentrer bg-white\" maxlength=\"4\" size=\"4\" app-numeric numericType=\"number\" /></td>\n                      </tr></table>\n                </td>\n                <td class=\"p-0 m-0\"><input type=\"text\" class=\"form-control\" style=\"width: 60px;\"></td>\n              </tbody>\n            </table>\n\n          </td>\n        </tr>\n      </table>\n    </div>\n\n    <div>\n      <h6><strong>8. Commentaires</strong></h6>\n      <div class=\"input-group w100\">\n        <input type=\"text\" class=\"form-control\">\n      </div>\n      <small style=\"font-weight: bold; font-size: 9px; color:grey;\">Le contenu ne peut pas excéder 500 caractères.</small>\n    </div>\n\n  </div>\n\n  <!-- FOOTER -->\n  <div class=\"card-footer\">\n    <div class=\" form-inline d-flex justify-content-end\">\n        <button type=\"button\" class=\"btn btn-sm btn-dark\" (click)=\"back()\"><i class=\"fa fa-arrow-left\"></i>&nbsp;Retour</button>&nbsp;\n        <button type=\"button\" class=\"btn btn-sm btn-danger\" (click)=\"back()\"><i class=\"fa fa-times\"></i>&nbsp;Effacer tout</button>&nbsp;\n        <button type=\"button\" class=\"btn btn-sm btn-primary\" (click)=\"back()\"><i class=\"fa fa-check\"></i>&nbsp;Soumettre</button>\n    </div>\n  </div>\n\n</div>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/dossier/dossier.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/dossier/dossier.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n <app-alert-dialog [myAlert]=\"myAlert\"></app-alert-dialog>\n\n\n<div *ngIf=\"errs.length > 0\">\n    <ul>\n      <li *ngFor=\"let err of errs\">\n          <span style=\"font-weight:bold; color:red; font-size: 12px\"><i class=\"rounded bg-danger fa fa-warning\" style=\"padding: 2px\"></i>&nbsp;{{err}}</span>\n      </li>\n    </ul>\n    <br>\n  </div>\n\n<div class=\"card m-0 p-0\">\n    <div class=\"p-2 bg-banq justify-content-center text-center\">\n      <strong style=\"font-size: 15px\">Vos coordonnées</strong>\n\n    </div>\n    <div class=\"card-body col-sm-12 m-0 px-2 py-2 animated fadeIn\">\n\n\n            <div class=\"panel-body col-md-12 p-2\">\n\n                <div class=\"panel panel-info col-sm-12 m-0 p-0\">\n                    <div class=\"panel-body border col-sm-12\">\n                        <h6 class=\"text-on-pannel\"><strong>Identification du / des répondant(s)</strong></h6>\n                        <div class=\"row col-sm-12 p-2\">\n                            <form [formGroup]=\"registerForm\" style=\"width:100%;margin:0px;padding:0px;\">\n                            <div id=\"table\" class=\"table-editable col-sm-12 m-0 p-0\" formArrayName=\"persons\">\n                                <table class=\"table table-bordered table-responsive-md table-striped text-center m-0 p-0\" scrollX=\"false\" scrollY=\"true\" style=\"width:100%\" maxHeight=\"200\">\n                                    <thead>\n                                        <tr>\n                                            <th class=\"offspaces text-center\">\n                                                <button class=\"btn btn-sm btn-success\" (click)=\"add()\"><i class=\"fa fa-plus\" tooltip=\"Cliquez ici pour ajouter un nouveau repondant\"></i></button>\n                                            </th>\n                                            <th class=\"offspaces bg-banq text-center align-middle\">Titre<span class=\"required\">*</span></th>\n                                            <th class=\"offspaces bg-banq text-center align-middle\">Nom<span class=\"required\">*</span></th>\n                                            <th class=\"offspaces bg-banq text-center align-middle\">Prénom<span class=\"required\">*</span></th>\n                                            <th class=\"offspaces bg-banq text-center align-middle\">Courriel<span class=\"required\">*</span></th>\n                                            <th class=\"offspaces bg-banq text-center align-middle\">Téléphone</th>\n                                            <th class=\"offspaces bg-banq text-center align-middle\">Poste</th>\n                                            <th class=\"offspaces bg-banq text-center align-middle\">Suppr.</th>\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                        <tr *ngFor=\"let person of persons.controls; let id = index\" [formGroupName]=\"id\">\n                                            <td class=\"offspaces align-middle text-center\">\n                                                {{id+1}}\n                                            </td>\n                                            <td class=\"offspaces\">\n                                                <select required formControlName=\"title\" class=\"form-control\"  > <!-- [class.is-invalid]=\"person.get('title').value === ''\" -->\n                                              <option value=\"M.\">M.</option>\n                                              <option value=\"Mme\">Mme</option>\n                                          </select>\n                                            </td>\n                                            <td class=\"offspaces\">\n                                                <input type=\"text\" required formControlName=\"firstname\" class=\"form-control\">\n                                            </td>\n                                            <td class=\"offspaces\">\n                                                <input type=\"text\" required formControlName=\"lastname\" class=\"form-control\">\n                                            </td>\n                                            <td class=\"offspaces\">\n                                                <input type=\"text\" required formControlName=\"email\" class=\"form-control\" >\n                                            </td>\n                                            <td class=\"offspaces\">\n                                                <table class=\"p-0 m-0\" style=\"border:none\"><tr>\n                                                    <td class=\"p-0 m-0\"><input matInput type=\"text\" required formControlName=\"phoneArea\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer bg-white\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\"/></td>\n                                                    <td class=\"p-0 m-0\"><input matInput type=\"text\" required formControlName=\"phone3\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer bg-white\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\" /></td>\n                                                    <td class=\"p-0 m-0 align-middle bg-white\">-</td>\n                                                    <td class=\"p-0 m-0\"><input matInput type=\"text\" required formControlName=\"phone4\" pattern=\"^\\d{4}$\" class=\"form-control telephone4 divCentrer bg-white\" maxlength=\"4\" size=\"4\" app-numeric numericType=\"number\" /></td>\n                                                  </tr></table>\n                                            </td>\n                                            <td class=\"offspaces\">\n                                                <input type=\"text\" formControlName=\"poste\" class=\"form-control\">\n                                            </td>\n                                            <td class=\"offspaces\">\n                                                <button type=\"button\" *ngIf=\"id > 0\" rounded=\"true\" size=\"sm\" class=\"btn btn-danger my-0\" (click)=\"remove(id)\" tooltip=\"Cliquez ici pour supprimer la ligne\"><i class=\"fa fa-remove\"></i></button>\n                                            </td>\n                                        </tr>\n                                    </tbody>\n                                </table>\n                            </div>\n                          </form>\n                        </div>\n                    </div>\n                </div>\n\n                <form #frmSignup=\"ngForm\">\n                <div class=\"panel panel-info py-3\">\n                    <div class=\"panel-body border\">\n                        <h6 class=\"text-on-pannel\"><strong>Information sur l'éditeur</strong></h6>\n                        <div class=\"row p-2\">\n                            <div class=\"col-sm-12\">\n                                <div class=\"input-group\">\n                                    <div class=\"input-group-prepend\"><span class=\"name\">Editeur<span class=\"required\">*</span><br><small class=\"hintText\">(Nom de l'entreprise, de <br>l'organisme ou de la personne)</small> </span>\n                                    </div>&nbsp;\n                                    <textarea required #txtInfos=\"ngModel\" [(ngModel)]=\"editor.nom\" class=\"form-control\" name=\"txtInfos\" rows=\"2\"></textarea>\n                                </div>\n                                <small style=\"font-weight: bold; font-size: 9px; color:grey; padding-left: 140px\">Le contenu ne peut pas excéder 500 caractères.</small>\n                            </div>\n\n                            <div class=\"col-sm-12 py-2 m-0\">\n                              <table class=\"w100 p-0 m-0\">\n                                <tr>\n                                  <td style=\"width: 50%;\">\n                                      <div class=\"input-group\">\n                                          <div class=\"input-group-prepend\"><span class=\"name\">Adresse<span class=\"required\">*</span><br><small class=\"hintText\">(Numéro d'immeuble / Rue)</small></span>\n                                          </div>&nbsp;\n                                          <input type=\"text\" name=\"txtAdrPos1\" required #txtAdrPos1=\"ngModel\" class=\"form-control\" [(ngModel)]=\"editor.adresse1\" placeholder=\"\">\n                                      </div>\n                                  </td>\n                                  <td>&nbsp;</td>\n                                  <td style=\"width: 50%;\">\n                                      <div class=\"input-group\">\n                                          <div class=\"input-group-prepend\"><span class=\"name\">Adresse<br><small class=\"hintText\">Appart, Bureau ou Case postale</small></span></div>&nbsp;\n                                          <input type=\"text\" name=\"txtAdrPos2\" #txtAdrPos2=\"ngModel\" class=\"form-control\" [(ngModel)]=\"editor.adresse2\" placeholder=\"\">\n                                      </div>\n                                  </td>\n                                </tr>\n                              </table>\n                            </div>\n\n                          <div class=\"col-sm-12 py-2 m-0\">\n                            <div class=\"row\">\n                                <div class=\"col-sm-4\">\n                                    <div class=\"input-group\">\n                                        <div class=\"input-group-prepend\"><span class=\"name\">Ville<span class=\"required\">*</span></span>\n                                        </div>&nbsp;\n                                        <input type=\"text\" name=\"cbxCity1\"  required #cbxCity1=\"ngModel\" class=\"form-control\" [(ngModel)]=\"editor.ville\" placeholder=\"\" maxlength=\"30\">\n                                    </div>\n                                </div>\n                                <div class=\"col-sm-5\">\n                                    <div class=\"input-group\">\n                                        <div class=\"input-group-prepend\"><span class=\"name\">Province/Territoire<span class=\"required\">*</span></span>\n                                        </div>&nbsp;\n                                        <select required #cbxState1=\"ngModel\" class=\"form-control\" name=\"cbxState1\" [(ngModel)]=\"editor.provinceTerritoire\">\n                                            <option *ngFor=\"let item of states\" value=\"{{item.name}}\" >{{item.name}}</option>\n                                        </select>\n                                    </div>\n                                </div>\n                                <div class=\"col-sm-3\">\n                                    <div class=\"input-group\">\n                                        <div class=\"input-group-prepend\"><span class=\"small-name\">Pays<span class=\"required\">*</span></span></div>&nbsp;\n                                        <input type=\"text\" name=\"cbxCountry1\" required #cbxCountry1=\"ngModel\" class=\"form-control\" [(ngModel)]=\"editor.pays\" placeholder=\"\" maxlength=\"7\">\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"col-sm-12 py-2 m-0\">\n                          <div class=\"input-group\">\n                              <div class=\"input-group-prepend\"><span class=\"name\">Code Postal<span class=\"required\">*</span></span></div>&nbsp;\n                              <div style=\"width: 100px\">\n                              <input type=\"text\" name=\"txtCPos1\" required #txtCPos1=\"ngModel\" class=\"form-control\" [(ngModel)]=\"editor.codePostal\" placeholder=\"\" maxlength=\"7\"></div>\n                          </div>\n                      </div>\n\n\n                      <div class=\"col-sm-12 px-3 my-2\">\n                        <div class=\"row col-sm-12\">\n                          <table>\n                            <tr>\n                              <td>\n                                  <div class=\"row input-group m-0 p-0\">\n                                      <div class=\"input-group-prepend\"><span class=\"name\">Téléphone<span class=\"required\">*</span></span></div>&nbsp;\n                                      <div class=\"p-0 m-0 border\">\n                                          <table class=\"p-0 m-0\" style=\"border:none\"><tr>\n                                              <td class=\"p-0 m-0\"><input matInput type=\"text\" required #txtPhoneArea1=\"ngModel\" name=\"txtPhoneArea1\" [(ngModel)]=\"phone.prefx\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\" /></td>\n                                              <td class=\"p-0 m-0\"><input matInput type=\"text\" required #txtPhone31=\"ngModel\" name=\"txtPhone31\" [(ngModel)]=\"phone.sufx\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\" /></td>\n                                              <td class=\"p-0 m-0\">-</td>\n                                              <td class=\"p-0 m-0\"><input matInput type=\"text\" required #txtPhone41=\"ngModel\" name=\"txtPhone41\" [(ngModel)]=\"phone.racine\" pattern=\"^\\d{4}$\"  class=\"form-control telephone4 divCentrer\" maxlength=\"4\" size=\"4\" app-numeric numericType=\"number\" /></td>\n                                            </tr></table>\n                                      </div>\n                                  </div>\n                              </td>\n\n                              <td>\n                                  <div class=\"input-group p-0 m-0 align-middle\">\n                                      <div class=\"input-group-prepend p-0 m-0\">\n                                        <div class=\"input-group-prepend\"><span>Confidentiel<span class=\"required\">*</span></span></div>&nbsp;\n                                        <div class=\"px-4 m-0 align-content-end\">\n                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"chkConf1\" [(ngModel)]=\"editor.confidentialPhone\" required #chkConf1=\"ngModel\">\n                                        </div>\n                                      </div>\n                                  </div>\n                              </td>\n\n                              <td>\n                                  <div class=\"input-group\">\n                                      <div class=\"input-group-prepend\"><span class=\"small-name\">Poste</span></div>&nbsp;\n                                      <input type=\"text\" name=\"txtPoste1\" #txtPoste1=\"ngModel\" class=\"form-control\" [(ngModel)]=\"editor.poste\" placeholder=\"\" maxlength=\"10\">\n                                  </div>\n                              </td>\n                            </tr>\n                          </table>\n\n                        </div>\n                    </div>\n                    <div class=\"col-sm-12 px-3 my-2\">\n                        <div class=\"input-group\">\n                            <div class=\"input-group-prepend\"><span class=\"name\">Télécopieur</span></div>&nbsp;\n                            <div class=\"p-0 m-0 border\">\n                                <table class=\"p-0 m-0\" style=\"border:none\"><tr>\n                                    <td class=\"p-0 m-0\"><input matInput type=\"text\" #txtTelecopieurArea1=\"ngModel\" name=\"txtTelecopieurArea1\" [(ngModel)]=\"telecop.racine\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\"/></td>\n                                    <td class=\"p-0 m-0\"><input matInput type=\"text\" #txtTelecopieur3=\"ngModel\" name=\"txtTelecopieur3\" [(ngModel)]=\"telecop.sufx\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\" /></td>\n                                    <td class=\"p-0 m-0\">-</td>\n                                    <td class=\"p-0 m-0\"><input matInput type=\"text\" #txtTelecopieur4=\"ngModel\" name=\"txtTelecopieur4\" [(ngModel)]=\"telecop.racine\" pattern=\"^\\d{4}$\"  class=\"form-control telephone4 divCentrer\" maxlength=\"4\" size=\"4\" app-numeric numericType=\"number\" /></td>\n                                  </tr></table>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-sm-12 px-3 my-2\">\n                      <div class=\"input-group\">\n                          <div class=\"input-group-prepend\"><span class=\"name\">Courriel</span></div>&nbsp;\n                          <input type=\"text\" name=\"txtCourriel1\" #txtCourriel1=\"ngModel\" class=\"form-control\" [(ngModel)]=\"editor.courriel\" placeholder=\"\" maxlength=\"50\">\n                      </div>\n                  </div>\n                  <div class=\"col-sm-12 px-3 my-2\">\n                      <div class=\"input-group\">\n                          <div class=\"input-group-prepend\"><span class=\"name\">Site Internet</span></div>&nbsp;\n                          <input type=\"text\" name=\"txtSite1\" #txtSite1=\"ngModel\" class=\"form-control\" [(ngModel)]=\"editor.siteInternet\" placeholder=\"\" maxlength=\"50\">\n                      </div>\n                  </div>\n                  <div class=\"col-sm-12 py-2 m-0\">\n                    <div class=\"input-group\">\n                        <div class=\"input-group-prepend\"><span class=\"name\">Commentaires</span></div>&nbsp;\n                        <textarea class=\"form-control\" rows=\"2\" name=\"txtComments1\" #txtComments1=\"ngModel\" [(ngModel)]=\"editor.commentaires\"></textarea>\n                    </div>\n                    <div style=\"padding-left: 140px;\"><small style=\"font-weight: bold; font-size: 9px; color:grey\">Le contenu ne peut pas excéder 2000 caractères</small></div>\n                </div>\n                <div class=\"col-sm-12 px-10 m-0\"><small style=\"font-weight: bold; font-size: 9px\">* Si votre demande vise à obtenir des numéros ISBN, vos coordonnées paraîtront au Bottin des éditeurs francophones canadiens, sauf le numéro de téléphone si celui-ci est confidentiel.</small></div>\n\n\n\n\n                        </div>\n                    </div>\n                </div>\n\n              </form>\n            </div>\n\n\n    </div>\n    <div class=\"card-footer\">\n        <div class=\" form-inline d-flex justify-content-end px-5\">\n            <button type=\"reset\" class=\"btn btn-sm btn-danger\" (click)=\"reset()\"><i class=\"fa fa-ban\"></i>&nbsp;Effacer tout</button>&nbsp;\n            <button type=\"submit\" class=\"btn btn-sm btn-primary\" data-toggle=\"modal\" (click)=\"submit()\"><i class=\"fa fa-dot-circle-o\"></i>&nbsp;Soumettre une demande de modification</button>&nbsp;<!--  [disabled]=\"frmSignup.form.invalid && registerForm.invalid\" -->\n            <button type=\"reset\" class=\"btn btn-sm btn-dark\" (click)=\"back()\"><i class=\"fa fa-plus\"></i>&nbsp;Quitter</button>\n        </div>\n    </div>\n\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/error/404.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/error/404.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app flex-row align-items-center\">\n  <div class=\"container\">\n    <div class=\"row justify-content-center\">\n      <div class=\"col-md-6\">\n        <div class=\"clearfix\">\n          <h1 class=\"float-left display-3 mr-4\">404</h1>\n          <h4 class=\"pt-3\">Oops! You're lost.</h4>\n          <p class=\"text-muted\">The page you are looking for was not found.</p>\n        </div>\n        <div class=\"input-prepend input-group\">\n          <div class=\"input-group-prepend\">\n            <span class=\"input-group-text\"><i class=\"fa fa-search\"></i></span>\n          </div>\n          <input id=\"prependedInput\" class=\"form-control\" size=\"16\" type=\"text\" placeholder=\"What are you looking for?\">\n          <span class=\"input-group-append\">\n            <button class=\"btn btn-info\" type=\"button\">Search</button>\n          </span>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/error/500.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/error/500.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app flex-row align-items-center\">\n  <div class=\"container\">\n    <div class=\"row justify-content-center\">\n      <div class=\"col-md-6\">\n        <div class=\"clearfix\">\n          <h1 class=\"float-left display-3 mr-4\">500</h1>\n          <h4 class=\"pt-3\">Houston, we have a problem!</h4>\n          <p class=\"text-muted\">The page you are looking for is temporarily unavailable.</p>\n        </div>\n        <div class=\"input-prepend input-group\">\n          <div class=\"input-group-prepend\">\n            <span class=\"input-group-text\"><i class=\"fa fa-search\"></i></span>\n          </div>\n          <input id=\"prependedInput\" class=\"form-control\" size=\"16\" type=\"text\" placeholder=\"What are you looking for?\">\n          <span class=\"input-group-append\">\n            <button class=\"btn btn-info\" type=\"button\">Search</button>\n          </span>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/home/dashboard/dashboard.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/home/dashboard/dashboard.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<!--BIG TITLE-->\n<div class=\"big-title w100\">Portail privé - parapublic</div>\n\n<!--MESSAGES-->\n<div class=\"p-10 w100\"><p style='color:red'><b><i>Vous avez présentement des réclamations en cours.</i></b></p></div>\n<div class=\"p-3 w100\" *ngIf=\"!isConnected()\"> <a [routerLink]=\"['/register']\" routerLinkActive=\"router-link-active\" >Inscrivez-vous ici</a> </div>\n\n<!--SMALL TITLE-->\n<div class=\"small-title w100\">Quoi de neuf</div>\n<div class=\"col-sm-12 home-bande border\">\n  <table class=\"p-0 m-0 w100\">\n      <tr>\n        <td class=\"justify-content-center\" style=\"width:75%\"><span style=\"color:#010302;font-size: 170%;font-weight: bold\">50 ans à rassembler la mémoire du Québec par le dépôt légal... Et c'est grâce à vous que c'est possible. Merci!</span>\n        </td>\n        <td><img src=\"assets/img/5_Guides-Form.jpg\" class=\"img-fluid\" style=\"height:150px\"></td>\n  </tr></table>\n</div>\n\n<!--VERTICAL SPACER-->\n<div class=\"w100\" style=\"height: 30px\"></div>\n\n<div class=\"col-sm-12 p-0 m-0 w100\">\n<table class=\"w100 p-0 m-0\">\n  <tr>\n    <td style=\"width:47%\">\n        <div class=\"small-title w100\">Votre dossier</div>\n        <div class=\"w100\">\n          <a [routerLink]=\"['/home/dossier']\" routerLinkActive=\"router-link-active\">Voir ou modifier vos coordonnées</a><br>\n          <a [routerLink]=\"['/home/licences']\" routerLinkActive=\"router-link-active\" >Licences d'autorisation accordées à BAnQ</a><br>\n          <a href=\"#\"></a><br>\n        </div>\n        <div class=\"small-title w100\">Dépôt de publication numérique</div>\n        <div class=\"w100\">\n          <a href=\"#\">Déposer une publication numérique libre d'accès</a><br>\n          <a href=\"#\">Déposer une publication numérique à accès restreint (commerciale)</a><br>\n          <a href=\"#\">Participer au programme de collecte des sites Web québécois</a><br>\n          <a href=\"http://acpt-www.banq.qc.ca/dl-editor-pp/doListTransaction\">Consulter la liste de vos dépôts de publications numériques</a><br>\n        </div>\n    </td>\n    <td style=\"width:6%\"></td>\n    <td style=\"width:47%\">\n        <div class=\"small-title w100\">Dépôt légal</div>\n        <div class=\"w100\">\n          <a [routerLink]=\"['/home/depol']\" routerLinkActive=\"router-link-active\" >Signaler un nouveau dépôt imprimé</a><br>\n          <a [routerLink]=\"['/home/list-depot']\" routerLinkActive=\"router-link-active\">Consulter la liste de vos dépôts</a><br>\n          <a [routerLink]=\"['/home/claim']\" routerLinkActive=\"router-link-active\">Consulter la liste de vos réclamations</a><br>\n        </div>\n        <div class=\"small-title w100\">ISBN</div>\n        <div class=\"w100\">\n          <a [routerLink]=\"['/home/dmd-isbn']\" routerLinkActive=\"router-link-active\">Faire une première demande pour une séquence ISBN</a><br>\n          <a [routerLink]=\"['/home/new-isbn']\" routerLinkActive=\"router-link-active\">Faire une demande additionnelle pour une nouvelle séquence ISBN</a><br>\n          <a [routerLink]=\"['/home/list-isbn']\" routerLinkActive=\"router-link-active\">Consulter la liste de vos numéros ISBN</a><br>\n          <a href=\"#\"></a><br>\n        </div>\n    </td>\n  </tr>\n</table>\n</div>\n\n<!--VERTICAL SPACER-->\n<div class=\"w100\" style=\"height: 30px\"></div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/home/footer/footer.component.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/home/footer/footer.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"w100 p-0 m-0 bg-white text-center\">\n    <div id=\"PdP\" class=\"print_noShow\">\n      <div class=\"pdp1\">\n        <p>Une question?\n          <a href=\"http://www.banq.qc.ca/formulaires/formulaire_reference/index.html\" class=\"LienQuestion\">Demandez à un bibliothécaire ou à un archiviste</a> |\n          <a href=\"http://www.banq.qc.ca/a_propos_banq/nous_joindre/index.html\" class=\"LienQuestion\">Pour nous joindre</a>\n        </p>\n      </div>\n      <p class=\"pdp2\">\n        <a href=\"http://www.banq.qc.ca/techno/plan_site.html\" accesskey=\"3\" title=\"Plan du site : Touche d'accès clavier=3.\">Plan du site</a> |\n        <a href=\"/http://www.banq.qc.caa_propos_banq/droit_auteur_avis_integrite/index.html\">Droit d'auteur</a> |\n        <a href=\"http://www.banq.qc.ca/a_propos_banq/mission_lois_reglements/lois_reglements_politiques/politiques_procedures/politique_confidentialite/index.html\">Confidentialité</a> |\n        <a href=\"http://www.banq.qc.ca/a_propos_banq/mission_lois_reglements/declaration_services/\">Déclaration de services aux citoyens</a> |\n        <a href=\"http://www.banq.qc.ca/a_propos_banq/acces_a_linfo/index.html\">Accès à l'information</a> |\n        <a href=\"http://www.banq.qc.ca/a_propos_banq/accessibilite/index.html\" accesskey=\"0\" title=\"Accessibilité : Touche d'accès clavier=0.\">Accessibilité</a> |\n        <a href=\"http://www.banq.qc.ca/a_propos_banq/salle_de_presse/index.html\">Salle de presse</a><br />Bibliothèque et Archives nationales du Québec<br />IDEL - <i>version 2.0.0</i>\n      </p>\n    </div>\n  </div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/home/header/header.component.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/home/header/header.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<!--LOGO-->\n<div class=\"w100\"> <img src=\"assets/img/logo_BAnQ.jpg\" alt=\"thumbnail\" class=\"img-fluid\"> </div>\n\n<!--TITLE-->\n<div class=\"bg-green-banq w100\">\n  <table class=\"p-0 m-0 w100 b0\">\n    <tr>\n      <td class=\"p-0 m-0\"><img src=\"assets/img/raye.png\" ></td>\n      <td class=\"p-0 m-0\" style=\"font-size: 40px; color:white; font-family: 'Times New Roman', Times, serif;\">&nbsp;Extranet</td>\n      <td style=\"width:46%\">&nbsp;</td>\n      <td class=\"p-0 m-0\"><img src=\"assets/img/banq-title.png\" ></td>\n    </tr>\n  </table>\n</div>\n\n<!--MENU-->\n<div class=\"col-sm-12 m-0 p-0 w100 bg-white\" style=\"margin-bottom:10px\">\n  <div id=\"divZoneMenuPrincipal\">\n    <div class=\"titreMenuPrincipal hoverBackGroundColor\" style=\"float:left;\">\n      <div class=\"titreMenu\">\n        <a [routerLink]=\"['/home/dashboard']\" routerLinkActive=\"router-link-active\" >Accueil</a>\n      </div>\n  </div>\n  <div class=\"titreMenuPrincipal\" id=\"titreMenuVotreDossier\" style=\"float:left\" (mouseover)=\"onMouseEnter('titreMenuVotreDossier')\" (mouseout)=\"onmouseout('titreMenuVotreDossier')\">\n    <div class=\"titreMenu hoverPointer\">Votre dossier</div>\n    <ul id=\"sous_titreMenuVotreDossier\" class=\"sousMenuPrincipal\">\n    <li><a [routerLink]=\"['/home/dossier']\" routerLinkActive=\"router-link-active\" >Voir ou modifier vos coordonnées</a></li>\n    <li><a [routerLink]=\"['/home/licences']\" routerLinkActive=\"router-link-active\" >Licences d'autorisation accordées à BAnQ</a></li>\n    </ul>\n  </div>\n  <div class=\"titreMenuPrincipal\" id=\"titreMenuDepotLegal\" style=\"float:left\" (mouseover)=\"onMouseEnter('titreMenuDepotLegal')\" (mouseout)=\"onmouseout('titreMenuDepotLegal')\">\n    <div class=\"titreMenu hoverPointer\">Dépôt légal</div>\n    <ul id=\"sous_titreMenuDepotLegal\" class=\"sousMenuPrincipal\">\n      <li><a [routerLink]=\"['/home/depol']\" routerLinkActive=\"router-link-active\" >Signaler un nouveau dépôt imprimé</a></li>\n      <li><a [routerLink]=\"['/home/list-depot']\" routerLinkActive=\"router-link-active\">Consulter la liste de vos dépôts</a></li>\n      <li><a [routerLink]=\"['/home/claim']\" routerLinkActive=\"router-link-active\" >Consulter la liste de vos réclamations</a></li>\n    </ul>\n  </div>\n  <div class=\"titreMenuPrincipal\" id=\"titreMenuDepotPublicationNum\" style=\"float:left\" (mouseover)=\"onMouseEnter('titreMenuDepotPublicationNum')\" (mouseout)=\"onmouseout('titreMenuDepotPublicationNum')\">\n    <div class=\"titreMenu hoverPointer\">Dépôt de publication numérique</div>\n    <ul id=\"sous_titreMenuDepotPublicationNum\" class=\"sousMenuPrincipal\">\n      <li><a href=\"#\">Déposer une publication numérique libre d'accès</a></li>\n      <li><a href=\"#\">Déposer une publication numérique à accès restreint (commerciale)</a></li>\n      <li><a href=\"#\">Participer au programme de collecte des sites Web québécois</a></li>\n      <li><a id=\"linkConsulterPubNum\" href=\"http://acpt-www.banq.qc.ca/dl-editor-pp/doListTransaction\" >Consulter la liste de vos dépôts de publications numériques</a></li>\n    </ul>\n  </div>\n\n  <div class=\"titreMenuPrincipal\" id=\"titreMenuISBN\" style=\"float:left\" (mouseover)=\"onMouseEnter('titreMenuISBN')\" (mouseout)=\"onmouseout('titreMenuISBN')\">\n    <div class=\"titreMenu hoverPointer\">ISBN</div>\n    <ul id=\"sous_titreMenuISBN\" class=\"sousMenuPrincipal\">\n      <li class=\"nonPROD\"><a [routerLink]=\"['/home/dmd-isbn']\" routerLinkActive=\"router-link-active\" id=\"formMenuPrincipal:sousMenuISBN_1\">Faire une première demande pour une séquence ISBN</a></li>\n      <li class=\"nonPROD\"><a [routerLink]=\"['/home/new-isbn']\" routerLinkActive=\"router-link-active\" id=\"formMenuPrincipal:sousMenuISBN_2\">Faire une demande additionnelle pour une nouvelle séquence ISBN</a></li>\n      <li class=\"nonPROD\"><a [routerLink]=\"['/home/list-isbn']\" routerLinkActive=\"router-link-active\">Consulter la liste de vos numéros ISBN</a></li>\n    </ul>\n  </div>\n  <div class=\"titreMenuPrincipal hoverBackGroundColor\" style=\"float:left;\"><div class=\"titreMenu\"><a [routerLink]=\"['/home/help']\" routerLinkActive=\"router-link-active\" >Aide</a></div></div>\n  <div class=\"titreMenuPrincipal hoverBackGroundColor\" style=\"float:left;border-right:none\"><div class=\"titreMenu\"><a [routerLink]=\"['/home/join-us']\" routerLinkActive=\"router-link-active\"  >Nous joindre</a></div></div>\n\n</div>\n\n<div class=\"justify-content-end text-right align-middle\">\n  <a [href]=\"logoutUrl\" (click)=\"logout()\">Se Déconnecter</a>\n</div>\n\n</div>\n\n<!--VERTICAL SPACER-->\n<div style=\"height: 30px;\"></div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/home/help/help.component.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/home/help/help.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card m-0 p-0\">\n    <!--<div class=\"p-2 bg-banq justify-content-center text-center\">\n      <strong style=\"font-size: 15px\">Licences d'autorisation accordées à BAnQ</strong>\n    </div>-->\n<div class=\"card-body col-sm-12 m-0 animated fadeIn\">\n    <div>\n      <h1>Aide</h1>\n      <ul class=\"ListeLiensPage\">\n          <li>\n            <a href=\"https://dev-www.banq.qc.ca/extranet/documents/editeurs/guide_gestion_isbn.pdf\">Guide de gestion de vos ISBN</a><span> [PDF - 360 Ko]</span>\n        </li>\n        <li>\n            <a href=\"https://dev-www.banq.qc.ca/extranet/documents/editeurs/Guide_du_depot_legal.pdf\">Guide pour effectuer le dépôt légal</a><span> [PDF - 857 Ko]</span>\n        </li>\n        <li>\n            <a href=\"https://dev-www.banq.qc.ca/extranet/documents/editeurs/Guide_depot_publications_numeriques.pdf\">Guidepour effectuer le dépôt légal des publications numériques</a><span> [PDF - 715 Ko]</span>\n        </li>\n        <li>\n            <a href=\"https://dev-www.banq.qc.ca/extranet/documents/editeurs/Questions_reponses_express.pdf\">Questions - Réponses Express</a><span> [PDF - 54 Ko]</span>\n        </li>\n        <li>\n            <a href=\"https://dev-www.banq.qc.ca/extranet/documents/editeurs/Renseignement_techniques.pdf\">Guide technique à l'intention des utilisateurs de l'extranet</a><span> [PDF - 578 Ko]</span>\n        </li>\n    </ul>\n   </div>\n   <div id=\"pdp\"> </div>\n   <div class=\"smalltextatbottom\" id=\"menubaspage\">\n     Derni&egrave;re modification : 25 Avril 2010 -- Derni&egrave;re refonte : Novembre 2019<br><br>\n     <a href=\"http://www.gouv.qc.ca\" target=\"_blank\" title=\"Portail du gouvernement du Québec\"><img src=\"/extranet/assets/img/QUEBw1.gif\" alt=\"Portail du gouvernement du Qu&eacute;bec\" width=\"105\" height=\"32\"></a><br>\n     <a href=\"http://www.droitauteur.gouv.qc.ca/copyright.php\" target=\"_blank\" title=\"&copy; Gouvernement du Qu&eacute;bec\">&copy; Gouvernement du Qu&eacute;bec, 2006-2010</a><br><br>\n   </div>\n\n   <div class=\"example-container\">\n      <mat-form-field  class=\"p-0 m-0 border text-dark\">\n        <input matInput placeholder=\"Input\">\n      </mat-form-field>\n\n      <mat-form-field class=\"p-0 m-0 border text-dark\">\n        <textarea matInput placeholder=\"Textarea\"></textarea>\n      </mat-form-field>\n\n      <mat-form-field class=\"p-0 m-0 border text-dark\">\n        <mat-select placeholder=\"Select\">\n          <mat-option value=\"option\">Option</mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n\n</div>\n\n<div class=\"card-footer\">\n  <div class=\" form-inline d-flex justify-content-start\">\n      <button type=\"reset\" class=\"btn btn-sm btn-dark\" (click)=\"back()\"><i class=\"fa fa-times\"></i>&nbsp;Retour</button>\n  </div>\n</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/home/home.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/home/home.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"app-body\">\n  <main class=\"main d-flex align-items-center\">\n      <div class=\"container\">\n          <div class=\"row d-flex justify-content-center\">\n              <div class=\"col-sm-10 border bg-white\">\n                    <div class=\"col-sm-12\">\n                      <app-header2></app-header2>\n                      <router-outlet></router-outlet>\n                      <app-footer2></app-footer2>\n                    </div>\n              </div>\n          </div>\n      </div>\n  </main>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/home/join-us/join-us.component.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/home/join-us/join-us.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card m-0 p-0\">\n  <!--<div class=\"p-2 bg-banq justify-content-center text-center\">\n    <strong style=\"font-size: 15px\">Licences d'autorisation accordées à BAnQ</strong>\n  </div>-->\n<div class=\"card-body col-sm-12 m-0 animated fadeIn\">\n\n    <div id=\"ColCentreIDEL\">\n        <h1>Pour nous joindre</h1>\n        <div>\n            Bibliothèque et Archives nationales du Québec<br>\n            Service des acquisitions des collections patrimoniales<br>\n            2275, rue Holt<br>\n            Montréal (Québec) H2G 3H1\n            <br><br>\n            Télécopieur 514 873-4310\n        </div>\n        <h2>Publications imprimées</h2>\n        <ul>\n            <li>514 873-1101 poste 3780</li>\n            <li>1 800 363-9028 (au Québec)</li>\n            <li><a href=\"mailto:depot@banq.qc.ca?Subject=Dépôt%20légal%20des%20publications%20imprimées\" >depot@banq.qc.ca</a></li>\n        </ul>\n        <h2>Publications numériques et collecte de sites web</h2>\n        <ul>\n\n            <li>\n                Carole Gagné<br>\n                514 873-1101 poste 3837\n            </li>\n            <li><a href=\"mailto:pubelectro@banq.qc.ca?Subject=Dépôt%20de%20publications%20numériques%20et%20Collecte%20de%20sites%20web\" >pubelectro@banq.qc.ca</a></li>\n        </ul>\n        <h2>Agence ISBN</h2>\n        <ul>\n            <li>514 873-1101 poste 3785</li>\n            <li>1 800 363-9028 poste 3785 (au Québec)</li>\n            <li><a href=\"mailto:isbn@banq.qc.ca?Subject=Agence%20ISBN\" >isbn@banq.qc.ca</a> </li>\n        </ul>\n        <h2>Extranet</h2>\n        <ul>\n           <li><a href=\"mailto:extranetediteur@banq.qc.ca?Subject=Extranet%20des%20éditeurs\">extranetediteur@banq.qc.ca</a> </li>\n        </ul>\n        <br>\n        <div>Utilisez cette adresse pour communiquer au sujet de questions techniques concernant l’extranet des éditeurs</div>\n      </div>\n      <div id=\"pdp\"> </div>\n      <div class=\"smalltextatbottom\" id=\"menubaspage\">\n        Derni&egrave;re modification : 25 octobre 2019 -- Derni&egrave;re refonte : avril 2010<br><br>\n        <a href=\"http://www.gouv.qc.ca\" target=\"_blank\" title=\"Portail du gouvernement du Québec\"><img src=\"/extranet/assets/img/QUEBw1.gif\" alt=\"Portail du gouvernement du Qu&eacute;bec\" width=\"105\" height=\"32\"></a><br>\n        <a href=\"http://www.droitauteur.gouv.qc.ca/copyright.php\" target=\"_blank\" title=\"&copy; Gouvernement du Qu&eacute;bec\">&copy; Gouvernement du Qu&eacute;bec, 2006-2010</a><br><br>\n      </div>\n\n\n</div>\n\n<div class=\"card-footer\">\n<div class=\" form-inline d-flex justify-content-start\">\n    <button type=\"reset\" class=\"btn btn-sm btn-dark\" (click)=\"back()\"><i class=\"fa fa-times\"></i>&nbsp;Retour</button>\n</div>\n</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/licence/edit-licence/edit-licence.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/licence/edit-licence/edit-licence.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card m-0 p-0\">\n    <div class=\"p-2 bg-banq justify-content-center text-center\">\n      <strong style=\"font-size: 15px\">Licence accordée à Bibliothèque et Archives nationales du Québec</strong>\n    </div>\n    <div class=\"card-body col-sm-12 m-0 animated fadeIn\" style=\"font-size: 11px;\">\n\n        <div class=\"w100\">\n          <span>Bibliothèque et Archives nationales du Québec (BAnQ) a notamment pour mandat de conserver et de diffuser le patrimoine documentaire publié québécois tout en respectant les dispositions de la Loi sur le droit d'auteur. Par la présente, l'éditeur autorise BAnQ à poser les actions suivantes :</span><br><br>\n        </div>\n        <div class=\"w100\">\n            <div class=\"input-group px-3\">\n                <input class=\"form-check-input\" type=\"checkbox\">\n                <label class=\"form-check-label\"><b>Publications numériques</b></label>\n            </div>\n        </div>\n        <div class=\"border p-2 w100\">\n            <p>1. reproduire et archiver une ou des copies des publications numériques sur une unité de stockage appartenant à BAnQ;</p>\n            <p>2. effectuer les opérations requises, notamment la migration, la conversion et la fusion, afin de répondre aux normes informatiques de BAnQ pour assurer la conservation et la diffusion à long terme;</p>\n            <p>3. donner accès à ses usagers aux fichiers ayant fait l'objet des opérations mentionnées aux points numéro 1 et 2 selon les modalités suivantes :</p>\n\n            <b>Publications numériques libres d'accès</b><br>\n            <div class=\"input-group px-3\">\n                <input class=\"form-check-input\" type=\"checkbox\">\n                <label class=\"form-check-label\">Sur le <b>portail Internet de BAnQ</b> (pour les publications que l'éditeur aura signalées comme étant \" <b>libre d'accès</b> \" au moment du dépôt).</label>\n            </div>\n            <br><b>Publications numériques à accès restreint (commercial)</b><br>\n            <div class=\"input-group px-3\">\n                <input class=\"form-check-input\" type=\"checkbox\">\n                <label class=\"form-check-label\"><b>Dans les locaux de BAnQ</b>, à partir de postes informatiques dédiés n'offrant aucune possibilité d'impression ni de téléchargement (pour les publications que l'éditeur aura signalées comme étant « <b>à accès restreint (commercial)</b> » au moment du dépôt).</label>\n            </div>\n            <br>\n            <span>Ces publications pourront être diffusées sur le portail Internet de BAnQ sans limite de territoire ni de temps au terme du délai suivant:</span><br>\n            <ul style=\"text-decoration: none;\">\n              <li><div class=\"input-group\"><input class=\"form-check-input\" type=\"radio\">&nbsp;<label class=\"form-check-label\">1 an</label></div></li>\n              <li><div class=\"input-group\"><input class=\"form-check-input\" type=\"radio\">&nbsp;<label class=\"form-check-label\">2 ans</label></div></li>\n              <li><div class=\"input-group\"><input class=\"form-check-input\" type=\"radio\">&nbsp;<label class=\"form-check-label\">5 ans</label></div></li>\n              <li><div class=\"input-group\"><input class=\"form-check-input\" type=\"radio\">&nbsp;<label class=\"form-check-label\">10 ans</label></div></li>\n              <li><div class=\"input-group\"><input class=\"form-check-input\" type=\"radio\">&nbsp;<label class=\"form-check-label\">Jamais (jusqu'à l'expiration du droit d'auteur)</label></div></li>\n              <li>\n                <div class=\"input-group\">\n                  <input class=\"form-check-input\" type=\"radio\">&nbsp;<label class=\"form-check-label\">Autre délai:</label>&nbsp;\n                  <input type=\"text\">\n                </div>\n              </li>\n            </ul>\n            <p>Il est convenu que le délai mentionné court depuis la date de réception des fichiers par BAnQ.</p>\n            <p>Au terme du délai indiqué, les usagers du portail Internet de BAnQ pourront utiliser les publications qui y sont diffusées à des fins privées, éducatives et non commerciales, à la condition d'indiquer la source.</p>\n            <span>L'éditeur autorise BAnQ à reproduire les publications recueillies et à en communiquer une copie à Bibliothèque et Archives Canada (BAC), conformément aux règles relatives au dépôt légal qui leur sont applicables :</span><br>\n            <ul style=\"text-decoration: none;\">\n                <li><div class=\"input-group\"><input class=\"form-check-input\" type=\"checkbox\">&nbsp;<label class=\"form-check-label\">Oui</label></div></li>\n                <li><div class=\"input-group\"><input class=\"form-check-input\" type=\"checkbox\">&nbsp;<label class=\"form-check-label\">Non</label></div></li>\n            </ul>\n            <p>Coordonnées de la personne à joindre pour toute question concernant les publications:</p>\n            <table class=\"p-2\" style=\"width: 80%;\">\n              <tr>\n                <td>Nom</td>\n                <td> <input type=\"text\"> </td>\n                <td>Prénom</td>\n                <td><input type=\"text\"></td>\n              </tr>\n              <tr>\n                <td>Téléphone</td>\n                <td> <input type=\"text\" style=\"width: 120px;\"> </td>\n                <td>Poste</td>\n                <td><input type=\"text\" style=\"width: 80px;\"></td>\n              </tr>\n              <tr>\n                <td>Courriel</td>\n                <td colspan=\"2\"><input type=\"text\" class=\"w100\"></td>\n                <td></td>\n              </tr>\n              </table>\n        </div>\n        <br><br>\n        <div class=\"w100\">\n            <div class=\"input-group px-3\">\n                <input class=\"form-check-input\" type=\"checkbox\">\n                <label class=\"form-check-label\"><b>Sites Web</b></label>\n            </div>\n        </div>\n        <div class=\"border w100 p-2\">\n          <p>Pour l'ensemble de ces desseins, l'éditeur accorde gratuitement à BAnQ une licence, à des fins non commerciales, de reproduction et de communication au public par télécommunications.</p>\n          <p>La licence est non exclusive, non transférable et sans limite de territoire ni de temps.</p>\n          <p>L'éditeur garantit à BAnQ qu'il détient les droits d'auteur et qu'il est dûment habilité et autorisé à accorder la présente licence.</p>\n          <p>Il est entendu que l'éditeur demeure le seul titulaire des droits d'auteur de ses publications numériques et de son ou ses sites Web.</p>\n          <p>L'éditeur peut résilier la présente licence en remettant à BAnQ un avis écrit 30 jours. Cependant, en cas de résiliation, BAnQ continue de jouir des droits d'utilisation consentis au préalable.</p>\n          <span>Adresse(s) du (des) sites Web ou autre(s) commentaire(s)</span><br>\n          <div class=\"w100\">\n              <textarea class=\"form-control\" rows=\"2\"></textarea>\n              <small  class=\"font-weight-bold text-muted\" style=\"font-size: 9px;\">Le contenu ne peut pas excéder 2000 caractères</small>\n          </div>\n          <br>\n          <span> <b>Pour l'éditeur:</b><br>Un courriel vous sera envoyé, dans lequel se trouvera un hyperlien permettant de confirmer l'octroi de la présente licence à BAnQ. Cet échange constituera l'autorisation de l'éditeur. <br> </span>\n          <table class=\"p-2\" style=\"width: 50%;\">\n            <tr>\n              <td>Nom :</td>\n              <td><input type=\"text\" class=\"form-control\"></td>\n            </tr>\n            <tr>\n              <td>Fonction :</td>\n              <td><input type=\"text\" class=\"form-control\"></td>\n            </tr>\n            <tr>\n              <td>Courriel :</td>\n              <td><input type=\"text\" class=\"form-control\"></td>\n            </tr>\n          </table>\n          <p>Veuillez indiquer le(s) courriel(s) au(x)quel(s) la confirmation de la signature de la licence sera envoyée :</p>\n          <table class=\"p-2\" style=\"width: 55%;\">\n              <tr>\n                <td class=\"px-2 text-center\"><button mat-button class=\"btn btn-sm btn-success\" matTooltip=\"Cliquez ici pour ajouter un nouveau repondant\"><i class=\"fa fa-plus\"></i></button></td>\n                <td><input type=\"text\" class=\"form-control\"></td>\n                <td class=\"p-0 text-center\"><button mat-button class=\"btn btn-sm btn-danger\" matTooltip=\"Cliquez ici pour supprimer la ligne\"><i class=\"fa fa-remove\"></i></button></td>\n              </tr>\n            </table>\n            <p>Les informations suivantes seront remplies lorsque le lien dans le courriel sera activé :</p>\n          <table class=\"w100\">\n            <tr>\n              <td style=\"width: 50%;\">Signature:</td><td style=\"width: 50%;\">Date:</td>\n            </tr>\n          </table><br>\n        </div>\n\n    </div>\n\n<div class=\"card-footer\">\n  <div class=\" form-inline d-flex justify-content-end\">\n      <button type=\"reset\" mat-button class=\"btn btn-sm btn-dark\" (click)=\"back()\"><i class=\"fa fa-times\"></i>&nbsp;Retour</button>&nbsp;&nbsp;\n      <button type=\"submit\" mat-button class=\"btn btn-sm btn-primary\" (click)=\"submit()\"><i class=\"fa fa-check\"></i>&nbsp;Soumettre</button>\n  </div>\n</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/licence/licence.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/licence/licence.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"card m-0 p-0\">\n    <div class=\"p-2 bg-banq justify-content-center text-center\">\n      <strong style=\"font-size: 15px\">Licences d'autorisation accordées à BAnQ</strong>\n    </div>\n    <div class=\"card-body col-sm-12 m-0 animated fadeIn\">\n\n      <div class=\"col-sm-12 my-2\">\n        <div class=\"border p-2\" style=\"background-color: rgb(237, 242, 233);\">\n            <h6><b>Licences accoordées :</b></h6>\n            <div class=\"input-group px-2\">\n                <input type=\"checkbox\" checked disabled=\"true\">&nbsp;\n                <label class=\"form-check-label\">Publications numériques libre d'accès </label>\n            </div>\n            <div class=\"input-group px-2\">\n                <input type=\"checkbox\" checked disabled=\"true\">&nbsp;\n                <label class=\"form-check-label\">Publications numériques à accès restreint (commerciales)</label>\n            </div>\n            <div class=\"input-group px-2\">\n                <input type=\"checkbox\" checked disabled=\"true\">&nbsp;\n                <label class=\"form-check-label\">Sites Web</label>\n            </div>\n      </div>\n    </div>\n\n    <div *ngIf=\"licences.length > 0\">\n        <h6><b>Consulter les licences accordées :</b></h6>\n        <ul style=\"list-style: none;\">\n          <li *ngFor=\"let lic of licences\"> <a href=\"#\">{{lic}}</a> </li>\n        </ul>\n      <br>\n    </div>\n\n    <div>\n      <a [routerLink]=\"['/home/edit-licence']\" routerLinkActive=\"router-link-active\">Accorder une nouvelle licence d'autorisation...</a>\n    </div>\n\n</div>\n\n<div class=\"card-footer\">\n  <div class=\" form-inline d-flex justify-content-start\">\n      <button type=\"reset\" class=\"btn btn-sm btn-dark\" (click)=\"back()\"><i class=\"fa fa-times\"></i>&nbsp;Retour</button>\n  </div>\n</div>\n</div>\n\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/list-isbn/list-isbn.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/list-isbn/list-isbn.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"card m-0 p-0\">\n  <!-- HEADER -->\n  <div class=\"p-2 bg-banq justify-content-center text-center\">\n      <strong style=\"font-size: 15px\">Vos numéros ISBN</strong>\n  </div>\n\n  <!-- BODY -->\n  <div class=\"card-body col-sm-12 m-0 animated fadeIn\">\n\n    <div>\n        <div class=\"input-group\">\n            <div class=\"input-group-prepend\"><span class=\"name\">Séquence ISBN</span>\n            </div>&nbsp;\n            <select class=\"form-control\" >\n                <option value=\"978-2-924809\" >978-2-924809</option>\n            </select>\n        </div>\n        <div class=\"input-group my-2\">\n            <div class=\"input-group-prepend\"><span class=\"name\">Titre</span></div>&nbsp;\n            <input type=\"text\" class=\"form-control\">\n        </div>\n        <div class=\"w100 form-inline d-flex justify-content-end\">\n            <button type=\"button\" class=\"btn btn-sm btn-dark\" (click)=\"back()\"><i class=\"fa fa-check\"></i>&nbsp;Soumettre</button>\n        </div>\n        <div class=\"w100 p-2 my-3 border\">\n            <table id=\"tblIsbn\" datatable class=\"table table-bordered table-sm table-responsive-sm table-striped row-border hover m-0 p-0 w100\" scrollX=\"false\" scrollY=\"true\" maxHeight=\"800\">\n                <thead>\n                    <tr>\n                        <th class=\"th-sm offspaces bg-banq text-center align-middle\">Numéro ISBN</th>\n                        <th class=\"th-sm offspaces bg-banq text-center align-middle\">Titre</th>\n                        <th class=\"th-sm offspaces bg-banq text-center align-middle\">Actif</th>\n                        <th class=\"th-sm offspaces bg-banq text-center align-middle\">Format de la publication</th>\n                        <th class=\"th-sm offspaces bg-banq text-center align-middle\">Date de publication</th>\n                        <th class=\"th-sm offspaces bg-banq text-center align-middle\">&nbsp;</th>\n                        <th class=\"th-sm offspaces bg-banq text-center align-middle\">&nbsp;</th>\n                    </tr>\n                </thead>\n              </table>\n\n        </div>\n    </div>\n\n    <div class=\"my-2 p-2 border\">\n        <div class=\"input-group\">\n            <div class=\"input-group-prepend\"><span class=\"name\">Séquence ISBN<br>actuelle</span>\n            </div>&nbsp;\n            <input type=\"text\"  class=\"form-control\" readonly=\"true\" value=\"978-2-924809\" >\n        </div>\n        <div class=\"input-group my-2\">\n            <div class=\"input-group-prepend\"><span class=\"name\">Titre</span></div>&nbsp;\n            <input type=\"text\"  class=\"form-control\" readonly=\"true\" >\n        </div>\n    </div>\n\n    <a href=\"#\">Générer un numéro ISBN</a>\n    <div class=\"w100 form-inline d-flex justify-content-end\">\n        <button type=\"button\" class=\"btn btn-sm btn-dark\" (click)=\"back()\"><i class=\"fa fa-times\"></i>&nbsp;Affichage par défaut</button>&nbsp;\n    </div>\n  </div>\n\n  <!-- FOOTER -->\n  <div class=\"card-footer\">\n    <div class=\" form-inline d-flex justify-content-start\">\n        <button type=\"button\" class=\"btn btn-sm btn-dark\" (click)=\"back()\"><i class=\"fa fa-arrow-left\"></i>&nbsp;Retour</button>&nbsp;\n    </div>\n  </div>\n\n</div>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/login/login.component.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/login/login.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br><br>\n\n<div class=\"row d-flex justify-content-center\">\n<div id=\"Global\" class=\"col-sm-6 p-2 border bg-white\">\n    <div id=\"Entete\" class=\"BarreNoire\">\n        <p><a href=\"http://www.banq.qc.ca\">Accueil</a> <a href=\"http://www.banq.qc.ca/aide/mon_dossier/\">Aide</a> <a href=\"http://www.banq.qc.ca/accueil/index.html?language_id=1\" class=\"Lang\">English</a></p>\n        <img src=\"https://www.banq.qc.ca/images/interface14/BAnQ-2.gif\" alt=\"Biblioth&egrave;que et Archives nationales du Qu&eacute;bec.\" />\n    </div>\n\n    <div id=\"Contenu\" class=\"justify-content-center\" style=\"vertical-align: middle\">\n\n        <p id=\"loginChemin\"><a href=\"http://www.banq.qc.ca/mon_dossier/mon_dossier.html\">Mon dossier</a>&nbsp;&gt;&nbsp;Pour acc&eacute;der &agrave; votre dossier</p>\n        <h1>Pour acc&eacute;der &agrave; votre dossier</h1>\n        <form name=\"sbLogin\" action=\"/idp/Authn/UserPassword\" method=\"post\">\n            <div class=\"EnteteForm\"></div>\n            <div class=\"Wrapper\">\n                <div class=\"ColG\">\n                    <p class=\"Chapo\">Entrez le num&eacute;ro de client (8 chiffres) et le mot de passe li&eacute;s &agrave; votre <a href=\"http://www.banq.qc.ca/mon_dossier/mon_dossier.html\">dossier</a>.</p>\n                    <div id=\"messageSafari\" class=\"loginMsg\" style=\"display:none;\">\n                        <div>Message aux abonnés utilisant le navigateur Web Safari.</div><br/> Il est possible que vous éprouviez certaines difficultés de navigation après vous ètre authentifié sur le portail de BAnQ. Nous vous recommandons d'utiliser les\n                        applications Firefox ou Google Chrome jusqu'à ce que ce problème soit définitivement réglé.\n                        <br/><br/> Merci de votre compréhension.\n                    </div>\n                    <label for=\"NUM\">Num&eacute;ro de client (8 chiffres)</label>\n                    <input matInput type=\"text\" required #txtUsername=\"ngModel\" [class.is-invalid]=\"txtUsername.invalid && txtUsername.touched\" class=\"form-control\" placeholder=\"\" autocomplete=\"username\" name=\"txtUsername\" [(ngModel)]=\"request.username\">\n                    <label for=\"PWD\">Mot de passe</label>\n                    <input matInput type=\"password\" required #txtPassword=\"ngModel\" [class.is-invalid]=\"txtPassword.invalid && txtPassword.touched\" class=\"form-control\" placeholder=\"\" name=\"txtPassword\" [(ngModel)]=\"request.password\">\n                    <button class=\"btnSubmit\" mat-button (click)=\"login()\">Connexion</button>\n                    <a href=\"https://www.banq.qc.ca/formulaires/extranet/index.html\">Mot de passe perdu?</a>\n                </div>\n\n                <div class=\"ColD\">\n                    <p class=\"Chapo\">Vous n'êtes pas abonn&eacute;?</p>\n                    <button mat-button class=\"Bouton\" (click)=\"onCmtAbonneClick()\">Comment s'abonner?</button>\n                    <div id=\"contenu_reponse_abon\" [ngStyle]=\"{'display': disp ? 'block' : 'none'}\" class=\"divReponse\">\n                        <a (click)=\"disp = false\"><img src=\"https://www.banq.qc.ca/extranet/images/interface/ic-fermer.gif\" alt=\"Fermer\" /></a>\n                        <p>Pour vous inscrire à l'extranet, veuillez compléter ce <a [routerLink]=\"['/register']\" routerLinkActive=\"router-link-active\" class=\"inlineLink\">formulaire d’inscription.</a></p>\n                        <p>Vous recevrez votre numéro de client et mot de passe dans un délai maximum de trois jours ouvrables.</p>\n                        <p>Pour toute question veuillez communiquer avec nous</p>\n                        <ul>\n                            <li>par courriel à <a href=\"mailto:extranetediteur@banq.qc.ca\" class=\"inlineLink\">extranetediteur@banq.qc.ca</a></li>\n                            <li>par téléphone au 514 873-1101 poste 3837 ou au 1 800 363-9028 poste 3837 (au Québec)</li>\n                        </ul>\n                    </div>\n                    <p><a href=\"http://www.banq.qc.ca/mon_dossier/a_propos_securite.html\">&Agrave; propos de la s&eacute;curit&eacute;</a></p>\n                </div>\n            </div>\n\n        </form>\n    </div>\n    <p style=\"text-align:right;margin:0px;\"><a href=\" http://www.banq.qc.ca/documents/services/depot_legal/guide_technique_extranet.pdf\" style=\"color: #31697F;\">Guide technique à l'intention des utilisateurs de l'extranet [PDF - 992 Ko]</a></p>\n    <p class=\"PdP\">&copy; Tous droits r&eacute;serv&eacute;s. Biblioth&egrave;que et Archives nationales du Qu&eacute;bec</p>\n</div><app-alert-dialog [myAlert]=\"myAlert\"></app-alert-dialog>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/new-isbn/new-isbn.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/new-isbn/new-isbn.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"card m-0 p-0\">\n  <!-- HEADER -->\n  <div class=\"p-2 bg-banq justify-content-center text-center\">\n      <strong style=\"font-size: 15px\">Nouvelle demande de numéro ISBN</strong>\n  </div>\n\n  <!-- BODY -->\n  <div class=\"card-body col-sm-12 m-0 animated fadeIn\">\n\n    <div>\n      <h6>Rappel : n'oubliez pas de mettre à jour vos coordonnées et le nom du répondant s'il y a lieu : <a href=\"#\">faire une mise à jour</a></h6>\n      <h6>Assurez-vous que le document est éligible à une demande ISBN en consultant le <a href=\"#\">Guide sur l'utilisation de l'ISBN</a>\t.</h6><br>\n      <h6>Combien de titres prévoyez-vous publier sous ce nom d'éditeur au cours des cinq prochaines années?</h6>\n      <div class=\"input-group\">\n          <div class=\"input-group-prepend\"><input type=\"radio\">&nbsp;<span>10 et moins</span></div>\n          <div class=\"input-group-prepend\" style=\"margin-left: 25px;\"><input type=\"radio\">&nbsp;<span>11 à 100</span></div>\n          <div class=\"input-group-prepend\" style=\"margin-left: 25px;\"><input type=\"radio\">&nbsp;<span>Plus de 100</span></div>\n        </div>\n    </div>\n    <br>\n    <div>\n        <div class=\"input-group\">\n            <div class=\"input-group-prepend\"><h6>Commentaires</h6></div>&nbsp;\n            <textarea class=\"form-control\" rows=\"2\"></textarea>\n        </div>\n        <div style=\"padding-left: 100px;\"><small style=\"font-weight: bold; font-size: 9px; color:grey\">Le contenu ne peut pas excéder 2000 caractères</small></div>\n    </div>\n\n  </div>\n\n  <!-- FOOTER -->\n  <div class=\"card-footer\">\n    <div class=\" form-inline d-flex justify-content-start\">\n        <button type=\"button\" class=\"btn btn-sm btn-dark\" (click)=\"back()\"><i class=\"fa fa-arrow-left\"></i>&nbsp;Retour</button>&nbsp;\n        <button type=\"button\" class=\"btn btn-sm btn-danger\" (click)=\"back()\"><i class=\"fa fa-times\"></i>&nbsp;Effacer tout</button>&nbsp;\n        <button type=\"button\" class=\"btn btn-sm btn-primary\" (click)=\"back()\"><i class=\"fa fa-check\"></i>&nbsp;Soumettre</button>\n    </div>\n  </div>\n\n</div>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/register/register.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/register/register.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br><br>\n\n<app-alert-dialog [myAlert]=\"myAlert\"></app-alert-dialog>\n\n<div class=\"app-body\">\n    <main class=\"main d-flex align-items-center\">\n        <div class=\"container\">\n            <div class=\"row d-flex justify-content-center\">\n                <div class=\"col-sm-12\">\n\n                    <div *ngIf=\"errs.length > 0\">\n                        <ul>\n                          <li *ngFor=\"let err of errs\">\n                              <span style=\"font-weight:bold; color:red; font-size: 12px\"><i class=\"rounded bg-danger fa fa-warning\" style=\"padding: 2px\"></i>&nbsp;{{err}}</span>\n                          </li>\n                        </ul>\n                        <br>\n                      </div>\n\n                    <div class=\"card m-0 p-0\">\n                        <div class=\"p-2 bg-banq justify-content-center text-center\">\n                          <strong style=\"font-size: 15px\">Formulaire d'inscription à l'extranet</strong>\n\n                        </div>\n                        <div class=\"card-body col-sm-12 m-0 px-2 py-2 animated fadeIn\">\n\n\n                                <div class=\"panel-body col-md-12 p-2\">\n\n                                    <div class=\"panel panel-info col-sm-12 m-0 p-0\">\n                                        <div class=\"panel-body border col-sm-12\">\n                                            <h6 class=\"text-on-pannel\"><strong>Identification du / des demandeur(s) :</strong></h6>\n                                            <div class=\"row col-sm-12 p-2\">\n                                                <form [formGroup]=\"registerForm\" style=\"width:100%;margin:0px;padding:0px;\">\n                                                <div id=\"table\" class=\"table-editable col-sm-12 m-0 p-0\" formArrayName=\"persons\">\n                                                    <table class=\"table table-bordered table-responsive-md table-striped text-center m-0 p-0\" scrollX=\"false\" scrollY=\"true\" style=\"width:100%\" maxHeight=\"200\">\n                                                        <thead>\n                                                            <tr>\n                                                                <th class=\"offspaces text-center\">\n                                                                    <button class=\"btn btn-sm btn-success\" (click)=\"add()\"><i class=\"fa fa-plus\" tooltip=\"Cliquez ici pour ajouter un nouveau repondant\"></i></button>\n                                                                </th>\n                                                                <th class=\"offspaces bg-banq text-center align-middle\">Titre<span class=\"required\">*</span></th>\n                                                                <th class=\"offspaces bg-banq text-center align-middle\">Nom<span class=\"required\">*</span></th>\n                                                                <th class=\"offspaces bg-banq text-center align-middle\">Prénom<span class=\"required\">*</span></th>\n                                                                <th class=\"offspaces bg-banq text-center align-middle\">Courriel<span class=\"required\">*</span></th>\n                                                                <th class=\"offspaces bg-banq text-center align-middle\">Téléphone</th>\n                                                                <th class=\"offspaces bg-banq text-center align-middle\">Poste</th>\n                                                                <th class=\"offspaces bg-banq text-center align-middle\">Suppr.</th>\n                                                            </tr>\n                                                        </thead>\n                                                        <tbody>\n                                                            <tr *ngFor=\"let person of persons.controls; let id = index\" [formGroupName]=\"id\">\n                                                                <td class=\"offspaces align-middle text-center\">\n                                                                    {{id+1}}\n                                                                </td>\n                                                                <td class=\"offspaces\">\n                                                                    <select required formControlName=\"title\" class=\"form-control\"  > <!-- [class.is-invalid]=\"person.get('title').value === ''\" -->\n                                                                  <option value=\"M.\">M.</option>\n                                                                  <option value=\"Mme\">Mme</option>\n                                                              </select>\n                                                                </td>\n                                                                <td class=\"offspaces\">\n                                                                    <input type=\"text\" required formControlName=\"firstname\" class=\"form-control\">\n                                                                </td>\n                                                                <td class=\"offspaces\">\n                                                                    <input type=\"text\" required formControlName=\"lastname\" class=\"form-control\">\n                                                                </td>\n                                                                <td class=\"offspaces\">\n                                                                    <input type=\"text\" required formControlName=\"email\" class=\"form-control\" >\n                                                                </td>\n                                                                <td class=\"offspaces\">\n                                                                    <table class=\"p-0 m-0\" style=\"border:none\"><tr>\n                                                                        <td class=\"p-0 m-0\"><input matInput type=\"text\" required formControlName=\"phoneArea\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer bg-white\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\"/></td>\n                                                                        <td class=\"p-0 m-0\"><input matInput type=\"text\" required formControlName=\"phone3\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer bg-white\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\" /></td>\n                                                                        <td class=\"p-0 m-0 align-middle bg-white\">-</td>\n                                                                        <td class=\"p-0 m-0\"><input matInput type=\"text\" required formControlName=\"phone4\" pattern=\"^\\d{4}$\" class=\"form-control telephone4 divCentrer bg-white\" maxlength=\"4\" size=\"4\" app-numeric numericType=\"number\" /></td>\n                                                                      </tr></table>\n                                                                    <!-- <input type=\"text\" [id]=\"'phone' + id\" required [name]=\"'txtRepPhone' + id\" formControlName=\"phone\" class=\"form-control\" pattern=\"^\\d{10}$\" [class.is-invalid]=\"registerForm.get('phone').invalid && registerForm.get('phone').touched\" > -->\n                                                                </td>\n                                                                <td class=\"offspaces\">\n                                                                    <input type=\"text\" formControlName=\"poste\" class=\"form-control\">\n                                                                </td>\n                                                                <td class=\"offspaces\">\n                                                                    <button type=\"button\" *ngIf=\"id > 0\" rounded=\"true\" size=\"sm\" class=\"btn btn-danger my-0\" (click)=\"remove(id)\" tooltip=\"Cliquez ici pour supprimer la ligne\"><i class=\"fa fa-remove\"></i></button>\n                                                                </td>\n                                                            </tr>\n                                                        </tbody>\n                                                    </table>\n                                                </div>\n                                              </form>\n                                            </div>\n                                        </div>\n                                    </div>\n\n                                    <form #frmSignup=\"ngForm\">\n                                    <div class=\"panel panel-info py-3\">\n                                        <div class=\"panel-body border\">\n                                            <h6 class=\"text-on-pannel\"><strong>Information sur l'éditeur</strong></h6>\n                                            <div class=\"row p-2\">\n                                                <div class=\"col-sm-12\">\n                                                    <div class=\"input-group\">\n                                                        <div class=\"input-group-prepend\"><span class=\"name\">Editeur<span class=\"required\">*</span><br><small class=\"hintText\">(Nom de l'entreprise, de <br>l'organisme ou de la personne)</small> </span>\n                                                        </div>&nbsp;\n                                                        <textarea required #txtInfos=\"ngModel\" [(ngModel)]=\"editor.nom\" class=\"form-control\" name=\"txtInfos\" rows=\"2\"></textarea> <!--  [class.is-invalid]=\"txtInfos.invalid && txtInfos.touched\" -->\n                                                    </div>\n                                                    <small style=\"font-weight: bold; font-size: 9px; color:grey; padding-left: 140px\">Le contenu ne peut pas excéder 500 caractères.</small>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n\n                                    <div class=\"panel panel-info py-3 m-0\">\n                                        <div class=\"panel-body border p-0 m-0\">\n                                            <h6 class=\"text-on-pannel\"><strong>Adresse</strong></h6>\n                                            <div class=\"row p-2 justify-content-center\">\n                                                <div class=\"\" style=\"width: 95%\">\n                                                    <div class=\"col-sm-12 py-2 m-0\">\n                                                        <div class=\"input-group\">\n                                                            <div class=\"input-group-prepend\"><span class=\"name\">Adresse Postale<span class=\"required\">*</span></span>\n                                                            </div>&nbsp;\n                                                            <input type=\"text\" name=\"txtAdrPos1\" required #txtAdrPos1=\"ngModel\" class=\"form-control\" [(ngModel)]=\"editor.adresse1\" placeholder=\"\"> <!--  [class.is-invalid]=\"txtAdrPos1.invalid && txtAdrPos1.touched\" -->\n                                                        </div>\n                                                    </div>\n                                                    <div class=\"col-sm-12 py-2 m-0\">\n                                                        <div class=\"input-group\">\n                                                            <div class=\"input-group-prepend\"><span class=\"name\">Adresse Postale 2</span></div>&nbsp;\n                                                            <input type=\"text\" name=\"txtAdrPos2\" #txtAdrPos2=\"ngModel\" class=\"form-control\" [(ngModel)]=\"editor.adresse2\" placeholder=\"\">\n                                                        </div>\n                                                    </div>\n                                                    <div class=\"col-sm-12 py-2 m-0\">\n                                                        <div class=\"row\">\n                                                            <div class=\"col-sm-4\">\n                                                                <div class=\"input-group\">\n                                                                    <div class=\"input-group-prepend\"><span class=\"name\">Ville<span class=\"required\">*</span></span>\n                                                                    </div>&nbsp;\n                                                                    <!-- <input type=\"text\" name=\"cbxCity1\"  required #cbxCity1=\"ngModel\" class=\"form-control\" [(ngModel)]=\"editor.ville\" placeholder=\"\" maxlength=\"30\">  -->\n                                                                    <div class=\"p-0 m-0\">\n                                                                      <!-- <mat-form-field class=\"p-0 m-0 border\"></mat-form-field>  -->\n                                                                        <input required matInput type=\"text\" [formControl]=\"searchCitiesCtrl\" [matAutocomplete]=\"auto\" [class.is-invalid]=\"searchCitiesCtrl.invalid && searchCitiesCtrl.touched\" class=\"form-control border\" [(ngModel)]=\"editor.ville\" maxlength=\"30\">\n                                                                        <mat-autocomplete #auto=\"matAutocomplete\" >\n                                                                          <mat-option *ngFor=\"let city of cities | async\" [value]=\"city.city\">{{city.city}}</mat-option>\n                                                                        </mat-autocomplete>\n\n                                                                    </div>\n\n                                                                </div>\n                                                            </div>\n                                                            <div class=\"col-sm-5\">\n                                                                <div class=\"input-group\">\n                                                                    <div class=\"input-group-prepend\"><span class=\"name\">Province/Territoire<span class=\"required\">*</span></span>\n                                                                    </div>&nbsp;\n                                                                    <select required #cbxState1=\"ngModel\" class=\"form-control\" name=\"cbxState1\" [(ngModel)]=\"editor.provinceTerritoire\"> <!--  [class.is-invalid]=\"cbxState1.invalid && cbxState1.touched\" -->\n                                                                        <option *ngFor=\"let item of states\" value=\"{{item.name}}\" >{{item.name}}</option>\n                                                                    </select>\n                                                                </div>\n                                                            </div>\n                                                            <div class=\"col-sm-3\">\n                                                                <div class=\"input-group\">\n                                                                    <div class=\"input-group-prepend\"><span class=\"small-name\">Pays<span class=\"required\">*</span></span></div>&nbsp;\n                                                                    <input type=\"text\" name=\"cbxCountry1\" required #cbxCountry1=\"ngModel\" class=\"form-control\" [(ngModel)]=\"editor.pays\" placeholder=\"\" maxlength=\"7\"> <!--  [class.is-invalid]=\"cbxCountry1.invalid && cbxCountry1.touched\" -->\n                                                                    <!--\n                                                                    <select required #cbxCountry1=\"ngModel\" [class.is-invalid]=\"cbxCountry1.invalid && cbxCountry1.touched\" class=\"form-control\" name=\"cbxCountry1\" [(ngModel)]=\"editor.pays\" placeholder=\"\">\n                                                            <option value=\"Canada\" selected>Canada</option>\n                                                          <option <span class=\"required\">*</span>ngFor=\"let item of countries\" value=\"{{item.name}}\" >{{item.name}}</option>\n                                                        </select> -->\n                                                                </div>\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                    <div class=\"col-sm-12 py-2 m-0\">\n                                                        <div class=\"input-group\">\n                                                            <div class=\"input-group-prepend\"><span class=\"name\">Code Postal<span class=\"required\">*</span></span></div>&nbsp;\n                                                            <div style=\"width: 100px\">\n                                                            <input type=\"text\" name=\"txtCPos1\" required #txtCPos1=\"ngModel\" class=\"form-control\" [(ngModel)]=\"editor.codePostal\" placeholder=\"\" maxlength=\"7\"></div> <!--  [class.is-invalid]=\"txtCPos1.invalid && txtCPos1.touched\" -->\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                                <div class=\"col-sm-12 px-3 my-2\">\n                                                    <div class=\"row col-sm-12\">\n                                                        <div class=\"col-sm-4 p-0 m-0 align-content-start\">\n                                                            <div class=\"row input-group m-0 p-0\">\n                                                                <div class=\"input-group-prepend\"><span class=\"name\">Téléphone<span class=\"required\">*</span></span></div>&nbsp;\n                                                                <div class=\"p-0 m-0 border\">\n                                                                    <table class=\"p-0 m-0\" style=\"border:none\"><tr>\n                                                                        <td class=\"p-0 m-0 border\"><input matInput type=\"text\" required #txtPhoneArea1=\"ngModel\" name=\"txtPhoneArea1\" [(ngModel)]=\"phone.prefx\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\"/></td> <!--  [class.is-invalid]=\"txtPhoneArea1.invalid && txtPhoneArea1.touched\" -->\n                                                                        <td class=\"p-0 m-0 border\"><input matInput type=\"text\" required #txtPhone31=\"ngModel\" name=\"txtPhone31\" [(ngModel)]=\"phone.sufx\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\" /></td> <!-- [class.is-invalid]=\"txtPhone31.invalid && txtPhone31.touched\" -->\n                                                                        <td class=\"p-0 m-0\">-</td>\n                                                                        <td class=\"p-0 m-0 border\"><input matInput type=\"text\" required #txtPhone41=\"ngModel\" name=\"txtPhone41\" [(ngModel)]=\"phone.racine\" pattern=\"^\\d{4}$\"  class=\"form-control telephone4 divCentrer\" maxlength=\"4\" size=\"4\" app-numeric numericType=\"number\" /></td> <!-- [class.is-invalid]=\"txtPhone41.invalid && txtPhone41.touched\" -->\n                                                                      </tr></table>\n                                                                </div>\n                                                            </div>\n                                                        </div>\n                                                        <div class=\"col-sm-2 align-middle\">\n\n                                                            <div class=\"input-group p-0 m-0 align-middle\">\n                                                                <div class=\"input-group-prepend p-0 m-0\">\n                                                                  <div class=\"input-group-prepend\"><span>Confidentiel<span class=\"required\">*</span></span></div>&nbsp;\n                                                                  <div class=\"px-4 m-0 align-content-end\">\n                                                                      <input class=\"form-check-input\" type=\"checkbox\" name=\"chkConf1\" [(ngModel)]=\"editor.confidentialPhone\" required #chkConf1=\"ngModel\"> <!--  [class.is-invalid]=\"!chkConf1.value && chkConf1.touched\" -->\n                                                                  </div>\n                                                                </div>\n\n                                                            </div>\n\n                                                        </div>\n                                                        <div class=\"col-sm-2 p-0 m-0 align-content-end\">\n                                                            <div class=\"input-group\">\n                                                                <div class=\"input-group-prepend\"><span class=\"small-name\">Poste</span></div>&nbsp;\n                                                                <input type=\"text\" name=\"txtPoste1\" #txtPoste1=\"ngModel\" class=\"form-control\" [(ngModel)]=\"editor.poste\" placeholder=\"\" maxlength=\"10\">\n                                                            </div>\n                                                        </div>\n                                                        <div class=\"col-sm-2\"></div>\n                                                        <div class=\"col-sm-2\"></div>\n                                                    </div>\n                                                </div>\n                                                <div class=\"col-sm-12 px-3 my-2\">\n                                                    <div class=\"input-group\">\n                                                        <div class=\"input-group-prepend\"><span class=\"name\">Télécopieur</span></div>&nbsp;\n                                                        <div class=\"p-0 m-0 border\">\n                                                            <table class=\"p-0 m-0\" style=\"border:none\"><tr>\n                                                                <td class=\"p-0 m-0 border\"><input matInput type=\"text\" #txtTelecopieurArea1=\"ngModel\" name=\"txtTelecopieurArea1\" [(ngModel)]=\"telecop.prefx\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\"/></td>\n                                                                <td class=\"p-0 m-0 border\"><input matInput type=\"text\" #txtTelecopieur3=\"ngModel\" name=\"txtTelecopieur3\" [(ngModel)]=\"telecop.sufx\" pattern=\"^\\d{3}$\" class=\"form-control telephone3 divCentrer\" maxlength=\"3\" size=\"3\" app-numeric numericType=\"number\" /></td>\n                                                                <td class=\"p-0 m-0\">-</td>\n                                                                <td class=\"p-0 m-0 border\"><input matInput type=\"text\" #txtTelecopieur4=\"ngModel\" name=\"txtTelecopieur4\" [(ngModel)]=\"telecop.racine\" pattern=\"^\\d{4}$\"  class=\"form-control telephone4 divCentrer\" maxlength=\"4\" size=\"4\" app-numeric numericType=\"number\" /></td>\n                                                              </tr></table>\n                                                        </div>\n                                                        <!-- <input type=\"text\" name=\"txtTeleCopieur1\" #txtTeleCopieur1=\"ngModel\" class=\"form-control\" [(ngModel)]=\"editor.telecopieur\" placeholder=\"Telecopieur...\"> -->\n                                                    </div>\n                                                </div>\n                                                <div class=\"col-sm-12 px-3 my-2\">\n                                                    <div class=\"input-group\">\n                                                        <div class=\"input-group-prepend\"><span class=\"name\">Courriel</span></div>&nbsp;\n                                                        <input type=\"text\" name=\"txtCourriel1\" #txtCourriel1=\"ngModel\" class=\"form-control\" [(ngModel)]=\"editor.courriel\" placeholder=\"\" maxlength=\"50\">\n                                                    </div>\n                                                </div>\n                                                <div class=\"col-sm-12 px-3 my-2\">\n                                                    <div class=\"input-group\">\n                                                        <div class=\"input-group-prepend\"><span class=\"name\">Site Internet</span></div>&nbsp;\n                                                        <input type=\"text\" name=\"txtSite1\" #txtSite1=\"ngModel\" class=\"form-control\" [(ngModel)]=\"editor.siteInternet\" placeholder=\"\" maxlength=\"50\">\n                                                    </div>\n                                                </div>\n                                                <div class=\"col-sm-12 px-3 my-2\">\n                                                    <div class=\"input-group border p-2\">\n                                                        <h6>Si vous prévoyez déposer des publications numériques, veuillez cocher le type de publication :</h6>\n                                                        <div class=\"input-group px-5\">\n                                                            <input class=\"form-check-input\" #chkPubLibre1=\"ngModel\" type=\"checkbox\" name=\"chkPubLibre1\" [(ngModel)]=\"editor.extranetPourPublicationAccesLibre\">\n                                                            <label class=\"form-check-label\" for=\"chkPubLibre1\">Publications numériques gratuites (libre d'accès) </label>\n                                                        </div>\n                                                        <div class=\"input-group px-5\">\n                                                            <input class=\"form-check-input\" #chkPubCom1=\"ngModel\" type=\"checkbox\" name=\"chkPubCom1\" [(ngModel)]=\"editor.extranetPourPublicationAccesRestreint\">\n                                                            <label class=\"form-check-label\" for=\"chkPubCom1\">Publications numériques commerciales (accès restreint)</label>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                                <div class=\"col-sm-12 py-2 m-0\">\n                                                    <div class=\"input-group\">\n                                                        <div class=\"input-group-prepend\"><span class=\"name\">Commentaires</span></div>&nbsp;\n                                                        <textarea class=\"form-control\" rows=\"2\" name=\"txtComments1\" #txtComments1=\"ngModel\" [(ngModel)]=\"editor.commentaires\"></textarea>\n                                                    </div>\n                                                    <div style=\"padding-left: 140px;\"><small style=\"font-weight: bold; font-size: 9px; color:grey\">Le contenu ne peut pas excéder 2000 caractères</small></div>\n                                                </div>\n\n                                                <div class=\"col-sm-12 px-10 m-0\"><small style=\"font-weight: bold; font-size: 9px\">* Si votre demande vise à obtenir des numéros ISBN, vos coordonnées paraîtront au Bottin des éditeurs francophones canadiens, sauf le numéro de téléphone si celui-ci est confidentiel.</small></div>\n                                            </div>\n\n\n                                          </div>\n                                    </div>\n                                  </form>\n                                </div>\n\n\n                        </div>\n                        <div class=\"card-footer\">\n                            <div class=\" form-inline d-flex justify-content-end px-5\">\n                                <button type=\"button\" hidden class=\"btn btn-sm btn-dark\"><i class=\"fa fa-ban\"></i>&nbsp;Test Waiting</button>&nbsp;\n                                <button type=\"reset\" mat-button class=\"btn btn-sm btn-danger\" (click)=\"reset()\"><i class=\"fa fa-ban\"></i>&nbsp;Effacer tout</button>&nbsp;\n                                <button type=\"submit\" mat-button class=\"btn btn-sm btn-primary\" data-toggle=\"modal\" (click)=\"submit()\"><i class=\"fa fa-dot-circle-o\"></i>&nbsp;Soumettre</button>&nbsp;<!--  [disabled]=\"frmSignup.form.invalid && registerForm.invalid\" -->\n                                <button type=\"reset\" mat-button class=\"btn btn-sm btn-dark\" (click)=\"back()\"><i class=\"fa fa-plus\"></i>&nbsp;Fermer</button>\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n        </div>\n    </main>\n\n</div>\n<br><br>\n\n<!--\n<div class=\"modal-pnl\" *ngIf=\"isLoading | async\">Loading...\n  <mat-spinner *ngIf=\"isLoading | async\" mode=\"indeterminate\" color=\"warn\" style=\"position: absolute; top: 0; z-index: 100;\"></mat-spinner>\n</div> -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/waiting-box/progress-layer/progress-layer.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/waiting-box/progress-layer/progress-layer.component.html ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"windowed\"\r\n     [ngClass]=\"{'fullscreen': fullscreen}\">\r\n    <div class=\"background\"></div>\r\n    <div class=\"spinner-outer\">\r\n        <div class=\"spinner-inner\">\r\n            <mat-progress-spinner mode=\"indeterminate\" [diameter]=\"diameter\" [color]=\"color\"></mat-progress-spinner>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/waiting-box/waiting-box.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/waiting-box/waiting-box.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div *ngIf=\"visible\" class=\"overlay\">\n  <app-banq-progress-layer [diameter]=\"100\" color=\"accent\"></app-banq-progress-layer>\n</div>\n"

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



/**
 * Composant principal
 */
var AppComponent = /** @class */ (function () {
    function AppComponent(router) {
        this.router = router;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.router.events.subscribe(function (evt) {
            if (!(evt instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"])) {
                return;
            }
            window.scrollTo(0, 0);
        });
    };
    AppComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            // tslint:disable-next-line
            selector: 'body',
            template: "\n  <router-outlet></router-outlet>\n  <app-waiting-box></app-waiting-box>\n"
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _views_error_404_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./views/error/404.component */ "./src/app/views/error/404.component.ts");
/* harmony import */ var _views_error_500_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./views/error/500.component */ "./src/app/views/error/500.component.ts");
/* harmony import */ var _views_login_login_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./views/login/login.component */ "./src/app/views/login/login.component.ts");
/* harmony import */ var _views_register_register_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./views/register/register.component */ "./src/app/views/register/register.component.ts");
/* harmony import */ var ngx_bootstrap_alert__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-bootstrap/alert */ "./node_modules/ngx-bootstrap/alert/fesm5/ngx-bootstrap-alert.js");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-bootstrap/tabs */ "./node_modules/ngx-bootstrap/tabs/fesm5/ngx-bootstrap-tabs.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/fesm5/ng2-charts.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_services_dao_generic_dao_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./core/services/dao/generic-dao.service */ "./src/app/core/services/dao/generic-dao.service.ts");
/* harmony import */ var _core_services_business_generic_manager_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./core/services/business/generic-manager.service */ "./src/app/core/services/business/generic-manager.service.ts");
/* harmony import */ var _core_services_dao_login_dao_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./core/services/dao/login-dao.service */ "./src/app/core/services/dao/login-dao.service.ts");
/* harmony import */ var _core_services_business_login_manager_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./core/services/business/login-manager.service */ "./src/app/core/services/business/login-manager.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _views_alert_dialog_alert_dialog_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./views/alert-dialog/alert-dialog.component */ "./src/app/views/alert-dialog/alert-dialog.component.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _core_shared_utils_auth_guard__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./core/shared/utils/auth.guard */ "./src/app/core/shared/utils/auth.guard.ts");
/* harmony import */ var _views_home_home_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./views/home/home.component */ "./src/app/views/home/home.component.ts");
/* harmony import */ var _core_shared_directives_numeric_directive__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./core/shared/directives/numeric.directive */ "./src/app/core/shared/directives/numeric.directive.ts");
/* harmony import */ var _angular_material_slider__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @angular/material/slider */ "./node_modules/@angular/material/esm5/slider.es5.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/esm5/autocomplete.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _views_depol_depol_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./views/depol/depol.component */ "./src/app/views/depol/depol.component.ts");
/* harmony import */ var _views_home_header_header_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./views/home/header/header.component */ "./src/app/views/home/header/header.component.ts");
/* harmony import */ var _views_home_footer_footer_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./views/home/footer/footer.component */ "./src/app/views/home/footer/footer.component.ts");
/* harmony import */ var _views_home_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./views/home/dashboard/dashboard.component */ "./src/app/views/home/dashboard/dashboard.component.ts");
/* harmony import */ var _views_dossier_dossier_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./views/dossier/dossier.component */ "./src/app/views/dossier/dossier.component.ts");
/* harmony import */ var _views_licence_licence_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./views/licence/licence.component */ "./src/app/views/licence/licence.component.ts");
/* harmony import */ var _views_claim_claim_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./views/claim/claim.component */ "./src/app/views/claim/claim.component.ts");
/* harmony import */ var _views_depol_list_depot_list_depot_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./views/depol/list-depot/list-depot.component */ "./src/app/views/depol/list-depot/list-depot.component.ts");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _views_dmd_isbn_dmd_isbn_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./views/dmd-isbn/dmd-isbn.component */ "./src/app/views/dmd-isbn/dmd-isbn.component.ts");
/* harmony import */ var _views_new_isbn_new_isbn_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./views/new-isbn/new-isbn.component */ "./src/app/views/new-isbn/new-isbn.component.ts");
/* harmony import */ var _views_list_isbn_list_isbn_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./views/list-isbn/list-isbn.component */ "./src/app/views/list-isbn/list-isbn.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _service_work_is_loading__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! @service-work/is-loading */ "./node_modules/@service-work/is-loading/fesm5/service-work-is-loading.js");
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! @angular/material/progress-spinner */ "./node_modules/@angular/material/esm5/progress-spinner.es5.js");
/* harmony import */ var _views_waiting_box_waiting_box_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./views/waiting-box/waiting-box.component */ "./src/app/views/waiting-box/waiting-box.component.ts");
/* harmony import */ var _views_home_join_us_join_us_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./views/home/join-us/join-us.component */ "./src/app/views/home/join-us/join-us.component.ts");
/* harmony import */ var _views_home_help_help_component__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./views/home/help/help.component */ "./src/app/views/home/help/help.component.ts");
/* harmony import */ var _views_licence_edit_licence_edit_licence_component__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./views/licence/edit-licence/edit-licence.component */ "./src/app/views/licence/edit-licence/edit-licence.component.ts");
/* harmony import */ var _views_waiting_box_progress_layer_progress_layer_component__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./views/waiting-box/progress-layer/progress-layer.component */ "./src/app/views/waiting-box/progress-layer/progress-layer.component.ts");






var DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
    suppressScrollX: true
};















































/**
 * Module principal
 */
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"],
                _app_routing__WEBPACK_IMPORTED_MODULE_12__["AppRoutingModule"],
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__["PerfectScrollbarModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_13__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_14__["TabsModule"].forRoot(), _angular_forms__WEBPACK_IMPORTED_MODULE_22__["ReactiveFormsModule"],
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_24__["ModalModule"].forRoot(), ngx_bootstrap_alert__WEBPACK_IMPORTED_MODULE_11__["AlertModule"].forRoot(), _angular_common_http__WEBPACK_IMPORTED_MODULE_16__["HttpClientModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_15__["ChartsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_22__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_16__["HttpClientModule"], _angular_router__WEBPACK_IMPORTED_MODULE_17__["RouterModule"], angular_datatables__WEBPACK_IMPORTED_MODULE_41__["DataTablesModule"],
                _angular_material_slider__WEBPACK_IMPORTED_MODULE_28__["MatSliderModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_30__["MatInputModule"], _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_29__["MatAutocompleteModule"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_31__["MatFormFieldModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_32__["MatMenuModule"], _service_work_is_loading__WEBPACK_IMPORTED_MODULE_46__["IsLoadingModule"], _angular_material__WEBPACK_IMPORTED_MODULE_45__["MatProgressBarModule"],
                _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_47__["MatProgressSpinnerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_45__["MatSelectModule"]
            ],
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"], _views_error_404_component__WEBPACK_IMPORTED_MODULE_7__["P404Component"], _views_error_500_component__WEBPACK_IMPORTED_MODULE_8__["P500Component"],
                _views_home_header_header_component__WEBPACK_IMPORTED_MODULE_34__["HeaderComponent"], _views_home_footer_footer_component__WEBPACK_IMPORTED_MODULE_35__["FooterComponent"],
                _views_login_login_component__WEBPACK_IMPORTED_MODULE_9__["LoginComponent"], _views_register_register_component__WEBPACK_IMPORTED_MODULE_10__["RegisterComponent"], _views_alert_dialog_alert_dialog_component__WEBPACK_IMPORTED_MODULE_23__["AlertDialogComponent"], _views_home_home_component__WEBPACK_IMPORTED_MODULE_26__["HomeComponent"], _core_shared_directives_numeric_directive__WEBPACK_IMPORTED_MODULE_27__["NumericDirective"], _views_home_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_36__["DashboardComponent"], _views_depol_depol_component__WEBPACK_IMPORTED_MODULE_33__["DepolComponent"],
                _views_dossier_dossier_component__WEBPACK_IMPORTED_MODULE_37__["DossierComponent"], _views_licence_licence_component__WEBPACK_IMPORTED_MODULE_38__["LicenceComponent"], _views_claim_claim_component__WEBPACK_IMPORTED_MODULE_39__["ClaimComponent"], _views_depol_list_depot_list_depot_component__WEBPACK_IMPORTED_MODULE_40__["ListDepotComponent"], _views_dmd_isbn_dmd_isbn_component__WEBPACK_IMPORTED_MODULE_42__["DmdIsbnComponent"], _views_new_isbn_new_isbn_component__WEBPACK_IMPORTED_MODULE_43__["NewIsbnComponent"], _views_list_isbn_list_isbn_component__WEBPACK_IMPORTED_MODULE_44__["ListIsbnComponent"],
                _views_waiting_box_waiting_box_component__WEBPACK_IMPORTED_MODULE_48__["WaitingBoxComponent"], _views_home_join_us_join_us_component__WEBPACK_IMPORTED_MODULE_49__["JoinUsComponent"], _views_home_help_help_component__WEBPACK_IMPORTED_MODULE_50__["HelpComponent"], _views_licence_edit_licence_edit_licence_component__WEBPACK_IMPORTED_MODULE_51__["EditLicenceComponent"], _views_waiting_box_progress_layer_progress_layer_component__WEBPACK_IMPORTED_MODULE_52__["ProgressLayerComponent"]
            ],
            providers: [_core_services_dao_generic_dao_service__WEBPACK_IMPORTED_MODULE_18__["GenericDaoService"], _core_services_business_generic_manager_service__WEBPACK_IMPORTED_MODULE_19__["GenericManagerService"], _core_services_dao_login_dao_service__WEBPACK_IMPORTED_MODULE_20__["LoginDaoService"], _core_services_business_login_manager_service__WEBPACK_IMPORTED_MODULE_21__["LoginManagerService"], _core_shared_utils_auth_guard__WEBPACK_IMPORTED_MODULE_25__["AuthGuard"],
                { provide: _angular_common__WEBPACK_IMPORTED_MODULE_3__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_3__["PathLocationStrategy"] }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: routes, AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _views_error_404_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./views/error/404.component */ "./src/app/views/error/404.component.ts");
/* harmony import */ var _views_error_500_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./views/error/500.component */ "./src/app/views/error/500.component.ts");
/* harmony import */ var _views_login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./views/login/login.component */ "./src/app/views/login/login.component.ts");
/* harmony import */ var _views_register_register_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./views/register/register.component */ "./src/app/views/register/register.component.ts");
/* harmony import */ var _core_shared_utils_auth_guard__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./core/shared/utils/auth.guard */ "./src/app/core/shared/utils/auth.guard.ts");
/* harmony import */ var _views_home_home_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./views/home/home.component */ "./src/app/views/home/home.component.ts");
/* harmony import */ var _views_depol_depol_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./views/depol/depol.component */ "./src/app/views/depol/depol.component.ts");
/* harmony import */ var _views_home_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./views/home/dashboard/dashboard.component */ "./src/app/views/home/dashboard/dashboard.component.ts");
/* harmony import */ var _views_dossier_dossier_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./views/dossier/dossier.component */ "./src/app/views/dossier/dossier.component.ts");
/* harmony import */ var _views_licence_licence_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./views/licence/licence.component */ "./src/app/views/licence/licence.component.ts");
/* harmony import */ var _views_claim_claim_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./views/claim/claim.component */ "./src/app/views/claim/claim.component.ts");
/* harmony import */ var _views_depol_list_depot_list_depot_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./views/depol/list-depot/list-depot.component */ "./src/app/views/depol/list-depot/list-depot.component.ts");
/* harmony import */ var _views_list_isbn_list_isbn_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./views/list-isbn/list-isbn.component */ "./src/app/views/list-isbn/list-isbn.component.ts");
/* harmony import */ var _views_new_isbn_new_isbn_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./views/new-isbn/new-isbn.component */ "./src/app/views/new-isbn/new-isbn.component.ts");
/* harmony import */ var _views_dmd_isbn_dmd_isbn_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./views/dmd-isbn/dmd-isbn.component */ "./src/app/views/dmd-isbn/dmd-isbn.component.ts");
/* harmony import */ var _views_home_join_us_join_us_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./views/home/join-us/join-us.component */ "./src/app/views/home/join-us/join-us.component.ts");
/* harmony import */ var _views_home_help_help_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./views/home/help/help.component */ "./src/app/views/home/help/help.component.ts");
/* harmony import */ var _views_licence_edit_licence_edit_licence_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./views/licence/edit-licence/edit-licence.component */ "./src/app/views/licence/edit-licence/edit-licence.component.ts");





















var routes = [
    {
        path: '',
        redirectTo: 'home/dashboard',
        pathMatch: 'full',
    },
    {
        path: '404',
        component: _views_error_404_component__WEBPACK_IMPORTED_MODULE_3__["P404Component"],
        data: {
            title: 'Page 404'
        }
    },
    {
        path: '500',
        component: _views_error_500_component__WEBPACK_IMPORTED_MODULE_4__["P500Component"],
        data: {
            title: 'Page 500'
        }
    },
    {
        path: 'home',
        component: _views_home_home_component__WEBPACK_IMPORTED_MODULE_8__["HomeComponent"],
        canActivate: [_core_shared_utils_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]],
        data: {
            title: 'Home'
        },
        children: [
            {
                path: 'dashboard',
                component: _views_home_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_10__["DashboardComponent"]
            },
            {
                path: 'depol',
                component: _views_depol_depol_component__WEBPACK_IMPORTED_MODULE_9__["DepolComponent"],
                data: {
                    title: 'Depot Legal'
                },
            },
            {
                path: 'list-depot',
                component: _views_depol_list_depot_list_depot_component__WEBPACK_IMPORTED_MODULE_14__["ListDepotComponent"],
                data: {
                    title: 'Liste des Depots'
                },
            },
            {
                path: 'licences',
                component: _views_licence_licence_component__WEBPACK_IMPORTED_MODULE_12__["LicenceComponent"]
            },
            {
                path: 'dossier',
                component: _views_dossier_dossier_component__WEBPACK_IMPORTED_MODULE_11__["DossierComponent"]
            },
            {
                path: 'claim',
                component: _views_claim_claim_component__WEBPACK_IMPORTED_MODULE_13__["ClaimComponent"]
            },
            {
                path: 'dmd-isbn',
                component: _views_dmd_isbn_dmd_isbn_component__WEBPACK_IMPORTED_MODULE_17__["DmdIsbnComponent"]
            },
            {
                path: 'new-isbn',
                component: _views_new_isbn_new_isbn_component__WEBPACK_IMPORTED_MODULE_16__["NewIsbnComponent"]
            },
            {
                path: 'list-isbn',
                component: _views_list_isbn_list_isbn_component__WEBPACK_IMPORTED_MODULE_15__["ListIsbnComponent"]
            },
            {
                path: 'join-us',
                component: _views_home_join_us_join_us_component__WEBPACK_IMPORTED_MODULE_18__["JoinUsComponent"]
            },
            {
                path: 'help',
                component: _views_home_help_help_component__WEBPACK_IMPORTED_MODULE_19__["HelpComponent"]
            },
            {
                path: 'edit-licence',
                component: _views_licence_edit_licence_edit_licence_component__WEBPACK_IMPORTED_MODULE_20__["EditLicenceComponent"]
            },
        ]
    },
    {
        path: 'login',
        component: _views_login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"],
        data: {
            title: 'Login Page'
        }
    },
    {
        path: 'register',
        component: _views_register_register_component__WEBPACK_IMPORTED_MODULE_6__["RegisterComponent"],
        data: {
            title: 'Register Page'
        }
    },
    { path: '**', component: _views_error_404_component__WEBPACK_IMPORTED_MODULE_3__["P404Component"] }
];
/**
 * Routage principal
 */
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { useHash: false })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/core/data/editor.ts":
/*!*************************************!*\
  !*** ./src/app/core/data/editor.ts ***!
  \*************************************/
/*! exports provided: Editor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Editor", function() { return Editor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _lang__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./lang */ "./src/app/core/data/lang.ts");
/* harmony import */ var _shared_utils_serializable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/utils/serializable */ "./src/app/core/shared/utils/serializable.ts");



/**
 * Classe representant un Editeur
 */
var Editor = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](Editor, _super);
    /**
     * Constructeur par defaut permettant d'initialiser un editeur
     */
    function Editor() {
        var _this = _super.call(this) || this;
        _this.idEditeur = null;
        _this.langue = new _lang__WEBPACK_IMPORTED_MODULE_1__["Lang"]();
        _this.repondantPrincipal = null;
        _this.editeurRefExterne = null;
        _this.idEditeurAssocie = null;
        _this.code = '';
        _this.nom = '';
        _this.adresse1 = '';
        _this.adresse2 = '';
        _this.ville = '';
        _this.provinceTerritoire = 'Québec';
        _this.pays = 'Canada';
        _this.codePostal = '';
        _this.telephone = '';
        _this.poste = '';
        _this.telecopieur = '';
        _this.courriel = '';
        _this.siteInternet = '';
        _this.commentaires = '';
        _this.editeurQuebecois = 'O';
        _this.editeurActif = 'O';
        _this.paraitBottin = 'N';
        _this.estEditeur = 'O';
        _this.principal = 'N';
        _this.confidentialPhone = false;
        _this.extranetPourPublicationAccesLibre = false;
        _this.extranetPourPublicationAccesRestreint = false;
        _this.repondants = new Array();
        return _this;
    }
    return Editor;
}(_shared_utils_serializable__WEBPACK_IMPORTED_MODULE_2__["Serializable"]));



/***/ }),

/***/ "./src/app/core/data/lang.ts":
/*!***********************************!*\
  !*** ./src/app/core/data/lang.ts ***!
  \***********************************/
/*! exports provided: Lang */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lang", function() { return Lang; });
/**
 * Classe decrivant la langue d'un' editeur
 */
var Lang = /** @class */ (function () {
    function Lang(idLangue, code, description) {
        this.idLangue = idLangue;
        this.code = code;
        this.description = description;
        this.idLangue = 0;
        this.code = '0';
        this.description = 'Français';
    }
    Lang.ctorParameters = function () { return [
        { type: Number },
        { type: String },
        { type: String }
    ]; };
    return Lang;
}());



/***/ }),

/***/ "./src/app/core/data/respondent.ts":
/*!*****************************************!*\
  !*** ./src/app/core/data/respondent.ts ***!
  \*****************************************/
/*! exports provided: Respondent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Respondent", function() { return Respondent; });
/**
 * Classe representant un Repondant
 */
var Respondent = /** @class */ (function () {
    /**
     * Constructeur par defaut permettant d'initialiser un Repondant
     */
    function Respondent() {
        this.idRepondant = null;
        this.titre = 'M.';
        this.nom = '';
        this.prenom = '';
        this.courriel = '';
        this.telephone = '';
        this.poste = '';
        this.repondantRefExterne = null;
        this.principal = false;
        this.dupliquer = false;
    }
    return Respondent;
}());



/***/ }),

/***/ "./src/app/core/data/user.ts":
/*!***********************************!*\
  !*** ./src/app/core/data/user.ts ***!
  \***********************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
/**
 * Classe representant un utilisateur de l'extranet
 */
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());



/***/ }),

/***/ "./src/app/core/enums/alert-type.enum.ts":
/*!***********************************************!*\
  !*** ./src/app/core/enums/alert-type.enum.ts ***!
  \***********************************************/
/*! exports provided: AlertType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertType", function() { return AlertType; });
/**
 * Enumeration des Types de notifications
 */
var AlertType;
(function (AlertType) {
    AlertType[AlertType["info"] = 0] = "info";
    AlertType[AlertType["error"] = 1] = "error";
    AlertType[AlertType["warning"] = 2] = "warning";
    AlertType[AlertType["confirm"] = 3] = "confirm";
    AlertType[AlertType["success"] = 4] = "success";
})(AlertType || (AlertType = {}));


/***/ }),

/***/ "./src/app/core/models/alert.ts":
/*!**************************************!*\
  !*** ./src/app/core/models/alert.ts ***!
  \**************************************/
/*! exports provided: Alert */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Alert", function() { return Alert; });
/* harmony import */ var _enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../enums/alert-type.enum */ "./src/app/core/enums/alert-type.enum.ts");

/**
 * Classe representant les notifications a afficher aux utilisateurs
 */
var Alert = /** @class */ (function () {
    /**
     * Constructeur permettant d'initialiser une notification
     * @param msg Message
     * @param title Titre de la boite de dialogue
     * @param type Type de notification
     * @param display Visible/non
     */
    function Alert(msg, title, type, display) {
        if (title === void 0) { title = 'info'; }
        if (type === void 0) { type = _enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_0__["AlertType"].info; }
        if (display === void 0) { display = false; }
        this.msg = msg;
        this.title = title;
        this.type = type;
        this.display = display;
    }
    Alert.prototype.setType = function (type) {
        this.type = type;
        this.title = _enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_0__["AlertType"][this.type].toString().toUpperCase();
    };
    /**
     * Affiche une notification a l'utilisateur
     * @param type Type de notification
     * @param msg Message de la notification
     */
    Alert.prototype.displayAlert = function (type, msg) {
        this.msg = msg;
        this.setType(type);
        this.show();
    };
    /**
     * Afiche la notification
     */
    Alert.prototype.show = function () {
        this.display = true;
    };
    /**
     * Ferme la notification
     */
    Alert.prototype.hide = function () {
        this.display = false;
    };
    Alert.ctorParameters = function () { return [
        { type: String },
        { type: String },
        { type: undefined },
        { type: Boolean }
    ]; };
    return Alert;
}());



/***/ }),

/***/ "./src/app/core/models/login-request.ts":
/*!**********************************************!*\
  !*** ./src/app/core/models/login-request.ts ***!
  \**********************************************/
/*! exports provided: LoginRequest */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginRequest", function() { return LoginRequest; });
/**
 * Modele de donnees d'une requete de demande d'authentification
 */
var LoginRequest = /** @class */ (function () {
    function LoginRequest(username, password) {
        this.username = username;
        this.password = password;
    }
    LoginRequest.ctorParameters = function () { return [
        { type: String },
        { type: String }
    ]; };
    return LoginRequest;
}());



/***/ }),

/***/ "./src/app/core/services/business/generic-manager.service.ts":
/*!*******************************************************************!*\
  !*** ./src/app/core/services/business/generic-manager.service.ts ***!
  \*******************************************************************/
/*! exports provided: GenericManagerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenericManagerService", function() { return GenericManagerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _dao_generic_dao_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../dao/generic-dao.service */ "./src/app/core/services/dao/generic-dao.service.ts");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! util */ "./node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_3__);




/**
 * Implementation du Service de fourniture des donnees generiques de l'application
 */
var GenericManagerService = /** @class */ (function () {
    function GenericManagerService(genericDAO) {
        this.genericDAO = genericDAO;
    }
    /**
     * Fournit la liste des Villes du Canada
     */
    GenericManagerService.prototype.loadCities = function () {
        var _this = this;
        var cities = [];
        this.errorMsg = null;
        this.genericDAO.loadCitiesJSONFile().subscribe(function (data) { for (var i = 0; i < data.length; i++) {
            cities.push(data[i]);
        } }, function (error) { return _this.errorMsg = error; });
        if (!Object(util__WEBPACK_IMPORTED_MODULE_3__["isNullOrUndefined"])(this.errorMsg)) {
            throw new Error(this.errorMsg);
        }
        return cities;
    };
    /**
     * Fournit la liste des pays
     */
    GenericManagerService.prototype.loadCountries = function () {
        var _this = this;
        var countries = [];
        this.errorMsg = null;
        this.genericDAO.loadCountriesJSONFile().subscribe(function (data) { for (var i = 0; i < data.length; i++) {
            countries.push(data[i]);
        } }, function (error) { return _this.errorMsg = error; });
        if (!Object(util__WEBPACK_IMPORTED_MODULE_3__["isNullOrUndefined"])(this.errorMsg)) {
            throw new Error(this.errorMsg);
        }
        return countries;
    };
    /**
     * Fournit la liste des provinces et Territoires
     */
    GenericManagerService.prototype.loadStates = function () {
        var _this = this;
        var states = [];
        this.errorMsg = null;
        this.genericDAO.loadStatesJSONFile().subscribe(function (data) { for (var i = 0; i < data.length; i++) {
            states.push(data[i]);
        } }, function (error) { return _this.errorMsg = error; });
        if (!Object(util__WEBPACK_IMPORTED_MODULE_3__["isNullOrUndefined"])(this.errorMsg)) {
            throw new Error(this.errorMsg);
        }
        return states;
    };
    GenericManagerService.ctorParameters = function () { return [
        { type: _dao_generic_dao_service__WEBPACK_IMPORTED_MODULE_2__["GenericDaoService"] }
    ]; };
    GenericManagerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_dao_generic_dao_service__WEBPACK_IMPORTED_MODULE_2__["GenericDaoService"]])
    ], GenericManagerService);
    return GenericManagerService;
}());



/***/ }),

/***/ "./src/app/core/services/business/login-manager.service.ts":
/*!*****************************************************************!*\
  !*** ./src/app/core/services/business/login-manager.service.ts ***!
  \*****************************************************************/
/*! exports provided: LoginManagerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginManagerService", function() { return LoginManagerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_editor__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../data/editor */ "./src/app/core/data/editor.ts");
/* harmony import */ var _data_user__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../data/user */ "./src/app/core/data/user.ts");
/* harmony import */ var _dao_login_dao_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../dao/login-dao.service */ "./src/app/core/services/dao/login-dao.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






/**
 * Implementation du Service de gestion de l'authentification
 */
var LoginManagerService = /** @class */ (function () {
    /**
     * Constructeur d'Initialisation
     * @param loginDAO Service DAO de gestion de l'authentification
     * @param router Service de Routage
     */
    function LoginManagerService(loginDAO, router) {
        this.loginDAO = loginDAO;
        this.router = router;
    }
    /**
     * Authentification d'un utilisateur
     */
    LoginManagerService.prototype.login = function (request) {
        var loginResponse;
        this.errorMsg = null;
        /*this.loginDAO.callLoginAPI(request).subscribe( response => loginResponse = response, error => this.errorMsg = error );
        if ( !isNullOrUndefined(this.errorMsg) ) { throw new Error(this.errorMsg); }
        localStorage.setItem('token', loginResponse.token);*/
        return new _data_user__WEBPACK_IMPORTED_MODULE_3__["User"]();
    };
    /**
     * Inscription d'un utilisateur
     */
    LoginManagerService.prototype.register = function (request) {
        var editor = new _data_editor__WEBPACK_IMPORTED_MODULE_2__["Editor"]();
        this.errorMsg = null;
        /*this.loginDAO.callRegisterAPI( request ).subscribe(response =>  {editor = new Editor().fillFromJSON( JSON.stringify(response) ); },
                                                              error => this.errorMsg = (error as string) );
        if ( !isNullOrUndefined(this.errorMsg) ) { throw new Error(this.errorMsg); }*/
        return editor;
    };
    /**
     * Reinitialisation du not de passe d'un utilisateur
     */
    LoginManagerService.prototype.resetPassword = function (request) {
        throw new Error('Method not implemented.');
    };
    /**
     * Deconnexion a l'application
     */
    LoginManagerService.prototype.logout = function () {
        localStorage.removeItem('token');
        localStorage.removeItem('uid');
        this.loginDAO.logout();
        // this.router.navigate(['/']);
    };
    LoginManagerService.prototype.checkShibbolethAuthentification = function () {
        var res = null;
        this.loginDAO.getUid().subscribe(function (response) {
            res = response.toString();
            return res;
        }, function (error) {
            console.log(error);
        });
        return res;
    };
    LoginManagerService.ctorParameters = function () { return [
        { type: _dao_login_dao_service__WEBPACK_IMPORTED_MODULE_4__["LoginDaoService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
    ]; };
    LoginManagerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_dao_login_dao_service__WEBPACK_IMPORTED_MODULE_4__["LoginDaoService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], LoginManagerService);
    return LoginManagerService;
}());



/***/ }),

/***/ "./src/app/core/services/dao/generic-dao.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/core/services/dao/generic-dao.service.ts ***!
  \**********************************************************/
/*! exports provided: GenericDaoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenericDaoService", function() { return GenericDaoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



/**
 * DAO du service de fourniture des donnees Generiques de l'application
 */
var GenericDaoService = /** @class */ (function () {
    /**
     * Injection du Fournisseur Http dans le constructeur
     * @param http Fournisseur Client Http
     */
    function GenericDaoService(http) {
        this.http = http;
    }
    /**
     * Retourne la liste des villes
     */
    GenericDaoService.prototype.loadCitiesJSONFile = function () {
        return this.http.get('/assets/ca-cities.json');
    };
    /**
     * Retourne la liste des Pays
     */
    GenericDaoService.prototype.loadCountriesJSONFile = function () {
        return this.http.get('/assets/countries.json');
    };
    /**
     * Retourne la liste des Provinces
     */
    GenericDaoService.prototype.loadStatesJSONFile = function () {
        return this.http.get('/assets/states.json');
    };
    GenericDaoService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    GenericDaoService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], GenericDaoService);
    return GenericDaoService;
}());



/***/ }),

/***/ "./src/app/core/services/dao/login-dao.service.ts":
/*!********************************************************!*\
  !*** ./src/app/core/services/dao/login-dao.service.ts ***!
  \********************************************************/
/*! exports provided: LoginDaoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginDaoService", function() { return LoginDaoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/catch */ "./node_modules/rxjs-compat/_esm5/add/operator/catch.js");
/* harmony import */ var rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/observable/throw */ "./node_modules/rxjs-compat/_esm5/add/observable/throw.js");
/* harmony import */ var _shared_utils_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/utils/helper */ "./src/app/core/shared/utils/helper.ts");
/* harmony import */ var _assets_config_json__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../assets/config.json */ "./src/assets/config.json");
var _assets_config_json__WEBPACK_IMPORTED_MODULE_6___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../assets/config.json */ "./src/assets/config.json", 1);







/**
 * DAO du service de gestion de l'authentification
 */
var LoginDaoService = /** @class */ (function () {
    /**
     * Injection du fournisseur http dans le constructeur
     * @param http Fournisseur Client Http
     */
    function LoginDaoService(http) {
        this.http = http;
        /**
         * URL de base des APIs backend
         */
        this.apiBaseUrl = _assets_config_json__WEBPACK_IMPORTED_MODULE_6__["backOfficeUrlApi"] + '/ext/login';
    }
    /**
     * Appel du webservice d'auhentification
     */
    LoginDaoService.prototype.callLoginAPI = function (request) {
        return Object(_shared_utils_helper__WEBPACK_IMPORTED_MODULE_5__["callRestAPI"])(this.http, this.apiBaseUrl + '/signin', 'post', request);
    };
    /**
     * Appel du webservice de souscription
     */
    LoginDaoService.prototype.callRegisterAPI = function (request) {
        return Object(_shared_utils_helper__WEBPACK_IMPORTED_MODULE_5__["callRestAPI"])(this.http, this.apiBaseUrl + '/signup', 'post', request);
    };
    /**
     * Appel du webservice de recherche de la liste des Editeurs
     */
    LoginDaoService.prototype.callListEditorsAPI = function (page, size) {
        return Object(_shared_utils_helper__WEBPACK_IMPORTED_MODULE_5__["callRestAPI"])(this.http, this.apiBaseUrl + '/lister?page=' + page + '&size=' + size, 'get');
    };
    /**
     * Appel du webservice de recherche d'un editeur
     */
    LoginDaoService.prototype.callFindEditorAPI = function (id) {
        return Object(_shared_utils_helper__WEBPACK_IMPORTED_MODULE_5__["callRestAPI"])(this.http, this.apiBaseUrl + '/' + id, 'get');
    };
    /**
     * Recupere l'identifiant utilisateur laisse par l'authentification Shibboleth
     */
    LoginDaoService.prototype.getUid = function () {
        return this.http.get(_assets_config_json__WEBPACK_IMPORTED_MODULE_6__["extranetUrlApi"] + '/getUid', { headers: Object(_shared_utils_helper__WEBPACK_IMPORTED_MODULE_5__["buildHttpHeaders"])(), responseType: 'text' })
            .catch(function (error) { return Object(_shared_utils_helper__WEBPACK_IMPORTED_MODULE_5__["handleHttpException"])(error); });
    };
    /**
     * Supprime l'identifiant utilisateur laisse par l'authentification Shibboleth
     */
    LoginDaoService.prototype.logout = function () {
        this.http.post(_assets_config_json__WEBPACK_IMPORTED_MODULE_6__["extranetUrlApi"] + '/logout', { headers: Object(_shared_utils_helper__WEBPACK_IMPORTED_MODULE_5__["buildHttpHeaders"])() }).catch(function (error) { return Object(_shared_utils_helper__WEBPACK_IMPORTED_MODULE_5__["handleHttpException"])(error); });
        this.http.post(_assets_config_json__WEBPACK_IMPORTED_MODULE_6__["logoutUrl"], { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set('Content-Type', '*/*') })
            .catch(function (error) { return Object(_shared_utils_helper__WEBPACK_IMPORTED_MODULE_5__["handleHttpException"])(error); });
    };
    LoginDaoService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    LoginDaoService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], LoginDaoService);
    return LoginDaoService;
}());



/***/ }),

/***/ "./src/app/core/shared/directives/numeric.directive.ts":
/*!*************************************************************!*\
  !*** ./src/app/core/shared/directives/numeric.directive.ts ***!
  \*************************************************************/
/*! exports provided: NumericDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NumericDirective", function() { return NumericDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


/**
 * Directive permettant d'empecher la saisie d'autres caracteres que des caracteres numeriques dans une zone de texte
 */
var NumericDirective = /** @class */ (function () {
    function NumericDirective(el) {
        this.el = el;
        this.regex = {
            number: new RegExp(/^\d+$/),
            decimal: new RegExp(/^[0-9]+(\.[0-9]*){0,1}$/g)
        };
        this.specialKeys = {
            number: ['Backspace', 'Tab', 'End', 'Home', 'ArrowLeft', 'ArrowRight'],
            decimal: ['Backspace', 'Tab', 'End', 'Home', 'ArrowLeft', 'ArrowRight'],
        };
    }
    NumericDirective.prototype.onKeyDown = function (event) {
        if (this.specialKeys[this.numericType].indexOf(event.key) !== -1) {
            return;
        }
        // Do not use event.keycode this is deprecated.
        // See: https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/keyCode
        var current = this.el.nativeElement.value;
        var next = current.concat(event.key);
        if (next && !String(next).match(this.regex[this.numericType])) {
            event.preventDefault();
        }
    };
    NumericDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('numericType'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], NumericDirective.prototype, "numericType", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('keydown', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [KeyboardEvent]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], NumericDirective.prototype, "onKeyDown", null);
    NumericDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[app-numeric]'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
    ], NumericDirective);
    return NumericDirective;
}());



/***/ }),

/***/ "./src/app/core/shared/mock/cities.ts":
/*!********************************************!*\
  !*** ./src/app/core/shared/mock/cities.ts ***!
  \********************************************/
/*! exports provided: CITIES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CITIES", function() { return CITIES; });
/**
 * Liste des villes du canada
 * (Donnees brutes)
 */
var CITIES = [
    {
        'city': 'Toronto',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '3934421',
        'iso2': 'CA',
        'capital': 'admin',
        'lat': '43.666667',
        'lng': '-79.416667',
        'population': '5213000'
    },
    {
        'city': 'Montreal',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '2356556',
        'iso2': 'CA',
        'capital': '',
        'lat': '45.5',
        'lng': '-73.583333',
        'population': '3678000'
    },
    {
        'city': 'Vancouver',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '603502',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.25',
        'lng': '-123.133333',
        'population': '2313328'
    },
    {
        'city': 'Ottawa',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '812129',
        'iso2': 'CA',
        'capital': 'primary',
        'lat': '45.416667',
        'lng': '-75.7',
        'population': '1145000'
    },
    {
        'city': 'Calgary',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '915322',
        'iso2': 'CA',
        'capital': '',
        'lat': '51.083333',
        'lng': '-114.083333',
        'population': '1110000'
    },
    {
        'city': 'Edmonton',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '712391',
        'iso2': 'CA',
        'capital': 'admin',
        'lat': '53.55',
        'lng': '-113.5',
        'population': '1058000'
    },
    {
        'city': 'Hamilton',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '519949',
        'iso2': 'CA',
        'capital': '',
        'lat': '43.256101',
        'lng': '-79.857484',
        'population': '721053'
    },
    {
        'city': 'Winnipeg',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '575313',
        'iso2': 'CA',
        'capital': 'admin',
        'lat': '49.883333',
        'lng': '-97.166667',
        'population': '632063'
    },
    {
        'city': 'Quebec',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '528595',
        'iso2': 'CA',
        'capital': 'admin',
        'lat': '46.8',
        'lng': '-71.25',
        'population': '624177'
    },
    {
        'city': 'Oshawa',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '247989',
        'iso2': 'CA',
        'capital': '',
        'lat': '43.9',
        'lng': '-78.866667',
        'population': '450963'
    },
    {
        'city': 'Kitchener',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '409112',
        'iso2': 'CA',
        'capital': '',
        'lat': '43.446976',
        'lng': '-80.472484',
        'population': '417001'
    },
    {
        'city': 'Halifax',
        'admin': 'Nova Scotia',
        'country': 'Canada',
        'population_proper': '222874',
        'iso2': 'CA',
        'capital': 'admin',
        'lat': '44.65',
        'lng': '-63.6',
        'population': '359111'
    },
    {
        'city': 'London',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '335035',
        'iso2': 'CA',
        'capital': '',
        'lat': '42.983333',
        'lng': '-81.25',
        'population': '346765'
    },
    {
        'city': 'Windsor',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '210891',
        'iso2': 'CA',
        'capital': '',
        'lat': '42.301649',
        'lng': '-83.030744',
        'population': '319246'
    },
    {
        'city': 'Victoria',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '251358',
        'iso2': 'CA',
        'capital': 'admin',
        'lat': '48.450234',
        'lng': '-123.343529',
        'population': '289625'
    },
    {
        'city': 'Saskatoon',
        'admin': 'Saskatchewan',
        'country': 'Canada',
        'population_proper': '189193',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.133333',
        'lng': '-106.666667',
        'population': '198958'
    },
    {
        'city': 'Barrie',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '119732',
        'iso2': 'CA',
        'capital': '',
        'lat': '44.383333',
        'lng': '-79.7',
        'population': '182041'
    },
    {
        'city': 'Regina',
        'admin': 'Saskatchewan',
        'country': 'Canada',
        'population_proper': '176183',
        'iso2': 'CA',
        'capital': 'admin',
        'lat': '50.45',
        'lng': '-104.616667',
        'population': '176183'
    },
    {
        'city': 'Sudbury',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '80507',
        'iso2': 'CA',
        'capital': '',
        'lat': '46.5',
        'lng': '-80.966667',
        'population': '157857'
    },
    {
        'city': 'Abbotsford',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '151683',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.05',
        'lng': '-122.3',
        'population': '151683'
    },
    {
        'city': 'Sarnia',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '82998',
        'iso2': 'CA',
        'capital': '',
        'lat': '42.978417',
        'lng': '-82.388177',
        'population': '144172'
    },
    {
        'city': 'Sherbrooke',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '129447',
        'iso2': 'CA',
        'capital': '',
        'lat': '45.4',
        'lng': '-71.9',
        'population': '139652'
    },
    {
        'city': 'Saint John\u2019s',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '99182',
        'iso2': 'CA',
        'capital': 'admin',
        'lat': '47.55',
        'lng': '-52.666667',
        'population': '131469'
    },
    {
        'city': 'Kelowna',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '95306',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.9',
        'lng': '-119.483333',
        'population': '125109'
    },
    {
        'city': 'Trois-Rivi\u00e8res',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '116409',
        'iso2': 'CA',
        'capital': '',
        'lat': '46.35',
        'lng': '-72.55',
        'population': '119693'
    },
    {
        'city': 'Kingston',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '102400',
        'iso2': 'CA',
        'capital': '',
        'lat': '44.3',
        'lng': '-76.566667',
        'population': '114195'
    },
    {
        'city': 'Thunder Bay',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '97374',
        'iso2': 'CA',
        'capital': '',
        'lat': '48.4',
        'lng': '-89.233333',
        'population': '99334'
    },
    {
        'city': 'Moncton',
        'admin': 'New Brunswick',
        'country': 'Canada',
        'population_proper': '87467',
        'iso2': 'CA',
        'capital': '',
        'lat': '46.09652',
        'lng': '-64.79757',
        'population': '90635'
    },
    {
        'city': 'Saint John',
        'admin': 'New Brunswick',
        'country': 'Canada',
        'population_proper': '54449',
        'iso2': 'CA',
        'capital': '',
        'lat': '45.230798',
        'lng': '-66.095316',
        'population': '87857'
    },
    {
        'city': 'Nanaimo',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '80491',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.15',
        'lng': '-123.916667',
        'population': '84905'
    },
    {
        'city': 'Peterborough',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '75877',
        'iso2': 'CA',
        'capital': '',
        'lat': '44.3',
        'lng': '-78.333333',
        'population': '83627'
    },
    {
        'city': 'Saint-Jer\u00f4me',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '54948',
        'iso2': 'CA',
        'capital': '',
        'lat': '45.766667',
        'lng': '-74.0',
        'population': '78439'
    },
    {
        'city': 'Red Deer',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '73593',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.266667',
        'lng': '-113.8',
        'population': '74857'
    },
    {
        'city': 'Lethbridge',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '58571',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.7',
        'lng': '-112.833333',
        'population': '70617'
    },
    {
        'city': 'Kamloops',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '68628',
        'iso2': 'CA',
        'capital': '',
        'lat': '50.666667',
        'lng': '-120.333333',
        'population': '68714'
    },
    {
        'city': 'Prince George',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '62707',
        'iso2': 'CA',
        'capital': '',
        'lat': '53.916667',
        'lng': '-122.766667',
        'population': '65558'
    },
    {
        'city': 'Medicine Hat',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '53626',
        'iso2': 'CA',
        'capital': '',
        'lat': '50.033333',
        'lng': '-110.683333',
        'population': '63138'
    },
    {
        'city': 'Drummondville',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '54123',
        'iso2': 'CA',
        'capital': '',
        'lat': '45.883333',
        'lng': '-72.483333',
        'population': '59489'
    },
    {
        'city': 'Chicoutimi',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '53940',
        'iso2': 'CA',
        'capital': '',
        'lat': '48.45',
        'lng': '-71.066667',
        'population': '53940'
    },
    {
        'city': 'Fredericton',
        'admin': 'New Brunswick',
        'country': 'Canada',
        'population_proper': '36713',
        'iso2': 'CA',
        'capital': 'admin',
        'lat': '45.910648',
        'lng': '-66.658649',
        'population': '52337'
    },
    {
        'city': 'Chilliwack',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '51942',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.166667',
        'lng': '-121.95',
        'population': '51942'
    },
    {
        'city': 'North Bay',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '41807',
        'iso2': 'CA',
        'capital': '',
        'lat': '46.3',
        'lng': '-79.45',
        'population': '50170'
    },
    {
        'city': 'Shawinigan-Sud',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '34342',
        'iso2': 'CA',
        'capital': '',
        'lat': '46.528557',
        'lng': '-72.751453',
        'population': '49161'
    },
    {
        'city': 'Cornwall',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '46382',
        'iso2': 'CA',
        'capital': '',
        'lat': '45.016667',
        'lng': '-74.733333',
        'population': '48821'
    },
    {
        'city': 'Joliette',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '34772',
        'iso2': 'CA',
        'capital': '',
        'lat': '46.034',
        'lng': '-73.441',
        'population': '45361'
    },
    {
        'city': 'Belleville',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '43990',
        'iso2': 'CA',
        'capital': '',
        'lat': '44.166667',
        'lng': '-77.383333',
        'population': '43990'
    },
    {
        'city': 'Charlottetown',
        'admin': 'Prince Edward Island',
        'country': 'Canada',
        'population_proper': '31293',
        'iso2': 'CA',
        'capital': 'admin',
        'lat': '46.238225',
        'lng': '-63.139481',
        'population': '42402'
    },
    {
        'city': 'Victoriaville',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '34426',
        'iso2': 'CA',
        'capital': '',
        'lat': '46.063106',
        'lng': '-71.958802',
        'population': '41500'
    },
    {
        'city': 'Grande Prairie',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '40845',
        'iso2': 'CA',
        'capital': '',
        'lat': '55.166667',
        'lng': '-118.8',
        'population': '41462'
    },
    {
        'city': 'Penticton',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '30349',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.5',
        'lng': '-119.583333',
        'population': '37721'
    },
    {
        'city': 'Sydney',
        'admin': 'Nova Scotia',
        'country': 'Canada',
        'population_proper': '37538',
        'iso2': 'CA',
        'capital': '',
        'lat': '46.15',
        'lng': '-60.166667',
        'population': '37538'
    },
    {
        'city': 'Orillia',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '30178',
        'iso2': 'CA',
        'capital': '',
        'lat': '44.6',
        'lng': '-79.416667',
        'population': '37483'
    },
    {
        'city': 'Rimouski',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '35584',
        'iso2': 'CA',
        'capital': '',
        'lat': '48.433333',
        'lng': '-68.516667',
        'population': '35584'
    },
    {
        'city': 'Timmins',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '32901',
        'iso2': 'CA',
        'capital': '',
        'lat': '48.466667',
        'lng': '-81.333333',
        'population': '34974'
    },
    {
        'city': 'Prince Albert',
        'admin': 'Saskatchewan',
        'country': 'Canada',
        'population_proper': '24678',
        'iso2': 'CA',
        'capital': '',
        'lat': '53.2',
        'lng': '-105.75',
        'population': '34609'
    },
    {
        'city': 'Campbell River',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '26453',
        'iso2': 'CA',
        'capital': '',
        'lat': '50.016667',
        'lng': '-125.25',
        'population': '33430'
    },
    {
        'city': 'Courtenay',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '25099',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.683333',
        'lng': '-125.0',
        'population': '32793'
    },
    {
        'city': 'Orangeville',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '28984',
        'iso2': 'CA',
        'capital': '',
        'lat': '43.916366',
        'lng': '-80.096671',
        'population': '32640'
    },
    {
        'city': 'Moose Jaw',
        'admin': 'Saskatchewan',
        'country': 'Canada',
        'population_proper': '30707',
        'iso2': 'CA',
        'capital': '',
        'lat': '50.4',
        'lng': '-105.55',
        'population': '32166'
    },
    {
        'city': 'Brandon',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '26234',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.833333',
        'lng': '-99.95',
        'population': '28418'
    },
    {
        'city': 'Brockville',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '23886',
        'iso2': 'CA',
        'capital': '',
        'lat': '44.594958',
        'lng': '-75.682133',
        'population': '26458'
    },
    {
        'city': 'Saint-Georges',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '26149',
        'iso2': 'CA',
        'capital': '',
        'lat': '46.116667',
        'lng': '-70.683333',
        'population': '26149'
    },
    {
        'city': 'Sept-\u00celes',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '25686',
        'iso2': 'CA',
        'capital': '',
        'lat': '50.2',
        'lng': '-66.383333',
        'population': '25686'
    },
    {
        'city': 'Rouyn-Noranda',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '24023',
        'iso2': 'CA',
        'capital': '',
        'lat': '48.25',
        'lng': '-79.016667',
        'population': '24602'
    },
    {
        'city': 'Whitehorse',
        'admin': 'Yukon',
        'country': 'Canada',
        'population_proper': '23272',
        'iso2': 'CA',
        'capital': 'admin',
        'lat': '60.716667',
        'lng': '-135.05',
        'population': '23276'
    },
    {
        'city': 'Owen Sound',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '22625',
        'iso2': 'CA',
        'capital': '',
        'lat': '44.566667',
        'lng': '-80.85',
        'population': '22625'
    },
    {
        'city': 'Fort McMurray',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '21863',
        'iso2': 'CA',
        'capital': '',
        'lat': '56.733333',
        'lng': '-111.383333',
        'population': '21863'
    },
    {
        'city': 'Corner Brook',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '18693',
        'iso2': 'CA',
        'capital': '',
        'lat': '48.95',
        'lng': '-57.933333',
        'population': '20791'
    },
    {
        'city': 'Val-d\u2019Or',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '20625',
        'iso2': 'CA',
        'capital': '',
        'lat': '48.116667',
        'lng': '-77.766667',
        'population': '20625'
    },
    {
        'city': 'New Glasgow',
        'admin': 'Nova Scotia',
        'country': 'Canada',
        'population_proper': '19445',
        'iso2': 'CA',
        'capital': '',
        'lat': '45.583333',
        'lng': '-62.633333',
        'population': '20322'
    },
    {
        'city': 'Terrace',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '10101',
        'iso2': 'CA',
        'capital': '',
        'lat': '54.5',
        'lng': '-128.583333',
        'population': '19443'
    },
    {
        'city': 'North Battleford',
        'admin': 'Saskatchewan',
        'country': 'Canada',
        'population_proper': '12003',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.766667',
        'lng': '-108.283333',
        'population': '19440'
    },
    {
        'city': 'Yellowknife',
        'admin': 'Northwest Territories',
        'country': 'Canada',
        'population_proper': '18083',
        'iso2': 'CA',
        'capital': 'admin',
        'lat': '62.45',
        'lng': '-114.35',
        'population': '19234'
    },
    {
        'city': 'Fort Saint John',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '17402',
        'iso2': 'CA',
        'capital': '',
        'lat': '56.25',
        'lng': '-120.833333',
        'population': '18776'
    },
    {
        'city': 'Cranbrook',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '17370',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.516667',
        'lng': '-115.766667',
        'population': '18610'
    },
    {
        'city': 'Edmundston',
        'admin': 'New Brunswick',
        'country': 'Canada',
        'population_proper': '17894',
        'iso2': 'CA',
        'capital': '',
        'lat': '47.36226',
        'lng': '-68.327874',
        'population': '17894'
    },
    {
        'city': 'Rivi\u00e8re-du-Loup',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '16403',
        'iso2': 'CA',
        'capital': '',
        'lat': '47.833333',
        'lng': '-69.533333',
        'population': '16403'
    },
    {
        'city': 'Camrose',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '15686',
        'iso2': 'CA',
        'capital': '',
        'lat': '53.016667',
        'lng': '-112.816667',
        'population': '15808'
    },
    {
        'city': 'Pembroke',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '15551',
        'iso2': 'CA',
        'capital': '',
        'lat': '45.816667',
        'lng': '-77.116667',
        'population': '15551'
    },
    {
        'city': 'Yorkton',
        'admin': 'Saskatchewan',
        'country': 'Canada',
        'population_proper': '13583',
        'iso2': 'CA',
        'capital': '',
        'lat': '51.216667',
        'lng': '-102.466667',
        'population': '15172'
    },
    {
        'city': 'Swift Current',
        'admin': 'Saskatchewan',
        'country': 'Canada',
        'population_proper': '14703',
        'iso2': 'CA',
        'capital': '',
        'lat': '50.283333',
        'lng': '-107.766667',
        'population': '14906'
    },
    {
        'city': 'Prince Rupert',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '14708',
        'iso2': 'CA',
        'capital': '',
        'lat': '54.316667',
        'lng': '-130.333333',
        'population': '14708'
    },
    {
        'city': 'Williams Lake',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '10554',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.116667',
        'lng': '-122.15',
        'population': '14168'
    },
    {
        'city': 'Brooks',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '12744',
        'iso2': 'CA',
        'capital': '',
        'lat': '50.566667',
        'lng': '-111.9',
        'population': '14163'
    },
    {
        'city': 'Quesnel',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '13788',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.983333',
        'lng': '-122.483333',
        'population': '13788'
    },
    {
        'city': 'Thompson',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '12467',
        'iso2': 'CA',
        'capital': '',
        'lat': '55.75',
        'lng': '-97.866667',
        'population': '13727'
    },
    {
        'city': 'Dolbeau',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '12916',
        'iso2': 'CA',
        'capital': '',
        'lat': '48.866667',
        'lng': '-72.233333',
        'population': '13337'
    },
    {
        'city': 'Powell River',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '3220',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.883333',
        'lng': '-124.55',
        'population': '12779'
    },
    {
        'city': 'Wetaskiwin',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '11302',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.966667',
        'lng': '-113.383333',
        'population': '11823'
    },
    {
        'city': 'Nelson',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '9813',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.483333',
        'lng': '-117.283333',
        'population': '11779'
    },
    {
        'city': 'Mont-Laurier',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '11642',
        'iso2': 'CA',
        'capital': '',
        'lat': '46.55',
        'lng': '-75.5',
        'population': '11642'
    },
    {
        'city': 'Kenora',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '10852',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.766667',
        'lng': '-94.466667',
        'population': '10852'
    },
    {
        'city': 'Dawson Creek',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '10551',
        'iso2': 'CA',
        'capital': '',
        'lat': '55.766667',
        'lng': '-120.233333',
        'population': '10802'
    },
    {
        'city': 'Amos',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '10435',
        'iso2': 'CA',
        'capital': '',
        'lat': '48.566667',
        'lng': '-78.116667',
        'population': '10516'
    },
    {
        'city': 'Baie-Comeau',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '7181',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.216667',
        'lng': '-68.15',
        'population': '10435'
    },
    {
        'city': 'Hinton',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '9889',
        'iso2': 'CA',
        'capital': '',
        'lat': '53.4',
        'lng': '-117.583333',
        'population': '10265'
    },
    {
        'city': 'Selkirk',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '9653',
        'iso2': 'CA',
        'capital': '',
        'lat': '50.15',
        'lng': '-96.883333',
        'population': '9986'
    },
    {
        'city': 'Steinbach',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '9607',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.516667',
        'lng': '-96.683333',
        'population': '9729'
    },
    {
        'city': 'Weyburn',
        'admin': 'Saskatchewan',
        'country': 'Canada',
        'population_proper': '9243',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.666667',
        'lng': '-103.85',
        'population': '9362'
    },
    {
        'city': 'Amherst',
        'admin': 'Nova Scotia',
        'country': 'Canada',
        'population_proper': '7927',
        'iso2': 'CA',
        'capital': '',
        'lat': '45.830019',
        'lng': '-64.210024',
        'population': '9336'
    },
    {
        'city': 'Kapuskasing',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '8224',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.416667',
        'lng': '-82.433333',
        'population': '9240'
    },
    {
        'city': 'Dauphin',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '8418',
        'iso2': 'CA',
        'capital': '',
        'lat': '51.15',
        'lng': '-100.05',
        'population': '9077'
    },
    {
        'city': 'Dryden',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '7862',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.783333',
        'lng': '-92.833333',
        'population': '7862'
    },
    {
        'city': 'Revelstoke',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '7533',
        'iso2': 'CA',
        'capital': '',
        'lat': '51.0',
        'lng': '-118.183333',
        'population': '7668'
    },
    {
        'city': 'Happy Valley',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '1047',
        'iso2': 'CA',
        'capital': '',
        'lat': '53.3',
        'lng': '-60.3',
        'population': '7572'
    },
    {
        'city': 'Banff',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '6292',
        'iso2': 'CA',
        'capital': '',
        'lat': '51.166667',
        'lng': '-115.566667',
        'population': '7502'
    },
    {
        'city': 'Yarmouth',
        'admin': 'Nova Scotia',
        'country': 'Canada',
        'population_proper': '7366',
        'iso2': 'CA',
        'capital': '',
        'lat': '43.833965',
        'lng': '-66.113926',
        'population': '7500'
    },
    {
        'city': 'La Sarre',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '5527',
        'iso2': 'CA',
        'capital': '',
        'lat': '48.8',
        'lng': '-79.2',
        'population': '7206'
    },
    {
        'city': 'Parry Sound',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '6469',
        'iso2': 'CA',
        'capital': '',
        'lat': '45.333333',
        'lng': '-80.033333',
        'population': '7105'
    },
    {
        'city': 'Stephenville',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '6278',
        'iso2': 'CA',
        'capital': '',
        'lat': '48.55',
        'lng': '-58.566667',
        'population': '7054'
    },
    {
        'city': 'Antigonish',
        'admin': 'Nova Scotia',
        'country': 'Canada',
        'population_proper': '5003',
        'iso2': 'CA',
        'capital': '',
        'lat': '45.616667',
        'lng': '-61.966667',
        'population': '6739'
    },
    {
        'city': 'Flin Flon',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '6002',
        'iso2': 'CA',
        'capital': '',
        'lat': '54.766667',
        'lng': '-101.883333',
        'population': '6393'
    },
    {
        'city': 'Fort Nelson',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '6315',
        'iso2': 'CA',
        'capital': '',
        'lat': '58.816667',
        'lng': '-122.533333',
        'population': '6315'
    },
    {
        'city': 'Smithers',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '5438',
        'iso2': 'CA',
        'capital': '',
        'lat': '54.766667',
        'lng': '-127.166667',
        'population': '6245'
    },
    {
        'city': 'Iqaluit',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '6124',
        'iso2': 'CA',
        'capital': 'admin',
        'lat': '63.733333',
        'lng': '-68.5',
        'population': '6124'
    },
    {
        'city': 'Bathurst',
        'admin': 'New Brunswick',
        'country': 'Canada',
        'population_proper': '4496',
        'iso2': 'CA',
        'capital': '',
        'lat': '47.558376',
        'lng': '-65.656517',
        'population': '6111'
    },
    {
        'city': 'The Pas',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '3802',
        'iso2': 'CA',
        'capital': '',
        'lat': '53.816667',
        'lng': '-101.233333',
        'population': '6055'
    },
    {
        'city': 'Norway House',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '5000',
        'iso2': 'CA',
        'capital': '',
        'lat': '53.966667',
        'lng': '-97.833333',
        'population': '6000'
    },
    {
        'city': 'Meadow Lake',
        'admin': 'Saskatchewan',
        'country': 'Canada',
        'population_proper': '4281',
        'iso2': 'CA',
        'capital': '',
        'lat': '54.129722',
        'lng': '-108.434722',
        'population': '5882'
    },
    {
        'city': 'Vegreville',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '5678',
        'iso2': 'CA',
        'capital': '',
        'lat': '53.5',
        'lng': '-112.05',
        'population': '5813'
    },
    {
        'city': 'Stettler',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '5405',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.333333',
        'lng': '-112.683333',
        'population': '5494'
    },
    {
        'city': 'Peace River',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '4689',
        'iso2': 'CA',
        'capital': '',
        'lat': '56.233333',
        'lng': '-117.283333',
        'population': '5340'
    },
    {
        'city': 'New Liskeard',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '5203',
        'iso2': 'CA',
        'capital': '',
        'lat': '47.5',
        'lng': '-79.666667',
        'population': '5203'
    },
    {
        'city': 'Hearst',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '4746',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.7',
        'lng': '-83.666667',
        'population': '5043'
    },
    {
        'city': 'Creston',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '4816',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.1',
        'lng': '-116.516667',
        'population': '4816'
    },
    {
        'city': 'Marathon',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '4627',
        'iso2': 'CA',
        'capital': '',
        'lat': '48.75',
        'lng': '-86.366667',
        'population': '4627'
    },
    {
        'city': 'Cochrane',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '4441',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.066667',
        'lng': '-81.016667',
        'population': '4441'
    },
    {
        'city': 'Kindersley',
        'admin': 'Saskatchewan',
        'country': 'Canada',
        'population_proper': '4249',
        'iso2': 'CA',
        'capital': '',
        'lat': '51.466667',
        'lng': '-109.133333',
        'population': '4383'
    },
    {
        'city': 'Liverpool',
        'admin': 'Nova Scotia',
        'country': 'Canada',
        'population_proper': '4331',
        'iso2': 'CA',
        'capital': '',
        'lat': '44.038414',
        'lng': '-64.718433',
        'population': '4331'
    },
    {
        'city': 'Melville',
        'admin': 'Saskatchewan',
        'country': 'Canada',
        'population_proper': '4173',
        'iso2': 'CA',
        'capital': '',
        'lat': '50.933333',
        'lng': '-102.8',
        'population': '4279'
    },
    {
        'city': 'Channel-Port aux Basques',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '2244',
        'iso2': 'CA',
        'capital': '',
        'lat': '47.566667',
        'lng': '-59.15',
        'population': '4220'
    },
    {
        'city': 'Deer Lake',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '3743',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.183333',
        'lng': '-57.433333',
        'population': '4163'
    },
    {
        'city': 'Saint-Augustin',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '3961',
        'iso2': 'CA',
        'capital': '',
        'lat': '51.233333',
        'lng': '-58.65',
        'population': '3961'
    },
    {
        'city': 'Digby',
        'admin': 'Nova Scotia',
        'country': 'Canada',
        'population_proper': '2052',
        'iso2': 'CA',
        'capital': '',
        'lat': '44.578466',
        'lng': '-65.783525',
        'population': '3949'
    },
    {
        'city': 'Jasper',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '3102',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.883333',
        'lng': '-118.083333',
        'population': '3907'
    },
    {
        'city': 'Hay River',
        'admin': 'Northwest Territories',
        'country': 'Canada',
        'population_proper': '3648',
        'iso2': 'CA',
        'capital': '',
        'lat': '60.85',
        'lng': '-115.7',
        'population': '3900'
    },
    {
        'city': 'Windsor',
        'admin': 'Nova Scotia',
        'country': 'Canada',
        'population_proper': '3654',
        'iso2': 'CA',
        'capital': '',
        'lat': '44.958995',
        'lng': '-64.144786',
        'population': '3864'
    },
    {
        'city': 'La Ronge',
        'admin': 'Saskatchewan',
        'country': 'Canada',
        'population_proper': '3071',
        'iso2': 'CA',
        'capital': '',
        'lat': '55.1',
        'lng': '-105.3',
        'population': '3783'
    },
    {
        'city': 'Deer Lake',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '3743',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.616667',
        'lng': '-94.066667',
        'population': '3743'
    },
    {
        'city': 'Gaspe',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '3331',
        'iso2': 'CA',
        'capital': '',
        'lat': '48.833333',
        'lng': '-64.483333',
        'population': '3677'
    },
    {
        'city': 'Atikokan',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '3625',
        'iso2': 'CA',
        'capital': '',
        'lat': '48.75',
        'lng': '-91.616667',
        'population': '3625'
    },
    {
        'city': 'Gander',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '3345',
        'iso2': 'CA',
        'capital': '',
        'lat': '48.95',
        'lng': '-54.55',
        'population': '3345'
    },
    {
        'city': 'Fort Chipewyan',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '3222',
        'iso2': 'CA',
        'capital': '',
        'lat': '58.716667',
        'lng': '-111.15',
        'population': '3222'
    },
    {
        'city': 'Shelburne',
        'admin': 'Nova Scotia',
        'country': 'Canada',
        'population_proper': '1939',
        'iso2': 'CA',
        'capital': '',
        'lat': '43.753356',
        'lng': '-65.246074',
        'population': '3167'
    },
    {
        'city': 'Inuvik',
        'admin': 'Northwest Territories',
        'country': 'Canada',
        'population_proper': '3022',
        'iso2': 'CA',
        'capital': '',
        'lat': '68.35',
        'lng': '-133.7',
        'population': '3022'
    },
    {
        'city': 'Lac La Biche',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '2919',
        'iso2': 'CA',
        'capital': '',
        'lat': '54.771944',
        'lng': '-111.964722',
        'population': '2986'
    },
    {
        'city': 'Lillooet',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '2893',
        'iso2': 'CA',
        'capital': '',
        'lat': '50.683333',
        'lng': '-121.933333',
        'population': '2893'
    },
    {
        'city': 'Chapleau',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '2663',
        'iso2': 'CA',
        'capital': '',
        'lat': '47.833333',
        'lng': '-83.4',
        'population': '2663'
    },
    {
        'city': 'Burns Lake',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '2631',
        'iso2': 'CA',
        'capital': '',
        'lat': '54.216667',
        'lng': '-125.766667',
        'population': '2635'
    },
    {
        'city': 'Gimli',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '2009',
        'iso2': 'CA',
        'capital': '',
        'lat': '50.633333',
        'lng': '-97.0',
        'population': '2623'
    },
    {
        'city': 'Athabasca',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '2260',
        'iso2': 'CA',
        'capital': '',
        'lat': '54.716667',
        'lng': '-113.266667',
        'population': '2539'
    },
    {
        'city': 'Nelson House',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '2500',
        'iso2': 'CA',
        'capital': '',
        'lat': '55.8',
        'lng': '-98.85',
        'population': '2500'
    },
    {
        'city': 'Rankin Inlet',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '2334',
        'iso2': 'CA',
        'capital': '',
        'lat': '62.816667',
        'lng': '-92.083333',
        'population': '2472'
    },
    {
        'city': 'Port Hardy',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '2295',
        'iso2': 'CA',
        'capital': '',
        'lat': '50.716667',
        'lng': '-127.5',
        'population': '2295'
    },
    {
        'city': 'Biggar',
        'admin': 'Saskatchewan',
        'country': 'Canada',
        'population_proper': '2068',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.05',
        'lng': '-107.983333',
        'population': '2192'
    },
    {
        'city': 'Wiarton',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '2182',
        'iso2': 'CA',
        'capital': '',
        'lat': '44.733333',
        'lng': '-81.133333',
        'population': '2182'
    },
    {
        'city': 'Wawa',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '2174',
        'iso2': 'CA',
        'capital': '',
        'lat': '47.99473',
        'lng': '-84.77002',
        'population': '2174'
    },
    {
        'city': 'Hudson Bay',
        'admin': 'Saskatchewan',
        'country': 'Canada',
        'population_proper': '1661',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.85',
        'lng': '-102.383333',
        'population': '2157'
    },
    {
        'city': 'Matagami',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '1183',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.75',
        'lng': '-77.633333',
        'population': '1966'
    },
    {
        'city': 'Arviat',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '1868',
        'iso2': 'CA',
        'capital': '',
        'lat': '61.116667',
        'lng': '-94.05',
        'population': '1868'
    },
    {
        'city': 'Attawapiskat',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '1802',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.916667',
        'lng': '-82.433333',
        'population': '1802'
    },
    {
        'city': 'Red Lake',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '1765',
        'iso2': 'CA',
        'capital': '',
        'lat': '51.033333',
        'lng': '-93.833333',
        'population': '1765'
    },
    {
        'city': 'Moosonee',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '1725',
        'iso2': 'CA',
        'capital': '',
        'lat': '51.266667',
        'lng': '-80.65',
        'population': '1725'
    },
    {
        'city': 'Tofino',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '1655',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.133333',
        'lng': '-125.9',
        'population': '1655'
    },
    {
        'city': 'Igloolik',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '1612',
        'iso2': 'CA',
        'capital': '',
        'lat': '69.4',
        'lng': '-81.8',
        'population': '1612'
    },
    {
        'city': 'Inukjuak',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '1597',
        'iso2': 'CA',
        'capital': '',
        'lat': '58.45334',
        'lng': '-78.102493',
        'population': '1597'
    },
    {
        'city': 'Little Current',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '1595',
        'iso2': 'CA',
        'capital': '',
        'lat': '45.966667',
        'lng': '-81.933333',
        'population': '1595'
    },
    {
        'city': 'Baker Lake',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '1584',
        'iso2': 'CA',
        'capital': '',
        'lat': '64.316667',
        'lng': '-96.016667',
        'population': '1584'
    },
    {
        'city': 'Pond Inlet',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '1549',
        'iso2': 'CA',
        'capital': '',
        'lat': '72.7',
        'lng': '-78.0',
        'population': '1549'
    },
    {
        'city': 'Cap-Chat',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '1466',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.083333',
        'lng': '-66.683333',
        'population': '1484'
    },
    {
        'city': 'Cambridge Bay',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '1477',
        'iso2': 'CA',
        'capital': '',
        'lat': '69.116667',
        'lng': '-105.033333',
        'population': '1477'
    },
    {
        'city': 'Thessalon',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '1369',
        'iso2': 'CA',
        'capital': '',
        'lat': '46.25',
        'lng': '-83.55',
        'population': '1464'
    },
    {
        'city': 'New Bella Bella',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '1400',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.166667',
        'lng': '-128.133333',
        'population': '1400'
    },
    {
        'city': 'Cobalt',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '1372',
        'iso2': 'CA',
        'capital': '',
        'lat': '47.383333',
        'lng': '-79.683333',
        'population': '1372'
    },
    {
        'city': 'Cape Dorset',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '1326',
        'iso2': 'CA',
        'capital': '',
        'lat': '64.233333',
        'lng': '-76.55',
        'population': '1326'
    },
    {
        'city': 'Pangnirtung',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '1320',
        'iso2': 'CA',
        'capital': '',
        'lat': '66.133333',
        'lng': '-65.75',
        'population': '1320'
    },
    {
        'city': 'West Dawson',
        'admin': 'Yukon',
        'country': 'Canada',
        'population_proper': '1319',
        'iso2': 'CA',
        'capital': '',
        'lat': '64.066667',
        'lng': '-139.45',
        'population': '1319'
    },
    {
        'city': 'Kugluktuk',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '1302',
        'iso2': 'CA',
        'capital': '',
        'lat': '67.833333',
        'lng': '-115.083333',
        'population': '1302'
    },
    {
        'city': 'Geraldton',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '1290',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.716667',
        'lng': '-86.966667',
        'population': '1290'
    },
    {
        'city': 'Gillam',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '1281',
        'iso2': 'CA',
        'capital': '',
        'lat': '56.35',
        'lng': '-94.7',
        'population': '1281'
    },
    {
        'city': 'Kuujjuaq',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '1273',
        'iso2': 'CA',
        'capital': '',
        'lat': '58.1',
        'lng': '-68.4',
        'population': '1273'
    },
    {
        'city': 'Lake Louise',
        'admin': 'Alberta',
        'country': 'Canada',
        'population_proper': '1248',
        'iso2': 'CA',
        'capital': '',
        'lat': '51.433333',
        'lng': '-116.183333',
        'population': '1248'
    },
    {
        'city': 'Nipigon',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '1204',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.016667',
        'lng': '-88.25',
        'population': '1204'
    },
    {
        'city': 'Nain',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '1151',
        'iso2': 'CA',
        'capital': '',
        'lat': '56.55',
        'lng': '-61.683333',
        'population': '1151'
    },
    {
        'city': 'Gjoa Haven',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '1109',
        'iso2': 'CA',
        'capital': '',
        'lat': '68.633333',
        'lng': '-95.916667',
        'population': '1109'
    },
    {
        'city': 'Fort McPherson',
        'admin': 'Northwest Territories',
        'country': 'Canada',
        'population_proper': '1069',
        'iso2': 'CA',
        'capital': '',
        'lat': '67.433333',
        'lng': '-134.866667',
        'population': '1069'
    },
    {
        'city': 'Argentia',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '1063',
        'iso2': 'CA',
        'capital': '',
        'lat': '47.3',
        'lng': '-54.0',
        'population': '1063'
    },
    {
        'city': 'Norman Wells',
        'admin': 'Northwest Territories',
        'country': 'Canada',
        'population_proper': '273',
        'iso2': 'CA',
        'capital': '',
        'lat': '65.283333',
        'lng': '-126.85',
        'population': '1027'
    },
    {
        'city': 'Churchill',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '923',
        'iso2': 'CA',
        'capital': '',
        'lat': '58.766667',
        'lng': '-94.166667',
        'population': '1000'
    },
    {
        'city': 'Repulse Bay',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '748',
        'iso2': 'CA',
        'capital': '',
        'lat': '66.516667',
        'lng': '-86.233333',
        'population': '1000'
    },
    {
        'city': 'Tuktoyaktuk',
        'admin': 'Northwest Territories',
        'country': 'Canada',
        'population_proper': '870',
        'iso2': 'CA',
        'capital': '',
        'lat': '69.45',
        'lng': '-133.066667',
        'population': '929'
    },
    {
        'city': 'Berens River',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '153',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.366667',
        'lng': '-97.033333',
        'population': '892'
    },
    {
        'city': 'Shamattawa',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '870',
        'iso2': 'CA',
        'capital': '',
        'lat': '55.85',
        'lng': '-92.083333',
        'population': '870'
    },
    {
        'city': 'Baddeck',
        'admin': 'Nova Scotia',
        'country': 'Canada',
        'population_proper': '852',
        'iso2': 'CA',
        'capital': '',
        'lat': '46.1',
        'lng': '-60.75',
        'population': '852'
    },
    {
        'city': 'Coral Harbour',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '834',
        'iso2': 'CA',
        'capital': '',
        'lat': '64.133333',
        'lng': '-83.166667',
        'population': '834'
    },
    {
        'city': 'La Scie',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '817',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.966667',
        'lng': '-55.583333',
        'population': '817'
    },
    {
        'city': 'Watson Lake',
        'admin': 'Yukon',
        'country': 'Canada',
        'population_proper': '802',
        'iso2': 'CA',
        'capital': '',
        'lat': '60.116667',
        'lng': '-128.8',
        'population': '802'
    },
    {
        'city': 'Taloyoak',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '774',
        'iso2': 'CA',
        'capital': '',
        'lat': '69.533333',
        'lng': '-93.533333',
        'population': '774'
    },
    {
        'city': 'Natashquan',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '722',
        'iso2': 'CA',
        'capital': '',
        'lat': '50.183333',
        'lng': '-61.816667',
        'population': '722'
    },
    {
        'city': 'Buchans',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '685',
        'iso2': 'CA',
        'capital': '',
        'lat': '48.816667',
        'lng': '-56.866667',
        'population': '685'
    },
    {
        'city': 'Hall Beach',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '654',
        'iso2': 'CA',
        'capital': '',
        'lat': '68.766667',
        'lng': '-81.2',
        'population': '654'
    },
    {
        'city': 'Arctic Bay',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '604',
        'iso2': 'CA',
        'capital': '',
        'lat': '73.033333',
        'lng': '-85.166667',
        'population': '604'
    },
    {
        'city': 'Fort Good Hope',
        'admin': 'Northwest Territories',
        'country': 'Canada',
        'population_proper': '597',
        'iso2': 'CA',
        'capital': '',
        'lat': '66.266667',
        'lng': '-128.633333',
        'population': '597'
    },
    {
        'city': 'Mingan',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '588',
        'iso2': 'CA',
        'capital': '',
        'lat': '50.3',
        'lng': '-64.016667',
        'population': '588'
    },
    {
        'city': 'Kangirsuk',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '549',
        'iso2': 'CA',
        'capital': '',
        'lat': '60.016667',
        'lng': '-70.033333',
        'population': '549'
    },
    {
        'city': 'Sandspit',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '538',
        'iso2': 'CA',
        'capital': '',
        'lat': '53.239111',
        'lng': '-131.818769',
        'population': '538'
    },
    {
        'city': 'Del\u012fne',
        'admin': 'Northwest Territories',
        'country': 'Canada',
        'population_proper': '262',
        'iso2': 'CA',
        'capital': '',
        'lat': '65.183333',
        'lng': '-123.416667',
        'population': '525'
    },
    {
        'city': 'Fort Smith',
        'admin': 'Northwest Territories',
        'country': 'Canada',
        'population_proper': '518',
        'iso2': 'CA',
        'capital': '',
        'lat': '60.0',
        'lng': '-111.883333',
        'population': '518'
    },
    {
        'city': 'Cartwright',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '505',
        'iso2': 'CA',
        'capital': '',
        'lat': '53.7',
        'lng': '-57.016667',
        'population': '505'
    },
    {
        'city': 'Holman',
        'admin': 'Northwest Territories',
        'country': 'Canada',
        'population_proper': '398',
        'iso2': 'CA',
        'capital': '',
        'lat': '70.733333',
        'lng': '-117.75',
        'population': '500'
    },
    {
        'city': 'Lynn Lake',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '482',
        'iso2': 'CA',
        'capital': '',
        'lat': '56.85',
        'lng': '-101.05',
        'population': '482'
    },
    {
        'city': 'Schefferville',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '471',
        'iso2': 'CA',
        'capital': '',
        'lat': '54.8',
        'lng': '-66.816667',
        'population': '471'
    },
    {
        'city': 'Trout River',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '452',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.483333',
        'lng': '-58.116667',
        'population': '452'
    },
    {
        'city': 'Forteau Bay',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '448',
        'iso2': 'CA',
        'capital': '',
        'lat': '51.45',
        'lng': '-56.95',
        'population': '448'
    },
    {
        'city': 'Fort Resolution',
        'admin': 'Northwest Territories',
        'country': 'Canada',
        'population_proper': '448',
        'iso2': 'CA',
        'capital': '',
        'lat': '61.166667',
        'lng': '-113.683333',
        'population': '448'
    },
    {
        'city': 'Hopedale',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '442',
        'iso2': 'CA',
        'capital': '',
        'lat': '55.45',
        'lng': '-60.216667',
        'population': '442'
    },
    {
        'city': 'Pukatawagan',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '431',
        'iso2': 'CA',
        'capital': '',
        'lat': '55.733333',
        'lng': '-101.316667',
        'population': '431'
    },
    {
        'city': 'Trepassey',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '398',
        'iso2': 'CA',
        'capital': '',
        'lat': '46.733333',
        'lng': '-53.366667',
        'population': '398'
    },
    {
        'city': 'Kimmirut',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '385',
        'iso2': 'CA',
        'capital': '',
        'lat': '62.85',
        'lng': '-69.883333',
        'population': '385'
    },
    {
        'city': 'Chesterfield Inlet',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '374',
        'iso2': 'CA',
        'capital': '',
        'lat': '63.333333',
        'lng': '-90.7',
        'population': '374'
    },
    {
        'city': 'Eastmain',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '335',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.233333',
        'lng': '-78.516667',
        'population': '335'
    },
    {
        'city': 'Dease Lake',
        'admin': 'British Columbia',
        'country': 'Canada',
        'population_proper': '303',
        'iso2': 'CA',
        'capital': '',
        'lat': '58.476697',
        'lng': '-129.96146',
        'population': '303'
    },
    {
        'city': 'Paulatuk',
        'admin': 'Northwest Territories',
        'country': 'Canada',
        'population_proper': '294',
        'iso2': 'CA',
        'capital': '',
        'lat': '69.383333',
        'lng': '-123.983333',
        'population': '294'
    },
    {
        'city': 'Fort Simpson',
        'admin': 'Northwest Territories',
        'country': 'Canada',
        'population_proper': '283',
        'iso2': 'CA',
        'capital': '',
        'lat': '61.85',
        'lng': '-121.333333',
        'population': '283'
    },
    {
        'city': 'Brochet',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '278',
        'iso2': 'CA',
        'capital': '',
        'lat': '57.883333',
        'lng': '-101.666667',
        'population': '278'
    },
    {
        'city': 'Cat Lake',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '277',
        'iso2': 'CA',
        'capital': '',
        'lat': '51.716667',
        'lng': '-91.8',
        'population': '277'
    },
    {
        'city': 'Radisson',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '270',
        'iso2': 'CA',
        'capital': '',
        'lat': '53.783333',
        'lng': '-77.616667',
        'population': '270'
    },
    {
        'city': 'Port-Menier',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '263',
        'iso2': 'CA',
        'capital': '',
        'lat': '49.816667',
        'lng': '-64.35',
        'population': '263'
    },
    {
        'city': 'Resolute',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '229',
        'iso2': 'CA',
        'capital': '',
        'lat': '74.683333',
        'lng': '-94.9',
        'population': '250'
    },
    {
        'city': 'Saint Anthony',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '224',
        'iso2': 'CA',
        'capital': '',
        'lat': '51.383333',
        'lng': '-55.6',
        'population': '224'
    },
    {
        'city': 'Port Hope Simpson',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '197',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.533333',
        'lng': '-56.3',
        'population': '197'
    },
    {
        'city': 'Oxford House',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '184',
        'iso2': 'CA',
        'capital': '',
        'lat': '54.95',
        'lng': '-95.266667',
        'population': '184'
    },
    {
        'city': 'Tsiigehtchic',
        'admin': 'Northwest Territories',
        'country': 'Canada',
        'population_proper': '175',
        'iso2': 'CA',
        'capital': '',
        'lat': '67.433333',
        'lng': '-133.75',
        'population': '175'
    },
    {
        'city': 'Ivujivik',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '156',
        'iso2': 'CA',
        'capital': '',
        'lat': '62.416667',
        'lng': '-77.9',
        'population': '156'
    },
    {
        'city': 'Stony Rapids',
        'admin': 'Saskatchewan',
        'country': 'Canada',
        'population_proper': '152',
        'iso2': 'CA',
        'capital': '',
        'lat': '59.266667',
        'lng': '-105.833333',
        'population': '152'
    },
    {
        'city': 'Alert',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '70',
        'iso2': 'CA',
        'capital': '',
        'lat': '82.483333',
        'lng': '-62.25',
        'population': '125'
    },
    {
        'city': 'Fort Severn',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '125',
        'iso2': 'CA',
        'capital': '',
        'lat': '55.983333',
        'lng': '-87.65',
        'population': '125'
    },
    {
        'city': 'Rigolet',
        'admin': 'Newfoundland and Labrador',
        'country': 'Canada',
        'population_proper': '124',
        'iso2': 'CA',
        'capital': '',
        'lat': '54.166667',
        'lng': '-58.433333',
        'population': '124'
    },
    {
        'city': 'Lansdowne House',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '120',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.216667',
        'lng': '-87.883333',
        'population': '120'
    },
    {
        'city': 'Salluit',
        'admin': 'Quebec',
        'country': 'Canada',
        'population_proper': '106',
        'iso2': 'CA',
        'capital': '',
        'lat': '62.2',
        'lng': '-75.633333',
        'population': '106'
    },
    {
        'city': '\u0141utselk\u2019e',
        'admin': 'Northwest Territories',
        'country': 'Canada',
        'population_proper': '102',
        'iso2': 'CA',
        'capital': '',
        'lat': '62.4',
        'lng': '-110.733333',
        'population': '102'
    },
    {
        'city': 'Uranium City',
        'admin': 'Saskatchewan',
        'country': 'Canada',
        'population_proper': '89',
        'iso2': 'CA',
        'capital': '',
        'lat': '59.566667',
        'lng': '-108.616667',
        'population': '89'
    },
    {
        'city': 'Burwash Landing',
        'admin': 'Yukon',
        'country': 'Canada',
        'population_proper': '73',
        'iso2': 'CA',
        'capital': '',
        'lat': '61.35',
        'lng': '-139.0',
        'population': '73'
    },
    {
        'city': 'Grise Fiord',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '23',
        'iso2': 'CA',
        'capital': '',
        'lat': '76.416667',
        'lng': '-82.95',
        'population': '23'
    },
    {
        'city': 'Big Beaverhouse',
        'admin': 'Ontario',
        'country': 'Canada',
        'population_proper': '10',
        'iso2': 'CA',
        'capital': '',
        'lat': '52.95',
        'lng': '-89.883333',
        'population': '10'
    },
    {
        'city': 'Island Lake',
        'admin': 'Manitoba',
        'country': 'Canada',
        'population_proper': '10',
        'iso2': 'CA',
        'capital': '',
        'lat': '53.966667',
        'lng': '-94.766667',
        'population': '10'
    },
    {
        'city': 'Ennadai',
        'admin': 'Nunavut',
        'country': 'Canada',
        'population_proper': '0',
        'iso2': 'CA',
        'capital': '',
        'lat': '61.133333',
        'lng': '-100.883333',
        'population': '0'
    }
];


/***/ }),

/***/ "./src/app/core/shared/mock/states.ts":
/*!********************************************!*\
  !*** ./src/app/core/shared/mock/states.ts ***!
  \********************************************/
/*! exports provided: STATES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STATES", function() { return STATES; });
/**
 * Liste des provinces du Canada
 * (Donnees brutes)
 */
var STATES = [
    {
        'name': 'Alberta',
        'abbreviation': 'AB'
    },
    {
        'name': 'British Columbia',
        'abbreviation': 'BC'
    },
    {
        'name': 'Manitoba',
        'abbreviation': 'MB'
    },
    {
        'name': 'New Brunswick',
        'abbreviation': 'NB'
    },
    {
        'name': 'Newfoundland and Labrador',
        'abbreviation': 'NL'
    },
    {
        'name': 'Northwest Territories',
        'abbreviation': 'NT'
    },
    {
        'name': 'Nova Scotia',
        'abbreviation': 'NS'
    },
    {
        'name': 'Nunavut',
        'abbreviation': 'NU'
    },
    {
        'name': 'Ontario',
        'abbreviation': 'ON'
    },
    {
        'name': 'Prince Edward Island',
        'abbreviation': 'PE'
    },
    {
        'name': 'Québec',
        'abbreviation': 'QC'
    },
    {
        'name': 'Saskatchewan',
        'abbreviation': 'SK'
    },
    {
        'name': 'Yukon Territory',
        'abbreviation': 'YT'
    }
];


/***/ }),

/***/ "./src/app/core/shared/utils/auth.guard.ts":
/*!*************************************************!*\
  !*** ./src/app/core/shared/utils/auth.guard.ts ***!
  \*************************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./helper */ "./src/app/core/shared/utils/helper.ts");
/* harmony import */ var _services_dao_login_dao_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/dao/login-dao.service */ "./src/app/core/services/dao/login-dao.service.ts");





/**
 * Implementation d'un garde routier afin d'empecher d'acceder a la page d'accueil de l'application sans etre authentifie
 */
var AuthGuard = /** @class */ (function () {
    function AuthGuard(router, loginService) {
        this.router = router;
        this.loginService = loginService;
    }
    AuthGuard.prototype.canActivate = function () {
        if (!Object(_helper__WEBPACK_IMPORTED_MODULE_3__["isLoggedIn"])()) {
            this.loginService.getUid().subscribe(function (response) {
                if (response != null && response !== undefined) {
                    localStorage.setItem('uid', response.toString());
                }
            }, function (error) {
                localStorage.setItem('uid', _helper__WEBPACK_IMPORTED_MODULE_3__["DEFAULT_UID"]);
                console.log(error);
            });
        }
        return true;
        /*
        if ( isLoggedIn() ) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
        */
    };
    AuthGuard.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _services_dao_login_dao_service__WEBPACK_IMPORTED_MODULE_4__["LoginDaoService"] }
    ]; };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _services_dao_login_dao_service__WEBPACK_IMPORTED_MODULE_4__["LoginDaoService"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/core/shared/utils/helper.ts":
/*!*********************************************!*\
  !*** ./src/app/core/shared/utils/helper.ts ***!
  \*********************************************/
/*! exports provided: IDEL_BO_CONTEXT_PATH, IDEL_EXT_CONTEXT_PATH, DEFAULT_UID, isLoggedIn, getUid, handleHttpException, buildHttpHeaders, callRestAPI, threatException */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IDEL_BO_CONTEXT_PATH", function() { return IDEL_BO_CONTEXT_PATH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IDEL_EXT_CONTEXT_PATH", function() { return IDEL_EXT_CONTEXT_PATH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DEFAULT_UID", function() { return DEFAULT_UID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isLoggedIn", function() { return isLoggedIn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUid", function() { return getUid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleHttpException", function() { return handleHttpException; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "buildHttpHeaders", function() { return buildHttpHeaders; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "callRestAPI", function() { return callRestAPI; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "threatException", function() { return threatException; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _models_alert__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../models/alert */ "./src/app/core/models/alert.ts");
/* harmony import */ var _enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../enums/alert-type.enum */ "./src/app/core/enums/alert-type.enum.ts");




var IDEL_BO_CONTEXT_PATH = 'http://localhost:9453/idel-bo/api';
var IDEL_EXT_CONTEXT_PATH = 'http://lvlidl0a.bnquebec.ca:8085/idel-v2/extranet/api';
var DEFAULT_UID = '0';
/**
 * Checks if user has logged in to the app
 */
function isLoggedIn() {
    var uid = localStorage.getItem('uid');
    return uid != null && uid !== undefined && uid !== DEFAULT_UID;
}
/**
 * Gets the uid in the localstorage
 */
function getUid() {
    return localStorage.getItem('uid');
}
/**
 * Throw all http errors
 */
function handleHttpException(error) {
    console.log('Here is the HttpErrorResponse: ', JSON.stringify(error));
    return rxjs__WEBPACK_IMPORTED_MODULE_1__["Observable"].throwError(error.statusText + ' - ' + error.message);
}
/**
 * Build Headers for Http Requests
 */
function buildHttpHeaders() {
    return new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]().set('Content-Type', 'application/json');
    /*.set('Accept-Charset', 'UTF-8' )
    .set('Authorization', localStorage.getItem('token') || '21232f297a57a5a743894a0e4a801fc3' );*/
}
/**
 * Generic API Call method
 * @param http Http Client
 * @param url Webservice url
 * @param method Method
 * @param request request
 * @param params Parameters
 */
function callRestAPI(http, url, method, request, params) {
    // console.log('Here is the Request: ', JSON.stringify(request) );
    switch (method) {
        case 'post':
            return http.post(url, request, { headers: buildHttpHeaders() }).catch(function (error) { return handleHttpException(error); });
            break;
        case 'get':
            return http.get(url, { headers: buildHttpHeaders() }).catch(function (error) { return handleHttpException(error); });
            break;
        case 'delete':
            return http.delete(url, { headers: buildHttpHeaders() }).catch(function (error) { return handleHttpException(error); });
            break;
        case 'put':
            return http.put(url, request, { headers: buildHttpHeaders() }).catch(function (error) { return handleHttpException(error); });
            break;
    }
}
/**
 * Threat an display Exceptions to the user
 * @param error Error message
 */
function threatException(error) {
    var a = new _models_alert__WEBPACK_IMPORTED_MODULE_2__["Alert"](error);
    a.setType(_enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_3__["AlertType"].error);
    a.show();
    return a;
}


/***/ }),

/***/ "./src/app/core/shared/utils/serializable.ts":
/*!***************************************************!*\
  !*** ./src/app/core/shared/utils/serializable.ts ***!
  \***************************************************/
/*! exports provided: Serializable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Serializable", function() { return Serializable; });
/**
 * De-Serialiateur:
 * Classe permettant de transformer toute chaine de caractere representant un objet JSon en objet TypeScript
 */
var Serializable = /** @class */ (function () {
    function Serializable() {
    }
    Serializable.prototype.fillFromJSON = function (json) {
        var jsonObj = JSON.parse(json);
        for (var propName in jsonObj) {
            this[propName] = jsonObj[propName];
        }
        return this;
    };
    return Serializable;
}());



/***/ }),

/***/ "./src/app/views/alert-dialog/alert-dialog.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/views/alert-dialog/alert-dialog.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL2FsZXJ0LWRpYWxvZy9hbGVydC1kaWFsb2cuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/views/alert-dialog/alert-dialog.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/views/alert-dialog/alert-dialog.component.ts ***!
  \**************************************************************/
/*! exports provided: AlertDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertDialogComponent", function() { return AlertDialogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_models_alert__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/models/alert */ "./src/app/core/models/alert.ts");
/* harmony import */ var _core_enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/enums/alert-type.enum */ "./src/app/core/enums/alert-type.enum.ts");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! util */ "./node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");






/**
 * Composant de gestion des notifications utilisateurs
 */
var AlertDialogComponent = /** @class */ (function () {
    function AlertDialogComponent() {
    }
    AlertDialogComponent.prototype.ngOnInit = function () {
        if (Object(util__WEBPACK_IMPORTED_MODULE_4__["isNullOrUndefined"])(this.myAlert)) {
            this.myAlert = new _core_models_alert__WEBPACK_IMPORTED_MODULE_2__["Alert"]('Everything is fine');
        }
    };
    AlertDialogComponent.prototype.getStyleClassSuffix = function () {
        switch (this.myAlert.type) {
            case _core_enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_3__["AlertType"].info: return 'info';
            case _core_enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_3__["AlertType"].error: return 'danger';
            case _core_enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_3__["AlertType"].warning: return 'warning';
            case _core_enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_3__["AlertType"].confirm: return 'primary';
            case _core_enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_3__["AlertType"].success: return 'success';
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('dlgAlert', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_5__["ModalDirective"])
    ], AlertDialogComponent.prototype, "dlgAlert", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _core_models_alert__WEBPACK_IMPORTED_MODULE_2__["Alert"])
    ], AlertDialogComponent.prototype, "myAlert", void 0);
    AlertDialogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-alert-dialog',
            template: __webpack_require__(/*! raw-loader!./alert-dialog.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/alert-dialog/alert-dialog.component.html"),
            styles: [__webpack_require__(/*! ./alert-dialog.component.scss */ "./src/app/views/alert-dialog/alert-dialog.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AlertDialogComponent);
    return AlertDialogComponent;
}());



/***/ }),

/***/ "./src/app/views/claim/claim.component.scss":
/*!**************************************************!*\
  !*** ./src/app/views/claim/claim.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".offspaces {\n  margin-top: 2px;\n  margin-bottom: 2px;\n  margin-left: 2px;\n  margin-right: 2px;\n  padding-top: 2px;\n  padding-bottom: 2px;\n  padding-left: 2px;\n  padding-right: 2px;\n}\n\n.nullspaces {\n  margin-top: 0px;\n  margin-bottom: 0px;\n  margin-left: 0px;\n  margin-right: 0px;\n  padding-top: 0px;\n  padding-bottom: 0px;\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.noborders {\n  border: none;\n}\n\n.bg-banq {\n  background-color: #d3dfc7;\n  font-weight: bold;\n  font-size: 11px;\n  color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvY2xhaW0vRTpcXHR1dG9cXHByb2plY3RzXFxhbmd1bGFyXFxjb3JldWkvc3JjXFxhcHBcXHZpZXdzXFxjbGFpbVxcY2xhaW0uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3ZpZXdzL2NsYWltL2NsYWltLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDQUY7O0FER0E7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNBRjs7QURHQTtFQUNFLFlBQUE7QUNBRjs7QURHQTtFQUNFLHlCQUFBO0VBRUEsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQ0RGIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvY2xhaW0vY2xhaW0uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi5vZmZzcGFjZXMge1xuICBtYXJnaW4tdG9wOiAycHg7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbiAgbWFyZ2luLWxlZnQ6IDJweDtcbiAgbWFyZ2luLXJpZ2h0OiAycHg7XG4gIHBhZGRpbmctdG9wOiAycHg7XG4gIHBhZGRpbmctYm90dG9tOiAycHg7XG4gIHBhZGRpbmctbGVmdDogMnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAycHg7XG59XG5cbi5udWxsc3BhY2VzIHtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuXG4ubm9ib3JkZXJzIHtcbiAgYm9yZGVyOiBub25lO1xufVxuXG4uYmctYmFucSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzcvKiNjNWUxYTUqL1xuICA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDExcHg7XG4gIGNvbG9yOiBibGFjaztcbn1cbiIsIi5vZmZzcGFjZXMge1xuICBtYXJnaW4tdG9wOiAycHg7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbiAgbWFyZ2luLWxlZnQ6IDJweDtcbiAgbWFyZ2luLXJpZ2h0OiAycHg7XG4gIHBhZGRpbmctdG9wOiAycHg7XG4gIHBhZGRpbmctYm90dG9tOiAycHg7XG4gIHBhZGRpbmctbGVmdDogMnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAycHg7XG59XG5cbi5udWxsc3BhY2VzIHtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuXG4ubm9ib3JkZXJzIHtcbiAgYm9yZGVyOiBub25lO1xufVxuXG4uYmctYmFucSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzc7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDExcHg7XG4gIGNvbG9yOiBibGFjaztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/views/claim/claim.component.ts":
/*!************************************************!*\
  !*** ./src/app/views/claim/claim.component.ts ***!
  \************************************************/
/*! exports provided: ClaimComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClaimComponent", function() { return ClaimComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



/**
 * Composant de consultation des reclamations des editeurs
 */
var ClaimComponent = /** @class */ (function () {
    function ClaimComponent(router) {
        this.router = router;
    }
    ClaimComponent.prototype.ngOnInit = function () {
    };
    ClaimComponent.prototype.back = function () {
        this.router.navigate(['/home/dashboard']);
    };
    ClaimComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    ClaimComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-claim',
            template: __webpack_require__(/*! raw-loader!./claim.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/claim/claim.component.html"),
            styles: [__webpack_require__(/*! ./claim.component.scss */ "./src/app/views/claim/claim.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ClaimComponent);
    return ClaimComponent;
}());



/***/ }),

/***/ "./src/app/views/depol/depol.component.scss":
/*!**************************************************!*\
  !*** ./src/app/views/depol/depol.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".telephone3 {\n  width: 40px;\n  padding-left: 0 !important;\n}\n\n.telephone4 {\n  width: 50px;\n  padding-left: 0 !important;\n}\n\n.divCentrer {\n  text-align: center;\n}\n\n.text-on-pannel {\n  background: #fff none repeat scroll 0 0;\n  height: auto;\n  margin-left: 20px;\n  padding: 3px 3px;\n  position: absolute;\n  margin-top: -15px;\n  border: 1px solid #fff;\n  color: dimgray;\n  border-radius: 3px;\n  font-weight: bold;\n  font-size: 12px;\n}\n\n.offspaces {\n  margin-top: 2px;\n  margin-bottom: 2px;\n  margin-left: 2px;\n  margin-right: 2px;\n  padding-top: 2px;\n  padding-bottom: 2px;\n  padding-left: 2px;\n  padding-right: 2px;\n}\n\n.nullspaces {\n  margin-top: 0px;\n  margin-bottom: 0px;\n  margin-left: 0px;\n  margin-right: 0px;\n  padding-top: 0px;\n  padding-bottom: 0px;\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.noborders {\n  border: none;\n}\n\n.bg-banq {\n  background-color: #d3dfc7;\n  font-weight: bold;\n  color: black;\n  font-size: 11px;\n}\n\n.name {\n  vertical-align: top;\n  /*float: left;\n  clear:left; */\n  padding: 5px;\n  margin: 0px;\n  font-weight: bold;\n  font-size: 12px;\n  text-align: right;\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n  color: #000;\n  width: 130px;\n  min-height: 20px;\n  /*max-height: 30px;*/\n  min-width: 120px;\n  /*max-width:250px;*/\n  background-color: #d3dfc7;\n  border: 1px solid #c8c8c8;\n}\n\n.small-name {\n  vertical-align: top;\n  padding: 5px;\n  margin: 0px;\n  font-weight: bold;\n  font-size: 12px;\n  text-align: right;\n  color: #000;\n  /*width: 75px;*/\n  min-height: 20px;\n  /*max-height: 30px;\n  min-width: 120px;*/\n  /*max-width:250px;*/\n  background-color: #d3dfc7;\n  border: 1px solid #c8c8c8;\n}\n\n.hintText {\n  font-size: 75%;\n  font-style: italic;\n  font-weight: normal;\n  padding: 0px;\n  margin: 0px;\n}\n\n.required {\n  color: #FF0000;\n  padding-left: 5px;\n}\n\n.w100 {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvZGVwb2wvRTpcXHR1dG9cXHByb2plY3RzXFxhbmd1bGFyXFxjb3JldWkvc3JjXFxhcHBcXHZpZXdzXFxkZXBvbFxcZGVwb2wuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3ZpZXdzL2RlcG9sL2RlcG9sLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQVksV0FBQTtFQUFXLDBCQUFBO0FDRXZCOztBRERBO0VBQVksV0FBQTtFQUFXLDBCQUFBO0FDTXZCOztBRExBO0VBQVksa0JBQUE7QUNTWjs7QURQQTtFQUNJLHVDQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFFQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNTSjs7QUROQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQ1NKOztBRE5BO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDU0o7O0FETkE7RUFDSSxZQUFBO0FDU0o7O0FETkE7RUFDSSx5QkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUNTSjs7QUROQTtFQUNJLG1CQUFBO0VBQ0E7ZUFBQTtFQUVBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxxQkFBQTtVQUFBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EseUJBQUE7QUNTSjs7QUROQTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQTtvQkFBQTtFQUVBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSx5QkFBQTtBQ1NKOztBRE5BO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ1NKOztBRE5BO0VBQ0ksY0FBQTtFQUNBLGlCQUFBO0FDU0o7O0FEUEE7RUFDRSxXQUFBO0FDVUYiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9kZXBvbC9kZXBvbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuLnRlbGVwaG9uZTN7d2lkdGg6NDBweDtwYWRkaW5nLWxlZnQ6MCAhaW1wb3J0YW50fVxuLnRlbGVwaG9uZTR7d2lkdGg6NTBweDtwYWRkaW5nLWxlZnQ6MCAhaW1wb3J0YW50fVxuLmRpdkNlbnRyZXJ7dGV4dC1hbGlnbjpjZW50ZXI7fVxuXG4udGV4dC1vbi1wYW5uZWwge1xuICAgIGJhY2tncm91bmQ6ICNmZmYgbm9uZSByZXBlYXQgc2Nyb2xsIDAgMDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XG4gICAgcGFkZGluZzogM3B4IDNweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbWFyZ2luLXRvcDogLTE1cHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2ZmZi8qY2ZkOGRjKi9cbiAgICA7XG4gICAgY29sb3I6IGRpbWdyYXk7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLm9mZnNwYWNlcyB7XG4gICAgbWFyZ2luLXRvcDogMnB4O1xuICAgIG1hcmdpbi1ib3R0b206IDJweDtcbiAgICBtYXJnaW4tbGVmdDogMnB4O1xuICAgIG1hcmdpbi1yaWdodDogMnB4O1xuICAgIHBhZGRpbmctdG9wOiAycHg7XG4gICAgcGFkZGluZy1ib3R0b206IDJweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDJweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAycHg7XG59XG5cbi5udWxsc3BhY2VzIHtcbiAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gICAgcGFkZGluZy10b3A6IDBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICAgIHBhZGRpbmctbGVmdDogMHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cblxuLm5vYm9yZGVycyB7XG4gICAgYm9yZGVyOiBub25lO1xufVxuXG4uYmctYmFucSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2QzZGZjNztcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBjb2xvcjogYmxhY2s7XG4gICAgZm9udC1zaXplOiAxMXB4O1xufVxuXG4ubmFtZSB7XG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgICAvKmZsb2F0OiBsZWZ0O1xuXHRjbGVhcjpsZWZ0OyAqL1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBtYXJnaW46IDBweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgICBjb2xvcjogIzAwMDtcbiAgICB3aWR0aDogMTMwcHg7XG4gICAgbWluLWhlaWdodDogMjBweDtcbiAgICAvKm1heC1oZWlnaHQ6IDMwcHg7Ki9cbiAgICBtaW4td2lkdGg6IDEyMHB4O1xuICAgIC8qbWF4LXdpZHRoOjI1MHB4OyovXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2QzZGZjNztcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjYzhjOGM4O1xufVxuXG4uc21hbGwtbmFtZSB7XG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgbWFyZ2luOiAwcHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIGNvbG9yOiAjMDAwO1xuICAgIC8qd2lkdGg6IDc1cHg7Ki9cbiAgICBtaW4taGVpZ2h0OiAyMHB4O1xuICAgIC8qbWF4LWhlaWdodDogMzBweDtcbiAgICBtaW4td2lkdGg6IDEyMHB4OyovXG4gICAgLyptYXgtd2lkdGg6MjUwcHg7Ki9cbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDNkZmM3O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjOGM4Yzg7XG59XG5cbi5oaW50VGV4dCB7XG4gICAgZm9udC1zaXplOiA3NSU7XG4gICAgZm9udC1zdHlsZTogaXRhbGljO1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgcGFkZGluZzogMHB4O1xuICAgIG1hcmdpbjogMHB4O1xufVxuXG4ucmVxdWlyZWQge1xuICAgIGNvbG9yOiAjRkYwMDAwO1xuICAgIHBhZGRpbmctbGVmdDogNXB4O1xufVxuLncxMDAge1xuICB3aWR0aDogMTAwJTtcbn1cbiIsIi50ZWxlcGhvbmUzIHtcbiAgd2lkdGg6IDQwcHg7XG4gIHBhZGRpbmctbGVmdDogMCAhaW1wb3J0YW50O1xufVxuXG4udGVsZXBob25lNCB7XG4gIHdpZHRoOiA1MHB4O1xuICBwYWRkaW5nLWxlZnQ6IDAgIWltcG9ydGFudDtcbn1cblxuLmRpdkNlbnRyZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi50ZXh0LW9uLXBhbm5lbCB7XG4gIGJhY2tncm91bmQ6ICNmZmYgbm9uZSByZXBlYXQgc2Nyb2xsIDAgMDtcbiAgaGVpZ2h0OiBhdXRvO1xuICBtYXJnaW4tbGVmdDogMjBweDtcbiAgcGFkZGluZzogM3B4IDNweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAtMTVweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2ZmZjtcbiAgY29sb3I6IGRpbWdyYXk7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLm9mZnNwYWNlcyB7XG4gIG1hcmdpbi10b3A6IDJweDtcbiAgbWFyZ2luLWJvdHRvbTogMnB4O1xuICBtYXJnaW4tbGVmdDogMnB4O1xuICBtYXJnaW4tcmlnaHQ6IDJweDtcbiAgcGFkZGluZy10b3A6IDJweDtcbiAgcGFkZGluZy1ib3R0b206IDJweDtcbiAgcGFkZGluZy1sZWZ0OiAycHg7XG4gIHBhZGRpbmctcmlnaHQ6IDJweDtcbn1cblxuLm51bGxzcGFjZXMge1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gIHBhZGRpbmctdG9wOiAwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG5cbi5ub2JvcmRlcnMge1xuICBib3JkZXI6IG5vbmU7XG59XG5cbi5iZy1iYW5xIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2QzZGZjNztcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiAxMXB4O1xufVxuXG4ubmFtZSB7XG4gIHZlcnRpY2FsLWFsaWduOiB0b3A7XG4gIC8qZmxvYXQ6IGxlZnQ7XG4gIGNsZWFyOmxlZnQ7ICovXG4gIHBhZGRpbmc6IDVweDtcbiAgbWFyZ2luOiAwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICBjb2xvcjogIzAwMDtcbiAgd2lkdGg6IDEzMHB4O1xuICBtaW4taGVpZ2h0OiAyMHB4O1xuICAvKm1heC1oZWlnaHQ6IDMwcHg7Ki9cbiAgbWluLXdpZHRoOiAxMjBweDtcbiAgLyptYXgtd2lkdGg6MjUwcHg7Ki9cbiAgYmFja2dyb3VuZC1jb2xvcjogI2QzZGZjNztcbiAgYm9yZGVyOiAxcHggc29saWQgI2M4YzhjODtcbn1cblxuLnNtYWxsLW5hbWUge1xuICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xuICBwYWRkaW5nOiA1cHg7XG4gIG1hcmdpbjogMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgY29sb3I6ICMwMDA7XG4gIC8qd2lkdGg6IDc1cHg7Ki9cbiAgbWluLWhlaWdodDogMjBweDtcbiAgLyptYXgtaGVpZ2h0OiAzMHB4O1xuICBtaW4td2lkdGg6IDEyMHB4OyovXG4gIC8qbWF4LXdpZHRoOjI1MHB4OyovXG4gIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzc7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjOGM4Yzg7XG59XG5cbi5oaW50VGV4dCB7XG4gIGZvbnQtc2l6ZTogNzUlO1xuICBmb250LXN0eWxlOiBpdGFsaWM7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIHBhZGRpbmc6IDBweDtcbiAgbWFyZ2luOiAwcHg7XG59XG5cbi5yZXF1aXJlZCB7XG4gIGNvbG9yOiAjRkYwMDAwO1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbn1cblxuLncxMDAge1xuICB3aWR0aDogMTAwJTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/views/depol/depol.component.ts":
/*!************************************************!*\
  !*** ./src/app/views/depol/depol.component.ts ***!
  \************************************************/
/*! exports provided: DepolComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DepolComponent", function() { return DepolComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_models_alert__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/models/alert */ "./src/app/core/models/alert.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




/**
 * Composant de gestion du Depot Legal
 */
var DepolComponent = /** @class */ (function () {
    function DepolComponent(router) {
        this.router = router;
        this.myAlert = new _core_models_alert__WEBPACK_IMPORTED_MODULE_2__["Alert"]('');
    }
    DepolComponent.prototype.ngOnInit = function () {
        this.errs = [];
        this.myAlert = new _core_models_alert__WEBPACK_IMPORTED_MODULE_2__["Alert"]('');
    };
    DepolComponent.prototype.back = function () {
        this.router.navigate(['/home/dashboard']);
    };
    DepolComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    DepolComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-depol',
            template: __webpack_require__(/*! raw-loader!./depol.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/depol/depol.component.html"),
            styles: [__webpack_require__(/*! ./depol.component.scss */ "./src/app/views/depol/depol.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], DepolComponent);
    return DepolComponent;
}());



/***/ }),

/***/ "./src/app/views/depol/list-depot/list-depot.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/views/depol/list-depot/list-depot.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".offspaces {\n  margin-top: 2px;\n  margin-bottom: 2px;\n  margin-left: 2px;\n  margin-right: 2px;\n  padding-top: 2px;\n  padding-bottom: 2px;\n  padding-left: 2px;\n  padding-right: 2px;\n}\n\n.nullspaces {\n  margin-top: 0px;\n  margin-bottom: 0px;\n  margin-left: 0px;\n  margin-right: 0px;\n  padding-top: 0px;\n  padding-bottom: 0px;\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.noborders {\n  border: none;\n}\n\n.bg-banq {\n  background-color: #d3dfc7;\n  font-weight: bold;\n  font-size: 11px;\n  color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvZGVwb2wvbGlzdC1kZXBvdC9FOlxcdHV0b1xccHJvamVjdHNcXGFuZ3VsYXJcXGNvcmV1aS9zcmNcXGFwcFxcdmlld3NcXGRlcG9sXFxsaXN0LWRlcG90XFxsaXN0LWRlcG90LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC92aWV3cy9kZXBvbC9saXN0LWRlcG90L2xpc3QtZGVwb3QuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNBRjs7QURHQTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQ0FGOztBREdBO0VBQ0UsWUFBQTtBQ0FGOztBREdBO0VBQ0UseUJBQUE7RUFFQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FDREYiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9kZXBvbC9saXN0LWRlcG90L2xpc3QtZGVwb3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi5vZmZzcGFjZXMge1xuICBtYXJnaW4tdG9wOiAycHg7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbiAgbWFyZ2luLWxlZnQ6IDJweDtcbiAgbWFyZ2luLXJpZ2h0OiAycHg7XG4gIHBhZGRpbmctdG9wOiAycHg7XG4gIHBhZGRpbmctYm90dG9tOiAycHg7XG4gIHBhZGRpbmctbGVmdDogMnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAycHg7XG59XG5cbi5udWxsc3BhY2VzIHtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuXG4ubm9ib3JkZXJzIHtcbiAgYm9yZGVyOiBub25lO1xufVxuXG4uYmctYmFucSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzcvKiNjNWUxYTUqL1xuICA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDExcHg7XG4gIGNvbG9yOiBibGFjaztcbn1cbiIsIi5vZmZzcGFjZXMge1xuICBtYXJnaW4tdG9wOiAycHg7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbiAgbWFyZ2luLWxlZnQ6IDJweDtcbiAgbWFyZ2luLXJpZ2h0OiAycHg7XG4gIHBhZGRpbmctdG9wOiAycHg7XG4gIHBhZGRpbmctYm90dG9tOiAycHg7XG4gIHBhZGRpbmctbGVmdDogMnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAycHg7XG59XG5cbi5udWxsc3BhY2VzIHtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuXG4ubm9ib3JkZXJzIHtcbiAgYm9yZGVyOiBub25lO1xufVxuXG4uYmctYmFucSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzc7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDExcHg7XG4gIGNvbG9yOiBibGFjaztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/views/depol/list-depot/list-depot.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/views/depol/list-depot/list-depot.component.ts ***!
  \****************************************************************/
/*! exports provided: ListDepotComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListDepotComponent", function() { return ListDepotComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var ListDepotComponent = /** @class */ (function () {
    function ListDepotComponent(router) {
        this.router = router;
    }
    ListDepotComponent.prototype.ngOnInit = function () {
    };
    ListDepotComponent.prototype.back = function () {
        this.router.navigate(['/home/dashboard']);
    };
    ListDepotComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    ListDepotComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-depot',
            template: __webpack_require__(/*! raw-loader!./list-depot.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/depol/list-depot/list-depot.component.html"),
            styles: [__webpack_require__(/*! ./list-depot.component.scss */ "./src/app/views/depol/list-depot/list-depot.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ListDepotComponent);
    return ListDepotComponent;
}());



/***/ }),

/***/ "./src/app/views/dmd-isbn/dmd-isbn.component.scss":
/*!********************************************************!*\
  !*** ./src/app/views/dmd-isbn/dmd-isbn.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".w100 {\n  width: 100%;\n}\n\n.telephone3 {\n  width: 40px;\n  padding-left: 0 !important;\n}\n\n.telephone4 {\n  width: 50px;\n  padding-left: 0 !important;\n}\n\n.divCentrer {\n  text-align: center;\n}\n\n.text-on-pannel {\n  background: #fff none repeat scroll 0 0;\n  height: auto;\n  margin-left: 20px;\n  padding: 3px 3px;\n  position: absolute;\n  margin-top: -15px;\n  border: 1px solid #fff;\n  color: dimgray;\n  border-radius: 3px;\n  font-weight: bold;\n  font-size: 12px;\n}\n\n.offspaces {\n  margin-top: 2px;\n  margin-bottom: 2px;\n  margin-left: 2px;\n  margin-right: 2px;\n  padding-top: 2px;\n  padding-bottom: 2px;\n  padding-left: 2px;\n  padding-right: 2px;\n}\n\n.nullspaces {\n  margin-top: 0px;\n  margin-bottom: 0px;\n  margin-left: 0px;\n  margin-right: 0px;\n  padding-top: 0px;\n  padding-bottom: 0px;\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.noborders {\n  border: none;\n}\n\n.bg-banq {\n  background-color: #d3dfc7;\n  font-weight: bold;\n  color: black;\n  font-size: 11px;\n}\n\n.name {\n  vertical-align: top;\n  /*float: left;\n  clear:left; */\n  padding: 5px;\n  margin: 0px;\n  font-weight: bold;\n  font-size: 12px;\n  text-align: right;\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n  color: #000;\n  width: 130px;\n  min-height: 20px;\n  /*max-height: 30px;*/\n  min-width: 120px;\n  /*max-width:250px;*/\n  background-color: #d3dfc7;\n  border: 1px solid #c8c8c8;\n}\n\n.small-name {\n  vertical-align: top;\n  padding: 5px;\n  margin: 0px;\n  font-weight: bold;\n  font-size: 12px;\n  text-align: right;\n  color: #000;\n  /*width: 75px;*/\n  min-height: 20px;\n  /*max-height: 30px;\n  min-width: 120px;*/\n  /*max-width:250px;*/\n  background-color: #d3dfc7;\n  border: 1px solid #c8c8c8;\n}\n\n.hintText {\n  font-size: 75%;\n  font-style: italic;\n  font-weight: normal;\n  padding: 0px;\n  margin: 0px;\n}\n\n.required {\n  color: #FF0000;\n  padding-left: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvZG1kLWlzYm4vRTpcXHR1dG9cXHByb2plY3RzXFxhbmd1bGFyXFxjb3JldWkvc3JjXFxhcHBcXHZpZXdzXFxkbWQtaXNiblxcZG1kLWlzYm4uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3ZpZXdzL2RtZC1pc2JuL2RtZC1pc2JuLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsV0FBQTtBQ0FGOztBREVBO0VBQVksV0FBQTtFQUFXLDBCQUFBO0FDR3ZCOztBREZBO0VBQVksV0FBQTtFQUFXLDBCQUFBO0FDT3ZCOztBRE5BO0VBQVksa0JBQUE7QUNVWjs7QURSQTtFQUNJLHVDQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFFQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNVSjs7QURQQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQ1VKOztBRFBBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDVUo7O0FEUEE7RUFDSSxZQUFBO0FDVUo7O0FEUEE7RUFDSSx5QkFBQTtFQUVBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUNTSjs7QUROQTtFQUNJLG1CQUFBO0VBQ0E7ZUFBQTtFQUVBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxxQkFBQTtVQUFBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EseUJBQUE7QUNTSjs7QUROQTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQTtvQkFBQTtFQUVBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSx5QkFBQTtBQ1NKOztBRE5BO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ1NKOztBRE5BO0VBQ0ksY0FBQTtFQUNBLGlCQUFBO0FDU0oiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9kbWQtaXNibi9kbWQtaXNibi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuLncxMDB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLnRlbGVwaG9uZTN7d2lkdGg6NDBweDtwYWRkaW5nLWxlZnQ6MCAhaW1wb3J0YW50fVxuLnRlbGVwaG9uZTR7d2lkdGg6NTBweDtwYWRkaW5nLWxlZnQ6MCAhaW1wb3J0YW50fVxuLmRpdkNlbnRyZXJ7dGV4dC1hbGlnbjpjZW50ZXI7fVxuXG4udGV4dC1vbi1wYW5uZWwge1xuICAgIGJhY2tncm91bmQ6ICNmZmYgbm9uZSByZXBlYXQgc2Nyb2xsIDAgMDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XG4gICAgcGFkZGluZzogM3B4IDNweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbWFyZ2luLXRvcDogLTE1cHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2ZmZi8qY2ZkOGRjKi9cbiAgICA7XG4gICAgY29sb3I6IGRpbWdyYXk7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLm9mZnNwYWNlcyB7XG4gICAgbWFyZ2luLXRvcDogMnB4O1xuICAgIG1hcmdpbi1ib3R0b206IDJweDtcbiAgICBtYXJnaW4tbGVmdDogMnB4O1xuICAgIG1hcmdpbi1yaWdodDogMnB4O1xuICAgIHBhZGRpbmctdG9wOiAycHg7XG4gICAgcGFkZGluZy1ib3R0b206IDJweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDJweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAycHg7XG59XG5cbi5udWxsc3BhY2VzIHtcbiAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gICAgcGFkZGluZy10b3A6IDBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICAgIHBhZGRpbmctbGVmdDogMHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cblxuLm5vYm9yZGVycyB7XG4gICAgYm9yZGVyOiBub25lO1xufVxuXG4uYmctYmFucSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2QzZGZjNy8qI2M1ZTFhNSovXG4gICAgO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGNvbG9yOiBibGFjaztcbiAgICBmb250LXNpemU6IDExcHg7XG59XG5cbi5uYW1lIHtcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xuICAgIC8qZmxvYXQ6IGxlZnQ7XG5cdGNsZWFyOmxlZnQ7ICovXG4gICAgcGFkZGluZzogNXB4O1xuICAgIG1hcmdpbjogMHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICAgIGNvbG9yOiAjMDAwO1xuICAgIHdpZHRoOiAxMzBweDtcbiAgICBtaW4taGVpZ2h0OiAyMHB4O1xuICAgIC8qbWF4LWhlaWdodDogMzBweDsqL1xuICAgIG1pbi13aWR0aDogMTIwcHg7XG4gICAgLyptYXgtd2lkdGg6MjUwcHg7Ki9cbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDNkZmM3O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjOGM4Yzg7XG59XG5cbi5zbWFsbC1uYW1lIHtcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBtYXJnaW46IDBweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgY29sb3I6ICMwMDA7XG4gICAgLyp3aWR0aDogNzVweDsqL1xuICAgIG1pbi1oZWlnaHQ6IDIwcHg7XG4gICAgLyptYXgtaGVpZ2h0OiAzMHB4O1xuICAgIG1pbi13aWR0aDogMTIwcHg7Ki9cbiAgICAvKm1heC13aWR0aDoyNTBweDsqL1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzc7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2M4YzhjODtcbn1cblxuLmhpbnRUZXh0IHtcbiAgICBmb250LXNpemU6IDc1JTtcbiAgICBmb250LXN0eWxlOiBpdGFsaWM7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICBwYWRkaW5nOiAwcHg7XG4gICAgbWFyZ2luOiAwcHg7XG59XG5cbi5yZXF1aXJlZCB7XG4gICAgY29sb3I6ICNGRjAwMDA7XG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XG59XG4iLCIudzEwMCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4udGVsZXBob25lMyB7XG4gIHdpZHRoOiA0MHB4O1xuICBwYWRkaW5nLWxlZnQ6IDAgIWltcG9ydGFudDtcbn1cblxuLnRlbGVwaG9uZTQge1xuICB3aWR0aDogNTBweDtcbiAgcGFkZGluZy1sZWZ0OiAwICFpbXBvcnRhbnQ7XG59XG5cbi5kaXZDZW50cmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4udGV4dC1vbi1wYW5uZWwge1xuICBiYWNrZ3JvdW5kOiAjZmZmIG5vbmUgcmVwZWF0IHNjcm9sbCAwIDA7XG4gIGhlaWdodDogYXV0bztcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG4gIHBhZGRpbmc6IDNweCAzcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDogLTE1cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmZmY7XG4gIGNvbG9yOiBkaW1ncmF5O1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5vZmZzcGFjZXMge1xuICBtYXJnaW4tdG9wOiAycHg7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbiAgbWFyZ2luLWxlZnQ6IDJweDtcbiAgbWFyZ2luLXJpZ2h0OiAycHg7XG4gIHBhZGRpbmctdG9wOiAycHg7XG4gIHBhZGRpbmctYm90dG9tOiAycHg7XG4gIHBhZGRpbmctbGVmdDogMnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAycHg7XG59XG5cbi5udWxsc3BhY2VzIHtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuXG4ubm9ib3JkZXJzIHtcbiAgYm9yZGVyOiBub25lO1xufVxuXG4uYmctYmFucSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzc7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogYmxhY2s7XG4gIGZvbnQtc2l6ZTogMTFweDtcbn1cblxuLm5hbWUge1xuICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xuICAvKmZsb2F0OiBsZWZ0O1xuICBjbGVhcjpsZWZ0OyAqL1xuICBwYWRkaW5nOiA1cHg7XG4gIG1hcmdpbjogMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgY29sb3I6ICMwMDA7XG4gIHdpZHRoOiAxMzBweDtcbiAgbWluLWhlaWdodDogMjBweDtcbiAgLyptYXgtaGVpZ2h0OiAzMHB4OyovXG4gIG1pbi13aWR0aDogMTIwcHg7XG4gIC8qbWF4LXdpZHRoOjI1MHB4OyovXG4gIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzc7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjOGM4Yzg7XG59XG5cbi5zbWFsbC1uYW1lIHtcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgcGFkZGluZzogNXB4O1xuICBtYXJnaW46IDBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIGNvbG9yOiAjMDAwO1xuICAvKndpZHRoOiA3NXB4OyovXG4gIG1pbi1oZWlnaHQ6IDIwcHg7XG4gIC8qbWF4LWhlaWdodDogMzBweDtcbiAgbWluLXdpZHRoOiAxMjBweDsqL1xuICAvKm1heC13aWR0aDoyNTBweDsqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDNkZmM3O1xuICBib3JkZXI6IDFweCBzb2xpZCAjYzhjOGM4O1xufVxuXG4uaGludFRleHQge1xuICBmb250LXNpemU6IDc1JTtcbiAgZm9udC1zdHlsZTogaXRhbGljO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBwYWRkaW5nOiAwcHg7XG4gIG1hcmdpbjogMHB4O1xufVxuXG4ucmVxdWlyZWQge1xuICBjb2xvcjogI0ZGMDAwMDtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/views/dmd-isbn/dmd-isbn.component.ts":
/*!******************************************************!*\
  !*** ./src/app/views/dmd-isbn/dmd-isbn.component.ts ***!
  \******************************************************/
/*! exports provided: DmdIsbnComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DmdIsbnComponent", function() { return DmdIsbnComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var DmdIsbnComponent = /** @class */ (function () {
    function DmdIsbnComponent(router) {
        this.router = router;
    }
    DmdIsbnComponent.prototype.ngOnInit = function () {
    };
    DmdIsbnComponent.prototype.back = function () {
        this.router.navigate(['/home/dashboard']);
    };
    DmdIsbnComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    DmdIsbnComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dmd-isbn',
            template: __webpack_require__(/*! raw-loader!./dmd-isbn.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/dmd-isbn/dmd-isbn.component.html"),
            styles: [__webpack_require__(/*! ./dmd-isbn.component.scss */ "./src/app/views/dmd-isbn/dmd-isbn.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], DmdIsbnComponent);
    return DmdIsbnComponent;
}());



/***/ }),

/***/ "./src/app/views/dossier/dossier.component.scss":
/*!******************************************************!*\
  !*** ./src/app/views/dossier/dossier.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".telephone3 {\n  width: 40px;\n  padding-left: 0 !important;\n}\n\n.telephone4 {\n  width: 50px;\n  padding-left: 0 !important;\n}\n\n.divCentrer {\n  text-align: center;\n}\n\n.text-on-pannel {\n  background: #fff none repeat scroll 0 0;\n  height: auto;\n  margin-left: 20px;\n  padding: 3px 3px;\n  position: absolute;\n  margin-top: -15px;\n  border: 1px solid #fff;\n  color: dimgray;\n  border-radius: 3px;\n  font-weight: bold;\n  font-size: 12px;\n}\n\n.offspaces {\n  margin-top: 2px;\n  margin-bottom: 2px;\n  margin-left: 2px;\n  margin-right: 2px;\n  padding-top: 2px;\n  padding-bottom: 2px;\n  padding-left: 2px;\n  padding-right: 2px;\n}\n\n.nullspaces {\n  margin-top: 0px;\n  margin-bottom: 0px;\n  margin-left: 0px;\n  margin-right: 0px;\n  padding-top: 0px;\n  padding-bottom: 0px;\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.noborders {\n  border: none;\n}\n\n.bg-banq {\n  background-color: #d3dfc7;\n  font-weight: bold;\n  color: black;\n}\n\n.name {\n  vertical-align: top;\n  /*float: left;\n  clear:left; */\n  padding: 5px;\n  margin: 0px;\n  font-weight: bold;\n  font-size: 12px;\n  text-align: right;\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n  color: #000;\n  width: 130px;\n  min-height: 20px;\n  /*max-height: 30px;*/\n  min-width: 120px;\n  /*max-width:250px;*/\n  background-color: #d3dfc7;\n  border: 1px solid #c8c8c8;\n}\n\n.small-name {\n  vertical-align: top;\n  padding: 5px;\n  margin: 0px;\n  font-weight: bold;\n  font-size: 12px;\n  text-align: right;\n  color: #000;\n  /*width: 75px;*/\n  min-height: 20px;\n  /*max-height: 30px;\n  min-width: 120px;*/\n  /*max-width:250px;*/\n  background-color: #d3dfc7;\n  border: 1px solid #c8c8c8;\n}\n\n.hintText {\n  font-size: 75%;\n  font-style: italic;\n  font-weight: normal;\n  padding: 0px;\n  margin: 0px;\n}\n\n.required {\n  color: #FF0000;\n  padding-left: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvZG9zc2llci9FOlxcdHV0b1xccHJvamVjdHNcXGFuZ3VsYXJcXGNvcmV1aS9zcmNcXGFwcFxcdmlld3NcXGRvc3NpZXJcXGRvc3NpZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3ZpZXdzL2Rvc3NpZXIvZG9zc2llci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUFZLFdBQUE7RUFBVywwQkFBQTtBQ0V2Qjs7QUREQTtFQUFZLFdBQUE7RUFBVywwQkFBQTtBQ012Qjs7QURMQTtFQUFZLGtCQUFBO0FDU1o7O0FEUEE7RUFDSSx1Q0FBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBRUEsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FDU0o7O0FETkE7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNTSjs7QUROQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQ1NKOztBRE5BO0VBQ0ksWUFBQTtBQ1NKOztBRE5BO0VBQ0kseUJBQUE7RUFFQSxpQkFBQTtFQUNBLFlBQUE7QUNRSjs7QURMQTtFQUNJLG1CQUFBO0VBQ0E7ZUFBQTtFQUVBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxxQkFBQTtVQUFBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EseUJBQUE7QUNRSjs7QURMQTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQTtvQkFBQTtFQUVBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSx5QkFBQTtBQ1FKOztBRExBO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ1FKOztBRExBO0VBQ0ksY0FBQTtFQUNBLGlCQUFBO0FDUUoiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9kb3NzaWVyL2Rvc3NpZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi50ZWxlcGhvbmUze3dpZHRoOjQwcHg7cGFkZGluZy1sZWZ0OjAgIWltcG9ydGFudH1cbi50ZWxlcGhvbmU0e3dpZHRoOjUwcHg7cGFkZGluZy1sZWZ0OjAgIWltcG9ydGFudH1cbi5kaXZDZW50cmVye3RleHQtYWxpZ246Y2VudGVyO31cblxuLnRleHQtb24tcGFubmVsIHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmIG5vbmUgcmVwZWF0IHNjcm9sbCAwIDA7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xuICAgIHBhZGRpbmc6IDNweCAzcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIG1hcmdpbi10b3A6IC0xNXB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNmZmYvKmNmZDhkYyovXG4gICAgO1xuICAgIGNvbG9yOiBkaW1ncmF5O1xuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDEycHg7XG59XG5cbi5vZmZzcGFjZXMge1xuICAgIG1hcmdpbi10b3A6IDJweDtcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XG4gICAgbWFyZ2luLWxlZnQ6IDJweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDJweDtcbiAgICBwYWRkaW5nLXRvcDogMnB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAycHg7XG4gICAgcGFkZGluZy1sZWZ0OiAycHg7XG4gICAgcGFkZGluZy1yaWdodDogMnB4O1xufVxuXG4ubnVsbHNwYWNlcyB7XG4gICAgbWFyZ2luLXRvcDogMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDBweDtcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xuICAgIG1hcmdpbi1yaWdodDogMHB4O1xuICAgIHBhZGRpbmctdG9wOiAwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDBweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG5cbi5ub2JvcmRlcnMge1xuICAgIGJvcmRlcjogbm9uZTtcbn1cblxuLmJnLWJhbnEge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzcvKiNjNWUxYTUqL1xuICAgIDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBjb2xvcjogYmxhY2s7XG59XG5cbi5uYW1lIHtcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xuICAgIC8qZmxvYXQ6IGxlZnQ7XG5cdGNsZWFyOmxlZnQ7ICovXG4gICAgcGFkZGluZzogNXB4O1xuICAgIG1hcmdpbjogMHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICAgIGNvbG9yOiAjMDAwO1xuICAgIHdpZHRoOiAxMzBweDtcbiAgICBtaW4taGVpZ2h0OiAyMHB4O1xuICAgIC8qbWF4LWhlaWdodDogMzBweDsqL1xuICAgIG1pbi13aWR0aDogMTIwcHg7XG4gICAgLyptYXgtd2lkdGg6MjUwcHg7Ki9cbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDNkZmM3O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjOGM4Yzg7XG59XG5cbi5zbWFsbC1uYW1lIHtcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBtYXJnaW46IDBweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgY29sb3I6ICMwMDA7XG4gICAgLyp3aWR0aDogNzVweDsqL1xuICAgIG1pbi1oZWlnaHQ6IDIwcHg7XG4gICAgLyptYXgtaGVpZ2h0OiAzMHB4O1xuICAgIG1pbi13aWR0aDogMTIwcHg7Ki9cbiAgICAvKm1heC13aWR0aDoyNTBweDsqL1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzc7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2M4YzhjODtcbn1cblxuLmhpbnRUZXh0IHtcbiAgICBmb250LXNpemU6IDc1JTtcbiAgICBmb250LXN0eWxlOiBpdGFsaWM7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICBwYWRkaW5nOiAwcHg7XG4gICAgbWFyZ2luOiAwcHg7XG59XG5cbi5yZXF1aXJlZCB7XG4gICAgY29sb3I6ICNGRjAwMDA7XG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XG59XG4iLCIudGVsZXBob25lMyB7XG4gIHdpZHRoOiA0MHB4O1xuICBwYWRkaW5nLWxlZnQ6IDAgIWltcG9ydGFudDtcbn1cblxuLnRlbGVwaG9uZTQge1xuICB3aWR0aDogNTBweDtcbiAgcGFkZGluZy1sZWZ0OiAwICFpbXBvcnRhbnQ7XG59XG5cbi5kaXZDZW50cmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4udGV4dC1vbi1wYW5uZWwge1xuICBiYWNrZ3JvdW5kOiAjZmZmIG5vbmUgcmVwZWF0IHNjcm9sbCAwIDA7XG4gIGhlaWdodDogYXV0bztcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG4gIHBhZGRpbmc6IDNweCAzcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDogLTE1cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmZmY7XG4gIGNvbG9yOiBkaW1ncmF5O1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5vZmZzcGFjZXMge1xuICBtYXJnaW4tdG9wOiAycHg7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbiAgbWFyZ2luLWxlZnQ6IDJweDtcbiAgbWFyZ2luLXJpZ2h0OiAycHg7XG4gIHBhZGRpbmctdG9wOiAycHg7XG4gIHBhZGRpbmctYm90dG9tOiAycHg7XG4gIHBhZGRpbmctbGVmdDogMnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAycHg7XG59XG5cbi5udWxsc3BhY2VzIHtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuXG4ubm9ib3JkZXJzIHtcbiAgYm9yZGVyOiBub25lO1xufVxuXG4uYmctYmFucSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzc7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogYmxhY2s7XG59XG5cbi5uYW1lIHtcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgLypmbG9hdDogbGVmdDtcbiAgY2xlYXI6bGVmdDsgKi9cbiAgcGFkZGluZzogNXB4O1xuICBtYXJnaW46IDBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gIGNvbG9yOiAjMDAwO1xuICB3aWR0aDogMTMwcHg7XG4gIG1pbi1oZWlnaHQ6IDIwcHg7XG4gIC8qbWF4LWhlaWdodDogMzBweDsqL1xuICBtaW4td2lkdGg6IDEyMHB4O1xuICAvKm1heC13aWR0aDoyNTBweDsqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDNkZmM3O1xuICBib3JkZXI6IDFweCBzb2xpZCAjYzhjOGM4O1xufVxuXG4uc21hbGwtbmFtZSB7XG4gIHZlcnRpY2FsLWFsaWduOiB0b3A7XG4gIHBhZGRpbmc6IDVweDtcbiAgbWFyZ2luOiAwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBjb2xvcjogIzAwMDtcbiAgLyp3aWR0aDogNzVweDsqL1xuICBtaW4taGVpZ2h0OiAyMHB4O1xuICAvKm1heC1oZWlnaHQ6IDMwcHg7XG4gIG1pbi13aWR0aDogMTIwcHg7Ki9cbiAgLyptYXgtd2lkdGg6MjUwcHg7Ki9cbiAgYmFja2dyb3VuZC1jb2xvcjogI2QzZGZjNztcbiAgYm9yZGVyOiAxcHggc29saWQgI2M4YzhjODtcbn1cblxuLmhpbnRUZXh0IHtcbiAgZm9udC1zaXplOiA3NSU7XG4gIGZvbnQtc3R5bGU6IGl0YWxpYztcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgcGFkZGluZzogMHB4O1xuICBtYXJnaW46IDBweDtcbn1cblxuLnJlcXVpcmVkIHtcbiAgY29sb3I6ICNGRjAwMDA7XG4gIHBhZGRpbmctbGVmdDogNXB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/views/dossier/dossier.component.ts":
/*!****************************************************!*\
  !*** ./src/app/views/dossier/dossier.component.ts ***!
  \****************************************************/
/*! exports provided: DossierComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DossierComponent", function() { return DossierComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_data_editor__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/data/editor */ "./src/app/core/data/editor.ts");
/* harmony import */ var _core_models_alert__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/models/alert */ "./src/app/core/models/alert.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_shared_mock_states__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../core/shared/mock/states */ "./src/app/core/shared/mock/states.ts");
/* harmony import */ var _core_enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../core/enums/alert-type.enum */ "./src/app/core/enums/alert-type.enum.ts");
/* harmony import */ var _core_data_respondent__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../core/data/respondent */ "./src/app/core/data/respondent.ts");









/**
 * Composant de gestion du dossier d'un editeur
 */
var DossierComponent = /** @class */ (function () {
    function DossierComponent(router, fb) {
        this.router = router;
        this.fb = fb;
        this.editor = new _core_data_editor__WEBPACK_IMPORTED_MODULE_2__["Editor"]();
        this.states = [];
        this.isLoading = false;
        this.myAlert = new _core_models_alert__WEBPACK_IMPORTED_MODULE_3__["Alert"]('');
        this.errs = [];
        this.createForm();
    }
    /**
     * Initialisation du formulaire
     */
    DossierComponent.prototype.ngOnInit = function () {
        try {
            this.states = _core_shared_mock_states__WEBPACK_IMPORTED_MODULE_6__["STATES"];
            this.reset();
        }
        catch (error) {
            console.log('Une erreur sest produite: ', error);
            this.myAlert.displayAlert(_core_enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_7__["AlertType"].error, error);
        }
    };
    /**
     * Initialisation du formulaire dynamique de gestion des repondants
     */
    DossierComponent.prototype.createForm = function () {
        this.registerForm = this.fb.group({
            title: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            firstname: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(2)]],
            lastname: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(2)]],
            email: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(2)]],
            phone: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('^\d{10}$')]],
            phoneArea: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('^\d{3}$')]],
            phone3: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('^\d{3}$')]],
            phone4: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('^\d{4}$')]],
            poste: [null, []],
            principal: [false, []],
            persons: this.fb.array([])
        });
    };
    Object.defineProperty(DossierComponent.prototype, "persons", {
        /**
         * Retourne la liste des controles de saisie des repondants
         */
        get: function () {
            return this.registerForm.get('persons');
        },
        enumerable: true,
        configurable: true
    });
    /**
      * Suppression du repondant
      * @param id Numero de ligne
      */
    DossierComponent.prototype.remove = function (id) {
        this.persons.removeAt(id);
    };
    /**
     * Ajout d'un nouveau Repondant dans la liste
     */
    DossierComponent.prototype.add = function () {
        var r = new _core_data_respondent__WEBPACK_IMPORTED_MODULE_8__["Respondent"]();
        this.persons.push(this.fb.group({
            title: [r.titre, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            firstname: [r.nom, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(2)]],
            lastname: [r.prenom, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(2)]],
            email: [r.courriel, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(2)]],
            phone: [r.telephone, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
            phoneArea: [r.telephone, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('^\d{3}$')]],
            phone3: [r.telephone, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('^\d{3}$')]],
            phone4: [r.telephone, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('^\d{4}$')]],
            poste: [r.poste, []],
            principal: [r.principal, []]
        }));
    };
    /**
     * Lecture des infos saisies sur le formulaire
     */
    DossierComponent.prototype.readPersons = function () {
        this.errs = [];
        this.editor.repondants = new Array();
        this.editor.telephone = this.phone.prefx + this.phone.sufx + this.phone.racine;
        this.editor.telecopieur = this.telecop.prefx + this.telecop.sufx + this.telecop.racine;
        for (var _i = 0, _a = this.persons.controls; _i < _a.length; _i++) {
            var p = _a[_i];
            var r = new _core_data_respondent__WEBPACK_IMPORTED_MODULE_8__["Respondent"]();
            r.titre = p.get('title').value;
            r.nom = p.get('firstname').value;
            r.prenom = p.get('lastname').value;
            /* r.telephoneArea = p.get('phoneArea').value;
            r.telephoneTrois = p.get('phone3').value;
            r.telephoneReste = p.get('phone4').value; */
            r.telephone = p.get('phoneArea').value + p.get('phone3').value + p.get('phone4').value;
            r.courriel = p.get('email').value;
            r.poste = p.get('poste').value;
            if (r.titre == null || r.titre === '') {
                this.errs.push('Veuillez cocher l\'option Mme ou M. pour chacune des personnes ajoutées dans le formulaire.');
            }
            if (r.nom == null || r.nom.trim() === '') {
                this.errs.push('Veuillez entrer une donnée dans le champ Nom.');
            }
            if (r.prenom == null || r.prenom.trim() === '') {
                this.errs.push('Veuillez entrer une donnée dans le champ Prénom.');
            }
            if (r.courriel == null || r.courriel.trim() === '') {
                this.errs.push('Vous devez spécifier l\'adresse courriel du répondant principal.');
            }
            this.editor.repondants.push(r);
        }
        if (this.editor.nom == null || this.editor.nom.trim() === '') {
            this.errs.push('Veuillez entrer une donnée dans le champ Éditeur.');
        }
        if (this.editor.adresse1 == null || this.editor.adresse1.trim() === '') {
            this.errs.push('Veuillez entrer une donnée dans le champ Adresse postale.');
        }
        if (this.editor.ville == null || this.editor.ville.trim() === '') {
            this.errs.push('Veuillez entrer une donnée dans le champ Ville.');
        }
        if (this.editor.codePostal.length < 6 || this.editor.codePostal.length > 7) {
            this.errs.push('Le code postal entré est invalide.');
        }
        if (this.editor.telephone.length !== 10 || this.editor.telephone.trim() === '') {
            this.errs.push('Le numéro de téléphone de l\'éditeur est invalide : il doit comporter 10 chiffres dont 3 pour le code régional.');
        }
    };
    /**
     * Validation du formulaire d'edition des coordonnees de l'editeur
     */
    DossierComponent.prototype.submit = function () {
        try {
            this.readPersons();
            if (this.errs.length > 0) {
                return;
            }
            /*this.loginDAO.callRegisterAPI(this.editor).subscribe(
              (response) => {
                this.saved = response;
                this.myAlert.displayAlert(AlertType.success, 'Inscription terminee avec succes! Veuillez consulter votre courriel pour recevoir vos parametres de connexion. Votre identifiant est: ' + this.saved.idEditeur);
                this.reset();
              },
              (error) => this.myAlert.displayAlert(AlertType.error, 'Une erreur s\'est produite. Echec de la mise a jour: ' + error.message)
            ); */
        }
        catch (error) {
            this.myAlert.displayAlert(_core_enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_7__["AlertType"].error, error);
            console.log('Une erreur sest produite: ', error);
        }
    };
    /**
     * Reinitialisation du formulaire de MAJ du dossier de l'editeur
     */
    DossierComponent.prototype.reset = function () {
        this.editor = new _core_data_editor__WEBPACK_IMPORTED_MODULE_2__["Editor"]();
        this.registerForm.reset();
        this.phone = { prefx: '', sufx: '', racine: '' };
        this.telecop = { prefx: '', sufx: '', racine: '' };
    };
    /**
     * Retour a la page d'accueil
     */
    DossierComponent.prototype.back = function () {
        this.router.navigate(['/home/dashboard']);
    };
    DossierComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] }
    ]; };
    DossierComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dossier',
            template: __webpack_require__(/*! raw-loader!./dossier.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/dossier/dossier.component.html"),
            styles: [__webpack_require__(/*! ./dossier.component.scss */ "./src/app/views/dossier/dossier.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]])
    ], DossierComponent);
    return DossierComponent;
}());



/***/ }),

/***/ "./src/app/views/error/404.component.ts":
/*!**********************************************!*\
  !*** ./src/app/views/error/404.component.ts ***!
  \**********************************************/
/*! exports provided: P404Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "P404Component", function() { return P404Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var P404Component = /** @class */ (function () {
    function P404Component() {
    }
    P404Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            template: __webpack_require__(/*! raw-loader!./404.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/error/404.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], P404Component);
    return P404Component;
}());



/***/ }),

/***/ "./src/app/views/error/500.component.ts":
/*!**********************************************!*\
  !*** ./src/app/views/error/500.component.ts ***!
  \**********************************************/
/*! exports provided: P500Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "P500Component", function() { return P500Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var P500Component = /** @class */ (function () {
    function P500Component() {
    }
    P500Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            template: __webpack_require__(/*! raw-loader!./500.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/error/500.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], P500Component);
    return P500Component;
}());



/***/ }),

/***/ "./src/app/views/home/dashboard/dashboard.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/views/home/dashboard/dashboard.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".big-title {\n  color: #30aba1;\n  font-size: 18px;\n  font-weight: bold;\n  border-bottom: 1px solid #30aba1;\n  margin-bottom: 5px;\n  padding-bottom: 5px;\n  text-align: left;\n}\n\n.small-title {\n  color: #30aba1;\n  font-size: 14px;\n  font-weight: bold;\n  border-bottom: 1px solid #30aba1;\n  margin-bottom: 5px;\n}\n\n.bottom-link {\n  color: white;\n  font-size: 12px;\n  font-weight: bold;\n  text-decoration: underline;\n}\n\n.bottom-link2 {\n  color: #525252;\n  font-size: 12px;\n  text-decoration: underline;\n}\n\n.home-bande {\n  width: 100%;\n  background-color: #eeeeee;\n  padding: 10px;\n}\n\n.w100 {\n  width: 100%;\n}\n\n.b0 {\n  border: none;\n}\n\na {\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvaG9tZS9kYXNoYm9hcmQvRTpcXHR1dG9cXHByb2plY3RzXFxhbmd1bGFyXFxjb3JldWkvc3JjXFxhcHBcXHZpZXdzXFxob21lXFxkYXNoYm9hcmRcXGRhc2hib2FyZC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvdmlld3MvaG9tZS9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGdDQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FDQUY7O0FER0E7RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxrQkFBQTtBQ0FGOztBREtBO0VBQ0UsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLDBCQUFBO0FDRkY7O0FES0E7RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLDBCQUFBO0FDRkY7O0FETUE7RUFDRSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxhQUFBO0FDSEY7O0FETUE7RUFDRSxXQUFBO0FDSEY7O0FETUE7RUFDRSxZQUFBO0FDSEY7O0FETUE7RUFDRSxlQUFBO0FDSEYiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9ob21lL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi5iaWctdGl0bGV7XG4gIGNvbG9yOiByZ2IoNDgsIDE3MSwgMTYxKSA7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2IoNDgsIDE3MSwgMTYxKTtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuXG4uc21hbGwtdGl0bGV7XG4gIGNvbG9yOiByZ2IoNDgsIDE3MSwgMTYxKSA7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2IoNDgsIDE3MSwgMTYxKTtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuXG5cblxuLmJvdHRvbS1saW5re1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xufVxuXG4uYm90dG9tLWxpbmsye1xuICBjb2xvcjogIzUyNTI1MjtcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbn1cblxuXG4uaG9tZS1iYW5kZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjM4LCAyMzgsIDIzOCkgO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4udzEwMCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uYjAge1xuICBib3JkZXI6IG5vbmU7XG59XG5cbmF7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cbiIsIi5iaWctdGl0bGUge1xuICBjb2xvcjogIzMwYWJhMTtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICMzMGFiYTE7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn1cblxuLnNtYWxsLXRpdGxlIHtcbiAgY29sb3I6ICMzMGFiYTE7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjMzBhYmExO1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG59XG5cbi5ib3R0b20tbGluayB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG59XG5cbi5ib3R0b20tbGluazIge1xuICBjb2xvcjogIzUyNTI1MjtcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbn1cblxuLmhvbWUtYmFuZGUge1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VlZWVlZTtcbiAgcGFkZGluZzogMTBweDtcbn1cblxuLncxMDAge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmIwIHtcbiAgYm9yZGVyOiBub25lO1xufVxuXG5hIHtcbiAgZm9udC1zaXplOiAxMnB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/views/home/dashboard/dashboard.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/views/home/dashboard/dashboard.component.ts ***!
  \*************************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_shared_utils_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../core/shared/utils/helper */ "./src/app/core/shared/utils/helper.ts");



/**
 * Composant Contenu de la page d'accueil
 */
var DashboardComponent = /** @class */ (function () {
    function DashboardComponent() {
    }
    DashboardComponent.prototype.ngOnInit = function () { };
    DashboardComponent.prototype.isConnected = function () {
        return Object(_core_shared_utils_helper__WEBPACK_IMPORTED_MODULE_2__["isLoggedIn"])();
    };
    DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard2',
            template: __webpack_require__(/*! raw-loader!./dashboard.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/home/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/views/home/dashboard/dashboard.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/views/home/footer/footer.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/views/home/footer/footer.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#PdP {\n  clear: both;\n  text-align: center;\n  font-weight: bold;\n  color: #777;\n  font-size: 0.84em;\n  padding: 30px 0 15px 0;\n}\n\n#PdP a:link, #PdP a:visited {\n  font-weight: normal;\n  color: #777;\n  margin: 0 5px;\n}\n\n#PdP a.LienQuestion:link, #PdP a.LienQuestion:visited {\n  font-weight: bold;\n  color: #fff;\n}\n\n#PdP a:hover, #PdP a:focus {\n  text-decoration: none;\n}\n\n#PdP div.pdp1 {\n  border-bottom: 1px solid #226d9a;\n  /* position:relative; cree un bug sous IE8 */\n  padding-top: 2px;\n  overflow: hidden;\n  background-image: url(\"http://www.banq.qc.ca/images/interface09/pdp_bleu.gif\");\n  background-repeat: no-repeat;\n  background-position: center top;\n}\n\n#PdP div.pdp1 p {\n  font-weight: bold;\n  color: #fff;\n  padding: 1px 0;\n  text-align: center;\n  width: 600px;\n  margin: 0 auto;\n}\n\n#PdP p.pdp2 {\n  line-height: 2em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvaG9tZS9mb290ZXIvRTpcXHR1dG9cXHByb2plY3RzXFxhbmd1bGFyXFxjb3JldWkvc3JjXFxhcHBcXHZpZXdzXFxob21lXFxmb290ZXJcXGZvb3Rlci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvdmlld3MvaG9tZS9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtBQ0NGOztBRENBO0VBQ0UsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtBQ0VGOztBREFBO0VBQ0UsaUJBQUE7RUFDQSxXQUFBO0FDR0Y7O0FEREE7RUFDRSxxQkFBQTtBQ0lGOztBREZBO0VBQ0UsZ0NBQUE7RUFDQSw0Q0FBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSw4RUFBQTtFQUNBLDRCQUFBO0VBQ0EsK0JBQUE7QUNLRjs7QURIQTtFQUNFLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0FDTUY7O0FESkE7RUFDRSxnQkFBQTtBQ09GIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvaG9tZS9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI1BkUCB7XG4gIGNsZWFyOmJvdGg7XG4gIHRleHQtYWxpZ246Y2VudGVyO1xuICBmb250LXdlaWdodDpib2xkO1xuICBjb2xvcjojNzc3O1xuICBmb250LXNpemU6Ljg0ZW07XG4gIHBhZGRpbmc6IDMwcHggMCAxNXB4IDA7XG59XG4jUGRQIGE6bGluaywgI1BkUCBhOnZpc2l0ZWQge1xuICBmb250LXdlaWdodDpub3JtYWw7XG4gIGNvbG9yOiM3Nzc7XG4gIG1hcmdpbjowIDVweDtcbn1cbiNQZFAgYS5MaWVuUXVlc3Rpb246bGluaywgI1BkUCBhLkxpZW5RdWVzdGlvbjp2aXNpdGVkIHtcbiAgZm9udC13ZWlnaHQ6Ym9sZDtcbiAgY29sb3I6I2ZmZjtcbn1cbiNQZFAgYTpob3ZlciwgI1BkUCBhOmZvY3VzIHtcbiAgdGV4dC1kZWNvcmF0aW9uOm5vbmU7XG59XG4jUGRQIGRpdi5wZHAxIHtcbiAgYm9yZGVyLWJvdHRvbToxcHggc29saWQgIzIyNmQ5YTtcbiAgLyogcG9zaXRpb246cmVsYXRpdmU7IGNyZWUgdW4gYnVnIHNvdXMgSUU4ICovXG4gIHBhZGRpbmctdG9wOjJweDtcbiAgb3ZlcmZsb3c6aGlkZGVuO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJodHRwOi8vd3d3LmJhbnEucWMuY2EvaW1hZ2VzL2ludGVyZmFjZTA5L3BkcF9ibGV1LmdpZlwiKTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIHRvcDtcbn1cbiNQZFAgZGl2LnBkcDEgcCB7XG4gIGZvbnQtd2VpZ2h0OmJvbGQ7XG4gIGNvbG9yOiNmZmY7XG4gIHBhZGRpbmc6MXB4IDA7XG4gIHRleHQtYWxpZ246Y2VudGVyO1xuICB3aWR0aDo2MDBweDtcbiAgbWFyZ2luOiAwIGF1dG87XG59XG4jUGRQIHAucGRwMiB7XG4gIGxpbmUtaGVpZ2h0OjJlbTtcbn1cbiIsIiNQZFAge1xuICBjbGVhcjogYm90aDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM3Nzc7XG4gIGZvbnQtc2l6ZTogMC44NGVtO1xuICBwYWRkaW5nOiAzMHB4IDAgMTVweCAwO1xufVxuXG4jUGRQIGE6bGluaywgI1BkUCBhOnZpc2l0ZWQge1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBjb2xvcjogIzc3NztcbiAgbWFyZ2luOiAwIDVweDtcbn1cblxuI1BkUCBhLkxpZW5RdWVzdGlvbjpsaW5rLCAjUGRQIGEuTGllblF1ZXN0aW9uOnZpc2l0ZWQge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICNmZmY7XG59XG5cbiNQZFAgYTpob3ZlciwgI1BkUCBhOmZvY3VzIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuXG4jUGRQIGRpdi5wZHAxIHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICMyMjZkOWE7XG4gIC8qIHBvc2l0aW9uOnJlbGF0aXZlOyBjcmVlIHVuIGJ1ZyBzb3VzIElFOCAqL1xuICBwYWRkaW5nLXRvcDogMnB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJodHRwOi8vd3d3LmJhbnEucWMuY2EvaW1hZ2VzL2ludGVyZmFjZTA5L3BkcF9ibGV1LmdpZlwiKTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIHRvcDtcbn1cblxuI1BkUCBkaXYucGRwMSBwIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjZmZmO1xuICBwYWRkaW5nOiAxcHggMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aWR0aDogNjAwcHg7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG4jUGRQIHAucGRwMiB7XG4gIGxpbmUtaGVpZ2h0OiAyZW07XG59Il19 */"

/***/ }),

/***/ "./src/app/views/home/footer/footer.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/views/home/footer/footer.component.ts ***!
  \*******************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


/**
 * Composant Pied de la page d'accueil
 */
var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer2',
            template: __webpack_require__(/*! raw-loader!./footer.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/home/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/views/home/footer/footer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/views/home/header/header.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/views/home/header/header.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-green-banq {\n  background-color: #30aba1;\n}\n\n.text-green-banq {\n  color: #30aba1;\n}\n\n.bg-mauve {\n  background-color: #33749b;\n}\n\n.home-bande {\n  width: 100%;\n  background-color: #eeeeee;\n  padding: 10px;\n}\n\n.w100 {\n  width: 100%;\n}\n\n.b0 {\n  border: none;\n}\n\n.formMenuPrincipal a:active {\n  color: #30ABA1;\n}\n\n#divZoneMenuPrincipal {\n  margin-top: 3px;\n  color: #525252;\n}\n\n#divZoneMenuPrincipal a:link, #divZoneMenuPrincipal a:visited {\n  color: #525252;\n  text-decoration: none;\n}\n\n.titreMenuPrincipal:hover {\n  background-color: #f3f3f3;\n}\n\n.titreMenuPrincipal {\n  border-right: 2px solid #d6d6d6;\n  font-size: 0.84em;\n  font-weight: bold;\n}\n\n.titreMenuPrincipal ul li {\n  padding: 3px 10px;\n}\n\n#lienMenuAccueil {\n  padding-left: 0;\n}\n\n.titreMenuPrincipal a {\n  list-style-type: none;\n}\n\n.titreMenu {\n  margin: 0 10px;\n  padding: 2px 0;\n}\n\n.sousMenuPrincipal {\n  position: absolute;\n  padding: 0;\n  margin: 0;\n  display: none;\n  list-style-type: none;\n  color: #000;\n  background-color: #f3f3f3;\n  z-index: 100;\n}\n\n.sousMenuPrincipal li:hover {\n  background-color: #D6D6D6;\n}\n\n.hoverBackGroundColor:hover {\n  background-color: #D6D6D6;\n}\n\n.hoverPointer:hover {\n  cursor: pointer;\n}\n\na {\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvaG9tZS9oZWFkZXIvRTpcXHR1dG9cXHByb2plY3RzXFxhbmd1bGFyXFxjb3JldWkvc3JjXFxhcHBcXHZpZXdzXFxob21lXFxoZWFkZXJcXGhlYWRlci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvdmlld3MvaG9tZS9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UseUJBQUE7QUNDRjs7QURFQTtFQUNFLGNBQUE7QUNDRjs7QURFQTtFQUNFLHlCQUFBO0FDQ0Y7O0FERUE7RUFDRSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxhQUFBO0FDQ0Y7O0FERUE7RUFDRSxXQUFBO0FDQ0Y7O0FERUE7RUFDRSxZQUFBO0FDQ0Y7O0FER0E7RUFBNkIsY0FBQTtBQ0M3Qjs7QURBQTtFQUNDLGVBQUE7RUFDQSxjQUFBO0FDR0Q7O0FEQUE7RUFDRSxjQUFBO0VBQ0EscUJBQUE7QUNHRjs7QURBQTtFQUEyQix5QkFBQTtBQ0kzQjs7QURGQTtFQUNDLCtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtBQ0tEOztBREZBO0VBQTJCLGlCQUFBO0FDTTNCOztBREpBO0VBQWtCLGVBQUE7QUNRbEI7O0FETkE7RUFBdUIscUJBQUE7QUNVdkI7O0FEUkE7RUFBWSxjQUFBO0VBQWMsY0FBQTtBQ2ExQjs7QURYQTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDRCxhQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0MseUJBQUE7RUFDQSxZQUFBO0FDY0Y7O0FEWEE7RUFBNEIseUJBQUE7QUNlNUI7O0FEZEE7RUFBNkIseUJBQUE7QUNrQjdCOztBRGpCQTtFQUFxQixlQUFBO0FDcUJyQjs7QURuQkE7RUFDRSxlQUFBO0FDc0JGIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvaG9tZS9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJnLWdyZWVuLWJhbnEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoNDgsIDE3MSwgMTYxKSA7XG59XG5cbi50ZXh0LWdyZWVuLWJhbnEge1xuICBjb2xvcjogcmdiKDQ4LCAxNzEsIDE2MSkgO1xufVxuXG4uYmctbWF1dmUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoNTEsIDExNiwgMTU1KSA7XG59XG5cbi5ob21lLWJhbmRlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyMzgsIDIzOCwgMjM4KSA7XG4gIHBhZGRpbmc6IDEwcHg7XG59XG5cbi53MTAwIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5iMCB7XG4gIGJvcmRlcjogbm9uZTtcbn1cblxuXG4uZm9ybU1lbnVQcmluY2lwYWwgYTphY3RpdmUge2NvbG9yOiMzMEFCQTF9XG4jZGl2Wm9uZU1lbnVQcmluY2lwYWx7XG5cdG1hcmdpbi10b3A6M3B4O1xuXHRjb2xvcjojNTI1MjUyO1xufVxuXG4jZGl2Wm9uZU1lbnVQcmluY2lwYWwgYTpsaW5rLCAjZGl2Wm9uZU1lbnVQcmluY2lwYWwgYTp2aXNpdGVkIHtcbiAgY29sb3I6IzUyNTI1MjtcbiAgdGV4dC1kZWNvcmF0aW9uOm5vbmU7XG59XG5cbi50aXRyZU1lbnVQcmluY2lwYWw6aG92ZXIge2JhY2tncm91bmQtY29sb3I6I2YzZjNmMzt9XG5cbi50aXRyZU1lbnVQcmluY2lwYWwge1xuXHRib3JkZXItcmlnaHQ6MnB4IHNvbGlkIHJnYigyMTQsIDIxNCwgMjE0KTtcblx0Zm9udC1zaXplOi44NGVtO1xuXHRmb250LXdlaWdodDpib2xkO1xufVxuXG4udGl0cmVNZW51UHJpbmNpcGFsIHVsIGxpIHtwYWRkaW5nOiAzcHggMTBweDt9XG5cbiNsaWVuTWVudUFjY3VlaWwge3BhZGRpbmctbGVmdDowO31cblxuLnRpdHJlTWVudVByaW5jaXBhbCBhIHtsaXN0LXN0eWxlLXR5cGU6bm9uZTt9XG5cbi50aXRyZU1lbnUge21hcmdpbjowIDEwcHg7cGFkZGluZzoycHggMH1cblxuLnNvdXNNZW51UHJpbmNpcGFsIHtcbiAgcG9zaXRpb246YWJzb2x1dGU7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbjogMDtcblx0ZGlzcGxheTpub25lO1xuXHRsaXN0LXN0eWxlLXR5cGU6bm9uZTtcblx0Y29sb3I6IzAwMDtcbiAgYmFja2dyb3VuZC1jb2xvcjojZjNmM2YzO1xuICB6LWluZGV4OiAxMDA7XG59XG5cbi5zb3VzTWVudVByaW5jaXBhbCBsaTpob3ZlcntiYWNrZ3JvdW5kLWNvbG9yOiNENkQ2RDY7fVxuLmhvdmVyQmFja0dyb3VuZENvbG9yOmhvdmVyIHtiYWNrZ3JvdW5kLWNvbG9yOiNENkQ2RDY7fVxuLmhvdmVyUG9pbnRlcjpob3ZlciB7Y3Vyc29yOnBvaW50ZXJ9XG5cbmF7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cbiIsIi5iZy1ncmVlbi1iYW5xIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzMwYWJhMTtcbn1cblxuLnRleHQtZ3JlZW4tYmFucSB7XG4gIGNvbG9yOiAjMzBhYmExO1xufVxuXG4uYmctbWF1dmUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzM3NDliO1xufVxuXG4uaG9tZS1iYW5kZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlZWVlO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4udzEwMCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uYjAge1xuICBib3JkZXI6IG5vbmU7XG59XG5cbi5mb3JtTWVudVByaW5jaXBhbCBhOmFjdGl2ZSB7XG4gIGNvbG9yOiAjMzBBQkExO1xufVxuXG4jZGl2Wm9uZU1lbnVQcmluY2lwYWwge1xuICBtYXJnaW4tdG9wOiAzcHg7XG4gIGNvbG9yOiAjNTI1MjUyO1xufVxuXG4jZGl2Wm9uZU1lbnVQcmluY2lwYWwgYTpsaW5rLCAjZGl2Wm9uZU1lbnVQcmluY2lwYWwgYTp2aXNpdGVkIHtcbiAgY29sb3I6ICM1MjUyNTI7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cblxuLnRpdHJlTWVudVByaW5jaXBhbDpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmM2YzZjM7XG59XG5cbi50aXRyZU1lbnVQcmluY2lwYWwge1xuICBib3JkZXItcmlnaHQ6IDJweCBzb2xpZCAjZDZkNmQ2O1xuICBmb250LXNpemU6IDAuODRlbTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi50aXRyZU1lbnVQcmluY2lwYWwgdWwgbGkge1xuICBwYWRkaW5nOiAzcHggMTBweDtcbn1cblxuI2xpZW5NZW51QWNjdWVpbCB7XG4gIHBhZGRpbmctbGVmdDogMDtcbn1cblxuLnRpdHJlTWVudVByaW5jaXBhbCBhIHtcbiAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xufVxuXG4udGl0cmVNZW51IHtcbiAgbWFyZ2luOiAwIDEwcHg7XG4gIHBhZGRpbmc6IDJweCAwO1xufVxuXG4uc291c01lbnVQcmluY2lwYWwge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbjogMDtcbiAgZGlzcGxheTogbm9uZTtcbiAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xuICBjb2xvcjogIzAwMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzZjNmMztcbiAgei1pbmRleDogMTAwO1xufVxuXG4uc291c01lbnVQcmluY2lwYWwgbGk6aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRDZENkQ2O1xufVxuXG4uaG92ZXJCYWNrR3JvdW5kQ29sb3I6aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRDZENkQ2O1xufVxuXG4uaG92ZXJQb2ludGVyOmhvdmVyIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG5hIHtcbiAgZm9udC1zaXplOiAxMnB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/views/home/header/header.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/views/home/header/header.component.ts ***!
  \*******************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_services_business_login_manager_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../core/services/business/login-manager.service */ "./src/app/core/services/business/login-manager.service.ts");
/* harmony import */ var _assets_config_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../assets/config.json */ "./src/assets/config.json");
var _assets_config_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../assets/config.json */ "./src/assets/config.json", 1);




/**
 * Composant Entete de la page d'accueil
 */
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(el, login) {
        this.el = el;
        this.login = login;
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    /**
     * Affiche du menu deroulant a l'approche de la souris
     * @param id
     */
    HeaderComponent.prototype.onMouseEnter = function (id) {
        if (id === null || id === undefined)
            return;
        var strTitreMenuID = document.getElementById(id).id;
        if (strTitreMenuID === undefined)
            return;
        var strSous_TitreMenuID = 'sous_' + strTitreMenuID;
        document.getElementById(strSous_TitreMenuID).setAttribute('style', 'zIndex: 100');
        document.getElementById(strSous_TitreMenuID).setAttribute('style', 'display: block');
    };
    /**
     * Masquage du menu deroulant a la sortie de la souris
     * @param id
     */
    HeaderComponent.prototype.onmouseout = function (id) {
        if (id === null || id === undefined)
            return;
        var strTitreMenuID = document.getElementById(id).id;
        if (strTitreMenuID === undefined)
            return;
        var strSous_TitreMenuID = 'sous_' + strTitreMenuID;
        document.getElementById(strSous_TitreMenuID).style.setProperty('display', 'none');
    };
    /**
     * Deconnexion a l'application
     */
    HeaderComponent.prototype.logout = function () {
        this.login.logout();
    };
    Object.defineProperty(HeaderComponent.prototype, "logoutUrl", {
        get: function () {
            return _assets_config_json__WEBPACK_IMPORTED_MODULE_3__["logoutUrl"];
        },
        enumerable: true,
        configurable: true
    });
    HeaderComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _core_services_business_login_manager_service__WEBPACK_IMPORTED_MODULE_2__["LoginManagerService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('mouseenter'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], HeaderComponent.prototype, "onMouseEnter", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('mouseout'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], HeaderComponent.prototype, "onmouseout", null);
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header2',
            template: __webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/home/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/views/home/header/header.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _core_services_business_login_manager_service__WEBPACK_IMPORTED_MODULE_2__["LoginManagerService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/views/home/help/help.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/views/home/help/help.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#pdp {\n  border-top: 1px solid #30aba1;\n  font-weight: bold;\n  clear: both;\n  color: #8a8a8a;\n  text-align: center;\n  padding: 5px;\n  margin-top: 20px;\n}\n\n#pdp1 {\n  text-align: center;\n}\n\ndiv.pdp2 {\n  font-size: 0.8em;\n  /*width:15%;*/\n  /*background-color:#BBBECD;*/\n}\n\ndiv.pdp2, div.pdp2 a:link, div.pdp2 a:visited {\n  font-family: Verdana, Arial, Helvetica, sans-serif;\n  color: #3E3E3E;\n  line-height: 10px;\n  text-decoration: none;\n  margin-top: 10px;\n  margin-bottom: 10px;\n}\n\ndiv.pdp2 a:link, div.pdp2 a:visited {\n  text-decoration: underline;\n}\n\ndiv.pdp2 a:hover, div.pdp2 a:active {\n  color: #60688b;\n}\n\n.smalltextatbottom {\n  /* style pour 'Derniere modification:*/\n  color: #3E3E3E;\n  font-size: 0.8em;\n  text-decoration: none;\n  text-align: center;\n}\n\n.smalltextatbottom a, .smalltextatbottom a:link, .smalltextatbottom a:visited, .smalltextatbottom a:hover {\n  color: #3E3E3E;\n  text-decoration: underline;\n}\n\na:link, a:visited {\n  color: #00457b;\n}\n\na.lien2:link, a.lien2:visited {\n  font-weight: bold;\n  color: #397aaa;\n}\n\nh1 {\n  color: #3e3e3e;\n  margin-top: 0;\n  font-size: 1.6em;\n}\n\nh2 {\n  color: #195d95;\n  font-size: 1.4em;\n}\n\nh3 {\n  color: #3e3e3e;\n  font-size: 1.2em;\n}\n\nul.square {\n  list-style-type: square;\n}\n\nul.none {\n  list-style-type: none;\n}\n\nul.liste_media {\n  margin-top: -10px;\n}\n\nul.ListeLiensPage li {\n  font-size: 0.8em;\n  padding-top: 3px;\n}\n\nul.ListeLiensPage li ul li {\n  font-size: 0.9em;\n  padding-top: 3px;\n}\n\nul.ListeLiensPage li a {\n  font-size: 1.2em;\n}\n\na.pdf {\n  background-image: url(\"/extranet/images/interface/ico_pdf.gif\");\n  background-repeat: no-repeat;\n  background-position: center right;\n  padding-right: 20px;\n  margin-right: 2px;\n}\n\n.example-container {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n}\n\n.example-container > * {\n  width: 100%;\n}\n\n:host ::ng-deep .mat-form-field-wrapper {\n  padding: 0px;\n  margin: 0px;\n}\n\n:host ::ng-deep .mat-form-field:last-child div {\n  padding: 0px;\n  margin: 0px;\n}\n\n:host ::ng-deep .mat-form-field-label {\n  display: none;\n}\n\n:host ::ng-deep .mat-form-field-label-floating {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvaG9tZS9oZWxwL0U6XFx0dXRvXFxwcm9qZWN0c1xcYW5ndWxhclxcY29yZXVpL3NyY1xcYXBwXFx2aWV3c1xcaG9tZVxcaGVscFxcaGVscC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvdmlld3MvaG9tZS9oZWxwL2hlbHAuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSw2QkFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDTyxnQkFBQTtBQ0NUOztBREVBO0VBQ0Usa0JBQUE7QUNDRjs7QURFQTtFQUNFLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLDRCQUFBO0FDQ0Y7O0FER0E7RUFDRSxrREFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQ0FGOztBRElBO0VBRUUsMEJBQUE7QUNGRjs7QURNQTtFQUNHLGNBQUE7QUNISDs7QURNQTtFQUFxQixzQ0FBQTtFQUNuQixjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtFQUNPLGtCQUFBO0FDRlQ7O0FESUE7RUFDRSxjQUFBO0VBQ0EsMEJBQUE7QUNERjs7QURJQTtFQUNFLGNBQUE7QUNERjs7QURHQTtFQUNFLGlCQUFBO0VBQ0EsY0FBQTtBQ0FGOztBREVBO0VBQ0UsY0FBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtBQ0NGOztBRENBO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0FDRUY7O0FEQUE7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7QUNHRjs7QURBQTtFQUNFLHVCQUFBO0FDR0Y7O0FEQUE7RUFDRSxxQkFBQTtBQ0dGOztBREFBO0VBQ0UsaUJBQUE7QUNHRjs7QUREQTtFQUNFLGdCQUFBO0VBQ0EsZ0JBQUE7QUNJRjs7QUREQTtFQUNFLGdCQUFBO0VBQ0EsZ0JBQUE7QUNJRjs7QURGQTtFQUNFLGdCQUFBO0FDS0Y7O0FESEE7RUFDRSwrREFBQTtFQUNBLDRCQUFBO0VBQ0EsaUNBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDTUY7O0FEREE7RUFDRSxvQkFBQTtFQUFBLGFBQUE7RUFDQSw0QkFBQTtFQUFBLDZCQUFBO1VBQUEsc0JBQUE7QUNJRjs7QUREQTtFQUNFLFdBQUE7QUNJRjs7QUREQTtFQUNFLFlBQUE7RUFBYyxXQUFBO0FDS2hCOztBREhBO0VBQ0UsWUFBQTtFQUFjLFdBQUE7QUNPaEI7O0FETEE7RUFDRSxhQUFBO0FDUUY7O0FETkE7RUFDRSxhQUFBO0FDU0YiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9ob21lL2hlbHAvaGVscC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNwZHAge1xuICBib3JkZXItdG9wOjFweCBzb2xpZCAjMzBhYmExO1xuICBmb250LXdlaWdodDpib2xkO1xuICBjbGVhcjpib3RoO1xuICBjb2xvcjojOGE4YThhO1xuICB0ZXh0LWFsaWduOmNlbnRlcjtcbiAgcGFkZGluZzo1cHg7XG4gICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuXG4jcGRwMSB7XG4gIHRleHQtYWxpZ246Y2VudGVyO1xuXG59XG5kaXYucGRwMntcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgLyp3aWR0aDoxNSU7Ki9cbiAgLypiYWNrZ3JvdW5kLWNvbG9yOiNCQkJFQ0Q7Ki9cblxuXG59XG5kaXYucGRwMiwgZGl2LnBkcDIgYTpsaW5rLCBkaXYucGRwMiBhOnZpc2l0ZWQge1xuICBmb250LWZhbWlseTogVmVyZGFuYSwgQXJpYWwsIEhlbHZldGljYSwgc2Fucy1zZXJpZjtcbiAgY29sb3I6ICMzRTNFM0U7XG4gIGxpbmUtaGVpZ2h0OiAxMHB4O1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG5cbn1cblxuZGl2LnBkcDIgYTpsaW5rLCBkaXYucGRwMiBhOnZpc2l0ZWQge1xuXG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xuXG59XG5cbmRpdi5wZHAyIGE6aG92ZXIsICBkaXYucGRwMiBhOmFjdGl2ZSB7XG4gICBjb2xvcjogIzYwNjg4Yjtcbn1cblxuLnNtYWxsdGV4dGF0Ym90dG9tIHsgLyogc3R5bGUgcG91ciAnRGVybmllcmUgbW9kaWZpY2F0aW9uOiovXG4gIGNvbG9yOiAjM0UzRTNFO1xuICBmb250LXNpemU6IC44ZW07XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgICAgIHRleHQtYWxpZ24gOiBjZW50ZXI7XG59XG4uc21hbGx0ZXh0YXRib3R0b20gYSwgLnNtYWxsdGV4dGF0Ym90dG9tIGE6bGluaywgLnNtYWxsdGV4dGF0Ym90dG9tIGE6dmlzaXRlZCwgLnNtYWxsdGV4dGF0Ym90dG9tIGE6aG92ZXIge1xuICBjb2xvcjogIzNFM0UzRTtcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG59XG5cbmE6bGluaywgYTp2aXNpdGVkIHtcbiAgY29sb3I6IzAwNDU3Yjtcbn1cbmEubGllbjI6bGluaywgYS5saWVuMjp2aXNpdGVkIHtcbiAgZm9udC13ZWlnaHQ6Ym9sZDtcbiAgY29sb3I6IzM5N2FhYTtcbn1cbmgxIHtcbiAgY29sb3I6IzNlM2UzZTtcbiAgbWFyZ2luLXRvcDowO1xuICBmb250LXNpemU6MS42ZW07XG59XG5oMiB7XG4gIGNvbG9yOiMxOTVkOTU7XG4gIGZvbnQtc2l6ZToxLjRlbTtcbn1cbmgzIHtcbiAgY29sb3I6IzNlM2UzZTtcbiAgZm9udC1zaXplOjEuMmVtO1xufVxuXG51bC5zcXVhcmUgIHtcbiAgbGlzdC1zdHlsZS10eXBlOnNxdWFyZTtcbiAgfVxuXG51bC5ub25lICB7XG4gIGxpc3Qtc3R5bGUtdHlwZTpub25lO1xuICB9XG5cbnVsLmxpc3RlX21lZGlhIHtcbiAgbWFyZ2luLXRvcDogLTEwcHg7XG4gIH1cbnVsLkxpc3RlTGllbnNQYWdlIGxpIHtcbiAgZm9udC1zaXplOiAwLjgwZW07XG4gIHBhZGRpbmctdG9wOiAzcHg7XG59XG5cbnVsLkxpc3RlTGllbnNQYWdlIGxpIHVsIGxpIHtcbiAgZm9udC1zaXplOiAwLjkwZW07XG4gIHBhZGRpbmctdG9wOiAzcHg7XG59XG51bC5MaXN0ZUxpZW5zUGFnZSBsaSBhIHtcbiAgZm9udC1zaXplOiAxLjIwZW07XG59XG5hLnBkZiB7XG4gIGJhY2tncm91bmQtaW1hZ2U6dXJsKCcvZXh0cmFuZXQvaW1hZ2VzL2ludGVyZmFjZS9pY29fcGRmLmdpZicpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDpuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb246Y2VudGVyIHJpZ2h0O1xuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDJweDtcbn1cblxuXG5cbi5leGFtcGxlLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG59XG5cbi5leGFtcGxlLWNvbnRhaW5lciA+ICoge1xuICB3aWR0aDogMTAwJTtcbn1cblxuOmhvc3QgOjpuZy1kZWVwIC5tYXQtZm9ybS1maWVsZC13cmFwcGVye1xuICBwYWRkaW5nOiAwcHg7IG1hcmdpbjogMHB4O1xufVxuOmhvc3QgOjpuZy1kZWVwIC5tYXQtZm9ybS1maWVsZDpsYXN0LWNoaWxkIGRpdiB7XG4gIHBhZGRpbmc6IDBweDsgbWFyZ2luOiAwcHg7XG59XG46aG9zdCA6Om5nLWRlZXAgLm1hdC1mb3JtLWZpZWxkLWxhYmVse1xuICBkaXNwbGF5OiBub25lO1xufVxuOmhvc3QgOjpuZy1kZWVwIC5tYXQtZm9ybS1maWVsZC1sYWJlbC1mbG9hdGluZ3tcbiAgZGlzcGxheTogbm9uZTtcbn1cbiIsIiNwZHAge1xuICBib3JkZXItdG9wOiAxcHggc29saWQgIzMwYWJhMTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNsZWFyOiBib3RoO1xuICBjb2xvcjogIzhhOGE4YTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiA1cHg7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG59XG5cbiNwZHAxIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5kaXYucGRwMiB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIC8qd2lkdGg6MTUlOyovXG4gIC8qYmFja2dyb3VuZC1jb2xvcjojQkJCRUNEOyovXG59XG5cbmRpdi5wZHAyLCBkaXYucGRwMiBhOmxpbmssIGRpdi5wZHAyIGE6dmlzaXRlZCB7XG4gIGZvbnQtZmFtaWx5OiBWZXJkYW5hLCBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xuICBjb2xvcjogIzNFM0UzRTtcbiAgbGluZS1oZWlnaHQ6IDEwcHg7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cblxuZGl2LnBkcDIgYTpsaW5rLCBkaXYucGRwMiBhOnZpc2l0ZWQge1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbn1cblxuZGl2LnBkcDIgYTpob3ZlciwgZGl2LnBkcDIgYTphY3RpdmUge1xuICBjb2xvcjogIzYwNjg4Yjtcbn1cblxuLnNtYWxsdGV4dGF0Ym90dG9tIHtcbiAgLyogc3R5bGUgcG91ciAnRGVybmllcmUgbW9kaWZpY2F0aW9uOiovXG4gIGNvbG9yOiAjM0UzRTNFO1xuICBmb250LXNpemU6IDAuOGVtO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnNtYWxsdGV4dGF0Ym90dG9tIGEsIC5zbWFsbHRleHRhdGJvdHRvbSBhOmxpbmssIC5zbWFsbHRleHRhdGJvdHRvbSBhOnZpc2l0ZWQsIC5zbWFsbHRleHRhdGJvdHRvbSBhOmhvdmVyIHtcbiAgY29sb3I6ICMzRTNFM0U7XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xufVxuXG5hOmxpbmssIGE6dmlzaXRlZCB7XG4gIGNvbG9yOiAjMDA0NTdiO1xufVxuXG5hLmxpZW4yOmxpbmssIGEubGllbjI6dmlzaXRlZCB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzM5N2FhYTtcbn1cblxuaDEge1xuICBjb2xvcjogIzNlM2UzZTtcbiAgbWFyZ2luLXRvcDogMDtcbiAgZm9udC1zaXplOiAxLjZlbTtcbn1cblxuaDIge1xuICBjb2xvcjogIzE5NWQ5NTtcbiAgZm9udC1zaXplOiAxLjRlbTtcbn1cblxuaDMge1xuICBjb2xvcjogIzNlM2UzZTtcbiAgZm9udC1zaXplOiAxLjJlbTtcbn1cblxudWwuc3F1YXJlIHtcbiAgbGlzdC1zdHlsZS10eXBlOiBzcXVhcmU7XG59XG5cbnVsLm5vbmUge1xuICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG59XG5cbnVsLmxpc3RlX21lZGlhIHtcbiAgbWFyZ2luLXRvcDogLTEwcHg7XG59XG5cbnVsLkxpc3RlTGllbnNQYWdlIGxpIHtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZy10b3A6IDNweDtcbn1cblxudWwuTGlzdGVMaWVuc1BhZ2UgbGkgdWwgbGkge1xuICBmb250LXNpemU6IDAuOWVtO1xuICBwYWRkaW5nLXRvcDogM3B4O1xufVxuXG51bC5MaXN0ZUxpZW5zUGFnZSBsaSBhIHtcbiAgZm9udC1zaXplOiAxLjJlbTtcbn1cblxuYS5wZGYge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvZXh0cmFuZXQvaW1hZ2VzL2ludGVyZmFjZS9pY29fcGRmLmdpZlwiKTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIHJpZ2h0O1xuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDJweDtcbn1cblxuLmV4YW1wbGUtY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cblxuLmV4YW1wbGUtY29udGFpbmVyID4gKiB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG46aG9zdCA6Om5nLWRlZXAgLm1hdC1mb3JtLWZpZWxkLXdyYXBwZXIge1xuICBwYWRkaW5nOiAwcHg7XG4gIG1hcmdpbjogMHB4O1xufVxuXG46aG9zdCA6Om5nLWRlZXAgLm1hdC1mb3JtLWZpZWxkOmxhc3QtY2hpbGQgZGl2IHtcbiAgcGFkZGluZzogMHB4O1xuICBtYXJnaW46IDBweDtcbn1cblxuOmhvc3QgOjpuZy1kZWVwIC5tYXQtZm9ybS1maWVsZC1sYWJlbCB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbjpob3N0IDo6bmctZGVlcCAubWF0LWZvcm0tZmllbGQtbGFiZWwtZmxvYXRpbmcge1xuICBkaXNwbGF5OiBub25lO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/views/home/help/help.component.ts":
/*!***************************************************!*\
  !*** ./src/app/views/home/help/help.component.ts ***!
  \***************************************************/
/*! exports provided: HelpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelpComponent", function() { return HelpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var HelpComponent = /** @class */ (function () {
    function HelpComponent(router) {
        this.router = router;
    }
    HelpComponent.prototype.ngOnInit = function () {
    };
    HelpComponent.prototype.back = function () {
        this.router.navigate(['/home/dashboard']);
    };
    HelpComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    HelpComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-help',
            template: __webpack_require__(/*! raw-loader!./help.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/home/help/help.component.html"),
            styles: [__webpack_require__(/*! ./help.component.scss */ "./src/app/views/home/help/help.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], HelpComponent);
    return HelpComponent;
}());



/***/ }),

/***/ "./src/app/views/home/home.component.scss":
/*!************************************************!*\
  !*** ./src/app/views/home/home.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".w100 {\n  width: 100%;\n}\n\n.b0 {\n  border: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvaG9tZS9FOlxcdHV0b1xccHJvamVjdHNcXGFuZ3VsYXJcXGNvcmV1aS9zcmNcXGFwcFxcdmlld3NcXGhvbWVcXGhvbWUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3ZpZXdzL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNFLFdBQUE7QUNBRjs7QURHQTtFQUNFLFlBQUE7QUNBRiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuLncxMDAge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmIwIHtcbiAgYm9yZGVyOiBub25lO1xufVxuIiwiLncxMDAge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmIwIHtcbiAgYm9yZGVyOiBub25lO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/views/home/home.component.ts":
/*!**********************************************!*\
  !*** ./src/app/views/home/home.component.ts ***!
  \**********************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


/**
 * Composant page d'accueil
 */
var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/views/home/home.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/views/home/join-us/join-us.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/views/home/join-us/join-us.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#pdp {\n  border-top: 1px solid #30aba1;\n  font-weight: bold;\n  clear: both;\n  color: #8a8a8a;\n  text-align: center;\n  padding: 5px;\n  margin-top: 20px;\n}\n\n#pdp1 {\n  text-align: center;\n}\n\ndiv.pdp2 {\n  font-size: 0.8em;\n  /*width:15%;*/\n  /*background-color:#BBBECD;*/\n}\n\ndiv.pdp2, div.pdp2 a:link, div.pdp2 a:visited {\n  font-family: Verdana, Arial, Helvetica, sans-serif;\n  color: #3E3E3E;\n  line-height: 10px;\n  text-decoration: none;\n  margin-top: 10px;\n  margin-bottom: 10px;\n}\n\ndiv.pdp2 a:link, div.pdp2 a:visited {\n  text-decoration: underline;\n}\n\ndiv.pdp2 a:hover, div.pdp2 a:active {\n  color: #60688b;\n}\n\n.smalltextatbottom {\n  /* style pour 'Derniere modification:*/\n  color: #3E3E3E;\n  font-size: 0.8em;\n  text-decoration: none;\n  text-align: center;\n}\n\n.smalltextatbottom a, .smalltextatbottom a:link, .smalltextatbottom a:visited, .smalltextatbottom a:hover {\n  color: #3E3E3E;\n  text-decoration: underline;\n}\n\na:link, a:visited {\n  color: #00457b;\n}\n\na.lien2:link, a.lien2:visited {\n  font-weight: bold;\n  color: #397aaa;\n}\n\nh1 {\n  color: #3e3e3e;\n  margin-top: 0;\n  font-size: 1.6em;\n}\n\nh2 {\n  color: #195d95;\n  font-size: 1.4em;\n}\n\nh3 {\n  color: #3e3e3e;\n  font-size: 1.2em;\n}\n\nul.square {\n  list-style-type: square;\n}\n\nul.none {\n  list-style-type: none;\n}\n\nul.liste_media {\n  margin-top: -10px;\n}\n\nul.ListeLiensPage li {\n  font-size: 0.8em;\n  padding-top: 3px;\n}\n\nul.ListeLiensPage li ul li {\n  font-size: 0.9em;\n  padding-top: 3px;\n}\n\nul.ListeLiensPage li a {\n  font-size: 1.2em;\n}\n\na.pdf {\n  background-image: url(\"/extranet/images/interface/ico_pdf.gif\");\n  background-repeat: no-repeat;\n  background-position: center right;\n  padding-right: 20px;\n  margin-right: 2px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvaG9tZS9qb2luLXVzL0U6XFx0dXRvXFxwcm9qZWN0c1xcYW5ndWxhclxcY29yZXVpL3NyY1xcYXBwXFx2aWV3c1xcaG9tZVxcam9pbi11c1xcam9pbi11cy5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvdmlld3MvaG9tZS9qb2luLXVzL2pvaW4tdXMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSw2QkFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDTyxnQkFBQTtBQ0NUOztBREVBO0VBQ0Usa0JBQUE7QUNDRjs7QURFQTtFQUNFLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLDRCQUFBO0FDQ0Y7O0FER0E7RUFDRSxrREFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQ0FGOztBRElBO0VBRUUsMEJBQUE7QUNGRjs7QURNQTtFQUNHLGNBQUE7QUNISDs7QURNQTtFQUFxQixzQ0FBQTtFQUNuQixjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtFQUNPLGtCQUFBO0FDRlQ7O0FESUE7RUFDRSxjQUFBO0VBQ0EsMEJBQUE7QUNERjs7QURJQTtFQUNFLGNBQUE7QUNERjs7QURHQTtFQUNFLGlCQUFBO0VBQ0EsY0FBQTtBQ0FGOztBREVBO0VBQ0UsY0FBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtBQ0NGOztBRENBO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0FDRUY7O0FEQUE7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7QUNHRjs7QURBQTtFQUNFLHVCQUFBO0FDR0Y7O0FEQUE7RUFDRSxxQkFBQTtBQ0dGOztBREFBO0VBQ0UsaUJBQUE7QUNHRjs7QUREQTtFQUNFLGdCQUFBO0VBQ0EsZ0JBQUE7QUNJRjs7QUREQTtFQUNFLGdCQUFBO0VBQ0EsZ0JBQUE7QUNJRjs7QURGQTtFQUNFLGdCQUFBO0FDS0Y7O0FESEE7RUFDRSwrREFBQTtFQUNBLDRCQUFBO0VBQ0EsaUNBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDTUYiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9ob21lL2pvaW4tdXMvam9pbi11cy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNwZHAge1xuICBib3JkZXItdG9wOjFweCBzb2xpZCAjMzBhYmExO1xuICBmb250LXdlaWdodDpib2xkO1xuICBjbGVhcjpib3RoO1xuICBjb2xvcjojOGE4YThhO1xuICB0ZXh0LWFsaWduOmNlbnRlcjtcbiAgcGFkZGluZzo1cHg7XG4gICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuXG4jcGRwMSB7XG4gIHRleHQtYWxpZ246Y2VudGVyO1xuXG59XG5kaXYucGRwMntcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgLyp3aWR0aDoxNSU7Ki9cbiAgLypiYWNrZ3JvdW5kLWNvbG9yOiNCQkJFQ0Q7Ki9cblxuXG59XG5kaXYucGRwMiwgZGl2LnBkcDIgYTpsaW5rLCBkaXYucGRwMiBhOnZpc2l0ZWQge1xuICBmb250LWZhbWlseTogVmVyZGFuYSwgQXJpYWwsIEhlbHZldGljYSwgc2Fucy1zZXJpZjtcbiAgY29sb3I6ICMzRTNFM0U7XG4gIGxpbmUtaGVpZ2h0OiAxMHB4O1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG5cbn1cblxuZGl2LnBkcDIgYTpsaW5rLCBkaXYucGRwMiBhOnZpc2l0ZWQge1xuXG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xuXG59XG5cbmRpdi5wZHAyIGE6aG92ZXIsICBkaXYucGRwMiBhOmFjdGl2ZSB7XG4gICBjb2xvcjogIzYwNjg4Yjtcbn1cblxuLnNtYWxsdGV4dGF0Ym90dG9tIHsgLyogc3R5bGUgcG91ciAnRGVybmllcmUgbW9kaWZpY2F0aW9uOiovXG4gIGNvbG9yOiAjM0UzRTNFO1xuICBmb250LXNpemU6IC44ZW07XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgICAgIHRleHQtYWxpZ24gOiBjZW50ZXI7XG59XG4uc21hbGx0ZXh0YXRib3R0b20gYSwgLnNtYWxsdGV4dGF0Ym90dG9tIGE6bGluaywgLnNtYWxsdGV4dGF0Ym90dG9tIGE6dmlzaXRlZCwgLnNtYWxsdGV4dGF0Ym90dG9tIGE6aG92ZXIge1xuICBjb2xvcjogIzNFM0UzRTtcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG59XG5cbmE6bGluaywgYTp2aXNpdGVkIHtcbiAgY29sb3I6IzAwNDU3Yjtcbn1cbmEubGllbjI6bGluaywgYS5saWVuMjp2aXNpdGVkIHtcbiAgZm9udC13ZWlnaHQ6Ym9sZDtcbiAgY29sb3I6IzM5N2FhYTtcbn1cbmgxIHtcbiAgY29sb3I6IzNlM2UzZTtcbiAgbWFyZ2luLXRvcDowO1xuICBmb250LXNpemU6MS42ZW07XG59XG5oMiB7XG4gIGNvbG9yOiMxOTVkOTU7XG4gIGZvbnQtc2l6ZToxLjRlbTtcbn1cbmgzIHtcbiAgY29sb3I6IzNlM2UzZTtcbiAgZm9udC1zaXplOjEuMmVtO1xufVxuXG51bC5zcXVhcmUgIHtcbiAgbGlzdC1zdHlsZS10eXBlOnNxdWFyZTtcbiAgfVxuXG51bC5ub25lICB7XG4gIGxpc3Qtc3R5bGUtdHlwZTpub25lO1xuICB9XG5cbnVsLmxpc3RlX21lZGlhIHtcbiAgbWFyZ2luLXRvcDogLTEwcHg7XG4gIH1cbnVsLkxpc3RlTGllbnNQYWdlIGxpIHtcbiAgZm9udC1zaXplOiAwLjgwZW07XG4gIHBhZGRpbmctdG9wOiAzcHg7XG59XG5cbnVsLkxpc3RlTGllbnNQYWdlIGxpIHVsIGxpIHtcbiAgZm9udC1zaXplOiAwLjkwZW07XG4gIHBhZGRpbmctdG9wOiAzcHg7XG59XG51bC5MaXN0ZUxpZW5zUGFnZSBsaSBhIHtcbiAgZm9udC1zaXplOiAxLjIwZW07XG59XG5hLnBkZiB7XG4gIGJhY2tncm91bmQtaW1hZ2U6dXJsKCcvZXh0cmFuZXQvaW1hZ2VzL2ludGVyZmFjZS9pY29fcGRmLmdpZicpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDpuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb246Y2VudGVyIHJpZ2h0O1xuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDJweDtcbn1cbiIsIiNwZHAge1xuICBib3JkZXItdG9wOiAxcHggc29saWQgIzMwYWJhMTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNsZWFyOiBib3RoO1xuICBjb2xvcjogIzhhOGE4YTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiA1cHg7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG59XG5cbiNwZHAxIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5kaXYucGRwMiB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIC8qd2lkdGg6MTUlOyovXG4gIC8qYmFja2dyb3VuZC1jb2xvcjojQkJCRUNEOyovXG59XG5cbmRpdi5wZHAyLCBkaXYucGRwMiBhOmxpbmssIGRpdi5wZHAyIGE6dmlzaXRlZCB7XG4gIGZvbnQtZmFtaWx5OiBWZXJkYW5hLCBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xuICBjb2xvcjogIzNFM0UzRTtcbiAgbGluZS1oZWlnaHQ6IDEwcHg7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cblxuZGl2LnBkcDIgYTpsaW5rLCBkaXYucGRwMiBhOnZpc2l0ZWQge1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbn1cblxuZGl2LnBkcDIgYTpob3ZlciwgZGl2LnBkcDIgYTphY3RpdmUge1xuICBjb2xvcjogIzYwNjg4Yjtcbn1cblxuLnNtYWxsdGV4dGF0Ym90dG9tIHtcbiAgLyogc3R5bGUgcG91ciAnRGVybmllcmUgbW9kaWZpY2F0aW9uOiovXG4gIGNvbG9yOiAjM0UzRTNFO1xuICBmb250LXNpemU6IDAuOGVtO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnNtYWxsdGV4dGF0Ym90dG9tIGEsIC5zbWFsbHRleHRhdGJvdHRvbSBhOmxpbmssIC5zbWFsbHRleHRhdGJvdHRvbSBhOnZpc2l0ZWQsIC5zbWFsbHRleHRhdGJvdHRvbSBhOmhvdmVyIHtcbiAgY29sb3I6ICMzRTNFM0U7XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xufVxuXG5hOmxpbmssIGE6dmlzaXRlZCB7XG4gIGNvbG9yOiAjMDA0NTdiO1xufVxuXG5hLmxpZW4yOmxpbmssIGEubGllbjI6dmlzaXRlZCB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzM5N2FhYTtcbn1cblxuaDEge1xuICBjb2xvcjogIzNlM2UzZTtcbiAgbWFyZ2luLXRvcDogMDtcbiAgZm9udC1zaXplOiAxLjZlbTtcbn1cblxuaDIge1xuICBjb2xvcjogIzE5NWQ5NTtcbiAgZm9udC1zaXplOiAxLjRlbTtcbn1cblxuaDMge1xuICBjb2xvcjogIzNlM2UzZTtcbiAgZm9udC1zaXplOiAxLjJlbTtcbn1cblxudWwuc3F1YXJlIHtcbiAgbGlzdC1zdHlsZS10eXBlOiBzcXVhcmU7XG59XG5cbnVsLm5vbmUge1xuICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG59XG5cbnVsLmxpc3RlX21lZGlhIHtcbiAgbWFyZ2luLXRvcDogLTEwcHg7XG59XG5cbnVsLkxpc3RlTGllbnNQYWdlIGxpIHtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZy10b3A6IDNweDtcbn1cblxudWwuTGlzdGVMaWVuc1BhZ2UgbGkgdWwgbGkge1xuICBmb250LXNpemU6IDAuOWVtO1xuICBwYWRkaW5nLXRvcDogM3B4O1xufVxuXG51bC5MaXN0ZUxpZW5zUGFnZSBsaSBhIHtcbiAgZm9udC1zaXplOiAxLjJlbTtcbn1cblxuYS5wZGYge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvZXh0cmFuZXQvaW1hZ2VzL2ludGVyZmFjZS9pY29fcGRmLmdpZlwiKTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIHJpZ2h0O1xuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDJweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/views/home/join-us/join-us.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/views/home/join-us/join-us.component.ts ***!
  \*********************************************************/
/*! exports provided: JoinUsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JoinUsComponent", function() { return JoinUsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var JoinUsComponent = /** @class */ (function () {
    function JoinUsComponent(router) {
        this.router = router;
    }
    JoinUsComponent.prototype.ngOnInit = function () {
    };
    JoinUsComponent.prototype.back = function () {
        this.router.navigate(['/home/dashboard']);
    };
    JoinUsComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    JoinUsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-join-us',
            template: __webpack_require__(/*! raw-loader!./join-us.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/home/join-us/join-us.component.html"),
            styles: [__webpack_require__(/*! ./join-us.component.scss */ "./src/app/views/home/join-us/join-us.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], JoinUsComponent);
    return JoinUsComponent;
}());



/***/ }),

/***/ "./src/app/views/licence/edit-licence/edit-licence.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/views/licence/edit-licence/edit-licence.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".w100 {\n  width: 100%;\n}\n\n.bg-banq {\n  background-color: #d3dfc7;\n  font-weight: bold;\n  color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvbGljZW5jZS9lZGl0LWxpY2VuY2UvRTpcXHR1dG9cXHByb2plY3RzXFxhbmd1bGFyXFxjb3JldWkvc3JjXFxhcHBcXHZpZXdzXFxsaWNlbmNlXFxlZGl0LWxpY2VuY2VcXGVkaXQtbGljZW5jZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvdmlld3MvbGljZW5jZS9lZGl0LWxpY2VuY2UvZWRpdC1saWNlbmNlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsV0FBQTtBQ0FGOztBREdBO0VBQ0UseUJBQUE7RUFFQSxpQkFBQTtFQUNBLFlBQUE7QUNERiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL2xpY2VuY2UvZWRpdC1saWNlbmNlL2VkaXQtbGljZW5jZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuLncxMDB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uYmctYmFucSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzcvKiNjNWUxYTUqL1xuICA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogYmxhY2s7XG59XG4iLCIudzEwMCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uYmctYmFucSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzc7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogYmxhY2s7XG59Il19 */"

/***/ }),

/***/ "./src/app/views/licence/edit-licence/edit-licence.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/views/licence/edit-licence/edit-licence.component.ts ***!
  \**********************************************************************/
/*! exports provided: EditLicenceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditLicenceComponent", function() { return EditLicenceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var EditLicenceComponent = /** @class */ (function () {
    function EditLicenceComponent(router) {
        this.router = router;
    }
    EditLicenceComponent.prototype.ngOnInit = function () {
    };
    EditLicenceComponent.prototype.submit = function () {
    };
    EditLicenceComponent.prototype.back = function () {
        this.router.navigate(['/home/dashboard']);
    };
    EditLicenceComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    EditLicenceComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-edit-licence',
            template: __webpack_require__(/*! raw-loader!./edit-licence.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/licence/edit-licence/edit-licence.component.html"),
            styles: [__webpack_require__(/*! ./edit-licence.component.scss */ "./src/app/views/licence/edit-licence/edit-licence.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], EditLicenceComponent);
    return EditLicenceComponent;
}());



/***/ }),

/***/ "./src/app/views/licence/licence.component.scss":
/*!******************************************************!*\
  !*** ./src/app/views/licence/licence.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-banq {\n  background-color: #d3dfc7;\n  font-weight: bold;\n  color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvbGljZW5jZS9FOlxcdHV0b1xccHJvamVjdHNcXGFuZ3VsYXJcXGNvcmV1aS9zcmNcXGFwcFxcdmlld3NcXGxpY2VuY2VcXGxpY2VuY2UuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3ZpZXdzL2xpY2VuY2UvbGljZW5jZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNFLHlCQUFBO0VBRUEsaUJBQUE7RUFDQSxZQUFBO0FDREYiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9saWNlbmNlL2xpY2VuY2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi5iZy1iYW5xIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2QzZGZjNy8qI2M1ZTFhNSovXG4gIDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiBibGFjaztcbn1cbiIsIi5iZy1iYW5xIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2QzZGZjNztcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiBibGFjaztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/views/licence/licence.component.ts":
/*!****************************************************!*\
  !*** ./src/app/views/licence/licence.component.ts ***!
  \****************************************************/
/*! exports provided: LicenceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LicenceComponent", function() { return LicenceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



/**
 * Composant de consultation des licences de la BANQ
 */
var LicenceComponent = /** @class */ (function () {
    function LicenceComponent(router) {
        this.router = router;
        this.licences = [];
    }
    LicenceComponent.prototype.ngOnInit = function () {
        this.licences.push('P-1843-20140204.txt');
        this.licences.push('P-1879-20140317.txt');
        this.licences.push('LPW-1880-20140317.txt');
        this.licences.push('LPW-1881-20140317.txt');
        this.licences.push('LPW-1882-20140317.txt');
        this.licences.push('LPW-1883-20140317.txt');
        this.licences.push('LPW-2108-20140925.txt');
        this.licences.push('LPW-1735-20131018.txt');
        this.licences.push('LPW-1841-20140204.txt');
        this.licences.push('LPW-1894-20140328.txt');
        this.licences.push('LPW-2107-20140925.txt');
        this.licences.push('LPW-2362-20150421.txt');
        this.licences.push('LPW-2490-20150715.txt');
        this.licences.push('LPW-2366-20150421.txt');
        this.licences.push('LPW-2367-20150421.txt');
        this.licences.push('LPW-2368-20150421.txt');
        this.licences.push('LPW-2369-20150421.txt');
        this.licences.push('LPW-2372-20150421.txt');
        this.licences.push('LPW-3110-20160209.txt');
        this.licences.push('LPW-3111-20160209.txt');
        this.licences.push('LPW-3129-20160215.txt');
        this.licences.push('LPW-3346-20160816.txt');
        this.licences.push('LPW-3358-20160825.txt');
    };
    LicenceComponent.prototype.back = function () {
        this.router.navigate(['/home/dashboard']);
    };
    LicenceComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    LicenceComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-licence',
            template: __webpack_require__(/*! raw-loader!./licence.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/licence/licence.component.html"),
            styles: [__webpack_require__(/*! ./licence.component.scss */ "./src/app/views/licence/licence.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], LicenceComponent);
    return LicenceComponent;
}());



/***/ }),

/***/ "./src/app/views/list-isbn/list-isbn.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/views/list-isbn/list-isbn.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".w100 {\n  width: 100%;\n}\n\n.telephone3 {\n  width: 40px;\n  padding-left: 0 !important;\n}\n\n.telephone4 {\n  width: 50px;\n  padding-left: 0 !important;\n}\n\n.divCentrer {\n  text-align: center;\n}\n\n.text-on-pannel {\n  background: #fff none repeat scroll 0 0;\n  height: auto;\n  margin-left: 20px;\n  padding: 3px 3px;\n  position: absolute;\n  margin-top: -15px;\n  border: 1px solid #fff;\n  color: dimgray;\n  border-radius: 3px;\n  font-weight: bold;\n  font-size: 12px;\n}\n\n.offspaces {\n  margin-top: 2px;\n  margin-bottom: 2px;\n  margin-left: 2px;\n  margin-right: 2px;\n  padding-top: 2px;\n  padding-bottom: 2px;\n  padding-left: 2px;\n  padding-right: 2px;\n}\n\n.nullspaces {\n  margin-top: 0px;\n  margin-bottom: 0px;\n  margin-left: 0px;\n  margin-right: 0px;\n  padding-top: 0px;\n  padding-bottom: 0px;\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.noborders {\n  border: none;\n}\n\n.bg-banq {\n  background-color: #d3dfc7;\n  font-weight: bold;\n  color: black;\n  font-size: 11px;\n}\n\n.name {\n  vertical-align: top;\n  /*float: left;\n  clear:left; */\n  padding: 5px;\n  margin: 0px;\n  font-weight: bold;\n  font-size: 12px;\n  text-align: right;\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n  color: #000;\n  width: 130px;\n  min-height: 20px;\n  /*max-height: 30px;*/\n  min-width: 120px;\n  /*max-width:250px;*/\n  background-color: #d3dfc7;\n  border: 1px solid #c8c8c8;\n}\n\n.small-name {\n  vertical-align: top;\n  padding: 5px;\n  margin: 0px;\n  font-weight: bold;\n  font-size: 12px;\n  text-align: right;\n  color: #000;\n  /*width: 75px;*/\n  min-height: 20px;\n  /*max-height: 30px;\n  min-width: 120px;*/\n  /*max-width:250px;*/\n  background-color: #d3dfc7;\n  border: 1px solid #c8c8c8;\n}\n\n.hintText {\n  font-size: 75%;\n  font-style: italic;\n  font-weight: normal;\n  padding: 0px;\n  margin: 0px;\n}\n\n.required {\n  color: #FF0000;\n  padding-left: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvbGlzdC1pc2JuL0U6XFx0dXRvXFxwcm9qZWN0c1xcYW5ndWxhclxcY29yZXVpL3NyY1xcYXBwXFx2aWV3c1xcbGlzdC1pc2JuXFxsaXN0LWlzYm4uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3ZpZXdzL2xpc3QtaXNibi9saXN0LWlzYm4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDRSxXQUFBO0FDQUY7O0FERUE7RUFBWSxXQUFBO0VBQVcsMEJBQUE7QUNHdkI7O0FERkE7RUFBWSxXQUFBO0VBQVcsMEJBQUE7QUNPdkI7O0FETkE7RUFBWSxrQkFBQTtBQ1VaOztBRFJBO0VBQ0ksdUNBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtFQUVBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQ1VKOztBRFBBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDVUo7O0FEUEE7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNVSjs7QURQQTtFQUNJLFlBQUE7QUNVSjs7QURQQTtFQUNJLHlCQUFBO0VBRUEsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQ1NKOztBRE5BO0VBQ0ksbUJBQUE7RUFDQTtlQUFBO0VBRUEsWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO1VBQUEseUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSx5QkFBQTtBQ1NKOztBRE5BO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBO29CQUFBO0VBRUEsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLHlCQUFBO0FDU0o7O0FETkE7RUFDSSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDU0o7O0FETkE7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7QUNTSiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL2xpc3QtaXNibi9saXN0LWlzYm4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi53MTAwe1xuICB3aWR0aDogMTAwJTtcbn1cbi50ZWxlcGhvbmUze3dpZHRoOjQwcHg7cGFkZGluZy1sZWZ0OjAgIWltcG9ydGFudH1cbi50ZWxlcGhvbmU0e3dpZHRoOjUwcHg7cGFkZGluZy1sZWZ0OjAgIWltcG9ydGFudH1cbi5kaXZDZW50cmVye3RleHQtYWxpZ246Y2VudGVyO31cblxuLnRleHQtb24tcGFubmVsIHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmIG5vbmUgcmVwZWF0IHNjcm9sbCAwIDA7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xuICAgIHBhZGRpbmc6IDNweCAzcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIG1hcmdpbi10b3A6IC0xNXB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNmZmYvKmNmZDhkYyovXG4gICAgO1xuICAgIGNvbG9yOiBkaW1ncmF5O1xuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDEycHg7XG59XG5cbi5vZmZzcGFjZXMge1xuICAgIG1hcmdpbi10b3A6IDJweDtcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XG4gICAgbWFyZ2luLWxlZnQ6IDJweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDJweDtcbiAgICBwYWRkaW5nLXRvcDogMnB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAycHg7XG4gICAgcGFkZGluZy1sZWZ0OiAycHg7XG4gICAgcGFkZGluZy1yaWdodDogMnB4O1xufVxuXG4ubnVsbHNwYWNlcyB7XG4gICAgbWFyZ2luLXRvcDogMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDBweDtcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xuICAgIG1hcmdpbi1yaWdodDogMHB4O1xuICAgIHBhZGRpbmctdG9wOiAwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDBweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG5cbi5ub2JvcmRlcnMge1xuICAgIGJvcmRlcjogbm9uZTtcbn1cblxuLmJnLWJhbnEge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzcvKiNjNWUxYTUqL1xuICAgIDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBjb2xvcjogYmxhY2s7XG4gICAgZm9udC1zaXplOiAxMXB4O1xufVxuXG4ubmFtZSB7XG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgICAvKmZsb2F0OiBsZWZ0O1xuXHRjbGVhcjpsZWZ0OyAqL1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBtYXJnaW46IDBweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgICBjb2xvcjogIzAwMDtcbiAgICB3aWR0aDogMTMwcHg7XG4gICAgbWluLWhlaWdodDogMjBweDtcbiAgICAvKm1heC1oZWlnaHQ6IDMwcHg7Ki9cbiAgICBtaW4td2lkdGg6IDEyMHB4O1xuICAgIC8qbWF4LXdpZHRoOjI1MHB4OyovXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2QzZGZjNztcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjYzhjOGM4O1xufVxuXG4uc21hbGwtbmFtZSB7XG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgbWFyZ2luOiAwcHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIGNvbG9yOiAjMDAwO1xuICAgIC8qd2lkdGg6IDc1cHg7Ki9cbiAgICBtaW4taGVpZ2h0OiAyMHB4O1xuICAgIC8qbWF4LWhlaWdodDogMzBweDtcbiAgICBtaW4td2lkdGg6IDEyMHB4OyovXG4gICAgLyptYXgtd2lkdGg6MjUwcHg7Ki9cbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDNkZmM3O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjOGM4Yzg7XG59XG5cbi5oaW50VGV4dCB7XG4gICAgZm9udC1zaXplOiA3NSU7XG4gICAgZm9udC1zdHlsZTogaXRhbGljO1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgcGFkZGluZzogMHB4O1xuICAgIG1hcmdpbjogMHB4O1xufVxuXG4ucmVxdWlyZWQge1xuICAgIGNvbG9yOiAjRkYwMDAwO1xuICAgIHBhZGRpbmctbGVmdDogNXB4O1xufVxuIiwiLncxMDAge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLnRlbGVwaG9uZTMge1xuICB3aWR0aDogNDBweDtcbiAgcGFkZGluZy1sZWZ0OiAwICFpbXBvcnRhbnQ7XG59XG5cbi50ZWxlcGhvbmU0IHtcbiAgd2lkdGg6IDUwcHg7XG4gIHBhZGRpbmctbGVmdDogMCAhaW1wb3J0YW50O1xufVxuXG4uZGl2Q2VudHJlciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnRleHQtb24tcGFubmVsIHtcbiAgYmFja2dyb3VuZDogI2ZmZiBub25lIHJlcGVhdCBzY3JvbGwgMCAwO1xuICBoZWlnaHQ6IGF1dG87XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xuICBwYWRkaW5nOiAzcHggM3B4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi10b3A6IC0xNXB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZmZmO1xuICBjb2xvcjogZGltZ3JheTtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4ub2Zmc3BhY2VzIHtcbiAgbWFyZ2luLXRvcDogMnB4O1xuICBtYXJnaW4tYm90dG9tOiAycHg7XG4gIG1hcmdpbi1sZWZ0OiAycHg7XG4gIG1hcmdpbi1yaWdodDogMnB4O1xuICBwYWRkaW5nLXRvcDogMnB4O1xuICBwYWRkaW5nLWJvdHRvbTogMnB4O1xuICBwYWRkaW5nLWxlZnQ6IDJweDtcbiAgcGFkZGluZy1yaWdodDogMnB4O1xufVxuXG4ubnVsbHNwYWNlcyB7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBtYXJnaW4tbGVmdDogMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDBweDtcbiAgcGFkZGluZy10b3A6IDBweDtcbiAgcGFkZGluZy1ib3R0b206IDBweDtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cblxuLm5vYm9yZGVycyB7XG4gIGJvcmRlcjogbm9uZTtcbn1cblxuLmJnLWJhbnEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDNkZmM3O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDExcHg7XG59XG5cbi5uYW1lIHtcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgLypmbG9hdDogbGVmdDtcbiAgY2xlYXI6bGVmdDsgKi9cbiAgcGFkZGluZzogNXB4O1xuICBtYXJnaW46IDBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gIGNvbG9yOiAjMDAwO1xuICB3aWR0aDogMTMwcHg7XG4gIG1pbi1oZWlnaHQ6IDIwcHg7XG4gIC8qbWF4LWhlaWdodDogMzBweDsqL1xuICBtaW4td2lkdGg6IDEyMHB4O1xuICAvKm1heC13aWR0aDoyNTBweDsqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDNkZmM3O1xuICBib3JkZXI6IDFweCBzb2xpZCAjYzhjOGM4O1xufVxuXG4uc21hbGwtbmFtZSB7XG4gIHZlcnRpY2FsLWFsaWduOiB0b3A7XG4gIHBhZGRpbmc6IDVweDtcbiAgbWFyZ2luOiAwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBjb2xvcjogIzAwMDtcbiAgLyp3aWR0aDogNzVweDsqL1xuICBtaW4taGVpZ2h0OiAyMHB4O1xuICAvKm1heC1oZWlnaHQ6IDMwcHg7XG4gIG1pbi13aWR0aDogMTIwcHg7Ki9cbiAgLyptYXgtd2lkdGg6MjUwcHg7Ki9cbiAgYmFja2dyb3VuZC1jb2xvcjogI2QzZGZjNztcbiAgYm9yZGVyOiAxcHggc29saWQgI2M4YzhjODtcbn1cblxuLmhpbnRUZXh0IHtcbiAgZm9udC1zaXplOiA3NSU7XG4gIGZvbnQtc3R5bGU6IGl0YWxpYztcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgcGFkZGluZzogMHB4O1xuICBtYXJnaW46IDBweDtcbn1cblxuLnJlcXVpcmVkIHtcbiAgY29sb3I6ICNGRjAwMDA7XG4gIHBhZGRpbmctbGVmdDogNXB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/views/list-isbn/list-isbn.component.ts":
/*!********************************************************!*\
  !*** ./src/app/views/list-isbn/list-isbn.component.ts ***!
  \********************************************************/
/*! exports provided: ListIsbnComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListIsbnComponent", function() { return ListIsbnComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var ListIsbnComponent = /** @class */ (function () {
    function ListIsbnComponent(router) {
        this.router = router;
    }
    ListIsbnComponent.prototype.ngOnInit = function () {
    };
    ListIsbnComponent.prototype.back = function () {
        this.router.navigate(['/home/dashboard']);
    };
    ListIsbnComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    ListIsbnComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-isbn',
            template: __webpack_require__(/*! raw-loader!./list-isbn.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/list-isbn/list-isbn.component.html"),
            styles: [__webpack_require__(/*! ./list-isbn.component.scss */ "./src/app/views/list-isbn/list-isbn.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ListIsbnComponent);
    return ListIsbnComponent;
}());



/***/ }),

/***/ "./src/app/views/login/login.component.scss":
/*!**************************************************!*\
  !*** ./src/app/views/login/login.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  padding: 0;\n  margin: 0;\n}\n\nbody {\n  font-size: 0.92em;\n  color: #333;\n  font-family: Helvetica, Arial, sans-serif;\n}\n\n#Global {\n  /*margin:0 auto;\n  padding:0;\n  background-color:#fff;\n  width:940px;*/\n  position: relative;\n}\n\n#Entete p {\n  margin: 12px 0 13px 0;\n  text-align: right;\n  font-size: 0.76em;\n}\n\n#Entete.BarreNoire {\n  border-bottom: 34px solid #000;\n}\n\n#Entete a:link, #Entete a:visited {\n  text-decoration: underline;\n  color: #646464;\n  margin: 3px;\n}\n\n#Entete a:hover, #Entete a:focus {\n  text-decoration: none;\n}\n\n#Entete a.Lang {\n  margin-left: 20px;\n}\n\n#Contenu {\n  padding: 10px 0 10px 185px;\n  background: #ffffff url(\"https://www.banq.qc.ca/images/interface09/logo_BAnQ.jpg\") no-repeat left bottom;\n}\n\nh1 {\n  margin: 7px 10px;\n  color: #4c4c4c;\n  font-size: 1.4em;\n}\n\n#loginChemin {\n  margin: 7px 10px;\n  font-size: 0.8em;\n}\n\n#loginChemin a:link, #loginChemin a:visited {\n  text-decoration: underline;\n  color: #31697f;\n}\n\n#loginChemin a:hover, #loginChemin a:focus {\n  text-decoration: none;\n  color: #31697f;\n}\n\n.EnteteForm {\n  border: 1px solid #b1b9bf;\n  margin: 10px 0 1px 0;\n  height: 32px;\n  background-color: #c9d1d4;\n}\n\n.Wrapper {\n  border: 1px solid #b1b9bf;\n  overflow: hidden;\n  padding: 10px 10px 5px 10px;\n  font-size: 0.92em;\n}\n\n.ColG {\n  float: left;\n  width: 48%;\n  padding-right: 10px;\n}\n\n.ColG a:link, .ColG a:visited {\n  text-decoration: none;\n  color: #333;\n}\n\n.ColG a:hover, .ColG a:focus {\n  text-decoration: underline;\n  color: #333;\n}\n\n.Chapo {\n  font-weight: bold;\n}\n\nlabel {\n  display: block;\n  font-size: 0.8em;\n  margin-top: 7px;\n}\n\ninput[type=text], input[type=password] {\n  width: 80%;\n}\n\ninput[type=submit], .btnSubmit {\n  font-weight: bold;\n  background-color: #8edbd8;\n  border: 2px solid #52c8c4;\n  border-radius: 5px;\n  margin: 20px 10px 20px 30px;\n  padding: 2px 14px;\n  cursor: pointer;\n}\n\ninput[type=submit]:hover {\n  background-color: #f4f4f4;\n  border: 2px solid #8edbd8;\n}\n\n.ColD {\n  float: left;\n  width: 47%;\n  margin: 0 0 10px 0;\n  border-left: 1px solid #b1b9bf;\n  padding: 0 10px;\n}\n\n.ColD a:link, .ColD a:visited {\n  display: block;\n  text-align: center;\n  text-decoration: none;\n  color: #333;\n  margin: 15px 0;\n  padding: 7px;\n}\n\n.ColD .Bouton:link, .ColD .Bouton:visited {\n  border: 1px solid #b4e7e5;\n  background-color: #e4fcfc;\n  font-weight: bold;\n}\n\n.ColD a:hover, .ColD a:focus {\n  text-decoration: underline;\n  color: #333;\n}\n\n.ColD .Bouton:hover, .ColD .Bouton:focus {\n  color: #52c8c4;\n  border: 1px solid #52c8c4;\n  background-color: #b4e7e5;\n  text-decoration: none;\n}\n\n.PdP {\n  margin-left: 185px;\n  text-align: center;\n  font-size: 0.8em;\n}\n\n.divReponse {\n  margin: 0 auto 10px auto;\n  text-align: left;\n  border: 1px solid #e7e7e7;\n  width: 340px;\n  padding: 8px;\n}\n\n.divReponse img {\n  border: none;\n  float: right;\n}\n\n.divReponse li {\n  margin-left: 20px;\n  padding-bottom: 10px;\n}\n\n#contenu_reponse_abon {\n  background-color: #eceff1;\n}\n\n#contenu_reponse_abon a:link, #contenu_reponse_abon a:visited {\n  display: inline;\n  text-align: left;\n  margin: 0px;\n  padding: 0px;\n}\n\n#contenu_reponse_abon a:link, #contenu_reponse_abon a:visited {\n  text-decoration: underline;\n  color: #646464;\n}\n\n#contenu_reponse_abon a:hover, #contenu_reponse_abon a:focus {\n  text-decoration: none;\n}\n\np {\n  margin: 20px 5px;\n}\n\nul {\n  margin-left: 15px;\n}\n\n/* Ajout pour Utilisation BD */\n\n.Ariane {\n  font-size: 0.88em;\n}\n\n.Bouton {\n  text-align: center;\n  font-weight: bold;\n  font-size: 1em;\n  display: inline-block;\n  margin: 10px;\n  background-color: #b4e7e5;\n  border: 1px solid #003a9d;\n  width: 100%;\n}\n\n.Bouton a {\n  display: inline-block;\n  margin: 10px;\n  color: #003a9d;\n}\n\n.p0m0 {\n  padding: 0px;\n  margin: 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvbG9naW4vRTpcXHR1dG9cXHByb2plY3RzXFxhbmd1bGFyXFxjb3JldWkvc3JjXFxhcHBcXHZpZXdzXFxsb2dpblxcbG9naW4uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3ZpZXdzL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksVUFBQTtFQUNBLFNBQUE7QUNDSjs7QURDRTtFQUNFLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLHlDQUFBO0FDRUo7O0FEQUU7RUFDRTs7O2VBQUE7RUFJQSxrQkFBQTtBQ0dKOztBRERFO0VBQVcscUJBQUE7RUFBc0IsaUJBQUE7RUFBa0IsaUJBQUE7QUNPckQ7O0FETkU7RUFBb0IsOEJBQUE7QUNVdEI7O0FEVEU7RUFBbUMsMEJBQUE7RUFBMkIsY0FBQTtFQUFlLFdBQUE7QUNlL0U7O0FEZEU7RUFBa0MscUJBQUE7QUNrQnBDOztBRGpCRTtFQUFnQixpQkFBQTtBQ3FCbEI7O0FEcEJFO0VBQVUsMEJBQUE7RUFBMEIsd0dBQUE7QUN5QnRDOztBRHhCRTtFQUFJLGdCQUFBO0VBQWlCLGNBQUE7RUFBYyxnQkFBQTtBQzhCckM7O0FEN0JFO0VBQWMsZ0JBQUE7RUFBaUIsZ0JBQUE7QUNrQ2pDOztBRGpDRTtFQUE2QywwQkFBQTtFQUEwQixjQUFBO0FDc0N6RTs7QURyQ0U7RUFBNEMscUJBQUE7RUFBcUIsY0FBQTtBQzBDbkU7O0FEekNFO0VBQWEseUJBQUE7RUFBeUIsb0JBQUE7RUFBb0IsWUFBQTtFQUFZLHlCQUFBO0FDZ0R4RTs7QUQvQ0U7RUFBVSx5QkFBQTtFQUF5QixnQkFBQTtFQUFnQiwyQkFBQTtFQUEyQixpQkFBQTtBQ3NEaEY7O0FEckRFO0VBQU8sV0FBQTtFQUFXLFVBQUE7RUFBVSxtQkFBQTtBQzJEOUI7O0FEMURFO0VBQStCLHFCQUFBO0VBQXNCLFdBQUE7QUMrRHZEOztBRDlERTtFQUErQiwwQkFBQTtFQUEwQixXQUFBO0FDbUUzRDs7QURsRUU7RUFBUSxpQkFBQTtBQ3NFVjs7QURyRUU7RUFBTyxjQUFBO0VBQWMsZ0JBQUE7RUFBZSxlQUFBO0FDMkV0Qzs7QUQxRUU7RUFBd0MsVUFBQTtBQzhFMUM7O0FEN0VFO0VBQWdDLGlCQUFBO0VBQWlCLHlCQUFBO0VBQXlCLHlCQUFBO0VBQXlCLGtCQUFBO0VBQ25HLDJCQUFBO0VBQTJCLGlCQUFBO0VBQWlCLGVBQUE7QUNzRjlDOztBRHJGRTtFQUEwQix5QkFBQTtFQUF5Qix5QkFBQTtBQzBGckQ7O0FEekZFO0VBQU8sV0FBQTtFQUFXLFVBQUE7RUFBVSxrQkFBQTtFQUFrQiw4QkFBQTtFQUE4QixlQUFBO0FDaUc5RTs7QURoR0U7RUFBK0IsY0FBQTtFQUFjLGtCQUFBO0VBQWtCLHFCQUFBO0VBQXFCLFdBQUE7RUFBVyxjQUFBO0VBQWMsWUFBQTtBQ3lHL0c7O0FEeEdFO0VBQTJDLHlCQUFBO0VBQTBCLHlCQUFBO0VBQTBCLGlCQUFBO0FDOEdqRzs7QUQ3R0U7RUFBK0IsMEJBQUE7RUFBMEIsV0FBQTtBQ2tIM0Q7O0FEakhFO0VBQTBDLGNBQUE7RUFBZ0IseUJBQUE7RUFBMEIseUJBQUE7RUFBeUIscUJBQUE7QUN3SC9HOztBRHZIRTtFQUFNLGtCQUFBO0VBQWtCLGtCQUFBO0VBQWtCLGdCQUFBO0FDNkg1Qzs7QUQ1SEU7RUFDRSx3QkFBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQytISjs7QUQ3SEU7RUFDRSxZQUFBO0VBQ0EsWUFBQTtBQ2dJSjs7QUQ5SEU7RUFDRSxpQkFBQTtFQUNBLG9CQUFBO0FDaUlKOztBRC9IRTtFQUNFLHlCQUFBO0FDa0lKOztBRGhJRTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDbUlOOztBRGpJRTtFQUErRCwwQkFBQTtFQUEwQixjQUFBO0FDc0kzRjs7QURySUU7RUFBOEQscUJBQUE7QUN5SWhFOztBRHhJRTtFQUNJLGdCQUFBO0FDMklOOztBRHpJRTtFQUVJLGlCQUFBO0FDMklOOztBRHpJRSw4QkFBQTs7QUFDQTtFQUFTLGlCQUFBO0FDNklYOztBRDVJRTtFQUFTLGtCQUFBO0VBQWtCLGlCQUFBO0VBQWlCLGNBQUE7RUFBZSxxQkFBQTtFQUFxQixZQUFBO0VBQWEseUJBQUE7RUFBeUIseUJBQUE7RUFBeUIsV0FBQTtBQ3VKako7O0FEdEpFO0VBQVkscUJBQUE7RUFBcUIsWUFBQTtFQUFhLGNBQUE7QUM0SmhEOztBRDFKRTtFQUNFLFlBQUE7RUFDQSxXQUFBO0FDNkpKIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIqIHtcbiAgICBwYWRkaW5nOjA7XG4gICAgbWFyZ2luOjA7XG4gIH1cbiAgYm9keSB7XG4gICAgZm9udC1zaXplOjAuOTJlbTtcbiAgICBjb2xvcjojMzMzO1xuICAgIGZvbnQtZmFtaWx5OkhlbHZldGljYSwgQXJpYWwsIHNhbnMtc2VyaWY7XG4gIH1cbiAgI0dsb2JhbCB7XG4gICAgLyptYXJnaW46MCBhdXRvO1xuICAgIHBhZGRpbmc6MDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNmZmY7XG4gICAgd2lkdGg6OTQwcHg7Ki9cbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcbiAgfVxuICAjRW50ZXRlIHAge21hcmdpbjoxMnB4IDAgMTNweCAwOyB0ZXh0LWFsaWduOnJpZ2h0OyBmb250LXNpemU6IDAuNzZlbTt9XG4gICNFbnRldGUuQmFycmVOb2lyZSB7Ym9yZGVyLWJvdHRvbTozNHB4IHNvbGlkICMwMDA7fVxuICAjRW50ZXRlIGE6bGluaywgI0VudGV0ZSBhOnZpc2l0ZWQge3RleHQtZGVjb3JhdGlvbjp1bmRlcmxpbmU7IGNvbG9yOiM2NDY0NjQ7IG1hcmdpbjozcHg7fVxuICAjRW50ZXRlIGE6aG92ZXIsICNFbnRldGUgYTpmb2N1cyB7dGV4dC1kZWNvcmF0aW9uOm5vbmU7fVxuICAjRW50ZXRlIGEuTGFuZyB7bWFyZ2luLWxlZnQ6MjBweDt9XG4gICNDb250ZW51IHtwYWRkaW5nOjEwcHggMCAxMHB4IDE4NXB4O2JhY2tncm91bmQ6I2ZmZmZmZiB1cmwoJ2h0dHBzOi8vd3d3LmJhbnEucWMuY2EvaW1hZ2VzL2ludGVyZmFjZTA5L2xvZ29fQkFuUS5qcGcnKSBuby1yZXBlYXQgbGVmdCBib3R0b207fVxuICBoMSB7bWFyZ2luOjdweCAxMHB4OyBjb2xvcjojNGM0YzRjO2ZvbnQtc2l6ZToxLjRlbTt9XG4gICNsb2dpbkNoZW1pbiB7bWFyZ2luOjdweCAxMHB4OyBmb250LXNpemU6IDAuOGVtO31cbiAgI2xvZ2luQ2hlbWluIGE6bGluaywgI2xvZ2luQ2hlbWluIGE6dmlzaXRlZCB7dGV4dC1kZWNvcmF0aW9uOnVuZGVybGluZTtjb2xvcjojMzE2OTdmO31cbiAgI2xvZ2luQ2hlbWluIGE6aG92ZXIsICNsb2dpbkNoZW1pbiBhOmZvY3VzIHt0ZXh0LWRlY29yYXRpb246bm9uZTtjb2xvcjojMzE2OTdmO31cbiAgLkVudGV0ZUZvcm0ge2JvcmRlcjoxcHggc29saWQgI2IxYjliZjttYXJnaW46MTBweCAwIDFweCAwO2hlaWdodDozMnB4O2JhY2tncm91bmQtY29sb3I6I2M5ZDFkNH1cbiAgLldyYXBwZXIge2JvcmRlcjoxcHggc29saWQgI2IxYjliZjtvdmVyZmxvdzpoaWRkZW47cGFkZGluZzoxMHB4IDEwcHggNXB4IDEwcHg7Zm9udC1zaXplOi45MmVtO31cbiAgLkNvbEcge2Zsb2F0OmxlZnQ7d2lkdGg6NDglO3BhZGRpbmctcmlnaHQ6MTBweDt9XG4gIC5Db2xHIGE6bGluaywgLkNvbEcgYTp2aXNpdGVkIHt0ZXh0LWRlY29yYXRpb246bm9uZTsgY29sb3I6IzMzMzt9XG4gIC5Db2xHIGE6aG92ZXIsIC5Db2xHIGE6Zm9jdXMgIHt0ZXh0LWRlY29yYXRpb246dW5kZXJsaW5lO2NvbG9yOiMzMzM7fVxuICAuQ2hhcG8ge2ZvbnQtd2VpZ2h0OmJvbGQ7fVxuICBsYWJlbCB7ZGlzcGxheTpibG9jaztmb250LXNpemU6LjhlbTttYXJnaW4tdG9wOjdweH1cbiAgaW5wdXRbdHlwZT10ZXh0XSwgaW5wdXRbdHlwZT1wYXNzd29yZF0ge3dpZHRoOjgwJX1cbiAgaW5wdXRbdHlwZT1zdWJtaXRdLCAuYnRuU3VibWl0IHtmb250LXdlaWdodDpib2xkO2JhY2tncm91bmQtY29sb3I6IzhlZGJkODtib3JkZXI6MnB4IHNvbGlkICM1MmM4YzQ7Ym9yZGVyLXJhZGl1czo1cHg7XG4gIG1hcmdpbjoyMHB4IDEwcHggMjBweCAzMHB4O3BhZGRpbmc6MnB4IDE0cHg7Y3Vyc29yOnBvaW50ZXI7fVxuICBpbnB1dFt0eXBlPXN1Ym1pdF06aG92ZXIge2JhY2tncm91bmQtY29sb3I6I2Y0ZjRmNDtib3JkZXI6MnB4IHNvbGlkICM4ZWRiZDg7fVxuICAuQ29sRCB7ZmxvYXQ6bGVmdDt3aWR0aDo0NyU7bWFyZ2luOjAgMCAxMHB4IDA7Ym9yZGVyLWxlZnQ6MXB4IHNvbGlkICNiMWI5YmY7cGFkZGluZzowIDEwcHg7fVxuICAuQ29sRCBhOmxpbmssIC5Db2xEIGE6dmlzaXRlZCB7ZGlzcGxheTpibG9jazt0ZXh0LWFsaWduOmNlbnRlcjt0ZXh0LWRlY29yYXRpb246bm9uZTtjb2xvcjojMzMzO21hcmdpbjoxNXB4IDA7cGFkZGluZzo3cHg7fVxuICAuQ29sRCAuQm91dG9uOmxpbmssIC5Db2xEIC5Cb3V0b246dmlzaXRlZCB7Ym9yZGVyOjFweCBzb2xpZCAjYjRlN2U1OyBiYWNrZ3JvdW5kLWNvbG9yOiNlNGZjZmM7IGZvbnQtd2VpZ2h0OmJvbGQ7fVxuICAuQ29sRCBhOmhvdmVyLCAuQ29sRCBhOmZvY3VzICB7dGV4dC1kZWNvcmF0aW9uOnVuZGVybGluZTtjb2xvcjojMzMzO31cbiAgLkNvbEQgLkJvdXRvbjpob3ZlciwgLkNvbEQgLkJvdXRvbjpmb2N1cyB7Y29sb3I6ICM1MmM4YzQ7IGJvcmRlcjoxcHggc29saWQgIzUyYzhjNDsgYmFja2dyb3VuZC1jb2xvcjojYjRlN2U1O3RleHQtZGVjb3JhdGlvbjpub25lO31cbiAgLlBkUCB7bWFyZ2luLWxlZnQ6MTg1cHg7dGV4dC1hbGlnbjpjZW50ZXI7Zm9udC1zaXplOi44ZW19XG4gIC5kaXZSZXBvbnNlIHtcbiAgICBtYXJnaW46IDAgYXV0byAxMHB4IGF1dG87XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZTdlN2U3O1xuICAgIHdpZHRoOiAzNDBweDtcbiAgICBwYWRkaW5nOjhweDtcbiAgICB9XG4gIC5kaXZSZXBvbnNlIGltZyB7XG4gICAgYm9yZGVyOm5vbmU7XG4gICAgZmxvYXQ6cmlnaHQ7XG4gIH1cbiAgLmRpdlJlcG9uc2UgbGl7XG4gICAgbWFyZ2luLWxlZnQ6MjBweDtcbiAgICBwYWRkaW5nLWJvdHRvbToxMHB4O1xuICB9XG4gICNjb250ZW51X3JlcG9uc2VfYWJvbiB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2VjZWZmMTtcbiAgfVxuICAjY29udGVudV9yZXBvbnNlX2Fib24gYTpsaW5rICwgI2NvbnRlbnVfcmVwb25zZV9hYm9uIGE6dmlzaXRlZCB7XG4gICAgICBkaXNwbGF5OmlubGluZTtcbiAgICAgIHRleHQtYWxpZ246bGVmdDtcbiAgICAgIG1hcmdpbjowcHg7XG4gICAgICBwYWRkaW5nOjBweDtcbiAgfVxuICAjY29udGVudV9yZXBvbnNlX2Fib24gYTpsaW5rLCAjY29udGVudV9yZXBvbnNlX2Fib24gYTp2aXNpdGVkIHt0ZXh0LWRlY29yYXRpb246dW5kZXJsaW5lO2NvbG9yOiM2NDY0NjQ7fVxuICAjY29udGVudV9yZXBvbnNlX2Fib24gYTpob3ZlciwgI2NvbnRlbnVfcmVwb25zZV9hYm9uIGE6Zm9jdXMge3RleHQtZGVjb3JhdGlvbjpub25lO31cbiAgcCB7XG4gICAgICBtYXJnaW46MjBweCA1cHhcbiAgfVxuICB1bFxuICB7XG4gICAgICBtYXJnaW4tbGVmdDoxNXB4XG4gIH1cbiAgLyogQWpvdXQgcG91ciBVdGlsaXNhdGlvbiBCRCAqL1xuICAuQXJpYW5lIHtmb250LXNpemU6Ljg4ZW07fVxuICAuQm91dG9uIHt0ZXh0LWFsaWduOmNlbnRlcjtmb250LXdlaWdodDpib2xkO2ZvbnQtc2l6ZToxZW07IGRpc3BsYXk6aW5saW5lLWJsb2NrO21hcmdpbjoxMHB4OyBiYWNrZ3JvdW5kLWNvbG9yOiNiNGU3ZTU7Ym9yZGVyOjFweCBzb2xpZCAjMDAzYTlkO3dpZHRoOiAxMDAlO31cbiAgLkJvdXRvbiAgYSB7ZGlzcGxheTppbmxpbmUtYmxvY2s7bWFyZ2luOjEwcHg7IGNvbG9yOiMwMDNhOWQ7fVxuXG4gIC5wMG0wIHtcbiAgICBwYWRkaW5nOiAwcHg7XG4gICAgbWFyZ2luOiAwcHg7XG4gIH1cbiIsIioge1xuICBwYWRkaW5nOiAwO1xuICBtYXJnaW46IDA7XG59XG5cbmJvZHkge1xuICBmb250LXNpemU6IDAuOTJlbTtcbiAgY29sb3I6ICMzMzM7XG4gIGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmO1xufVxuXG4jR2xvYmFsIHtcbiAgLyptYXJnaW46MCBhdXRvO1xuICBwYWRkaW5nOjA7XG4gIGJhY2tncm91bmQtY29sb3I6I2ZmZjtcbiAgd2lkdGg6OTQwcHg7Ki9cbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4jRW50ZXRlIHAge1xuICBtYXJnaW46IDEycHggMCAxM3B4IDA7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBmb250LXNpemU6IDAuNzZlbTtcbn1cblxuI0VudGV0ZS5CYXJyZU5vaXJlIHtcbiAgYm9yZGVyLWJvdHRvbTogMzRweCBzb2xpZCAjMDAwO1xufVxuXG4jRW50ZXRlIGE6bGluaywgI0VudGV0ZSBhOnZpc2l0ZWQge1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbiAgY29sb3I6ICM2NDY0NjQ7XG4gIG1hcmdpbjogM3B4O1xufVxuXG4jRW50ZXRlIGE6aG92ZXIsICNFbnRldGUgYTpmb2N1cyB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cblxuI0VudGV0ZSBhLkxhbmcge1xuICBtYXJnaW4tbGVmdDogMjBweDtcbn1cblxuI0NvbnRlbnUge1xuICBwYWRkaW5nOiAxMHB4IDAgMTBweCAxODVweDtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZiB1cmwoXCJodHRwczovL3d3dy5iYW5xLnFjLmNhL2ltYWdlcy9pbnRlcmZhY2UwOS9sb2dvX0JBblEuanBnXCIpIG5vLXJlcGVhdCBsZWZ0IGJvdHRvbTtcbn1cblxuaDEge1xuICBtYXJnaW46IDdweCAxMHB4O1xuICBjb2xvcjogIzRjNGM0YztcbiAgZm9udC1zaXplOiAxLjRlbTtcbn1cblxuI2xvZ2luQ2hlbWluIHtcbiAgbWFyZ2luOiA3cHggMTBweDtcbiAgZm9udC1zaXplOiAwLjhlbTtcbn1cblxuI2xvZ2luQ2hlbWluIGE6bGluaywgI2xvZ2luQ2hlbWluIGE6dmlzaXRlZCB7XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xuICBjb2xvcjogIzMxNjk3Zjtcbn1cblxuI2xvZ2luQ2hlbWluIGE6aG92ZXIsICNsb2dpbkNoZW1pbiBhOmZvY3VzIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBjb2xvcjogIzMxNjk3Zjtcbn1cblxuLkVudGV0ZUZvcm0ge1xuICBib3JkZXI6IDFweCBzb2xpZCAjYjFiOWJmO1xuICBtYXJnaW46IDEwcHggMCAxcHggMDtcbiAgaGVpZ2h0OiAzMnB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzlkMWQ0O1xufVxuXG4uV3JhcHBlciB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNiMWI5YmY7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHBhZGRpbmc6IDEwcHggMTBweCA1cHggMTBweDtcbiAgZm9udC1zaXplOiAwLjkyZW07XG59XG5cbi5Db2xHIHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHdpZHRoOiA0OCU7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG59XG5cbi5Db2xHIGE6bGluaywgLkNvbEcgYTp2aXNpdGVkIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBjb2xvcjogIzMzMztcbn1cblxuLkNvbEcgYTpob3ZlciwgLkNvbEcgYTpmb2N1cyB7XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xuICBjb2xvcjogIzMzMztcbn1cblxuLkNoYXBvIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbmxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIG1hcmdpbi10b3A6IDdweDtcbn1cblxuaW5wdXRbdHlwZT10ZXh0XSwgaW5wdXRbdHlwZT1wYXNzd29yZF0ge1xuICB3aWR0aDogODAlO1xufVxuXG5pbnB1dFt0eXBlPXN1Ym1pdF0sIC5idG5TdWJtaXQge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzhlZGJkODtcbiAgYm9yZGVyOiAycHggc29saWQgIzUyYzhjNDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBtYXJnaW46IDIwcHggMTBweCAyMHB4IDMwcHg7XG4gIHBhZGRpbmc6IDJweCAxNHB4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbmlucHV0W3R5cGU9c3VibWl0XTpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNGY0ZjQ7XG4gIGJvcmRlcjogMnB4IHNvbGlkICM4ZWRiZDg7XG59XG5cbi5Db2xEIHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHdpZHRoOiA0NyU7XG4gIG1hcmdpbjogMCAwIDEwcHggMDtcbiAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjYjFiOWJmO1xuICBwYWRkaW5nOiAwIDEwcHg7XG59XG5cbi5Db2xEIGE6bGluaywgLkNvbEQgYTp2aXNpdGVkIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBjb2xvcjogIzMzMztcbiAgbWFyZ2luOiAxNXB4IDA7XG4gIHBhZGRpbmc6IDdweDtcbn1cblxuLkNvbEQgLkJvdXRvbjpsaW5rLCAuQ29sRCAuQm91dG9uOnZpc2l0ZWQge1xuICBib3JkZXI6IDFweCBzb2xpZCAjYjRlN2U1O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTRmY2ZjO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLkNvbEQgYTpob3ZlciwgLkNvbEQgYTpmb2N1cyB7XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xuICBjb2xvcjogIzMzMztcbn1cblxuLkNvbEQgLkJvdXRvbjpob3ZlciwgLkNvbEQgLkJvdXRvbjpmb2N1cyB7XG4gIGNvbG9yOiAjNTJjOGM0O1xuICBib3JkZXI6IDFweCBzb2xpZCAjNTJjOGM0O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjRlN2U1O1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbi5QZFAge1xuICBtYXJnaW4tbGVmdDogMTg1cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAwLjhlbTtcbn1cblxuLmRpdlJlcG9uc2Uge1xuICBtYXJnaW46IDAgYXV0byAxMHB4IGF1dG87XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNlN2U3ZTc7XG4gIHdpZHRoOiAzNDBweDtcbiAgcGFkZGluZzogOHB4O1xufVxuXG4uZGl2UmVwb25zZSBpbWcge1xuICBib3JkZXI6IG5vbmU7XG4gIGZsb2F0OiByaWdodDtcbn1cblxuLmRpdlJlcG9uc2UgbGkge1xuICBtYXJnaW4tbGVmdDogMjBweDtcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XG59XG5cbiNjb250ZW51X3JlcG9uc2VfYWJvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlY2VmZjE7XG59XG5cbiNjb250ZW51X3JlcG9uc2VfYWJvbiBhOmxpbmssICNjb250ZW51X3JlcG9uc2VfYWJvbiBhOnZpc2l0ZWQge1xuICBkaXNwbGF5OiBpbmxpbmU7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIG1hcmdpbjogMHB4O1xuICBwYWRkaW5nOiAwcHg7XG59XG5cbiNjb250ZW51X3JlcG9uc2VfYWJvbiBhOmxpbmssICNjb250ZW51X3JlcG9uc2VfYWJvbiBhOnZpc2l0ZWQge1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbiAgY29sb3I6ICM2NDY0NjQ7XG59XG5cbiNjb250ZW51X3JlcG9uc2VfYWJvbiBhOmhvdmVyLCAjY29udGVudV9yZXBvbnNlX2Fib24gYTpmb2N1cyB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cblxucCB7XG4gIG1hcmdpbjogMjBweCA1cHg7XG59XG5cbnVsIHtcbiAgbWFyZ2luLWxlZnQ6IDE1cHg7XG59XG5cbi8qIEFqb3V0IHBvdXIgVXRpbGlzYXRpb24gQkQgKi9cbi5BcmlhbmUge1xuICBmb250LXNpemU6IDAuODhlbTtcbn1cblxuLkJvdXRvbiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbjogMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2I0ZTdlNTtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAwM2E5ZDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5Cb3V0b24gYSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiAxMHB4O1xuICBjb2xvcjogIzAwM2E5ZDtcbn1cblxuLnAwbTAge1xuICBwYWRkaW5nOiAwcHg7XG4gIG1hcmdpbjogMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/views/login/login.component.ts":
/*!************************************************!*\
  !*** ./src/app/views/login/login.component.ts ***!
  \************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_services_business_login_manager_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/services/business/login-manager.service */ "./src/app/core/services/business/login-manager.service.ts");
/* harmony import */ var _core_models_login_request__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../core/models/login-request */ "./src/app/core/models/login-request.ts");
/* harmony import */ var _core_shared_utils_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../core/shared/utils/helper */ "./src/app/core/shared/utils/helper.ts");






/**
 * Composant d'authentification d'un utilisateur a l'application
 */
var LoginComponent = /** @class */ (function () {
    function LoginComponent(router, loginService) {
        this.router = router;
        this.loginService = loginService;
    }
    /**
     * initialisation du composant
     */
    LoginComponent.prototype.ngOnInit = function () {
        this.request = new _core_models_login_request__WEBPACK_IMPORTED_MODULE_4__["LoginRequest"]('', '');
        this.disp = false;
    };
    /**
     * Effectue l'authentification de l'utilisateur sur la base des parametres saisis
     */
    LoginComponent.prototype.login = function () {
        if (this.request.username === 'admin' && this.request.password === 'admin') {
            localStorage.setItem('token', '21232f297a57a5a743894a0e4a801fc3');
            this.router.navigate(['/home/dashboard']);
        }
        else {
            this.myAlert = Object(_core_shared_utils_helper__WEBPACK_IMPORTED_MODULE_5__["threatException"])('Invalid Username or Password');
        }
        /*
        try {
          this.loginService.login( this.request );
          this.router.navigate(['/dashboard']);
        } catch (error) {
          this.myAlert = threatException(error);
        }
        */
    };
    /**
     * Deconnexion
     */
    LoginComponent.prototype.logout = function () {
        this.loginService.logout();
    };
    /**
     * Navigation vers la page d'inscription
     */
    LoginComponent.prototype.register = function () {
        this.router.navigate(['/register']);
    };
    /**
     * Afficher/Masquer le panneur de description de comment s'inscrire
     */
    LoginComponent.prototype.onCmtAbonneClick = function () {
        this.disp = !this.disp;
    };
    LoginComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _core_services_business_login_manager_service__WEBPACK_IMPORTED_MODULE_3__["LoginManagerService"] }
    ]; };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/views/login/login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _core_services_business_login_manager_service__WEBPACK_IMPORTED_MODULE_3__["LoginManagerService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/views/new-isbn/new-isbn.component.scss":
/*!********************************************************!*\
  !*** ./src/app/views/new-isbn/new-isbn.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".w100 {\n  width: 100%;\n}\n\n.telephone3 {\n  width: 40px;\n  padding-left: 0 !important;\n}\n\n.telephone4 {\n  width: 50px;\n  padding-left: 0 !important;\n}\n\n.divCentrer {\n  text-align: center;\n}\n\n.text-on-pannel {\n  background: #fff none repeat scroll 0 0;\n  height: auto;\n  margin-left: 20px;\n  padding: 3px 3px;\n  position: absolute;\n  margin-top: -15px;\n  border: 1px solid #fff;\n  color: dimgray;\n  border-radius: 3px;\n  font-weight: bold;\n  font-size: 12px;\n}\n\n.offspaces {\n  margin-top: 2px;\n  margin-bottom: 2px;\n  margin-left: 2px;\n  margin-right: 2px;\n  padding-top: 2px;\n  padding-bottom: 2px;\n  padding-left: 2px;\n  padding-right: 2px;\n}\n\n.nullspaces {\n  margin-top: 0px;\n  margin-bottom: 0px;\n  margin-left: 0px;\n  margin-right: 0px;\n  padding-top: 0px;\n  padding-bottom: 0px;\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.noborders {\n  border: none;\n}\n\n.bg-banq {\n  background-color: #d3dfc7;\n  font-weight: bold;\n  color: black;\n  font-size: 11px;\n}\n\n.name {\n  vertical-align: top;\n  /*float: left;\n  clear:left; */\n  padding: 5px;\n  margin: 0px;\n  font-weight: bold;\n  font-size: 12px;\n  text-align: right;\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n  color: #000;\n  width: 130px;\n  min-height: 20px;\n  /*max-height: 30px;*/\n  min-width: 120px;\n  /*max-width:250px;*/\n  background-color: #d3dfc7;\n  border: 1px solid #c8c8c8;\n}\n\n.small-name {\n  vertical-align: top;\n  padding: 5px;\n  margin: 0px;\n  font-weight: bold;\n  font-size: 12px;\n  text-align: right;\n  color: #000;\n  /*width: 75px;*/\n  min-height: 20px;\n  /*max-height: 30px;\n  min-width: 120px;*/\n  /*max-width:250px;*/\n  background-color: #d3dfc7;\n  border: 1px solid #c8c8c8;\n}\n\n.hintText {\n  font-size: 75%;\n  font-style: italic;\n  font-weight: normal;\n  padding: 0px;\n  margin: 0px;\n}\n\n.required {\n  color: #FF0000;\n  padding-left: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvbmV3LWlzYm4vRTpcXHR1dG9cXHByb2plY3RzXFxhbmd1bGFyXFxjb3JldWkvc3JjXFxhcHBcXHZpZXdzXFxuZXctaXNiblxcbmV3LWlzYm4uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3ZpZXdzL25ldy1pc2JuL25ldy1pc2JuLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsV0FBQTtBQ0FGOztBREVBO0VBQVksV0FBQTtFQUFXLDBCQUFBO0FDR3ZCOztBREZBO0VBQVksV0FBQTtFQUFXLDBCQUFBO0FDT3ZCOztBRE5BO0VBQVksa0JBQUE7QUNVWjs7QURSQTtFQUNJLHVDQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFFQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNVSjs7QURQQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQ1VKOztBRFBBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDVUo7O0FEUEE7RUFDSSxZQUFBO0FDVUo7O0FEUEE7RUFDSSx5QkFBQTtFQUVBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUNTSjs7QUROQTtFQUNJLG1CQUFBO0VBQ0E7ZUFBQTtFQUVBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxxQkFBQTtVQUFBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EseUJBQUE7QUNTSjs7QUROQTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQTtvQkFBQTtFQUVBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSx5QkFBQTtBQ1NKOztBRE5BO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ1NKOztBRE5BO0VBQ0ksY0FBQTtFQUNBLGlCQUFBO0FDU0oiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9uZXctaXNibi9uZXctaXNibi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuLncxMDB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLnRlbGVwaG9uZTN7d2lkdGg6NDBweDtwYWRkaW5nLWxlZnQ6MCAhaW1wb3J0YW50fVxuLnRlbGVwaG9uZTR7d2lkdGg6NTBweDtwYWRkaW5nLWxlZnQ6MCAhaW1wb3J0YW50fVxuLmRpdkNlbnRyZXJ7dGV4dC1hbGlnbjpjZW50ZXI7fVxuXG4udGV4dC1vbi1wYW5uZWwge1xuICAgIGJhY2tncm91bmQ6ICNmZmYgbm9uZSByZXBlYXQgc2Nyb2xsIDAgMDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XG4gICAgcGFkZGluZzogM3B4IDNweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbWFyZ2luLXRvcDogLTE1cHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2ZmZi8qY2ZkOGRjKi9cbiAgICA7XG4gICAgY29sb3I6IGRpbWdyYXk7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLm9mZnNwYWNlcyB7XG4gICAgbWFyZ2luLXRvcDogMnB4O1xuICAgIG1hcmdpbi1ib3R0b206IDJweDtcbiAgICBtYXJnaW4tbGVmdDogMnB4O1xuICAgIG1hcmdpbi1yaWdodDogMnB4O1xuICAgIHBhZGRpbmctdG9wOiAycHg7XG4gICAgcGFkZGluZy1ib3R0b206IDJweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDJweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAycHg7XG59XG5cbi5udWxsc3BhY2VzIHtcbiAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gICAgcGFkZGluZy10b3A6IDBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICAgIHBhZGRpbmctbGVmdDogMHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cblxuLm5vYm9yZGVycyB7XG4gICAgYm9yZGVyOiBub25lO1xufVxuXG4uYmctYmFucSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2QzZGZjNy8qI2M1ZTFhNSovXG4gICAgO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGNvbG9yOiBibGFjaztcbiAgICBmb250LXNpemU6IDExcHg7XG59XG5cbi5uYW1lIHtcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xuICAgIC8qZmxvYXQ6IGxlZnQ7XG5cdGNsZWFyOmxlZnQ7ICovXG4gICAgcGFkZGluZzogNXB4O1xuICAgIG1hcmdpbjogMHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICAgIGNvbG9yOiAjMDAwO1xuICAgIHdpZHRoOiAxMzBweDtcbiAgICBtaW4taGVpZ2h0OiAyMHB4O1xuICAgIC8qbWF4LWhlaWdodDogMzBweDsqL1xuICAgIG1pbi13aWR0aDogMTIwcHg7XG4gICAgLyptYXgtd2lkdGg6MjUwcHg7Ki9cbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDNkZmM3O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjOGM4Yzg7XG59XG5cbi5zbWFsbC1uYW1lIHtcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBtYXJnaW46IDBweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgY29sb3I6ICMwMDA7XG4gICAgLyp3aWR0aDogNzVweDsqL1xuICAgIG1pbi1oZWlnaHQ6IDIwcHg7XG4gICAgLyptYXgtaGVpZ2h0OiAzMHB4O1xuICAgIG1pbi13aWR0aDogMTIwcHg7Ki9cbiAgICAvKm1heC13aWR0aDoyNTBweDsqL1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzc7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2M4YzhjODtcbn1cblxuLmhpbnRUZXh0IHtcbiAgICBmb250LXNpemU6IDc1JTtcbiAgICBmb250LXN0eWxlOiBpdGFsaWM7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICBwYWRkaW5nOiAwcHg7XG4gICAgbWFyZ2luOiAwcHg7XG59XG5cbi5yZXF1aXJlZCB7XG4gICAgY29sb3I6ICNGRjAwMDA7XG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XG59XG4iLCIudzEwMCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4udGVsZXBob25lMyB7XG4gIHdpZHRoOiA0MHB4O1xuICBwYWRkaW5nLWxlZnQ6IDAgIWltcG9ydGFudDtcbn1cblxuLnRlbGVwaG9uZTQge1xuICB3aWR0aDogNTBweDtcbiAgcGFkZGluZy1sZWZ0OiAwICFpbXBvcnRhbnQ7XG59XG5cbi5kaXZDZW50cmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4udGV4dC1vbi1wYW5uZWwge1xuICBiYWNrZ3JvdW5kOiAjZmZmIG5vbmUgcmVwZWF0IHNjcm9sbCAwIDA7XG4gIGhlaWdodDogYXV0bztcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG4gIHBhZGRpbmc6IDNweCAzcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDogLTE1cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmZmY7XG4gIGNvbG9yOiBkaW1ncmF5O1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5vZmZzcGFjZXMge1xuICBtYXJnaW4tdG9wOiAycHg7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbiAgbWFyZ2luLWxlZnQ6IDJweDtcbiAgbWFyZ2luLXJpZ2h0OiAycHg7XG4gIHBhZGRpbmctdG9wOiAycHg7XG4gIHBhZGRpbmctYm90dG9tOiAycHg7XG4gIHBhZGRpbmctbGVmdDogMnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAycHg7XG59XG5cbi5udWxsc3BhY2VzIHtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuXG4ubm9ib3JkZXJzIHtcbiAgYm9yZGVyOiBub25lO1xufVxuXG4uYmctYmFucSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzc7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogYmxhY2s7XG4gIGZvbnQtc2l6ZTogMTFweDtcbn1cblxuLm5hbWUge1xuICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xuICAvKmZsb2F0OiBsZWZ0O1xuICBjbGVhcjpsZWZ0OyAqL1xuICBwYWRkaW5nOiA1cHg7XG4gIG1hcmdpbjogMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgY29sb3I6ICMwMDA7XG4gIHdpZHRoOiAxMzBweDtcbiAgbWluLWhlaWdodDogMjBweDtcbiAgLyptYXgtaGVpZ2h0OiAzMHB4OyovXG4gIG1pbi13aWR0aDogMTIwcHg7XG4gIC8qbWF4LXdpZHRoOjI1MHB4OyovXG4gIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzc7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjOGM4Yzg7XG59XG5cbi5zbWFsbC1uYW1lIHtcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgcGFkZGluZzogNXB4O1xuICBtYXJnaW46IDBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIGNvbG9yOiAjMDAwO1xuICAvKndpZHRoOiA3NXB4OyovXG4gIG1pbi1oZWlnaHQ6IDIwcHg7XG4gIC8qbWF4LWhlaWdodDogMzBweDtcbiAgbWluLXdpZHRoOiAxMjBweDsqL1xuICAvKm1heC13aWR0aDoyNTBweDsqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDNkZmM3O1xuICBib3JkZXI6IDFweCBzb2xpZCAjYzhjOGM4O1xufVxuXG4uaGludFRleHQge1xuICBmb250LXNpemU6IDc1JTtcbiAgZm9udC1zdHlsZTogaXRhbGljO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBwYWRkaW5nOiAwcHg7XG4gIG1hcmdpbjogMHB4O1xufVxuXG4ucmVxdWlyZWQge1xuICBjb2xvcjogI0ZGMDAwMDtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/views/new-isbn/new-isbn.component.ts":
/*!******************************************************!*\
  !*** ./src/app/views/new-isbn/new-isbn.component.ts ***!
  \******************************************************/
/*! exports provided: NewIsbnComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewIsbnComponent", function() { return NewIsbnComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var NewIsbnComponent = /** @class */ (function () {
    function NewIsbnComponent(router) {
        this.router = router;
    }
    NewIsbnComponent.prototype.ngOnInit = function () {
    };
    NewIsbnComponent.prototype.back = function () {
        this.router.navigate(['/home/dashboard']);
    };
    NewIsbnComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    NewIsbnComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-new-isbn',
            template: __webpack_require__(/*! raw-loader!./new-isbn.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/new-isbn/new-isbn.component.html"),
            styles: [__webpack_require__(/*! ./new-isbn.component.scss */ "./src/app/views/new-isbn/new-isbn.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], NewIsbnComponent);
    return NewIsbnComponent;
}());



/***/ }),

/***/ "./src/app/views/register/register.component.scss":
/*!********************************************************!*\
  !*** ./src/app/views/register/register.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ::ng-deep .mat-form-field-wrapper {\n  padding: 0px;\n  margin: 0px;\n}\n\n:host ::ng-deep .mat-form-field:last-child div {\n  padding: 0px;\n  margin: 0px;\n}\n\n.telephone3 {\n  width: 40px;\n  padding-left: 0 !important;\n}\n\n.telephone4 {\n  width: 50px;\n  padding-left: 0 !important;\n}\n\n.divCentrer {\n  text-align: center;\n}\n\n.text-on-pannel {\n  background: #fff none repeat scroll 0 0;\n  height: auto;\n  margin-left: 20px;\n  padding: 3px 3px;\n  position: absolute;\n  margin-top: -15px;\n  border: 1px solid #fff;\n  color: dimgray;\n  border-radius: 3px;\n  font-weight: bold;\n  font-size: 12px;\n}\n\n.offspaces {\n  margin-top: 2px;\n  margin-bottom: 2px;\n  margin-left: 2px;\n  margin-right: 2px;\n  padding-top: 2px;\n  padding-bottom: 2px;\n  padding-left: 2px;\n  padding-right: 2px;\n}\n\n.nullspaces {\n  margin-top: 0px;\n  margin-bottom: 0px;\n  margin-left: 0px;\n  margin-right: 0px;\n  padding-top: 0px;\n  padding-bottom: 0px;\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.noborders {\n  border: none;\n}\n\n.bg-banq {\n  background-color: #d3dfc7;\n  font-weight: bold;\n  color: black;\n}\n\n.name {\n  vertical-align: top;\n  /*float: left;\n  clear:left; */\n  padding: 5px;\n  margin: 0px;\n  font-weight: bold;\n  font-size: 12px;\n  text-align: right;\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n  color: #000;\n  width: 130px;\n  min-height: 20px;\n  /*max-height: 30px;*/\n  min-width: 120px;\n  /*max-width:250px;*/\n  background-color: #d3dfc7;\n  border: 1px solid #c8c8c8;\n}\n\n.small-name {\n  vertical-align: top;\n  padding: 5px;\n  margin: 0px;\n  font-weight: bold;\n  font-size: 12px;\n  text-align: right;\n  color: #000;\n  /*width: 75px;*/\n  min-height: 20px;\n  /*max-height: 30px;\n  min-width: 120px;*/\n  /*max-width:250px;*/\n  background-color: #d3dfc7;\n  border: 1px solid #c8c8c8;\n}\n\n.hintText {\n  font-size: 75%;\n  font-style: italic;\n  font-weight: normal;\n  padding: 0px;\n  margin: 0px;\n}\n\n.required {\n  color: #FF0000;\n  padding-left: 5px;\n}\n\n.modal-pnl {\n  width: 100%;\n  height: 100%;\n  z-index: 100;\n  background-color: grey;\n  opacity: 0.5;\n  filter: alpha(opacity=50);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvcmVnaXN0ZXIvRTpcXHR1dG9cXHByb2plY3RzXFxhbmd1bGFyXFxjb3JldWkvc3JjXFxhcHBcXHZpZXdzXFxyZWdpc3RlclxccmVnaXN0ZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3ZpZXdzL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsWUFBQTtFQUFjLFdBQUE7QUNDaEI7O0FEQ0E7RUFDRSxZQUFBO0VBQWMsV0FBQTtBQ0doQjs7QURDQTtFQUFZLFdBQUE7RUFBVywwQkFBQTtBQ0l2Qjs7QURIQTtFQUFZLFdBQUE7RUFBVywwQkFBQTtBQ1F2Qjs7QURQQTtFQUFZLGtCQUFBO0FDV1o7O0FEVEE7RUFDSSx1Q0FBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBRUEsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FDV0o7O0FEUkE7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNXSjs7QURSQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQ1dKOztBRFJBO0VBQ0ksWUFBQTtBQ1dKOztBRFJBO0VBQ0kseUJBQUE7RUFFQSxpQkFBQTtFQUNBLFlBQUE7QUNVSjs7QURQQTtFQUNJLG1CQUFBO0VBQ0E7ZUFBQTtFQUVBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxxQkFBQTtVQUFBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EseUJBQUE7QUNVSjs7QURQQTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQTtvQkFBQTtFQUVBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSx5QkFBQTtBQ1VKOztBRFBBO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ1VKOztBRFBBO0VBQ0ksY0FBQTtFQUNBLGlCQUFBO0FDVUo7O0FETkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFBYyxzQkFBQTtFQUF3QixZQUFBO0VBQWMseUJBQUE7QUNZdEQiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuOmhvc3QgOjpuZy1kZWVwIC5tYXQtZm9ybS1maWVsZC13cmFwcGVye1xuICBwYWRkaW5nOiAwcHg7IG1hcmdpbjogMHB4O1xufVxuOmhvc3QgOjpuZy1kZWVwIC5tYXQtZm9ybS1maWVsZDpsYXN0LWNoaWxkIGRpdiB7XG4gIHBhZGRpbmc6IDBweDsgbWFyZ2luOiAwcHg7XG59XG5cblxuLnRlbGVwaG9uZTN7d2lkdGg6NDBweDtwYWRkaW5nLWxlZnQ6MCAhaW1wb3J0YW50fVxuLnRlbGVwaG9uZTR7d2lkdGg6NTBweDtwYWRkaW5nLWxlZnQ6MCAhaW1wb3J0YW50fVxuLmRpdkNlbnRyZXJ7dGV4dC1hbGlnbjpjZW50ZXI7fVxuXG4udGV4dC1vbi1wYW5uZWwge1xuICAgIGJhY2tncm91bmQ6ICNmZmYgbm9uZSByZXBlYXQgc2Nyb2xsIDAgMDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XG4gICAgcGFkZGluZzogM3B4IDNweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbWFyZ2luLXRvcDogLTE1cHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2ZmZi8qY2ZkOGRjKi9cbiAgICA7XG4gICAgY29sb3I6IGRpbWdyYXk7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLm9mZnNwYWNlcyB7XG4gICAgbWFyZ2luLXRvcDogMnB4O1xuICAgIG1hcmdpbi1ib3R0b206IDJweDtcbiAgICBtYXJnaW4tbGVmdDogMnB4O1xuICAgIG1hcmdpbi1yaWdodDogMnB4O1xuICAgIHBhZGRpbmctdG9wOiAycHg7XG4gICAgcGFkZGluZy1ib3R0b206IDJweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDJweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAycHg7XG59XG5cbi5udWxsc3BhY2VzIHtcbiAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAwcHg7XG4gICAgcGFkZGluZy10b3A6IDBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICAgIHBhZGRpbmctbGVmdDogMHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cblxuLm5vYm9yZGVycyB7XG4gICAgYm9yZGVyOiBub25lO1xufVxuXG4uYmctYmFucSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2QzZGZjNy8qI2M1ZTFhNSovXG4gICAgO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGNvbG9yOiBibGFjaztcbn1cblxuLm5hbWUge1xuICAgIHZlcnRpY2FsLWFsaWduOiB0b3A7XG4gICAgLypmbG9hdDogbGVmdDtcblx0Y2xlYXI6bGVmdDsgKi9cbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgbWFyZ2luOiAwcHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gICAgY29sb3I6ICMwMDA7XG4gICAgd2lkdGg6IDEzMHB4O1xuICAgIG1pbi1oZWlnaHQ6IDIwcHg7XG4gICAgLyptYXgtaGVpZ2h0OiAzMHB4OyovXG4gICAgbWluLXdpZHRoOiAxMjBweDtcbiAgICAvKm1heC13aWR0aDoyNTBweDsqL1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzc7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2M4YzhjODtcbn1cblxuLnNtYWxsLW5hbWUge1xuICAgIHZlcnRpY2FsLWFsaWduOiB0b3A7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIG1hcmdpbjogMHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBjb2xvcjogIzAwMDtcbiAgICAvKndpZHRoOiA3NXB4OyovXG4gICAgbWluLWhlaWdodDogMjBweDtcbiAgICAvKm1heC1oZWlnaHQ6IDMwcHg7XG4gICAgbWluLXdpZHRoOiAxMjBweDsqL1xuICAgIC8qbWF4LXdpZHRoOjI1MHB4OyovXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2QzZGZjNztcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjYzhjOGM4O1xufVxuXG4uaGludFRleHQge1xuICAgIGZvbnQtc2l6ZTogNzUlO1xuICAgIGZvbnQtc3R5bGU6IGl0YWxpYztcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIHBhZGRpbmc6IDBweDtcbiAgICBtYXJnaW46IDBweDtcbn1cblxuLnJlcXVpcmVkIHtcbiAgICBjb2xvcjogI0ZGMDAwMDtcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcbn1cblxuXG4ubW9kYWwtcG5se1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICB6LWluZGV4OiAxMDA7IGJhY2tncm91bmQtY29sb3I6IGdyZXk7IG9wYWNpdHk6IDAuNTsgZmlsdGVyOiBhbHBoYShvcGFjaXR5PTUwKTtcbn1cbiIsIjpob3N0IDo6bmctZGVlcCAubWF0LWZvcm0tZmllbGQtd3JhcHBlciB7XG4gIHBhZGRpbmc6IDBweDtcbiAgbWFyZ2luOiAwcHg7XG59XG5cbjpob3N0IDo6bmctZGVlcCAubWF0LWZvcm0tZmllbGQ6bGFzdC1jaGlsZCBkaXYge1xuICBwYWRkaW5nOiAwcHg7XG4gIG1hcmdpbjogMHB4O1xufVxuXG4udGVsZXBob25lMyB7XG4gIHdpZHRoOiA0MHB4O1xuICBwYWRkaW5nLWxlZnQ6IDAgIWltcG9ydGFudDtcbn1cblxuLnRlbGVwaG9uZTQge1xuICB3aWR0aDogNTBweDtcbiAgcGFkZGluZy1sZWZ0OiAwICFpbXBvcnRhbnQ7XG59XG5cbi5kaXZDZW50cmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4udGV4dC1vbi1wYW5uZWwge1xuICBiYWNrZ3JvdW5kOiAjZmZmIG5vbmUgcmVwZWF0IHNjcm9sbCAwIDA7XG4gIGhlaWdodDogYXV0bztcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG4gIHBhZGRpbmc6IDNweCAzcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDogLTE1cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmZmY7XG4gIGNvbG9yOiBkaW1ncmF5O1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5vZmZzcGFjZXMge1xuICBtYXJnaW4tdG9wOiAycHg7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbiAgbWFyZ2luLWxlZnQ6IDJweDtcbiAgbWFyZ2luLXJpZ2h0OiAycHg7XG4gIHBhZGRpbmctdG9wOiAycHg7XG4gIHBhZGRpbmctYm90dG9tOiAycHg7XG4gIHBhZGRpbmctbGVmdDogMnB4O1xuICBwYWRkaW5nLXJpZ2h0OiAycHg7XG59XG5cbi5udWxsc3BhY2VzIHtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7XG4gIG1hcmdpbi1yaWdodDogMHB4O1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufVxuXG4ubm9ib3JkZXJzIHtcbiAgYm9yZGVyOiBub25lO1xufVxuXG4uYmctYmFucSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkM2RmYzc7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogYmxhY2s7XG59XG5cbi5uYW1lIHtcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgLypmbG9hdDogbGVmdDtcbiAgY2xlYXI6bGVmdDsgKi9cbiAgcGFkZGluZzogNXB4O1xuICBtYXJnaW46IDBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gIGNvbG9yOiAjMDAwO1xuICB3aWR0aDogMTMwcHg7XG4gIG1pbi1oZWlnaHQ6IDIwcHg7XG4gIC8qbWF4LWhlaWdodDogMzBweDsqL1xuICBtaW4td2lkdGg6IDEyMHB4O1xuICAvKm1heC13aWR0aDoyNTBweDsqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDNkZmM3O1xuICBib3JkZXI6IDFweCBzb2xpZCAjYzhjOGM4O1xufVxuXG4uc21hbGwtbmFtZSB7XG4gIHZlcnRpY2FsLWFsaWduOiB0b3A7XG4gIHBhZGRpbmc6IDVweDtcbiAgbWFyZ2luOiAwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBjb2xvcjogIzAwMDtcbiAgLyp3aWR0aDogNzVweDsqL1xuICBtaW4taGVpZ2h0OiAyMHB4O1xuICAvKm1heC1oZWlnaHQ6IDMwcHg7XG4gIG1pbi13aWR0aDogMTIwcHg7Ki9cbiAgLyptYXgtd2lkdGg6MjUwcHg7Ki9cbiAgYmFja2dyb3VuZC1jb2xvcjogI2QzZGZjNztcbiAgYm9yZGVyOiAxcHggc29saWQgI2M4YzhjODtcbn1cblxuLmhpbnRUZXh0IHtcbiAgZm9udC1zaXplOiA3NSU7XG4gIGZvbnQtc3R5bGU6IGl0YWxpYztcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgcGFkZGluZzogMHB4O1xuICBtYXJnaW46IDBweDtcbn1cblxuLnJlcXVpcmVkIHtcbiAgY29sb3I6ICNGRjAwMDA7XG4gIHBhZGRpbmctbGVmdDogNXB4O1xufVxuXG4ubW9kYWwtcG5sIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgei1pbmRleDogMTAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmV5O1xuICBvcGFjaXR5OiAwLjU7XG4gIGZpbHRlcjogYWxwaGEob3BhY2l0eT01MCk7XG59Il19 */"

/***/ }),

/***/ "./src/app/views/register/register.component.ts":
/*!******************************************************!*\
  !*** ./src/app/views/register/register.component.ts ***!
  \******************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_data_editor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/data/editor */ "./src/app/core/data/editor.ts");
/* harmony import */ var _core_data_respondent__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../core/data/respondent */ "./src/app/core/data/respondent.ts");
/* harmony import */ var _core_services_business_generic_manager_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../core/services/business/generic-manager.service */ "./src/app/core/services/business/generic-manager.service.ts");
/* harmony import */ var _core_services_business_login_manager_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../core/services/business/login-manager.service */ "./src/app/core/services/business/login-manager.service.ts");
/* harmony import */ var _core_models_alert__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../core/models/alert */ "./src/app/core/models/alert.ts");
/* harmony import */ var _core_enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../core/enums/alert-type.enum */ "./src/app/core/enums/alert-type.enum.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _core_services_dao_login_dao_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../core/services/dao/login-dao.service */ "./src/app/core/services/dao/login-dao.service.ts");
/* harmony import */ var _core_shared_mock_cities__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../core/shared/mock/cities */ "./src/app/core/shared/mock/cities.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _core_shared_mock_states__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../core/shared/mock/states */ "./src/app/core/shared/mock/states.ts");
/* harmony import */ var _waiting_box_waiting_box_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../waiting-box/waiting-box.service */ "./src/app/views/waiting-box/waiting-box.service.ts");















// import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
/**
 * Composant Fiche d'inscription
 */
var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(router, generic, loginService, fb, loginDAO, _loading) {
        this.router = router;
        this.generic = generic;
        this.loginService = loginService;
        this.fb = fb;
        this.loginDAO = loginDAO;
        this._loading = _loading;
        this.editor = new _core_data_editor__WEBPACK_IMPORTED_MODULE_3__["Editor"]();
        this.personList = [];
        this.awaitingPersonList = [];
        this.countries = [];
        this.states = [];
        this.searchCitiesCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormControl"]();
        this.errorMsg = [];
        this.createForm();
        this.errs = [];
    }
    RegisterComponent.prototype.ngOnInit = function () {
        var _this = this;
        try {
            this.isLoading = false; // this.isLoadingService.isLoading$().;
            this.countries = this.generic.loadCountries();
            this.states = _core_shared_mock_states__WEBPACK_IMPORTED_MODULE_13__["STATES"]; // this.generic.loadStates();
            this.reset();
            this.add();
            this.myAlert = new _core_models_alert__WEBPACK_IMPORTED_MODULE_7__["Alert"]('Everything is fine!');
        }
        catch (error) {
            console.log('Une erreur sest produite: ', error);
            this.myAlert.displayAlert(_core_enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_8__["AlertType"].error, error);
        }
        this.cities = this.searchCitiesCtrl.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_12__["debounceTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_12__["tap"])(function () { }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_12__["startWith"])(''), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_12__["map"])(function (value) { return _this._filter(value); }));
    };
    RegisterComponent.prototype._filter = function (value) {
        var filterValue = value.toLowerCase();
        return _core_shared_mock_cities__WEBPACK_IMPORTED_MODULE_11__["CITIES"].filter(function (option) { return option.city.toLowerCase().includes(filterValue); });
    };
    RegisterComponent.prototype.displayFn = function (city) {
        return city ? city.city : undefined;
    };
    /**
     * Initialisation du formulaire dynamique des gestion des repondants
     */
    RegisterComponent.prototype.createForm = function () {
        this.registerForm = this.fb.group({
            title: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required],
            firstname: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].minLength(2)]],
            lastname: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].minLength(2)]],
            email: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].minLength(2)]],
            phone: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].pattern('^\d{10}$')]],
            phoneArea: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].pattern('^\d{3}$')]],
            phone3: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].pattern('^\d{3}$')]],
            phone4: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].pattern('^\d{4}$')]],
            poste: [null, []],
            persons: this.fb.array([])
        });
    };
    Object.defineProperty(RegisterComponent.prototype, "persons", {
        /**
         * Retourne la liste des champs de saisie decrivant les repondants
         */
        get: function () {
            return this.registerForm.get('persons');
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Suppresion d'un repondant de la liste
     * @param id identifiant de la ligne
     */
    RegisterComponent.prototype.remove = function (id) {
        this.persons.removeAt(id);
    };
    /**
     * Ajout d'un nouveau repondant dans la liste
     */
    RegisterComponent.prototype.add = function () {
        var r = new _core_data_respondent__WEBPACK_IMPORTED_MODULE_4__["Respondent"]();
        this.persons.push(this.fb.group({
            title: [r.titre, _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required],
            firstname: [r.nom, [_angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].minLength(2)]],
            lastname: [r.prenom, [_angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].minLength(2)]],
            email: [r.courriel, [_angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].minLength(2)]],
            phone: [r.telephone, [_angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required]],
            phoneArea: [r.telephone, [_angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].pattern('^\d{3}$')]],
            phone3: [r.telephone, [_angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].pattern('^\d{3}$')]],
            phone4: [r.telephone, [_angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].pattern('^\d{4}$')]],
            poste: [r.poste, []]
        }));
    };
    /**
     * Lecture des donnees resseignees sur le formulaire
     */
    RegisterComponent.prototype.readPersons = function () {
        this.errs = [];
        this.editor.repondants = new Array();
        this.editor.telephone = this.phone.prefx + this.phone.sufx + this.phone.racine;
        this.editor.telecopieur = this.telecop.prefx + this.telecop.sufx + this.telecop.racine;
        for (var _i = 0, _a = this.persons.controls; _i < _a.length; _i++) {
            var p = _a[_i];
            var r = new _core_data_respondent__WEBPACK_IMPORTED_MODULE_4__["Respondent"]();
            r.titre = p.get('title').value;
            r.nom = p.get('firstname').value;
            r.prenom = p.get('lastname').value;
            r.telephone = p.get('phoneArea').value + p.get('phone3').value + p.get('phone4').value;
            r.courriel = p.get('email').value;
            r.poste = p.get('poste').value;
            if (r.titre == null || r.titre === '') {
                this.errs.push('Veuillez cocher l\'option Mme ou M. pour chacune des personnes ajoutées dans le formulaire.');
            }
            if (r.nom == null || r.nom.trim() === '') {
                this.errs.push('Veuillez entrer une donnée dans le champ Nom.');
            }
            if (r.prenom == null || r.prenom.trim() === '') {
                this.errs.push('Veuillez entrer une donnée dans le champ Prénom.');
            }
            if (r.courriel == null || r.courriel.trim() === '') {
                this.errs.push('Vous devez spécifier l\'adresse courriel du répondant principal.');
            }
            this.editor.repondants.push(r);
        }
        if (this.editor.nom == null || this.editor.nom.trim() === '') {
            this.errs.push('Veuillez entrer une donnée dans le champ Éditeur.');
        }
        if (this.editor.adresse1 == null || this.editor.adresse1.trim() === '') {
            this.errs.push('Veuillez entrer une donnée dans le champ Adresse postale.');
        }
        if (this.editor.ville == null || this.editor.ville.trim() === '') {
            this.errs.push('Veuillez entrer une donnée dans le champ Ville.');
        }
        if (this.editor.codePostal.length < 6 || this.editor.codePostal.length > 7) {
            this.errs.push('Le code postal entré est invalide.');
        }
        if (this.editor.telephone.length !== 10 || this.editor.telephone.trim() === '') {
            this.errs.push('Le numéro de téléphone de l\'éditeur est invalide : il doit comporter 10 chiffres dont 3 pour le code régional.');
        }
    };
    /**
     * Soumission du formulaire d'inscription a l'enregistrement
     */
    RegisterComponent.prototype.submit = function () {
        var _this = this;
        try {
            this.readPersons();
            if (this.errs.length > 0) {
                return;
            }
            this.showProgressBar();
            this.loginDAO.callRegisterAPI(this.editor).subscribe(function (response) {
                var resp = response;
                if (resp.success) {
                    _this.saved = resp.data;
                    _this.myAlert.displayAlert(_core_enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_8__["AlertType"].success, 'Inscription terminee avec succes! Veuillez consulter votre courriel pour recevoir vos parametres de connexion. Votre identifiant est: ' + _this.saved.idEditeur);
                    _this.reset();
                }
                else {
                    _this.saved = null;
                    _this.myAlert.displayAlert(_core_enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_8__["AlertType"].error, 'Une erreur s\'est produite: ' + resp.message);
                }
            }, function (error) {
                _this.myAlert.displayAlert(_core_enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_8__["AlertType"].error, 'Une erreur s\'est produite. Echec de la mise a jour: ' + error.message);
            }, function () {
                _this.hideProgressBar();
            });
        }
        catch (error) {
            this.myAlert.displayAlert(_core_enums_alert_type_enum__WEBPACK_IMPORTED_MODULE_8__["AlertType"].error, error);
            console.log('Une erreur sest produite: ', error);
        }
    };
    /**
     * Reinitialisation du formulaire d'inscription
     */
    RegisterComponent.prototype.reset = function () {
        this.editor = new _core_data_editor__WEBPACK_IMPORTED_MODULE_3__["Editor"]();
        this.registerForm.reset();
        this.phone = { prefx: '', sufx: '', racine: '' };
        this.telecop = { prefx: '', sufx: '', racine: '' };
    };
    /**
     * Retour a la page de connexion
     */
    RegisterComponent.prototype.back = function () {
        this.router.navigate(['/login']);
    };
    RegisterComponent.prototype.showProgressBar = function () {
        this._loading.show();
    };
    RegisterComponent.prototype.hideProgressBar = function () {
        this._loading.hide();
    };
    RegisterComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _core_services_business_generic_manager_service__WEBPACK_IMPORTED_MODULE_5__["GenericManagerService"] },
        { type: _core_services_business_login_manager_service__WEBPACK_IMPORTED_MODULE_6__["LoginManagerService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormBuilder"] },
        { type: _core_services_dao_login_dao_service__WEBPACK_IMPORTED_MODULE_10__["LoginDaoService"] },
        { type: _waiting_box_waiting_box_service__WEBPACK_IMPORTED_MODULE_14__["WaitingBoxService"] }
    ]; };
    RegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! raw-loader!./register.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.scss */ "./src/app/views/register/register.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _core_services_business_generic_manager_service__WEBPACK_IMPORTED_MODULE_5__["GenericManagerService"],
            _core_services_business_login_manager_service__WEBPACK_IMPORTED_MODULE_6__["LoginManagerService"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormBuilder"],
            _core_services_dao_login_dao_service__WEBPACK_IMPORTED_MODULE_10__["LoginDaoService"], _waiting_box_waiting_box_service__WEBPACK_IMPORTED_MODULE_14__["WaitingBoxService"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/views/waiting-box/progress-layer/progress-activity-display-mode.ts":
/*!************************************************************************************!*\
  !*** ./src/app/views/waiting-box/progress-layer/progress-activity-display-mode.ts ***!
  \************************************************************************************/
/*! exports provided: ProgressLayerDisplayMode */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProgressLayerDisplayMode", function() { return ProgressLayerDisplayMode; });
var ProgressLayerDisplayMode;
(function (ProgressLayerDisplayMode) {
    ProgressLayerDisplayMode[ProgressLayerDisplayMode["region"] = 0] = "region";
    ProgressLayerDisplayMode[ProgressLayerDisplayMode["fullscreen"] = 1] = "fullscreen";
})(ProgressLayerDisplayMode || (ProgressLayerDisplayMode = {}));


/***/ }),

/***/ "./src/app/views/waiting-box/progress-layer/progress-layer.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/views/waiting-box/progress-layer/progress-layer.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".windowed {\n  display: block;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n}\n\n.fullscreen {\n  position: fixed;\n}\n\n.background {\n  left: 0;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  background-color: rgba(1, 1, 1, 0.2);\n}\n\n.spinner-outer {\n  display: -webkit-box;\n  display: flex;\n  height: 100%;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n          flex-direction: row;\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.spinner-outer .spinner-inner {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3Mvd2FpdGluZy1ib3gvcHJvZ3Jlc3MtbGF5ZXIvRTpcXHR1dG9cXHByb2plY3RzXFxhbmd1bGFyXFxjb3JldWkvc3JjXFxhcHBcXHZpZXdzXFx3YWl0aW5nLWJveFxccHJvZ3Jlc3MtbGF5ZXJcXHByb2dyZXNzLWxheWVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC92aWV3cy93YWl0aW5nLWJveC9wcm9ncmVzcy1sYXllci9wcm9ncmVzcy1sYXllci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDQ0o7O0FERUE7RUFDSSxlQUFBO0FDQ0o7O0FERUE7RUFDSSxPQUFBO0VBQ0EsTUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQ0FBQTtBQ0NKOztBREVBO0VBQ0ksb0JBQUE7RUFBQSxhQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0VBQUEsNkJBQUE7VUFBQSxtQkFBQTtFQUNBLHdCQUFBO1VBQUEsdUJBQUE7QUNDSjs7QURDSTtFQUNJLG9CQUFBO0VBQUEsYUFBQTtFQUNBLDRCQUFBO0VBQUEsNkJBQUE7VUFBQSxzQkFBQTtFQUNBLHdCQUFBO1VBQUEsdUJBQUE7QUNDUiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3dhaXRpbmctYm94L3Byb2dyZXNzLWxheWVyL3Byb2dyZXNzLWxheWVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndpbmRvd2VkIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgbGVmdDogMDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG4uZnVsbHNjcmVlbiB7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbn1cclxuXHJcbi5iYWNrZ3JvdW5kIHtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB0b3A6IDA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMS4wLCAxLjAsIDEuMCwgMC4yKTtcclxufVxyXG5cclxuLnNwaW5uZXItb3V0ZXIge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuXHJcbiAgICAuc3Bpbm5lci1pbm5lciB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgfVxyXG59XHJcbiIsIi53aW5kb3dlZCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbi5mdWxsc2NyZWVuIHtcbiAgcG9zaXRpb246IGZpeGVkO1xufVxuXG4uYmFja2dyb3VuZCB7XG4gIGxlZnQ6IDA7XG4gIHRvcDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDEsIDEsIDEsIDAuMik7XG59XG5cbi5zcGlubmVyLW91dGVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgaGVpZ2h0OiAxMDAlO1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi5zcGlubmVyLW91dGVyIC5zcGlubmVyLWlubmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59Il19 */"

/***/ }),

/***/ "./src/app/views/waiting-box/progress-layer/progress-layer.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/views/waiting-box/progress-layer/progress-layer.component.ts ***!
  \******************************************************************************/
/*! exports provided: ProgressLayerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProgressLayerComponent", function() { return ProgressLayerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _progress_activity_display_mode__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./progress-activity-display-mode */ "./src/app/views/waiting-box/progress-layer/progress-activity-display-mode.ts");



var ProgressLayerComponent = /** @class */ (function () {
    function ProgressLayerComponent() {
        this._visibilityChangedEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this._displayMode = _progress_activity_display_mode__WEBPACK_IMPORTED_MODULE_2__["ProgressLayerDisplayMode"].region;
        this._diameter = 100;
        this._visibilityCount = 0;
        this._visible = false;
        this._color = 'primary';
    }
    ProgressLayerComponent.prototype.ngOnInit = function () { };
    Object.defineProperty(ProgressLayerComponent.prototype, "onVisibilityChanged", {
        get: function () {
            return this._visibilityChangedEvent;
        },
        enumerable: true,
        configurable: true
    });
    ProgressLayerComponent.prototype.show = function () {
        this._visibilityCount++;
        if (this._visibilityCount === 1) {
            this._visible = true;
            this._visibilityChangedEvent.emit(this);
        }
    };
    ProgressLayerComponent.prototype.hide = function () {
        this._visibilityCount--;
        if (this._visibilityCount === 0) {
            this._visible = false;
            this._visibilityChangedEvent.emit(this);
        }
    };
    Object.defineProperty(ProgressLayerComponent.prototype, "fullscreen", {
        get: function () {
            return this._displayMode === _progress_activity_display_mode__WEBPACK_IMPORTED_MODULE_2__["ProgressLayerDisplayMode"].fullscreen;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProgressLayerComponent.prototype, "displayMode", {
        get: function () {
            return _progress_activity_display_mode__WEBPACK_IMPORTED_MODULE_2__["ProgressLayerDisplayMode"][this._displayMode];
        },
        set: function (value) {
            this._displayMode = _progress_activity_display_mode__WEBPACK_IMPORTED_MODULE_2__["ProgressLayerDisplayMode"][value];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProgressLayerComponent.prototype, "visible", {
        get: function () {
            return this._visible;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProgressLayerComponent.prototype, "color", {
        get: function () {
            return this._color;
        },
        set: function (clr) {
            this._color = clr;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProgressLayerComponent.prototype, "diameter", {
        get: function () {
            return this._diameter;
        },
        set: function (value) {
            this._diameter = value;
        },
        enumerable: true,
        configurable: true
    });
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [String])
    ], ProgressLayerComponent.prototype, "displayMode", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [String])
    ], ProgressLayerComponent.prototype, "color", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Number])
    ], ProgressLayerComponent.prototype, "diameter", null);
    ProgressLayerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-banq-progress-layer',
            template: __webpack_require__(/*! raw-loader!./progress-layer.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/waiting-box/progress-layer/progress-layer.component.html"),
            styles: [__webpack_require__(/*! ./progress-layer.component.scss */ "./src/app/views/waiting-box/progress-layer/progress-layer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ProgressLayerComponent);
    return ProgressLayerComponent;
}());



/***/ }),

/***/ "./src/app/views/waiting-box/waiting-box.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/views/waiting-box/waiting-box.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".overlay {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100vw;\n  height: 100vh;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3Mvd2FpdGluZy1ib3gvRTpcXHR1dG9cXHByb2plY3RzXFxhbmd1bGFyXFxjb3JldWkvc3JjXFxhcHBcXHZpZXdzXFx3YWl0aW5nLWJveFxcd2FpdGluZy1ib3guY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3ZpZXdzL3dhaXRpbmctYm94L3dhaXRpbmctYm94LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtBQ0NGIiwiZmlsZSI6InNyYy9hcHAvdmlld3Mvd2FpdGluZy1ib3gvd2FpdGluZy1ib3guY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIub3ZlcmxheSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgd2lkdGg6IDEwMHZ3O1xuICBoZWlnaHQ6IDEwMHZoO1xufVxuIiwiLm92ZXJsYXkge1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIHdpZHRoOiAxMDB2dztcbiAgaGVpZ2h0OiAxMDB2aDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/views/waiting-box/waiting-box.component.ts":
/*!************************************************************!*\
  !*** ./src/app/views/waiting-box/waiting-box.component.ts ***!
  \************************************************************/
/*! exports provided: WaitingBoxComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WaitingBoxComponent", function() { return WaitingBoxComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _waiting_box_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./waiting-box.service */ "./src/app/views/waiting-box/waiting-box.service.ts");



var WaitingBoxComponent = /** @class */ (function () {
    function WaitingBoxComponent(_waitingBoxService) {
        this._waitingBoxService = _waitingBoxService;
        this._visible = false;
    }
    WaitingBoxComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._waitingBoxService.showProgressLayer.subscribe(function (service) {
            _this._visible = true;
        });
        this._waitingBoxService.hideProgressLayer.subscribe(function (service) {
            _this._visible = false;
        });
    };
    Object.defineProperty(WaitingBoxComponent.prototype, "visible", {
        get: function () {
            return this._visible;
        },
        enumerable: true,
        configurable: true
    });
    WaitingBoxComponent.ctorParameters = function () { return [
        { type: _waiting_box_service__WEBPACK_IMPORTED_MODULE_2__["WaitingBoxService"] }
    ]; };
    WaitingBoxComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-waiting-box',
            template: __webpack_require__(/*! raw-loader!./waiting-box.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/waiting-box/waiting-box.component.html"),
            styles: [__webpack_require__(/*! ./waiting-box.component.scss */ "./src/app/views/waiting-box/waiting-box.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_waiting_box_service__WEBPACK_IMPORTED_MODULE_2__["WaitingBoxService"]])
    ], WaitingBoxComponent);
    return WaitingBoxComponent;
}());



/***/ }),

/***/ "./src/app/views/waiting-box/waiting-box.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/views/waiting-box/waiting-box.service.ts ***!
  \**********************************************************/
/*! exports provided: WaitingBoxService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WaitingBoxService", function() { return WaitingBoxService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var WaitingBoxService = /** @class */ (function () {
    function WaitingBoxService() {
        this._showProgressLayerEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this._hideProgressLayerEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this._activeCount = 0;
        this._timeoutHandle = null;
    }
    Object.defineProperty(WaitingBoxService.prototype, "showProgressLayer", {
        get: function () {
            return this._showProgressLayerEvent;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WaitingBoxService.prototype, "hideProgressLayer", {
        get: function () {
            return this._hideProgressLayerEvent;
        },
        enumerable: true,
        configurable: true
    });
    WaitingBoxService.prototype.show = function (delay) {
        var _this = this;
        if (delay === void 0) { delay = 1000; }
        this._activeCount++;
        if (this._activeCount === 1) {
            if (this._timeoutHandle) {
                clearTimeout(this._timeoutHandle);
            }
            this._timeoutHandle = setTimeout(function () {
                _this._showProgressLayerEvent.emit(_this);
                _this._timeoutHandle = null;
            }, delay);
        }
    };
    WaitingBoxService.prototype.hide = function () {
        if (this._activeCount > 0) {
            this._activeCount--;
            if (this._activeCount === 0) {
                if (this._timeoutHandle) {
                    clearTimeout(this._timeoutHandle);
                    this._timeoutHandle = null;
                }
                else {
                    this._hideProgressLayerEvent.emit(this);
                }
            }
        }
    };
    WaitingBoxService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], WaitingBoxService);
    return WaitingBoxService;
}());



/***/ }),

/***/ "./src/assets/config.json":
/*!********************************!*\
  !*** ./src/assets/config.json ***!
  \********************************/
/*! exports provided: appname, extranetUrlApi, backOfficeUrlApi, logoutUrl, default */
/***/ (function(module) {

module.exports = {"appname":"IDEL - Extranet des Editeurs","extranetUrlApi":"http://lvlidl0a.bnquebec.ca:8085/idel-v2/extranet/api","backOfficeUrlApi":"http://lvlidl0a.bnquebec.ca:9453/idel-bo/api","logoutUrl":"https://dev-www.banq.qc.ca/idp/logout_banq.jsp?return=https://dev-www.banq.qc.ca/idel-v2/extranet/"};

/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: true
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"], {
    useJit: true,
    preserveWhitespaces: true
})
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\tuto\projects\angular\coreui\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map